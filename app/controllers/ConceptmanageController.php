<?php

class ConceptmanageController extends ControllerBase
{

	public function initialize() 
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    function indexAction()
    {
    	$getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }
    function addconceptAction()
    {
    	$getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }
} ?>