<?php

class PackageController extends ControllerBase {

    public function initialize() {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Discover A Smart Marketplace... ');
        parent::initialize();
    }

    function selectpackageAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            if ($this->session->has("user")) {
                $user_session = $this->session->get("user");
                $this->view->setVar("user_session", $user_session);
            } else {
                return $response->redirect("index");
            }
            $url = $postval['url'];
            $package = MasterPackages::findFirst(array("id='" . $postval['packageid'] . "'"));
            $point = MasterUserpoint::findFirst("uid='{$user_session['id']}'");
            if ($package->price < $point->points || $package->price == $point->points) {
                $points = $point->points - $package->price;
                $pdata = ["uid" => $point->uid, "points" => $points];
                $mup = new MasterUserpoint();
                $mup->save($pdata);
            } else {
                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>You Don't Have sufficient Points Please purchase some points.</div>");
                return $response->redirect($url);
            }
            $postval['userid'] = $user_session['id'];
            $postval['nots'] = $package->not;
            $postval['test_type'] = $package->test_type;
            $postval['price'] = $package->price;
            $userpackage = new MasterUserPackage();
            $userpackage->save($postval);
            return $response->redirect($url);
        }
        exit();
    }

    public function viewpracticepackageAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $exp = '"id":"' . $postval['id_topic'] . '"';
            $practicepckg = MasterPracticePackage::find(array("topic LIKE '%" . $exp . "%' AND status=1 AND course=" . $postval['id_course']));
            $str = '';
            if (count($practicepckg) != 0) {
                foreach ($practicepckg as $pak) {
                    $str.='<div class="package-list">';
                    $str.='<div class="radio-custom radio-success"><input type="radio" class="chkeb" name="packageid" value="' . $pak->id . '">';
                    $str.="<label>" . $pak->name . " (price : Rs." . $pak->price . " , validaity : " . $pak->validity . " days)</label> </div>
                     <h5><a target='_blank' href=".BASEURL."index/packagedetails/2/".$pak->id." class='btn btn-primary'>View Details</a></h5><div class='clearfix'></div>";
                    $str.='</div>';
                }
            } else {
                $str.= 'No packages are defined.';
            }
            echo $str;
            exit;
        }
    }
    public function getfixpackageAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $package = MasterPackages::findFirst(array("id='" . $postval['id_package'] . "'"));
            $str = '';
            if (count($package) != 0) {
                    $str.='<div class="package-list">';
                    $str.='<div class="radio-custom radio-success"><input type="radio" class="chkeb" name="packageid" value="' . $package->id . '">';
                    $str.="<label for='packageid'>" . $package->name . " (price : Rs." .$package->price . ")</div> <h5>" . $package->description . "</h5></label>";
                    $str.='</div>';
            } else {
                $str.= 'No packages are defined.';
            }
            echo $str;
            exit;
        }
    }

    public function selectpracpackageAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
             $postval = $this->request->getPost();
            if ($this->session->has("user")) {
                $user_session = $this->session->get("user");
                $this->view->setVar("user_session", $user_session);
            } else {
                return $response->redirect("index");
            }
            $url = $postval['url'];
            $package = MasterPracticePackage::findFirst(array("id='" . $postval['packageid'] . "'"));
            $point = MasterUserpoint::findFirst("uid='{$user_session['id']}'");
            if ($package->price < $point->points || $package->price == $point->points) {
                $points = $point->points - $package->price;
                $sql = "UPDATE  MasterUserpoint SET points = $points WHERE uid=$point->uid";
                $status = $this->modelsManager->executeQuery($sql);
            } else {
                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>You Don't Have sufficient Points Please purchase some points.</div>");
                return $response->redirect($url);
            }
            $postvalnew['id_user'] = $user_session['id'];
            $postvalnew['id_package'] = $postval['packageid'];
            $postvalnew['price'] = $package->price;
            $postvalnew['add_date'] = date("Y-m-d");
            $userpackage = new UserPracticePackage();
            $userpackage->save($postvalnew);
            return $response->redirect($url);
        }
    }

}

?>