<?php
class SubjectController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
     * Its a index page or landing page for subject
     * @return [array] [retrun a Subject  data array to the view part]
     * @author Rajesh
     */
    function indexAction()
    {
      $data = MasterSubject::find();
      $this->view->setVar("subject", $data);
    }
    /**
     * Add Subject
     * @return [boolen] [Get true or false result when it enter to database]
     * @author Rajesh
     */
    function addsubjectAction()
    {
        $response = new \Phalcon\Http\Response();
        $course = MasterCourse::find(array('status=1'));
        $this->view->setVar("course", $course);
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $postval['courses'] = json_encode($this->request->getPost('courses'));
            $Addcourse= new MasterSubject();
            $Addcourse->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Subject Successfully Added</div>");
            return $response->redirect("subject");
        }
    }

    function updatesubjectAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $conditions = "id = :id:";
            $parameters = array("id" => $id);
            $subject = MasterSubject::find(array($conditions,"bind" => $parameters));
            $this->view->setVar("subject", $subject);
            $course = MasterCourse::find(array('status=1'));
            $this->view->setVar("course", $course);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            if(empty($postval['subjectname']) || empty($postval['slug']) || empty($postval['status'])){
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Subject Successfully Updated</div>");
                return $response->redirect("subject/updatesubject/".$id);
            }
            $postval['courses']=json_encode($this->request->getPost('courses'));
            $update= new MasterSubject();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Subject Successfully Updated</div>");
            return $response->redirect("subject");
        }
    }
    /**
     * Delete Subject data form database
     * @param  [int] $id [Subject id]
     * @return [boolen]     [True or False result]
     */
    function deletesubjectAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
          $phql = "DELETE FROM MasterSubject WHERE id = '".$id."'";
          $this->modelsManager->executeQuery($phql);
          $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Subject Deleted Successfully</div>");
          return $response->redirect("subject");
        }
    }
    /**
     * Subject status function is for changing the subject status
     * @return [redirect] [After update the database it redirect to subject page with alert]
     * @author Rajesh 
     */
    function subjectstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterSubject SET status = 0 where id=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterSubject SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Status Updated Successfully</div>");
             return $response->redirect("subject");
        }
    }
    /**
     * Its a index page or landing page for subject
     * @return [array] [retrun a course data array to the view part]
     * @author Rajesh <rajeshkumarsardar@gmail.com>
     */
    function subsubjectAction()
    {
      if($this->request->isGet())
      {
        $getVal=$this->request->get();//
        if (isset($getVal['subject'])) {
          $getSub = MasterSubSubject::find(array("subid='".$getVal['subject']."' "));
          $this->view->setVar("subsubject", $getSub);
        }
      }
        $data = MasterSubject::find(array("status=1"));
        $this->view->setVar("subject", $data);
    }
    /**
     * Add Sub Subject
     * @return [alert] [Retun alert whether data is entered or not]
     */
    function addsubsubjectAction()
    {
        $response = new \Phalcon\Http\Response();
        $conditions = " status = :status:";
        $parameters = array("status"=>1 );
        $data = MasterSubject::find(array($conditions,"bind" => $parameters));
        $this->view->setVar("subject", $data);
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $Addsubsubject= new MasterSubSubject();
            $Addsubsubject->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Subject Successfully Added</div>");
            return $response->redirect("subject/subsubject");
        }
    }
    /**
     * Update sub subsubject data
     * @return [text/html] Update sub subject
     */
    function updatesubsubjectAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
          // Get the Perticular Sub Subject data from database
          $conditions = "ssid = :ssid: ";
          $parameters = array("ssid" => $id);
          $subsubject = MasterSubSubject::findFirst(array($conditions,"bind" => $parameters));
          $this->view->setVar("subsubject", $subsubject);
          // Get list of subject regarding course id above
          $subject = MasterSubject::find(array());
          $this->view->setVar("subject", $subject);
        }
        
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            if(empty($postval['slug'])){
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Mandatory  field are Not be Empty</div>");
              return $response->redirect("subject/updatesubsubject");
            }
            $update= new MasterSubSubject();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Sub Subject Successfully Updated</div>");
            return $response->redirect("subject/subsubject");
        }
    }
    function deletesubsubjectAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
          $phql = "DELETE FROM MasterSubSubject WHERE ssid = '".$id."'";
          $this->modelsManager->executeQuery($phql);
          $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Sub Subject Deleted Successfully</div>");
          $subsubject = MasterSubSubject::findFirst(array("ssid='".$id."' "));
          return $response->redirect("subject/subsubject/?subject=".$subsubject->subid." ");
        }
    }
    /**
     * Update status of subject
     * @param  [int] $u_status [description]
     * @param  [int] $id       [description]
     * @return [return]           [description]
     */
    function updsubstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterSubject SET status = 0 where id=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterSubject SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Subject Status Updated Successfully</div>");
             return $response->redirect("subject");
        }
    }
    /**
     * Update status of Sub subject
     * @param  [int] $u_status [description]
     * @param  [int] $id       [description]
     * @return [return]           [description]
     */
    function updsubsubstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterSubSubject SET status = 0 where ssid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterSubSubject SET status = 1 where ssid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Subject Status Updated Successfully</div>");
            $subsubject = MasterSubSubject::findFirst(array("ssid='".$id."' "));
            return $response->redirect("subject/subsubject/?subject=".$subsubject->subid." ");
        }
    }
    /**
     * Ajax call for subject list
     * @return [type] [Return ]
     * @author preetish priyabrata
     */
    function subjectlistAction()
    {
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $subdata = MasterSubSubject::find(array("subid='".$postval['subjectid']."' AND status=1"));
            if(count($subdata)>0){ ?>
                <option value="">--Select Sub Subject--</option>
                <?php foreach($subdata as $value){ ?>
                    <option value="<?=$value->ssid?>"><?=$value->ssname?></option>
                <?php }
             }
        }else{
            echo 'No Post data are found.';
        }
    }
    /**
     * Topics view 
     * @return [type] [description]
     */
    function topicsAction()
    {
        if($this->request->isGet())
        {
          $getVal=$this->request->get();
          $getSub = MasterSubject::find(array("status=1"));
          $this->view->setVar("subdet", $getSub);
          if (isset($getVal['subject'])) {
            $getSubSub = MasterSubSubject::find(array("subid='".$getVal['subject']."' AND status=1 "));
            $this->view->setVar("subsubject", $getSubSub);
              if (isset($getVal['topics'])) {
              $gettopics = MasterTopics::find(array("subid='".$getVal['subject']."' AND ssubid='".$getVal['topics']."' "));
              $this->view->setVar("topics", $gettopics);
          }
            }
        }
        
    }
    /**
    * @action adding topic
     * @author preetish
     */
    function addtopicsAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            if(empty($postval['slug'])){
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Mandatory  field are Not be Empty</div>");
              return $response->redirect("subject/addtopics");
            }
            $Addtopic= new MasterTopics();
            $Addtopic->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Topic Successfully Added</div>");
            return $response->redirect("subject/topics");
        }
        $data = MasterSubject::find(array("status=1"));
        $this->view->setVar("subject", $data);
    }
    /**
     * 
     */
    function updatetopicsAction($id)
    {
        $response = new \Phalcon\Http\Response();
         if($id!="")
          {
            // Get the Perticular Sub Subject data from database
            $conditions = "tid = :tid: ";
            $parameters = array("tid" => $id);
            $topic = MasterTopics::findFirst(array($conditions,"bind" => $parameters));
            $this->view->setVar("topic", $topic);
            // Get list of subject regarding course id above
            $subject = MasterSubject::find(array());
            $this->view->setVar("subject", $subject);
            //Get list of subsubject regarding course id above
            $subjec = MasterSubSubject::find(array("subid='".$topic->subid."' "));
            $this->view->setVar("subsubject", $subjec);
          }
        
          if($this->request->isPost()){
              $postval = $this->request->getPost();
              $update= new MasterTopics();
              $update->save($postval);
              $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Topics Successfully Updated</div>");
              return $response->redirect("subject/topics");
          }
    }
    /**
     * Topic status changer
     * @param  [int] $u_status [Status id Now]
     * @param  [id] $id       [Status id after change]
     * @return [return]           [Return Topics alert to topics function]
     */
    function updtopstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        //echo $u_status.$id;exits();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterTopics SET status = 0 where tid=".$id."";
            }else{
                $phql = "UPDATE MasterTopics SET status = 1 where tid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Topics Status Updated Successfully</div>");
            $topics = MasterTopics::findFirst(array("tid='".$id."' "));
            return $response->redirect("subject/topics/?subject=".$topics->subid."&topics=".$topics->ssubid."");
        }
    }
    /**
     * Delete topics address
     * @return [retun] [Delete alert]
     */
    function deletetopicsAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $topics = MasterTopics::findFirst(array("tid='".$id."' "));
            $phql = "DELETE FROM MasterTopics WHERE tid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Topic Deleted Successfully</div>");
            return $response->redirect("subject/topics/?subject=".$topics->subid."&topics=".$topics->ssubid."");
        }
    }
    /**
     * Sub Topics view 
     * @return [type] [description]
     */
    function subtopicsAction()
    {
        if($this->request->isGet())
        {
          $getVal=$this->request->get();
          $getSub = MasterSubject::find(array("status=1"));
          $this->view->setVar("subdet", $getSub);
          if (isset($getVal['subject'])) {
            $getSubSub = MasterSubSubject::find(array("subid='".$getVal['subject']."' AND status=1 "));
            $this->view->setVar("subsubject", $getSubSub);
            if (isset($getVal['topics'])) {
            $gettopics = MasterTopics::find(array("subid='".$getVal['subject']."' AND ssubid='".$getVal['topics']."' AND status=1 "));
            $this->view->setVar("topics", $gettopics);
              if (isset($getVal['subtopics'])) {
              $getsubtopics = MasterSubTopics::find(array("subid='".$getVal['subject']."' AND ssubid='".$getVal['topics']."' AND tid='".$getVal['subtopics']."' "));
              $this->view->setVar("subtopics", $getsubtopics);
              }
            }
          }
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }
    /**
     * 
     */
    function addsubtopicsAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            //print_r($postval);exit();
            $Addtopic= new MasterSubTopics();
            $Addtopic->save($postval);
            $this->flashSession->success("<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Sub Topic Successfully Added</div>");
            return $response->redirect("subject/subtopics");
        }
        if($this->request->isGet())
        {
          $getVal=$this->request->get();
          $getSub = MasterSubject::find(array(""));
          $this->view->setVar("subdet", $getSub);
          if (isset($getVal['subject'])) {
            $getSubSub = MasterSubSubject::find(array("subid='".$getVal['subject']."' "));
            $this->view->setVar("subsubject", $getSubSub);
              if (isset($getVal['topics'])) {
              $gettopics = MasterTopics::find(array("subid='".$getVal['subject']."' AND ssubid='".$getVal['topics']."' "));
              $this->view->setVar("topics", $gettopics);
            }
          }
        }
        /*$conditions = " status = :status: ";
        $parameters = array("status"=>1 );
        $data = MasterCourse::find(array($conditions,"bind" => $parameters));
        $this->view->setVar("course", $data);*/
    }
    /**
     * 
     */
    function updatesubtopicsAction($id)
    {
      $response = new \Phalcon\Http\Response();
         if($id!="")
          {
            // Get the Perticular Sub Subject data from database
            $conditions = "stid = :stid: ";
            $parameters = array("stid" => $id);
            $subtopic= MasterSubTopics::findFirst(array($conditions,"bind" => $parameters));
            $this->view->setVar("subtopic", $subtopic);
            // Get list of subject regarding subtopic id above
            $subject = MasterSubject::find(array());
            $this->view->setVar("subject", $subject);
            //Get list of subsubject regarding course id above
            $subjec = MasterSubSubject::find(array("subid='".$subtopic->subid."' "));
            $this->view->setVar("subsubject", $subjec);
            //Get list of topic regarding course id above
             $topicdet = MasterTopics::find(array("tid='".$subtopic->tid."' "));
            $this->view->setVar("topicdet", $topicdet);
          }
        
          if($this->request->isPost()){
              $postval = $this->request->getPost();
              $update= new MasterSubTopics();
              $update->save($postval);
              $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Sub Topics Successfully Updated</div>");
              return $response->redirect("subject/subtopics");
          }

    }
    /*
     * Delete Subject data form database
     * @param  [int] $id [Subject id]
     * @return [boolen]     [True or False result]
     */
    function deletesubtopicsAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterSubTopics WHERE stid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Sub Topic Deleted Successfully</div>");
                return $response->redirect("subject/subtopics");
        }
    }
     /**
     * Topic status changer
     * @param  [int] $u_status [Status id Now]
     * @param  [id] $id       [Status id after change]
     * @return [return]           [Return Topics alert to topics function]
     */
    function updasub_topstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterSubTopics SET status = 0 where stid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterSubTopics SET status = 1 where stid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Sub Topics Status Updated Successfully</div>");
            $stopics = MasterSubTopics::findFirst(array("stid='".$id."' "));
            return $response->redirect("subject/subtopics/?subject=".$stopics->subid."&topics=".$stopics->ssubid."&subtopics=".$stopics->tid);
        }
    }
    



    // All Ajax call Start Here 
    
    /**
     * Ajax call for retrive subject names
     * @return [type/html] [select option are viewed]
     */
    function subjectnameAction()
    {
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $subname = MasterSubject::find(array("courseid='".$postval['courseid']."' AND status=1 order by subjectname ASC"));
            if(count($subname)>0){ ?>
                <option value="">Choose Subject</option>
                <?php foreach($subname as $value){ ?>
                    <option value="<?=$value->id?>"><?=$value->subjectname?></option>
            <?php  }
            }
        }else{
            echo 'No data is posted';
        }
        exit();
    }
    /**
     * Ajax call for retrive Sub subject names
     * @return [type] [description]
     */
    function subsubjectnameAction()
    {
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $subname = MasterSubject::find(array("courseid='".$postval['courseid']."' AND status=1 order by subjectname ASC"));
            if(count($subname)>0){ ?>
                <option value="">Choose Subject</option>
                <?php foreach($subname as $value){ ?>
                    <option value="<?=$value->id?>"><?=$value->subjectname?></option>
            <?php  }
            }
        }else{
            echo 'No data is posted';
        }
        exit();
    }
    /**
     * Ajax call for retrive subsubject datas.
     * @return [type/html] [Table td tags are return]
     */
    function getSubSubjectAction()
    {
      if($this->request->isGet())
      {
        $getVal=$this->request->get();//
        $getSub = MasterSubject::find(array("courseid='".$getVal['courseid']."' "));
        $this->view->setVar("subdet", $getSub);
      }
  
    }
    static public function slugifyAction($text)
    {
        if ($text=="") {
            echo $text="";exit();
        }
        else
        {
          $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        
          // trim
          $text = trim($text, '-');
        
          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        
          // lowercase
          $text = strtolower($text);
        
          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);
        
          if (empty($text))
          {
            return 'n-a';
          }
          
          $sub = MasterSubSubject::find(array("slug = :slug:",'bind' => array('slug' => $text)));
            //print_r(count($page));
            if (count($sub)!=0) {
                $msg="&nbsp;&nbsp;<span class='has-error'><label class='control-label' for='inputError'><i class='fa fa-times-circle-o'></i>&nbsp;Invalid Slug Name</label></span>";
                $status=0;
            }
            else
            {
                $msg="&nbsp;&nbsp;<span class='has-success'><label class='control-label' for='inputSuccess'><i class='fa fa-check'></i> Slug Name is Valid</label></span>";
                $status=1;
            }

          $slug_status=array("value"=>$text,"msg"=>$msg, "status"=>$status);
          echo json_encode($slug_status); exit();
        }
    }
    /**
     * Ajax call from sub subject list 
     * @return [type] [Return ]Ajax call for topic list 
     * @author preetish priyabrata
     * 
     */
    function subsubjectlistAction()
    {
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $topicdata = MasterTopics::find(array(" subid='".$postval['subjectid']."' AND  ssubid='".$postval['subsubjectid']."' order by tname ASC"));
            if(count($topicdata)>0)
              { ?>
                <option value="">Choose Subject</option>
                <?php foreach($topicdata as $value){ ?>
                <option value="<?=$value->tid?>"><?=$value->tname?></option>
                <?php }
             }
        }else{
            echo 'No Post data are found.';
        }
    }/**
     * @author preetish priyabrata <[email address]>
     * @required for slug value in topics
     */

    static public function slug_topic_ifyAction($text)
    {
        if ($text=="") {
            echo $text="";exit();
        }
        else
        {
          $text = preg_replace('~[^\\pL\d]+~u', '-', $text);        
          // trim
          $text = trim($text, '-');        
          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);        
          // lowercase
          $text = strtolower($text);        
          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);        
          if (empty($text))
          {
            return 'n-a';
          }          
          $sub = MasterTopics::find(array("slug = :slug:",'bind' => array('slug' => $text)));
            //print_r(count($page));
            if (count($sub)!=0) {
                $msg="&nbsp;&nbsp;<span class='has-error'><label class='control-label' for='inputError'><i class='fa fa-times-circle-o'></i>&nbsp;Invalid Slug Name</label></span>";
                $status=0;
            }
            else
            {
                $msg="&nbsp;&nbsp;<span class='has-success'><label class='control-label' for='inputSuccess'><i class='fa fa-check'></i> Slug Name is Valid</label></span>";
                $status=1;
            }
          $slug_status=array("value"=>$text,"msg"=>$msg, "status"=>$status);
          echo json_encode($slug_status); exit();
        }
    }/**
     * @author preetishpriyabrata <[email address]>
     * @required for slug function in sub topics for checking in table name subtopics
     */
     static public function slug_subtopicAction($text)
    {
        if ($text=="") {
            echo $text="";exit();
        }
        else
        {
          $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        
          // trim
          $text = trim($text, '-');
        
          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        
          // lowercase
          $text = strtolower($text);
        
          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);
        
          if (empty($text))
          {
            return 'n-a';
          }
          
          $sub = MasterSubTopics::find(array("slug = :slug:",'bind' => array('slug' => $text)));
            //print_r(count($page));
            if (count($sub)!=0) {
                $msg="&nbsp;&nbsp;<span class='has-error'><label class='control-label' for='inputError'><i class='fa fa-times-circle-o'></i>&nbsp;Invalid Slug Name</label></span>";
                $status=0;
            }
            else
            {
                $msg="&nbsp;&nbsp;<span class='has-success'><label class='control-label' for='inputSuccess'><i class='fa fa-check'></i> Slug Name is Valid</label></span>";
                $status=1;
            }

          $slug_status=array("value"=>$text,"msg"=>$msg, "status"=>$status);
          echo json_encode($slug_status); exit();
        }
    }
    function updatetopicAction(){

      if($this->request->isGet())
        {
          $getVal=$this->request->get();
          $getSub = MasterSubject::find(array("courseid='".$getVal['courseid']."' "));
          $this->view->setVar("subdet", $getSub);
          if (isset($getVal['subject'])) {
            $getSubSub = MasterSubSubject::find(array("courseid='".$getVal['courseid']."' AND subid='".$getVal['subject']."' "));
            $this->view->setVar("subsubject", $getSubSub);
              if (isset($getVal['topics'])) {
              $gettopics = MasterTopics::find(array("courseid='".$getVal['courseid']."' AND subid='".$getVal['subject']."' AND ssubid='".$getVal['topics']."' "));
              $this->view->setVar("topics", $gettopics);
          }
            }
      }
      $conditions = " status = :status:";
        $parameters = array("status"=>1 );
        $data = MasterCourse::find(array($conditions,"bind" => $parameters));
        $this->view->setVar("course", $data);
    }

   
}
?>