<?php

class QuestionsController extends ControllerBase
{/**
* Questions Controller
* Controller Name: QusetionsController
* Author By: Preetish priyabrata
* Created On:1/06/2015
* Modified: N/A
**/
    public function initialize() 
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
    * Function To View Question Type
    * Action Name: view_question_typeAction
    * Created Date: 2-06-2015
    * Author By: preetish priyabrata
    **/
    public function view_question_typeAction()
    {
        $data = MasterQuesType::find();
        $this->view->setVar("ques_type", $data);
    }
    /**
    * Function To Add new Question Type
    * Action Name: add_question_typeAction
    * Created Date: 2-06-2015
    * Author By: preetish priyabrata
    **/
    public function add_question_typeAction()
    {
      	$response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) 
        {
            $postval=$this->request->getPost();
            $Addques= new MasterQuesType();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Type Successfully Add</div>");
            return $response->redirect("questions/view_question_type");
        }
    }
    /**
    * @author preetish priyabrata
    * Function created for update status in question type detail
    * Created on 2-june-2015
    */
    public function update_typrstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterQuesType SET status = 0 where qtid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterQuesType SET status = 1 where qtid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Type Status Updated Successfully</div>");
            return $response->redirect("questions/view_question_type");
        }
    }
    /**
    * Function To update changees in question type
    * Action Name: addpageAction
    * Created Date: 26-05-2015
    * Author By: preetish priyabrata
    **/
    public function update_question_typeAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!=''){
            $postval=$this->request->getPost();
            $conditions = "qtid = :qtid:";
            $parameters = array("qtid" => $id);
            $qtupdet = MasterQuesType::find(array($conditions,"bind" => $parameters));
            $this->view->setVar("qtupdet", $qtupdet);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $update= new MasterQuesType();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Type Successfully Update</div>");
            return $response->redirect("questions/view_question_type");
        }
    }
    /**
    * @author preetish priyabrata
    *function created for Question Type Deleted Successfully
    */
    public function qt_deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterQuesType WHERE qtid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Type Deleted Successfully</div>");
            return $response->redirect("questions/view_question_type");
        }
    }  
    /**
    * Function To View Question Source Type
    * Action Name: view_question_sourceAction
    * Created Date: 3-06-2015
    * Author By: preetish priyabrata
    **/
    public function view_question_sourceAction()
    {
        $data = MasterQuestionSource::find();
        $this->view->setVar("ques_sour", $data);
    }
    /**
    * Function To Add new Question Type
    * Action Name: add_question_typeAction
    * Created Date: 3-06-2015
    * Author By: preetish priyabrata
    **/
    public function add_question_sourceAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) 
        {
            $postval=$this->request->getPost();
            $Addques= new MasterQuestionSource();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Source Successfully Add</div>");
            return $response->redirect("questions/view_question_source");
        }
    }
    /**
    * @author preetish priyabrata
    * Function created for update status in question Source detail
    * Created on 3-june-2015
    */
    public function update_sourcestatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterQuestionSource SET status = 0 where qsid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterQuestionSource SET status = 1 where qsid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Source Status Updated Successfully</div>");
            return $response->redirect("questions/view_question_source");
        }
    }
    /**
    * Function To update changees in question type
    * Action Name:update_question_sourceAction
    * Created Date: 3-06-2015
    * Author By: preetish priyabrata
    **/
    public function update_question_sourceAction($id){
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $conditions = "qsid = :qsid:";
            $parameters = array("qsid" => $id);
            $qsupdet = MasterQuestionSource::find(array($conditions,"bind" => $parameters));
            $this->view->setVar("qsupdet", $qsupdet);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $update= new MasterQuestionSource();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Source Successfully Update</div>");
            return $response->redirect("questions/view_question_source");
        }
    }
    /**
    * @author preetish priyabrata
    *function created for Question Source Deleted Successfully
    * Created Date: 3-06-2015
    */
    public function qs_deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterQuestionSource WHERE qsid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Source Deleted Successfully</div>");
            return $response->redirect("questions/view_question_source");
        }
    }
    /**
     * [getQueDetails : To get the Details of a question]
     * @param  [varchar] $ques_ID [description]
     * @return [type]          [description]
     */
    public function getQueDetailsAction(){
        if ($this->request->isPost()) {
            $quesID = $this->request->getPost("questionID");
            echo QuestionList::getQuestion($quesID);
        }
    }
}