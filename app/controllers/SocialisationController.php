<?php
class SocialisationController extends ControllerBase {
public function initialize() {
$this->view->setTemplateAfter('main');
Phalcon\Tag::setTitle('Ischool4u | ADMIN');
parent::initialize();
//        if (!$this->session->has("user")) {
//            header("location:" . BASEURL);
//        }
}
/* Author: Satyanada,
* date:21/09/15
* invite friends from facebook,gmail,ischool friend
*/
function invitefriendAction() {
$response = new \Phalcon\Http\Response();
$user_session = $this->session->get("user");
$this->view->setVar("user_session", $user_session);
$course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
$this->view->setVar("course", $course);
$subject = MasterSubject::find(array("status=1"));
$this->view->setVar("subject", $subject);
if ($this->request->isPost()) {
$data = $this->request->getPost();
if ($data['friend_type'] == 1 || $data['friend_type'] == 2) {//for facebook
$this->session->set('dataredirect',$_SERVER['HTTP_REFERER']);
$this->view->setVar("friend_type", $data['friend_type']);
if (!empty($data['quesid'])) {
$_SESSION['quesid'] = base64_encode($data['quesid']);
}
} elseif ($data['friend_type'] == 4) {
$this->session->set('dataredirect',$_SERVER['HTTP_REFERER']);
$quesid = base64_encode($data['quesid']);
return $response->redirect("socialisation/challengefriend/$quesid");
} else {
$this->view->setVar("friend_type", $data['friend_type']);
$user_session = $this->session->get("user");
$user = MasterUsers::find(array("uid !='" . $user_session['id'] . "'"));
$this->view->setVar("user", $user);
//all invite friend in ischools user
$invitedus = MasterFriendlist::find(array("LOCATE({$user_session['id']},invite_friend)"));
$arr = array();
foreach ($invitedus as $use) {
$arr[] = $use->uid;
}
$alluinvfriend = MasterFriendlist::find(array("uid ='" . $user_session['id'] . "'"));
$arrall = array();
foreach ($alluinvfriend as $all) {
$arrall = json_decode($all->invite_friend);
}
$all_invitedfirend = array_unique(array_merge((array) $arr, (array) $arrall));
$this->view->setVar("arrinvt", $all_invitedfirend);
//all accept friend in ischools user
$allac = array();
foreach ($alluinvfriend as $all) {
$allac = json_decode($all->accept_friend);
}
$acceptus = MasterFriendlist::find(array("LOCATE({$user_session['id']},accept_friend)"));
$arrac = array();
foreach ($acceptus as $acuse) {
$arrac[] = $acuse->uid;
}
$arrac = array_unique(array_merge((array) $arrac, (array) $allac));
$this->view->setVar("acceptus", $arrac);
//all reject friend in ischools user
$allrej = array();
foreach ($alluinvfriend as $all) {
$allrej = json_decode($all->reject_friend);
}
$rejectus = MasterFriendlist::find(array("LOCATE ({$user_session['id']},reject_friend)"));
foreach ($rejectus as $reuse) {
$reject[] = $reuse->uid;
}
$arrrej = array_unique(array_merge((array) $reject, (array) $allrej));
$this->view->setVar("rejectus", $arrrej);
}
}
}
/* Author: Satyanada,
* date:21/09/15
* send invitaion request to ischool user
*/
function invitesuccessAction() {
$response = new \Phalcon\Http\Response();
$postdata = $this->request->getPost();
$user_session = $this->session->get("user");
$chkuser = MasterFriendlist::findFirst(array("uid='" . $user_session['id'] . "'"));
if ($chkuser->uid != '') {
if (trim($chkuser->invite_friend) != '') {
$user = json_decode($chkuser->invite_friend);
$finalarr = array_merge((array) $user, (array) $postdata['inviteuser']);
} else {
$finalarr = $postdata['inviteuser'];
}
$jsondata = json_encode(array_values($finalarr));
$sql = "UPDATE  MasterFriendlist SET invite_friend = '$jsondata' WHERE uid='{$user_session['id']}'";
$status = $this->modelsManager->executeQuery($sql);
} else {
$jsondata = json_encode(array_values($postdata['inviteuser']));
$sql = "INSERT INTO  MasterFriendlist (invite_friend,uid) VALUES ('$jsondata','{$user_session['id']}')";
$status = $this->modelsManager->executeQuery($sql);
}
if (!empty($chkuser->reject_friend)) {
$rej = json_decode($chkuser->reject_friend);
$fin_rej = array_diff((array) $rej, (array) $postdata['inviteuser']);
$fin_rej = count($fin_rej) > 0 ? json_encode(array_values($fin_rej)) : NULL;
$sql_rej = "UPDATE  MasterFriendlist SET reject_friend = '{$fin_rej}' WHERE uid='{$user_session['id']}'";
$this->modelsManager->executeQuery($sql_rej);
}
if (!empty($user_session['id'])) {
foreach ($postdata['inviteuser'] as $vv) {
$cond = "(LOCATE({$user_session['id']},reject_friend) AND uid='$vv')";
$ppp = MasterFriendlist::findFirst(array($cond));
if (!empty($ppp->id) && !empty($ppp->reject_friend)) {
$ffff = array_diff((array) $ppp->reject_friend, array($user_session['id']));
$fin_ff = count($ffff) > 0 ? json_encode(array_values($ffff)) : NULL;
$sql_fff = "UPDATE  MasterFriendlist SET reject_friend = '{$fin_ff}' WHERE uid='{$vv}'";
$this->modelsManager->executeQuery($sql_fff);
}
}
}
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Invitation send Successfully</div>");
return $response->redirect("index/dashboard");
}
//Click on accept invitation link for ischool user
function acceptinvbyuserAction($create_id='') {
$response = new \Phalcon\Http\Response();
if (!empty($create_id)) {
$usersession = $this->session->get("user");
$postdata['inviteuser'] = array($usersession['id']);
$user = MasterUsers::findFirst(array("md5(uid)='" . $create_id . "'"));
$chkuser = MasterFriendlist::findFirst(array("uid='" . $user->uid . "'"));
$cond = "(LOCATE({$usersession['id']},accept_friend) AND md5(uid)='{$create_id}') OR (LOCATE('{$create_id}' ,md5(accept_friend)) AND uid={$usersession['id']})";
$inviteduser = MasterFriendlist::findFirst(array($cond));
if ((!empty($user->uid) && !empty($usersession['id'])) && empty($inviteduser->id)) {
if ($chkuser->uid != '') {
if ($chkuser->accept_friend != '') {
$userallacc = json_decode($chkuser->accept_friend);
$finalarr = array_merge((array) $userallacc, (array) $postdata['inviteuser']);
} else {
$finalarr = $postdata['inviteuser'];
}
$jsondata = json_encode(array_values($finalarr));
$sql = "UPDATE  MasterFriendlist SET accept_friend = '$jsondata' WHERE uid='{$user->uid}'";
$status = $this->modelsManager->executeQuery($sql);
} else {
$jsondata = json_encode(array_values($postdata['inviteuser']));
$sql = "INSERT INTO  MasterFriendlist (accept_friend,uid) VALUES ('$jsondata','{$user->uid}')";
$status = $this->modelsManager->executeQuery($sql);
}
if (!empty($chkuser->invite_friend)) {
$inv = json_decode($chkuser->invite_friend);
$fin_inv = array_diff($inv, $postdata['inviteuser']);
$fin_inv = count($fin_inv) > 0 ? json_encode(array_values($fin_inv)) : NULL;
$sql_inv = "UPDATE  MasterFriendlist SET invite_friend = '{$fin_inv}' WHERE uid='{$user->uid}'";
$status = $this->modelsManager->executeQuery($sql_inv);
}
}
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Invitation accepted Successfully</div>");
return $response->redirect("index/dashboard");
}
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something error happen</div>");
return $response->redirect("index/dashboard");
}
//Click on reject invitation link for ischool user
function rejectinvbyuserAction($create_id='') {
$response = new \Phalcon\Http\Response();
if (!empty($create_id)) {
$usersession = $this->session->get("user");
$postdata['inviteuser'] = array($usersession['id']);
$user = MasterUsers::findFirst(array("md5(uid)='" . $create_id . "'"));
$chkuser = MasterFriendlist::findFirst(array("uid='" . $user->uid . "'"));
$cond = "(LOCATE({$usersession['id']},reject_friend) AND md5(uid)='{$create_id}') OR (LOCATE('{$create_id}' ,md5(reject_friend)) AND uid={$usersession['id']})";
$inviteduser = MasterFriendlist::findFirst(array($cond));
if ((!empty($user->uid) && !empty($usersession['id'])) && empty($inviteduser->id)) {
if ($chkuser->uid != '') {
if ($chkuser->reject_friend != '') {
$userallacc = json_decode($chkuser->reject_friend);
$finalarr = array_merge((array) $userallacc, (array) $postdata['inviteuser']);
} else {
$finalarr = $postdata['inviteuser'];
}
$jsondata = json_encode(array_values($finalarr));
$sql = "UPDATE  MasterFriendlist SET reject_friend = '$jsondata' WHERE uid='{$user->uid}'";
$status = $this->modelsManager->executeQuery($sql);
} else {
$jsondata = json_encode(array_values($postdata['inviteuser']));
$sql = "INSERT INTO  MasterFriendlist (reject_friend,uid) VALUES ('$jsondata','{$user->uid}')";
$status = $this->modelsManager->executeQuery($sql);
}
if (!empty($chkuser->invite_friend)) {
$inv = json_decode($chkuser->invite_friend);
$fin_inv = array_diff($inv, $postdata['inviteuser']);
$fin_inv = count($fin_inv) > 0 ? json_encode(array_values($fin_inv)) : NULL;
$sql_inv = "UPDATE  MasterFriendlist SET invite_friend = '{$fin_inv}' WHERE uid='{$user->uid}'";
$this->modelsManager->executeQuery($sql_inv);
}
}
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Invitation rejected Successfully</div>");
return $response->redirect("index/dashboard");
}
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something error happen</div>");
return $response->redirect("index/dashboard");
}
//fetch gamil contacts
public function getgmailcontactAction() {
if(!empty($_SESSION['quesid'])){
$qesid=$_SESSION['quesid'];
}else{
$qesid='';
}
$responsephal = new \Phalcon\Http\Response();
$clientid = '301667846490-4j3d37dcdj5a120vg8nonvsfk25nhkah.apps.googleusercontent.com';
$clientsecret = '6aQzLzJXjNeDCuo9pzXBy4Lp';
$redirecturi = BASEURL . 'socialisation/getgmailcontact';
$maxresults = 40; // Number of mailid you want to display.
$authcode = $_GET["code"];
$fields = array(
'code' => urlencode($authcode),
'client_id' => urlencode($clientid),
'client_secret' => urlencode($clientsecret),
'redirect_uri' => urlencode($redirecturi),
'grant_type' => urlencode('authorization_code'));
$fields_string = '';
foreach ($fields as $key => $value) {
$fields_string .= $key . '=' . $value . '&';
}
$fields_string = rtrim($fields_string, '&');
$ch = curl_init(); //open connection
curl_setopt($ch, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
curl_setopt($ch, CURLOPT_POST, 5);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($ch);
curl_close($ch);
$response = json_decode($result);
$accesstoken = $response->access_token;
$url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results=' . $maxresults . '&oauth_token=' . $accesstoken;
$xmlresponse = $this->curl_file_get_contents($url);
if ((strlen(stristr($xmlresponse, 'Authorization required')) > 0) && (strlen(stristr($xmlresponse, 'Error ')) > 0)) { //At times you get Authorization error from Google.
echo "<h2>OOPS !! Something went wrong. Please try reloading the page.</h2>";
exit();
}
$xml = new SimpleXMLElement($xmlresponse);
$xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
$result = $xml->xpath('//gd:email');
$mail = array();
$arr1 = array();
foreach ($result as $title) {
$chkuseralldata = MasterUsers::findFirst(array("mail_id='{$title->attributes()->address}'"));
if (!empty($chkuseralldata->uid)) {
$mail[] = $title->attributes()->address;
}
$arr1[] = $title->attributes()->address;
}
$arr = array_diff($arr1, $mail);
$user_session = $this->session->get("user");
$this->view->setVar("user_session", $user_session);
$course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
$this->view->setVar("course", $course);
$subject = MasterSubject::find(array("status=1"));
$this->view->setVar("subject", $subject);
$this->view->setVar("user", $arr);
if (!empty($qesid)) {
$this->view->setVar("qesid", $qesid);
$this->session->remove("quesid");
}
}
public function curl_file_get_contents($url) {
$curl = curl_init();
$userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
curl_setopt($curl, CURLOPT_URL, $url); //The URL to fetch. This can also be set when initializing a session with curl_init().
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 8); //The number of seconds to wait while trying to connect.
curl_setopt($curl, CURLOPT_USERAGENT, $userAgent); //The contents of the "User-Agent: " header to be used in a HTTP request.
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE); //To follow any "Location: " header that the server sends as part of the HTTP header.
curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); //To automatically set the Referer: field in requests where it follows a Location: redirect.
curl_setopt($curl, CURLOPT_TIMEOUT, 12); //The maximum number of seconds to allow cURL functions to execute.
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); //To stop cURL from verifying the peer's certificate.
$contents = curl_exec($curl);
curl_close($curl);
return $contents;
}
//send mail here to selected gmail user
function invitemailsuccessAction() {
$response = new \Phalcon\Http\Response();
$postdata = $this->request->getPost();
$user_session = $this->session->get("user");
if (!empty($postdata['quesid'])) {
foreach ($postdata['inviteuser'] as $use) {
$this->getDI()->getMail()->send(
array(
$use => $use
), "Challenge question By your friend", 'confirmation', array(
'confirmUrl' => 'socialisation/inviteuser/' . md5($user_session['id']) . "/" . md5($use) . "/" . $postdata['quesid'],
'sendername'=>$user_session['name'],
'status'=>1
)
);
}
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Challenge notification send Successfully</div>");
return $response->redirect("index/dashboard");
} else {
foreach ($postdata['inviteuser'] as $use) {
$this->getDI()->getMail()->send(
array(
$use => $use
), "Invitation For joining Ischool4u", 'confirmation', array(
'confirmUrl' => 'socialisation/inviteuser/' . md5($user_session['id']) . "/" . md5($use),
'sendername'=>$user_session['name'],
'status'=>2
)
);
}
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Invitation send Successfully</div>");
if (empty($this->session->get('dataredirect')) && empty($postdata['quesid'])) {
return $response->redirect("index/dashboard");
} else {
return $response->redirect($this->session->get('dataredirect'));
}
}
}
//when click on invite in gmail invitation link
function inviteuserAction($id_create='', $tomail='', $queid='') {
$response = new \Phalcon\Http\Response();
$cond = "md5(uid)='{$id_create}'";
$chkexist = MasterUsers::findFirst(array($cond));
if (!empty($id_create) && !empty($tomail) && !empty($chkexist->uid)) {
$cond = "md5(mail_id)='{$tomail}'";
$chkuseralldata1 = MasterUsers::findFirst(array($cond));
if ($chkuseralldata1->uid != '') {
$inviteid = $chkuseralldata1->uid;
} else {
$inviteid = '';
}
$_SESSION['id_create'] = $id_create;
$_SESSION['tomail'] = $tomail;
$_SESSION['inviteid'] = $inviteid;
if (!empty($queid)) {
$_SESSION['quesid'] = $queid;
}
return $response->redirect("index");
} else {
echo "page not found";
exit;
}
}
//create link here to selected facebook user
public function facebookinviteAction() {
$response = new \Phalcon\Http\Response();
if ($_REQUEST['success'] == 1) {
if (!empty($_SESSION['quesid'])) {
$this->session->remove("quesid");
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Challenge notification send Successfully</div>");
} else {
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Invitation send Successfully</div>");
}
} else {
$this->flashSession->success("<div class='alert alert-success alert-dismissable'>
<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Some error genreated</div>");
}
if (empty($this->session->get('dataredirect')) && empty($_SESSION['quesid'])) {
return $response->redirect("index/dashboard");
} else {
return $response->redirect($this->session->get('dataredirect'));
}
}
//when click on invite in facebook invitation link
public function facebookacceptAction($id='', $queid='') {
$response = new \Phalcon\Http\Response();
$cond = "md5(uid)='{$id}'";
$chkexist = MasterUsers::findFirst(array($cond));
if (!empty($id) && !empty($chkexist->uid)) {
$_SESSION['id_create'] = $id;
if (!empty($queid)) {
$_SESSION['quesid'] = $queid;
}
return $response->redirect("index");
} else {
echo "page has been expired";
exit;
}
}
//show invite friend notification
public function getinvitefriendAction() {
$response = new \Phalcon\Http\Response();
if (!$this->session->has("user")) {
header("location:" . BASEURL);
}
$usersession = $this->session->get("user");
$chkexist = MasterFriendlist::find(array("LOCATE({$usersession['id']},invite_friend)"));
$arr = array();
$i = 0;
foreach ($chkexist as $chkuser) {
$getus = MasterUsers::findFirst(array("uid='" . $chkuser->uid . "'"));
if(!empty($getus->uid)){
$arr[$i]['name'] = $getus->first_name . " " . $getus->last_name;
$arr[$i]['accpt'] = BASEURL . "socialisation/acceptinvbyuser/" . md5($getus->uid);
$arr[$i]['reject'] = BASEURL . "socialisation/rejectinvbyuser/" . md5($getus->uid);
}
$i++;
}
$total_inv = count($arr);
$str = '<li><a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown"><i class="fa fa-bell"></i>';
if($total_inv!==0){
        $str.='<span class="badge">' . $total_inv . '</span>';
    }
 $str.='</a>
    <div class="dropdown-menu notification-menu">
        <div class="notification-title">
            <span class="pull-right label label-default">' . $total_inv . '</span>
            Friend Invitation
        </div>
        <div class="content">
            <ul>';
            foreach ($arr as $val) {
            $str.='<li>
                    <span class="clearfix">
                            <div class="image">
                                    <i class="fa fa-thumbs-up bg-info"></i>
                            </div>
                            <span class="title">' . ucfirst($val['name']) . ' invited You!</span>
                    <span class="btn btn-link"><a href="' . $val['accpt'] . '">Accept</a></span><span class="btn btn-link"> <a href="' . $val['reject'] . '">Reject</a></span>
                    </span>
                </li>';
            }
            $str.='</ul><hr /><div class="text-right"><a href="#" class="view-more"></a></div></div></div></li>';
            echo $str;
            exit;
            }
            //show  notification
            public function getnotificationAction() {
            $response = new \Phalcon\Http\Response();
            if (!$this->session->has("user")) {
            header("location:" . BASEURL);
            }
            $usersession = $this->session->get("user");
            $sql="SELECT * FROM MasterNotification WHERE friend_id='{$usersession['id']}' AND status=1 AND type=2 ORDER BY id DESC";
            $chkexist =  $this->modelsManager->executeQuery($sql);
            $arr = array();
            $i = 0;
            foreach ($chkexist as $chkuser) {
            if($chkuser->notification_type==12 || $chkuser->notification_type==13){
                $getus = MasteradminUsers::findFirst(array("aid='" . $chkuser->uid . "'"));
				 if(!empty($getus->aid)){
            		$arr[$i]['name'] ='';
            		$arr[$i]['msg'] = $chkuser->message;
            		$arr[$i]['link'] =BASEURL."question/queryquestion/".$chkuser->link."/".$chkuser->id;
            	} 
            }else{
				 $getus = MasterUsers::findFirst(array("uid='" . $chkuser->uid . "'"));
				 if(!empty($getus->uid)){
            		$arr[$i]['name'] = $getus->first_name . " " . $getus->last_name;
            		$arr[$i]['msg'] = $chkuser->message;
            		$arr[$i]['link'] =BASEURL."question/challenged_question/".$chkuser->link."/".$chkuser->id;
            	}            	
            }
            
            $i++;
            }
            $total_inv = count($arr);
            $str = '<li><a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown"><i class="fa fa-envelope"></i>';
                if($total_inv!=0){
                    $str.='<span class="badge">' . $total_inv . '</span>';
                }
                $str.='</a>
                <div class="dropdown-menu notification-menu">
                    <div class="notification-title">
                        <span class="pull-right label label-default">' . $total_inv . '</span>
                        Notification
                    </div>
                    <div class="content">
                        <ul>';
                        foreach ($arr as $val) {
                        $str.='<li>
                                <span class="clearfix">
                                        <div class="image">
                                                <i class="fa fa-thumbs-up bg-info"></i>
                                        </div>
                                        <span class="title"><a href="'.$val['link'].'">' . ucfirst($val['name']) ." ".$val['msg'].' !</a></span>
                                </span>
                            </li>';
                        }
                        $str.='</ul><hr /><div class="text-right"><a href="#" class="view-more"></a></div></div></div></li>';
                        echo $str;
                        exit;
                        }
                        //gmail login or registration
                        function gmailloginAction() {
                        $responsedata = new \Phalcon\Http\Response();
                        $authcode = $_GET["code"];
                        $sitesetting = SiteSetting::findfirst("id_setting=1"); 
                        if (empty($_GET["error"]) && !empty($authcode)) {
                        $clientid = '301667846490-4j3d37dcdj5a120vg8nonvsfk25nhkah.apps.googleusercontent.com';
                        $clientsecret = '6aQzLzJXjNeDCuo9pzXBy4Lp';
                        $redirecturi = BASEURL . 'socialisation/gmaillogin';
                        $fields = array(
                        'code' => urlencode($authcode),
                        'client_id' => urlencode($clientid),
                        'client_secret' => urlencode($clientsecret),
                        'redirect_uri' => urlencode($redirecturi),
                        'grant_type' => urlencode('authorization_code'));
                        $fields_string = '';
                        foreach ($fields as $key => $value) {
                        $fields_string .= $key . '=' . $value . '&';
                        }
                        $fields_string = rtrim($fields_string, '&');
                        $ch = curl_init(); //open connection
                        curl_setopt($ch, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
                        curl_setopt($ch, CURLOPT_POST, 5);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result);
                        $accesstoken = $response->access_token;
                        $url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" . $accesstoken;
                        $xmlresponse = $this->curl_file_get_contents($url);
                        $data = json_decode($xmlresponse);
                        $mvcheck = MasterUsers::findFirst(array("mail_id='" . $data->email . "'"));
                        if (!empty($mvcheck->uid)) {//directly login if email id alredy registered
                        $this->session->set('user', array(
                        'id' => $mvcheck->uid,
                        'type' => 'user',
                        'name' => $mvcheck->first_name . ' ' . $mvcheck->last_name,
                        'course' => $mvcheck->course,
                        'sup_cat' => $mvcheck->sup_cat
                        ));
                        if (!empty($_GET["state"])) {
                        $courses = json_decode(urldecode($_GET["state"]));
                        if (!empty($courses->quesid)) {
                        return $response->redirect("question/challenged_question/" . $courses->quesid); //redirect to challenged question
                        } else {
                        $this->flashSession->success("<div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>You have already registered with us before</div>");
                        }
                        }
                        return $responsedata->redirect("index/dashboard");
                        } else {
                        if (empty($_GET["state"])) { //registration when login with gmail if user not registered before
                        $this->session->set('gmailreg', array(
                        'google_id' => $data->id,
                        'first_name' => $data->given_name,
                        'last_name' => $data->family_name,
                        'mail_id' => $data->email
                        ));
                        return $responsedata->redirect("socialisation/signup");
                        } else {//registered with gmail if not registered
                        $courses = json_decode(urldecode($_GET["state"]));
                        $post['mail_id'] = $data->email;
                        $post['first_name'] = $data->given_name;
                        $post['last_name'] = $data->family_name;
                        $post['google_id'] = $data->id;
                        $post['sup_cat'] = $courses->supcat;
                        $post['course'] = $courses->course;
                        $post['created'] = date('Y-m-d h:i:s');
                        $post['mail_code'] = rand(1000, 2000);
                        $post['is_active'] = 1;
                        $post['mail_verified'] = 0;
                        $con = new MasterUsers();
                        if ($con->save($post)) {
                        ///add as friend done by satya
                        $signpoint =$sitesetting->new_student;// (int) $signpoint + (int) 50; //point for invited user
                        if ($courses->create_user != '') {
                        $inviterpoint = $sitesetting->invite_by; //point for who invite
                        $postdata['inviteuser'] = array($con->uid);
                        $user = MasterUsers::findFirst(array("md5(uid)='" . $courses->create_user . "'"));
                        $chkuser = MasterFriendlist::findFirst(array("uid='" . $user->uid . "'"));
                        if ($chkuser->uid != '') {
                        if ($chkuser->accept_friend != '') {
                        $userall = json_decode($chkuser->accept_friend);
                        $finalarr = array_merge((array) $userall, (array) $postdata['inviteuser']);
                        } else {
                        $finalarr = $postdata['inviteuser'];
                        }
                        $jsondata = json_encode(array_values($finalarr));
                        $sql = "UPDATE  MasterFriendlist SET accept_friend = '$jsondata' WHERE uid='{$user->uid}'";
                        $status = $this->modelsManager->executeQuery($sql);
                        } else {
                        $jsondata = json_encode(array_values($postdata['inviteuser']));
                        $sql = "INSERT INTO  MasterFriendlist (accept_friend,uid) VALUES ('$jsondata','{$user->uid}')";
                        $status = $this->modelsManager->executeQuery($sql);
                        }
                        $sql_actpoint = "UPDATE  MasterUserpoint SET points=points+$inviterpoint WHERE uid={$user->uid}";
                        $uo_sign = $this->modelsManager->executeQuery($sql_actpoint);
                        //challenged question
                        if (!empty($courses->quesid)) {
                        $quesid = base64_decode($courses->quesid);
                        $add_date = date("y-m-d H:i:s");
                        $cond2 = "(md5(uid)='{$courses->create_user}' AND questionid='{$quesid}')";
                        $chkuser = MasterChallengeques::findFirst(array($cond2));
                        if (!empty($chkuser->id)) {
                        if (trim($chkuser->challenge_id) != '') {
                        $userdta1 = json_decode($chkuser->challenge_id);
                        $finalarr1 = array_merge((array) $userdta1, (array) $postdata['inviteuser']);
                        } else {
                        $finalarr1 = $postdata['inviteuser'];
                        }
                        $jsondata1 = json_encode(array_values($finalarr1));
                        $sql1 = "UPDATE  MasterChallengeques SET challenge_id = '$jsondata1' WHERE uid='{$user->uid}' AND questionid='{$quesid}'";
                        $status = $this->modelsManager->executeQuery($sql1);
                        } else {
                        $jsondata1 = json_encode(array_values($postdata['inviteuser']));
                        $sql1 = "INSERT INTO  MasterChallengeques (challenge_id,uid,questionid,add_date) VALUES ('$jsondata1','{$user->uid}','{$quesid}','{$add_date}')";
                        $status = $this->modelsManager->executeQuery($sql1);
                        }
                        //replace with new notification with prevoius notification
                        /*$rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 GROUP BY add_date ORDER BY add_date ASC";
                        $cne = $this->modelsManager->executeQuery($rep_sql);
                        $totcnt = $cne->count();
                        $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 ORDER BY add_date ASC LIMIT 0,1";
                        $dele = $this->modelsManager->executeQuery($del_sql);
                        if ($totcnt > 1) {
                        foreach ($dele as $deval) {
                        $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                        $this->modelsManager->executeQuery($deldate);
                        }
                        }*/
                        //add notification tabel
                        $message=" challenged a question";
                        $link=$courses->quesid;//addslashes(htmlentities(BASEURL."question/challenged_question/".$courses->quesid));
                        $sqlnot = "INSERT INTO  MasterNotification (uid,friend_id,message,add_date,status,type,link,notification_type) VALUES ('{$user->uid}','{$con->uid}','{$message}','{$add_date}','0','2','$link',1)";
                        $getnot = $this->modelsManager->executeQuery($sqlnot);
                        }
                        }
                        $sql_signpoint = "INSERT INTO  MasterUserpoint (points,uid) VALUES ($signpoint,'{$con->uid}')";
                        $status_sign = $this->modelsManager->executeQuery($sql_signpoint);
                        $this->session->set('user', array(
                        'id' => $con->uid,
                        'type' => 'user',
                        'name' => $con->first_name . ' ' . $con->last_name,
                        'course' => $con->course,
                        'sup_cat' => $con->sup_cat
                        ));
                        if (!empty($courses->quesid)) {
                        return $response->redirect("question/challenged_question/" . $courses->quesid."/".$getnot->id); //redirect to challenged question
                        } else {
                        return $responsedata->redirect("index/dashboard");
                        }
                        }
                        }
                        }
                        } else {
                        return $responsedata->redirect("index");
                        }
                        }
                        public function getcourseAction($scid='') {
                        if ($scid != '') {
                        $curs = MasterCourse::find(array("status=1 AND scid=$scid"));
                        $str = "<h3>Select a Class</h3>";
                        foreach ($curs as $val) {
                        $str.='<a class="actinactaa11" onClick="chkrrr12(' . $val->id . ');" id="all' . $val->id . '">
                            <input type="radio" name="course" class="hidden chkclass required" value="' . $val->id . '" id="sub' . $val->id . '">
                            <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>' . $val->name . '</a>';
                            }
                            echo $str;
                            exit;
                            }
                            }
                            public function getcoursesignAction($scid='') {
                            if ($scid != '') {
                            $curs = MasterCourse::find(array("status=1 AND scid=$scid"));
                            $str = "";
                            foreach ($curs as $value) {
                            $str.='<div class="clearfix" onClick="getcurs(' . $value->id . ');"><p class="pull-left para">' . $value->name . '</p><i class="fa fa-chevron-right pull-right icon"></i></div>';
                            }
                            echo $str;
                            exit;
                            }
                            }
                            function facebookloginAction() {
                            $responsedata = new \Phalcon\Http\Response();
                            $app_id = "1141358925893848";
                            $app_secrect = "919b867fdd4e49f56a7c0b2103edd96c";
                            $red_uri = BASEURL . 'socialisation/facebooklogin/';
                            $code = $_REQUEST['code'];
                            $token_url = "https://graph.facebook.com/v2.5/oauth/access_token?client_id=" . $app_id . "&redirect_uri=" .urlencode($red_uri) . "&client_secret=" . $app_secrect . "&scope=email&code=" . $code;
                            $sitesetting = SiteSetting::findfirst("id_setting=1"); 
                            if (!empty($code) && empty($_REQUEST['error'])) {
                            $res = $this->curl_file_get_contents($token_url);
                            $arr=json_decode($res);
                            //parse_str($res, $arr);
                            $final_url = "https://graph.facebook.com/v2.5/me?fields=id,name,email&access_token=" . $arr->access_token;
                            $final_res = $this->curl_file_get_contents($final_url);
                            $user_info = json_decode($final_res);
                            $parts = explode(" ", $user_info->name);
                            $lastname = array_pop($parts);
                            $firstname = implode(" ", $parts);
                            if (!empty($user_info->error)) {
                            return $responsedata->redirect("index");
                            }
                            if($user_info->email!=''){
                            $mvcheck = MasterUsers::findFirst(array("mail_id='" . $user_info->email . "'"));
                            }else{
                            $mvcheck = MasterUsers::findFirst(array("facebook_id='" . $user_info->id . "'"));
                            }
                            if ($mvcheck->uid != '') {//directly login if email id alredy registered
                            $this->session->set('user', array(
                            'id' => $mvcheck->uid,
                            'type' => 'user',
                            'name' => $mvcheck->first_name . ' ' . $mvcheck->last_name,
                            'course' => $mvcheck->course,
                            'sup_cat' => $mvcheck->sup_cat
                            ));
                            if (!empty($_GET["state"])) {
                            $courses = json_decode(urldecode($_GET["state"]));
                            if (!empty($courses->quesid)) {
                            return $response->redirect("question/challenged_question/" . $courses->quesid); //redirect to challenged question
                            } else {
                            $this->flashSession->success("<div class='alert alert-success alert-dismissable'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>You have already registered with us before</div>");
                            }
                            }
                            return $responsedata->redirect("index/dashboard");
                            } else {
                            if (empty($_GET["state"])) { //registration when login with facebook if user not registered before
                            $this->session->set('gmailreg', array(
                            'facebook_id' => $user_info->id,
                            'first_name' => $firstname,
                            'last_name' => $lastname,
                            'mail_id' => $user_info->email
                            ));
                            return $responsedata->redirect("socialisation/signup");
                            } else {//registered with facebook if not registered
                            $courses = json_decode(urldecode($_GET["state"]));
                            $post['mail_id'] = $user_info->email;
                            $post['first_name'] = $firstname;
                            $post['last_name'] = $lastname;
                            $post['facebook_id'] = $user_info->id;
                            $post['sup_cat'] = $courses->supcat;
                            $post['course'] = $courses->course;
                            $post['created'] = date('Y-m-d h:i:s');
                            $post['mail_code'] = rand(1000, 2000);
                            $post['is_active'] = 1;
                            $post['mail_verified'] = 0;
                            $con = new MasterUsers();
                            if ($con->save($post)) {
                            $signpoint = $sitesetting->new_student; // (int) $signpoint + (int) 50; //point for invited user
                            ///add as friend if invited any other user
                            if ($courses->create_user != '') {
                            $inviterpoint = $sitesetting->invite_by; //point for who invite
                            $postdata['inviteuser'] = array($con->uid);
                            $user = MasterUsers::findFirst(array("md5(uid)='" . $courses->create_user . "'"));
                            $chkuser = MasterFriendlist::findFirst(array("uid='" . $user->uid . "'"));
                            if ($chkuser->uid != '') {
                            if ($chkuser->accept_friend != '') {
                            $userall = json_decode($chkuser->accept_friend);
                            $finalarr = array_merge((array) $userall, (array) $postdata['inviteuser']);
                            } else {
                            $finalarr = $postdata['inviteuser'];
                            }
                            $jsondata = json_encode(array_values($finalarr));
                            $sql = "UPDATE  MasterFriendlist SET accept_friend = '$jsondata' WHERE uid='{$user->uid}'";
                            $status = $this->modelsManager->executeQuery($sql);
                            } else {
                            $jsondata = json_encode(array_values($postdata['inviteuser']));
                            $sql = "INSERT INTO  MasterFriendlist (accept_friend,uid) VALUES ('$jsondata','{$user->uid}')";
                            $status = $this->modelsManager->executeQuery($sql);
                            }
                            $sql_actpoint = "UPDATE  MasterUserpoint SET points=points+$inviterpoint WHERE uid={$user->uid}";
                            $uo_sign = $this->modelsManager->executeQuery($sql_actpoint);
                            //challenged question
                            if (!empty($courses->quesid)) {
                            $quesid = base64_decode($courses->quesid);
                            $add_date = date("y-m-d H:i:s");
                            $cond2 = "(md5(uid)='{$courses->create_user}' AND questionid='{$quesid}')";
                            $chkuser = MasterChallengeques::findFirst(array($cond2));
                            if (!empty($chkuser->id)) {
                            if (trim($chkuser->challenge_id) != '') {
                            $userdta1 = json_decode($chkuser->challenge_id);
                            $finalarr1 = array_merge((array) $userdta1, (array) $postdata['inviteuser']);
                            } else {
                            $finalarr1 = $postdata['inviteuser'];
                            }
                            $jsondata1 = json_encode(array_values($finalarr1));
                            $sql1 = "UPDATE  MasterChallengeques SET challenge_id = '$jsondata1' WHERE uid='{$user->uid}' AND questionid='{$quesid}'";
                            $status = $this->modelsManager->executeQuery($sql1);
                            } else {
                            $jsondata1 = json_encode(array_values($postdata['inviteuser']));
                            $sql1 = "INSERT INTO  MasterChallengeques (challenge_id,uid,questionid,add_date) VALUES ('$jsondata1','{$user->uid}','{$quesid}','{$add_date}')";
                            $status = $this->modelsManager->executeQuery($sql1);
                            }
                            //replace with new notification with prevoius notification
                            /* $rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 GROUP BY add_date ORDER BY add_date ASC";
                            $cne = $this->modelsManager->executeQuery($rep_sql);
                            $totcnt = $cne->count();
                            $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 ORDER BY add_date ASC LIMIT 0,1";
                            $dele = $this->modelsManager->executeQuery($del_sql);
                            if ($totcnt > 1) {
                            foreach ($dele as $deval) {
                            $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                            $this->modelsManager->executeQuery($deldate);
                            }
                            } */
                            //add notification tabel
                            $message = "challenged a question";
                            $link = $courses->quesid; // addslashes(htmlentities(BASEURL . "question/challenged_question/" . $courses->quesid));
                            $sqlnot = "INSERT INTO  MasterNotification (uid,friend_id,message,add_date,status,type,link,notification_type) VALUES ('{$user->uid}','{$con->uid}','{$message}','{$add_date}','0','2','$link',1)";
                            $getnot = $this->modelsManager->executeQuery($sqlnot);
                            }
                            }
                            $sql_signpoint = "INSERT INTO  MasterUserpoint (points,uid) VALUES ($signpoint,'{$con->uid}')";
                            $status_sign = $this->modelsManager->executeQuery($sql_signpoint);
                            $this->session->set('user', array(
                            'id' => $con->uid,
                            'type' => 'user',
                            'name' => $con->first_name . ' ' . $con->last_name,
                            'course' => $con->course,
                            'sup_cat' => $con->sup_cat
                            ));
                            if (!empty($courses->quesid)) {
                            return $response->redirect("question/challenged_question/" . $courses->quesid . "/" . $getnot->id); //redirect to challenged question
                            } else {
                            return $responsedata->redirect("index/dashboard");
                            }
                            }
                            }
                            }
                            } else {
                            return $responsedata->redirect("index");
                            }
                            }
                            //when registration with facebook and regisration
                            function signupAction() {
                            $supcat = MasterSupCat::find(array("status=1"));
                            $this->view->setVar("supcat", $supcat);
                            $curs = MasterCourse::find(array("status=1"));
                            $arr = '';
                            foreach ($curs as $val) {
                            $arr[$val->scid][] = $val->name;
                            }
                            $this->view->setVar("curs", $arr);
                            }
                            function challengefriendAction($quesid="") {
                            $response = new \Phalcon\Http\Response();
                            $user_session = $this->session->get("user");
                            $this->view->setVar("user_session", $user_session);
                            $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
                            $this->view->setVar("course", $course);
                            $subject = MasterSubject::find(array("status=1"));
                            $this->view->setVar("subject", $subject);
                            $quesid = base64_decode($quesid);
                            $this->view->setVar("quesid", $quesid);
                            $usersession = $this->session->get("user");
                            $cond = "uid={$usersession['id']}";
                            $chalenge1 = MasterFriendlist::findFirst(array($cond));
                            $arr = array();
                            if (!empty($chalenge1->accept_friend)) {
                            $arr = json_decode($chalenge1->accept_friend);
                            }
                            $cond2 = "LOCATE({$usersession['id']},accept_friend)";
                            $chalenge2 = MasterFriendlist::find(array($cond2));
                            $arr1 = array();
                            foreach ($chalenge2 as $val) {
                            $arr1[] = $val->uid;
                            }
                            $alcall = MasterChallengeques::findfirst(array("uid='" . $user_session['id'] . "' AND questionid='" . $quesid . "'"));
                            $arrmer = array_merge($arr, $arr1);
                            $arr2 = array();
                            if (!empty($alcall->challenge_id)) {
                            $arr2 = json_decode($alcall->challenge_id);
                            }
                            $arrdif=array("");
                            $arrdif = array_diff((array)$arrmer,(array)$arr2);
                            $neww = implode(",", array_values($arrdif));
                            $user = MasterUsers::find(array("uid IN (" . $neww . ")"));
                            $this->view->setVar("user", $user);
                            }
                            function challengesuccessAction() {
                            $response = new \Phalcon\Http\Response();
                            $postdata = $this->request->getPost();
                            $user_session = $this->session->get("user");
                            $add_date = date("y-m-d H:i:s");
                            $chkuser = MasterChallengeques::findFirst(array("uid='" . $user_session['id'] . "' AND questionid='" . $postdata['quesid'] . "'"));
                            if (!empty($chkuser->uid)) {
                            if (trim($chkuser->challenge_id) != '') {
                            $user = json_decode($chkuser->challenge_id);
                            $finalarr = array_merge((array) $user, (array) $postdata['challengeuser']);
                            } else {
                            $finalarr = $postdata['challengeuser'];
                            }
                            $jsondata = json_encode(array_values($finalarr));
                            $sql = "UPDATE  MasterChallengeques SET challenge_id = '$jsondata' WHERE uid='{$user_session['id']}' AND questionid='{$postdata['quesid']}'";
                            $status = $this->modelsManager->executeQuery($sql);
                            } else {
                            $jsondata = json_encode(array_values($postdata['challengeuser']));
                            $sql = "INSERT INTO  MasterChallengeques (challenge_id,uid,questionid,add_date) VALUES ('$jsondata','{$user_session['id']}','{$postdata['quesid']}','{$add_date}')";
                            $status = $this->modelsManager->executeQuery($sql);
                            }
                            
                            //replace with new notification with prevoius notification
                            /*$rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$user_session['id']}' AND notification_type=1 GROUP BY add_date ORDER BY add_date ASC";
                            $cne = $this->modelsManager->executeQuery($rep_sql);
                            $totcnt = $cne->count();
                            $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$user_session['id']}' AND notification_type=1 ORDER BY add_date ASC LIMIT 0,1";
                            $dele = $this->modelsManager->executeQuery($del_sql);
                            if ($totcnt > 1) {
                            foreach ($dele as $deval) {
                            $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                            $this->modelsManager->executeQuery($deldate);
                            }
                            }*/
                            //store in notification table
                            $getus = MasterUsers::findFirst(array("uid='" . $user_session['id'] . "'"));
                            $quesid = base64_encode($postdata['quesid']);
                            $message="challenged a question";
                            $link=$quesid;//addslashes(htmlentities(BASEURL."question/challenged_question/".$quesid));
                            foreach($postdata['challengeuser'] as $val){
                            $sql = "INSERT INTO  MasterNotification (uid,friend_id,message,add_date,type,link,notification_type) VALUES ('{$user_session['id']}','{$val}','{$message}','{$add_date}','2','{$link}',1)";
                            $status = $this->modelsManager->executeQuery($sql);
                            }
                            
                            $this->flashSession->success("<div class='alert alert-success alert-dismissable'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Challenge notification send Successfully</div>");
                            if (empty($this->session->get('dataredirect'))) {
                            return $response->redirect("index/dashboard");
                            } else {
                            return $response->redirect($this->session->get('dataredirect'));
                            }
                            }
                            public function checkmailexixtAction($emailid=''){
                            $check_email = MasterUsers::findFirst(array("mail_id='" . $emailid . "'"));
                            if ($check_email->uid != '') {
                            echo 2;exit;
                            }else{
                            echo 1;exit;
                            }
                            }
                            }
                            ?>