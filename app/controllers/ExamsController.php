<?php
class ExamsController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
     * Show all Exam types
     * @return [result] [Result from database and show]
     * @author Rajesh Sardar
     */
    function indexAction()
    {
        $data = MasterExamType::find();
        $this->view->setVar("exam_type", $data);
        $supcat = MasterSupCat::find("status=1");
        $arr=array();
        foreach($supcat as $cca){
          $arr[$cca->scid]=$cca->scname;  
        }
        $this->view->setVar("supcat", $arr);
        $course = MasterCourse::find("status=1");
        $arrc=array();
        foreach($course as $cc){
          $arrc[$cc->id]=$cc->name;  
        }
        $this->view->setVar("course", $arrc);
    }
    /**
     * Add Exam type
     * @return [status] [Return status whether exam type is added or not]
     * @author Rajesh Sardar
     */
    function addexamtypeAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $postval['q_type'] = json_encode($postval['q_type']);
            $postval['course'] = json_encode($postval['course']);
            $postval['ql_easy'] = json_encode($postval['ql_easy']);
            $postval['ql_medium'] = json_encode($postval['ql_medium']);
            $postval['ql_hard'] = json_encode($postval['ql_hard']);
            $postval['date'] = date("Y-m-d",strtotime($postval['date']));
            $Addques= new MasterExamType();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Successfully Add</div>");
            return $response->redirect("exams");
        }
        $supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("qtype", $qtype);
    }
    /**
     * Update Detail of perticular exam type
     * @param  [int] $id [Exam type id]
     * @return [return]     [Return update status]
     * @author Rajesh Sardar
     */
    function updexamtypeAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $exam_type = MasterExamType::findFirst(array("etid='".$id."'"));
            $this->view->setVar("exam_type", $exam_type);
            $supcat = MasterSupCat::find("status=1");
            $this->view->setVar("supcat", $supcat);
            $course = MasterCourse::find("scid='".$exam_type->sup_cat."' AND status=1");
            $this->view->setVar("course", $course);
            $qtype = MasterQuesType::find("status=1");
            $this->view->setVar("qtype", $qtype);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['q_type'] = json_encode($postval['q_type']);
            $postval['course'] = json_encode($postval['course']);
            $postval['ql_easy'] = json_encode($postval['ql_easy']);
            $postval['ql_medium'] = json_encode($postval['ql_medium']);
            $postval['ql_hard'] = json_encode($postval['ql_hard']);
            if($postval['no_qs']==''){$postval['no_qs']=null;};
            if($postval['time']==''){$postval['time']=null;};
            $date = strtotime($postval['date']);
            $postval['date'] = date("Y-m-d",$date);
            $update= new MasterExamType();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Source Successfully Update</div>");
            return $response->redirect("exams");
        }
    }
    /**
     * Delete Perticular exam type
     * @param  [int] $id [Exam type id]
     * @return [return]     [Return delete status]
     * @author Rajesh Sardar
     */
    function delexamtypeAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterExamType WHERE etid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Deleted Successfully</div>");
            return $response->redirect("exams");
        }
    }
    /**
     * Update status of Exam type
     * @param  [int] $u_status [Update status code]
     * @param  [int] $id       [Exam type perticular id]
     * @return [return]           [Return Success Status]
     * @author Rajesh Sardar
     */
    function updextystatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterExamType SET status = 0 where etid=".$id."";
            }else{
                $phql = "UPDATE MasterExamType SET status = 1 where etid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Status Updated Successfully</div>");
            return $response->redirect("exams");
        }
    }


    ##############################################################
    ################## Exam Assign ###############################
    ##############################################################
    
    
    function examassignAction() {
        if(isset($_SESSION['redirectdta'])){
            unset($_SESSION['redirectdta']);
        }
        if ($this->request->isGet()) {
            $getVal = $this->request->get();
            if (isset($getVal['submit'])) {
                $where = '';
                if ($getVal['subject'] == '') {
                    unset($getVal['subject']);
                } else {
                    $where .= "subject='" . $getVal['subject'] . "' ";
                    $subsubdet = MasterSubSubject::find("subid = '" . $getVal['subject'] . "' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if ($getVal['subsubject'] == '') {
                    unset($getVal['subsubject']);
                } else {
                    $where .= " AND subsubject='" . $getVal['subsubject'] . "' ";
                    $mTopicdet = MasterTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if ($getVal['topics'] == '') {
                    unset($getVal['topics']);
                } else {
                    $where .= " AND topics='" . $getVal['topics'] . "' ";
                    $msTopicdet = MasterSubTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND tid='" . $getVal['topics'] . "' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if ($getVal['subtopics'] == '') {
                    unset($getVal['subtopics']);
                } else {
                    $where .= " AND subtopics='" . $getVal['subtopics'] . "' ";
                };
                if ($getVal['qtype'] == '') {
                    unset($getVal['qtype']);
                } else {
                    if ($where == '') {
                        $where .= "q_type='" . $getVal['qtype'] . "' ";
                    } else {
                        $where .= " AND q_type='" . $getVal['qtype'] . "' ";
                    }
                };
                if ($getVal['qlevel'] == '') {
                    unset($getVal['qlevel']);
                } else {
                    if ($where == '') {
                        $where .="q_level='" . $getVal['qlevel'] . "'";
                    } else {
                        $where .=" AND q_level='" . $getVal['qlevel'] . "' ";
                    }
                };
                if ($getVal['etype'] == '') {
                    unset($getVal['etype']);
                } else {
                    $where .= " AND LOCATE ('" . $getVal['etype'] . "',e_type) ";
                };
                unset($getVal['_url'], $getVal['submit']);
                if ($where == '') {
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion';
                } else {
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion WHERE ' . $where;
                }
                $currentPage = $_GET['page'];
                $question = $this->modelsManager->executeQuery($phql);
                $i = 0;
                $data = array();
                foreach ($question as $val) {
                    $data[$i]['tableid'] = $val->tableid;
                    $data[$i]['questionid'] = $val->questionid;
                    $data[$i]['q_type'] = $val->q_type;
                    $data[$i]['e_type'] = $val->e_type;
                    $data[$i]['qusid'] = $val->qusid;
                    $i++;
           }
                $paginator = new \Phalcon\Paginator\Adapter\NativeArray(array(
                            "data" => $data,
                            "limit" => 10,
                            "page" => $currentPage
                                )
                );
                $question = $paginator->getPaginate();
                $this->view->setVar("question", $question);
            }
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
        $qtype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $qtype);
    }

    /**
     * Update question detail (throuing question id to question update link)
     * @param  [int] $id [questionid]
     * @return [redirect]     [redirect to question update table]
     */
    function updquestionAction($id,$noteid='',$type='')
    {
        $response = new \Phalcon\Http\Response();
         $this->session->set('redirectdta',$_SERVER['HTTP_REFERER']);
        $tableid = MasterQuestion::findFirst(array("questionid='".$id."'"));
       // print_r($tableid->tableid);
        echo $id;
        if($tableid->tableid==1){
            $qid = MasterQuestionBank::findFirst(array("questionid='".$id."'"));
            $url="questionbank/updatequestion/".$qid->qid;
        }else if($tableid->tableid==3){
            $qid = MasterNumericQuestion::findFirst(array("questionid='".$id."'"));
            $url="numericquestion/updatenuquestion/".$qid->qid;
        }else if($tableid->tableid==4){
            $qid = MasterParaQuestion::findFirst(array("questionid='".$id."'"));
            $url="paraquestion/update/".$qid->qid;
        }else if($tableid->tableid==5){
            $qid = MasterReasonQuestion::findFirst(array("questionid='".$id."'"));
            $url="questionbank/updateraquestion/".$qid->qid;
        }else if($tableid->tableid==6){
            $qid = MasterReasonQuestion::findFirst(array("questionid='".$id."'"));
            $url="subtheory/update/".$qid->qid;
        }else{
            $qid = MasterSubtheoQuestion::findFirst(array("questionid='".$id."'"));
            $url="questionbank/updmmquestion/".$qid->qid;
        }
        if($noteid!='' && $type!=''){
            $url=$url."/".$noteid."/".$type;
        }
        return $response->redirect($url);
    }
    /**
     * Update exam type with their respective value
     * @return [status] [Retrun Status when it success]
     */
    function updqtAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            if($postval['qid']!="")
            {
                $phql = "UPDATE MasterQuestion SET e_type = '".$postval['etype']."' where questionid='".$postval['qid']."' ";
                $status = $this->modelsManager->executeQuery($phql);
                if($status){
                    echo 'Status Ok';
                }
                exit();
            }
        }
        
    }

    function viewassessAction()
    {
         if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if($getVal['submit']){
                if($getVal['subject']==''){
                    unset($getVal['subject']);
                }else{
                    $where= ' subject="'.$getVal['subject'].'"';
                    $subsubdet = MasterSubSubject::find("subid = '".$getVal['subject']."' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if($getVal['subsubject']==''){
                    unset($getVal['subsubject']);
                }else{
                    $where .= ' AND subsubject ="'.$getVal['subsubject'].'"';
                    $mTopicdet = MasterTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if($getVal['topics']==''){
                    unset($getVal['topics']);
                }else{
                    $where .= ' AND topics="'.$getVal['topics'].'"';
                    $msTopicdet = MasterSubTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND tid='".$getVal['topics']."' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if($getVal['subtopics']==''){
                    unset($getVal['subtopics']);
                }else{
                    $where .= ' AND subtopics="'.$getVal['subtopics'].'"';
                };
               $arr=array($where);
            }else{
              $arr=array();  
            }
             $assessment = MasterAssessment::find($arr);
              $this->view->setVar("assessment", $assessment);
               
        }
         $getSub = MasterSubject::find(array("status=1"));
         $this->view->setVar("subdet", $getSub);
    }

    function assessmentAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $postval['subtio']=$postval['subtopics'];
            $subtopi=implode(",",$postval['subtopics']);
            $sqlqu="SELECT * FROM MasterQuestion WHERE subtopics IN(".$subtopi.") AND q_type NOT IN (5,6,8) AND q_level IN(1,2,3) AND  LOCATE(2,e_type) AND q_status=1";
            $dataqu= $this->modelsManager->executeQuery($sqlqu);
            if($dataqu->count()<$postval['no_qs']){
                 $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Insufficient Number Of Question</div>");
            return $response->redirect("exams/assessment");
            }
            $postval['subtopics'] = json_encode($postval['subtopics']);
            $Addques= new MasterAssessment();
            $Addques->save($postval);
            UIElementsAdmin::assessmentList($Addques->id,$postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Successfully Added</div>");
            return $response->redirect("exams/viewassess");
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
    }

    function updassessAction($id)
    {
       $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $postval['subtio']=$postval['subtopics'];
            $subtopi=implode(",",$postval['subtopics']);
            $sqlqu="SELECT * FROM MasterQuestion WHERE subtopics IN(".$subtopi.") AND q_type NOT IN (5,6,8) AND q_level IN(1,2,3) AND  LOCATE(2,e_type) AND q_status=1";
            $dataqu= $this->modelsManager->executeQuery($sqlqu);
            if($dataqu->count()<$postval['no_qs']){
                 $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Insufficient Number Of Question</div>");
            return $response->redirect("exams/updassess");
            }
            $postval['subtopics'] = json_encode($postval['subtopics']);
            $Addques= new MasterAssessment();
            $Addques->save($postval);
            $delallprevrecord = "DELETE FROM AssessmentQuestionList WHERE assessment_id = '".$postval['id']."'";
            $this->modelsManager->executeQuery($delallprevrecord);
            UIElementsAdmin::assessmentList($Addques->id,$postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Updated Successfully</div>");
            return $response->redirect("exams/viewassess");
        }
        if($id!=''){
            $assessment = MasterAssessment::findFirst(array("id='$id'"));
            $this->view->setVar("assessment", $assessment);
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
        $subsubdet = MasterSubSubject::find("subid='$assessment->subject' AND status=1");
        $this->view->setVar("subsubdet", $subsubdet);
        $mTopicdet = MasterTopics::find("subid='$assessment->subject' AND ssubid='$assessment->subsubject' AND status=1");
        $this->view->setVar("mTopicdet", $mTopicdet);
        $msTopicdet = MasterSubTopics::find("subid='$assessment->subject' AND ssubid='$assessment->subsubject' AND tid='$assessment->topics' AND status=1");
        $this->view->setVar("msTopicdet", $msTopicdet);
    }

    function upstassessAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterAssessment SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterAssessment SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Status Updated Successfully</div>");
            return $response->redirect("exams/viewassess");
        }
    }

    function getsubsubjectAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $msSubdet = MasterSubSubject::find("subid = '".$postval['subid']."' AND status=1");
            echo "<option value=''>[ Select Sub Subject ]</option>";
            foreach($msSubdet as $msSubres){
                echo "<option value='".$msSubres->ssid."'>".$msSubres->ssname."</option>";
            }
        }
    }
    function gettopicsAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $mTopicdet = MasterTopics::find("subid = '".$postval['subid']."' AND ssubid='".$postval['ssubid']."' AND status=1");
            echo "<option value=''>[ Select Topics ]</option>";
            foreach($mTopicdet as $mTopicres){
                echo "<option value='".$mTopicres->tid."'>".$mTopicres->tname."</option>";
            }
        }
    }
    function getsubtopicsAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $msubTopicdet = MasterSubTopics::find("subid = '".$postval['subid']."' AND ssubid='".$postval['ssubid']."' AND tid='".$postval['tid']."' AND status=1");
            echo "<option value=''>[ Select Sub Topics ]</option>";
            foreach($msubTopicdet as $msubTopicres){
                echo "<option value='".$msubTopicres->stid."'>".$msubTopicres->stname."</option>";
            }
        }
    }


// Ajax............ 
// 
    function getstcheckboxAction()
    {
        if($this->request->isPost()){
            $getVal = $this->request->getPost();
            $getsubtopics = MasterSubTopics::find(array("subid='".$getVal['subjectid']."' AND ssubid='".$getVal['ssubid']."' AND tid='".$getVal['topics']."' "));
            foreach($getsubtopics as $value){ ?>
                <div class="checkbox-custom checkbox-default">
                      <input type="checkbox" id="st<?=$value->stid?>" value="<?=$value->stid?>" name="subtopics[]">
                      <label for="st<?=$value->stid?>"><?=$value->stname?></label>
                </div>
            <?php
            }
        }
        exit();
    }

    function getcourseAction()
    {
        if($this->request->isPost()){
            $getVal = $this->request->getPost();
        }
        $course = MasterCourse::find("scid='".$getVal['sup_cat']."' AND status=1");
        $str='';
        foreach($course as $courseRes){
           $str.='<option value="'.$courseRes->id.'" >'.$courseRes->name.'</option>';
        }
        echo $str;
        exit();
    }
    function getnewcourseAction()
    {
        if($this->request->isPost()){
            $getVal = $this->request->getPost();
        }
        $course = MasterCourse::find("scid='".$getVal['sup_cat']."' AND status=1");
        $str='';
        foreach($course as $courseRes){
           $str.='<div class="checkbox-inline">
                <label for="curs'.$courseRes->id.'" ><input type="checkbox" id="curs'.$courseRes->id.'" value="'.$courseRes->id.'" name="course[]">'.$courseRes->name.'</label></div>';
        }
        echo $str;
        exit();
    }

    function deleteassessAction($id){
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterAssessment WHERE id = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $delallprevrecord = "DELETE FROM AssessmentQuestionList WHERE assessment_id = '".$id."'";
            $this->modelsManager->executeQuery($delallprevrecord);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Deleted Successfully</div>");
            return $response->redirect("exams/viewassess");
        }
    }

    function practiceAction($topic=NULL) {
        //$data = MasterPractice::find();
        $practiceCourse = $this->modelsManager->executeQuery("SELECT p.id as p_id, c.name as c_name, p.status as p_status FROM MasterPractice p, MasterCourse c WHERE c.id=p.course");
        $this->view->setVar("exam_type", $practiceCourse);
        $supcat = MasterSupCat::find("status=1");
        foreach ($supcat as $va) {
            $act[$va->scid] = $va->scname;
        }
        $this->view->setVar("supcat", $act);
        $course = MasterCourse::find("status=1");
        foreach ($course as $vaa) {
            $course1[$vaa->id] = $vaa->name;
        }
        if($topic!=''){
            $topics = $this->modelsManager->executeQuery("SELECT * FROM MasterTopics WHERE tid IN(".$topic.")");
            foreach($topics as $newtopic){
                $toics[$newtopic->tid]=$newtopic->tname;
            }
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".implode(",",$toics)." has not minimum number of question</div>");
        }
        $this->view->setVar("course", $course1);
    }

    function addpracticeAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postvalnew=$this->request->getPost();
            parse_str($postvalnew['formRecord'],$postval);
            $postval['q_type'] = json_encode($postval['q_type']);
            $Addques= new MasterPractice();
            if($Addques->save($postval)){
                $topicres=UIElementsAdmin::practiseList($Addques->id,$postval,$postval['minimum_question']);
                /* $postval Contain all the practice series conditions for the question listing */
                echo $topicres;exit;
            }else{
              echo 2;exit;  
            }
            //
            //return $response->redirect("exams/practice");
        }
        //$courseName = MasterCourse::find("status=1");
        $practiceCourse = $this->modelsManager->executeQuery("SELECT * FROM MasterCourse WHERE id NOT IN(select MasterPractice.course from MasterPractice) AND status=1 ORDER BY id DESC");
        $this->view->setVar("courseName", $practiceCourse);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("qtype", $qtype);
    }

    function updatepracticeAction($id='')
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $exam_type = MasterPractice::findFirst(array("id='".$id."'"));
            $this->view->setVar("exam_type", $exam_type);
            $course = MasterCourse::findFirst("id='".$exam_type->course."'");
            $this->view->setVar("course", $course);
            $qtype = MasterQuesType::find("status=1");
            $this->view->setVar("qtype", $qtype);
        }
        if($this->request->isPost()){
            $postvalnew=$this->request->getPost();
            parse_str($postvalnew['formRecord'],$postval);
            $postval['q_type'] = json_encode($postval['q_type']);
            $update= new MasterPractice();
            if($update->save($postval)){
                $delallprevrecord = "DELETE FROM PracticeQuestionList WHERE practice_series = '".$postval['id']."'";
               if($this->modelsManager->executeQuery($delallprevrecord)){
                    $resout=UIElementsAdmin::practiseList($update->id,$postval,$postval['minimum_question']);
                    /* $postval Contain all the practice series conditions for the question listing */
                   echo $resout;exit;
                }else{
                     $resout=UIElementsAdmin::practiseList($update->id,$postval,$postval['minimum_question']);
                     echo $resout;exit;
                }
            }else{
                echo 2;exit;
            }
            //return $response->redirect("exams/practice");
        }
    }
    /**
     * Delete Perticular exam type
     * @param  [int] $id [Exam type id]
     * @return [return]     [Return delete status]
     * @author Rajesh Sardar
     */
    function deletePracticeAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterPractice WHERE id = '".$id."'";
            if($this->modelsManager->executeQuery($phql)){
                $delallprevrecord = "DELETE FROM PracticeQuestionList WHERE practice_series = '".$id."'";
                $this->modelsManager->executeQuery($delallprevrecord);
            }
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Practice Series Deleted Successfully</div>");
            return $response->redirect("exams/practice");
        }
    }
    public function updateteststatusAction($status='',$id=''){
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($status==2){
                $phql = "UPDATE MasterTests SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterTests SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Status Updated Successfully</div>");
            return $response->redirect("tests");
        }
    }
    public function practicedetailsAction($practice_id = ""){
        $response = new \Phalcon\Http\Response();
        if($practice_id!="")
        {
            $qus_details = PracticeQuestionList::find("id='".$practice_id."'");
            $this->view->setVar("qus_details", $qus_details);
            //=================== list of question to practice ===============//
            $prac_details = MasterPractice::findFirst("id='".$practice_id."'");
            $this->view->setVar("prac_details", $prac_details);
            //=================== Details of practice ===============//
            $get_sub = MasterCourse::findFirst("id='".$prac_details->course."'");
            $this->view->setVar("get_sub", $get_sub);
            //=================== Details of course ===============//
            $subList = implode(',',json_decode($get_sub->subject, true));
            $sub_name = MasterSubject::find("id IN (".$subList.")");
            $this->view->setVar("sub_name", $sub_name);
            //=================== list of Subject =================//
            //=================== List of SubSubject ===============//
            $subSubjeList = implode(',',json_decode($get_sub->subsubject, true));
            $subSubject = MasterSubSubject::find("ssid IN (".$subSubjeList.")");
            $this->view->setVar("subSubject", $subSubject);
            //=================== list of Subject ===============//
        }
    }
}
?>