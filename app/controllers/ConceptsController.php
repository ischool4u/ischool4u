<?php

class ConceptsController extends ControllerBase
{

	public function initialize() 
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
     * concept View Part Only retrive data from database and show the data.
     * @return [array] [Fetched Data]
     */
    function indexAction()
    {
        if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if(isset($getVal['submit'])){
                $where = '[';
                if($getVal['subject']==''){
                    unset($getVal['subject']);
                }else{
                    $where .= '"'.$getVal['subject'].'"';
                    $subsubdet = MasterSubSubject::find("subid = '".$getVal['subject']."' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if($getVal['subsubject']==''){
                    unset($getVal['subsubject']);
                }else{
                    $where .= ',"'.$getVal['subsubject'].'"';
                    $mTopicdet = MasterTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if($getVal['topics']==''){
                    unset($getVal['topics']);
                }else{
                    $where .= ',"'.$getVal['topics'].'"';
                    $msTopicdet = MasterSubTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND tid='".$getVal['topics']."' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if($getVal['subtopics']==''){
                    unset($getVal['subtopics']);
                }else{
                    $where .= ',"'.$getVal['subtopics'].'"]';
                };
                if($getVal['contype']==''){
                    unset($getVal['contype']);
                    $conce = MasterConcept::find(array("cat LIKE '".$where."%'"));
                }else{
                    $con = $getVal['contype'];
                    $conce = MasterConcept::find(array("cat LIKE '".$where."%' AND contype='".$con."'"));
                };
                $this->view->setVar("conce", $conce);
            }else{
                $conce = MasterConcept::find(array());
                $this->view->setVar("conce", $conce);
            }
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $contype = MasterConceptType::find(array());
        $this->view->setVar("contype", $contype);
    }
    /**
     * addconcept Add new  Part Only Store data from  and show the data.
     * @return [array] [Fetched Data]
     */
    function addconceptAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $con= new MasterConcept();
            $con->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Successfully Added</div>");
            return $response->redirect("concepts");
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $contype = MasterConceptType::find(array());
        $this->view->setVar("contype", $contype);
    }
    function updconceptAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            
            $concept = MasterConcept::findFirst("cid='".$id."'");
            $this->view->setVar("concept", $concept);
            $contype = MasterConceptType::find(array(""));
            $this->view->setVar("contype", $contype); 
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();                      
            $update= new MasterConcept();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Source Successfully Update</div>");
            return $response->redirect("concepts");
        }
    }

    function concepttypeAction()
    {
        $contype = MasterConceptType::find(array());
        $this->view->setVar("contype", $contype); 
    }
    function addcontypeAction()
    {
      $response = new \Phalcon\Http\Response();
      if($this->request->isPost()){ 
        $postval = $this->request->getPost();
        $con= new MasterConceptType();
        $con->save($postval);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Successfully Added</div>");
        return $response->redirect("concepts/concepttype");
    }
}
function updcontypeAction()
{
    $response = new \Phalcon\Http\Response();
    if($this->request->isPost()){
        $postval = $this->request->getPost();
        $update= new MasterConceptType();
        $update->save($postval);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Successfully Update</div>");
        return $response->redirect("concepts/concepttype");
    }
}
function deletecontypAction($id)
{
    $response = new \Phalcon\Http\Response();
    if($id!="")
    {
        $phql = "DELETE FROM MasterConceptType WHERE id = '".$id."'";
        $this->modelsManager->executeQuery($phql);
        $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Deleted Successfully</div>");
        return $response->redirect("concepts/concepttype");
    }
}
function updcontystaAction($u_status,$id)
{
  $response = new \Phalcon\Http\Response();
  if($id!="")
  {
    if($u_status==2){
        $phql = "UPDATE MasterConceptType SET status = 0 where id=".$id."";
    }
    else
    {
        $phql = "UPDATE MasterConceptType SET status = 1 where id=".$id."";
    }
    $status = $this->modelsManager->executeQuery($phql);
    $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Status Updated Successfully</div>");
    return $response->redirect("concepts/concepttype");
}
}
    /**
     * Function created for slug value to check in database and send it status to add news
     * Created Date: 18-06-2015
     */

    static public function slugifyAction($text)
    { 
        if ($text=="") {
            echo $text="";exit();
        }
        else
        {
          $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
          // trim
          $text = trim($text, '-');
          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
          // lowercase
          $text = strtolower($text);
          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);
          if (empty($text))
          {
            return 'n-a';
        }
        $next = MasterConcept::find(array("slug = :slug:",'bind' => array('slug' => $text)));
        if (count($next)==1) {
            $msg="&nbsp;&nbsp;<span class='has-error'><label class='control-label' for='inputError'><i class='fa fa-times-circle-o'></i>&nbsp;Invalid Slug Name</label></span>";
            $status=0;
        }
        else
        {
            $msg="&nbsp;&nbsp;<span class='has-success'><label class='control-label' for='inputSuccess'><i class='fa fa-check'></i> Slug Name is Valid</label></span>";
            $status=1;
        }
        $slug_status=array("value"=>$text,"msg"=>$msg, "status"=>$status);
        echo json_encode($slug_status); exit();
    }
}




}