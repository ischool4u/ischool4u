<?php

class NumericquestionController extends ControllerBase {

    public function initialize() {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    function indexAction() {
        $getval=$this->request->get();
        $data=array();
        if($getval['subject']!='' && $getval['subsubject']!='' && $getval['topic']!=''){
        $data = UIElementsAdmin::getpagination("MasterNumericQuestion", 'question',$getval);
        }
        //$data = UIElementsAdmin::getpagination("MasterMatchQuestion", 'question');
        $this->view->setVar("mquestions", $data);
         $access = $this->session->get('admin');
        if($access['is_verifier']==1){
            $subrole=VerifierRole::find(array("id_user"=>$access['id'],"group" => array("id_subject")));
            $idsib="";
            foreach($subrole as $rs){
                $idsib=$idsib.",".$rs->id_subject;
            }
            $allid=trim($idsib,",");
            $getSub = MasterSubject::find(array("id IN (". $allid.")"));
        }else{
            $getSub = MasterSubject::find(array());
        }
        $this->view->setVar("subdet", $getSub);
        //$data = UIElementsAdmin::getpagination("MasterNumericQuestion", 'question');
        // $data = MasterNumericQuestion::find(array());
        $this->view->setVar("questions", $data);
        $qtype = MasterQuesType::find(array());
        $this->view->setVar("qtype", $qtype);
    }

    function addnuquestionAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['created'] = date("Y-m-d h:i:s");
            $questionid = MasterQuestion::find()->getLast();
            $questionid = str_split($questionid->questionid, 3);
            $questionid = $questionid['2'] + 1;
            $questionid = 'QID000' . $questionid;
            $qid = MasterNumericQuetion::find()->getLast();
            $qid = $qid->qid + 1;
            $postval['qusid'] = $qid;
            $postval['questionid'] = $questionid;
            $postval['tableid'] = 3;
            $update = new MasterNumericQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Added</div>");
            return $response->redirect("numericquestion");
        }
        $questype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $questype);
        $quessource = MasterQuestionSource::find(array("status=1"));
        $this->view->setVar("qsources", $quessource);
        $access = $this->session->get('admin');
            if($access['is_verifier']==1){
                $subrole=VerifierRole::find(array("id_user"=>$access['id'],"group" => array("id_subject")));
                $idsib="";
                foreach($subrole as $rs){
                    $idsib=$idsib.",".$rs->id_subject;
                }
                $allid=trim($idsib,",");
                $getSub = MasterSubject::find(array("id IN (". $allid.")"));
            }else{
                $getSub = MasterSubject::find(array());
            }
            $this->view->setVar("subdet", $getSub);
    }

    /**
     * Update Reason questions
     * @param  [int] $id [Question id]
     * @return [massage]     [Success Massage]
     * @author Rajesh
     */
    function updatenuquestionAction($id,$noteid='',$type='') {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['modified'] = date("Y-m-d h:i:s");
            $user_session = $this->session->get('admin');
            $postval['modified_by'] = $user_session['id'];
            $postval['qusid'] = $postval['qid'];
            $postval['tableid'] = 3;
            $qid = MasterQuestion::findFirst(array("questionid='" . $postval['questionid'] . "'"));
            $postval['id'] = $qid->id;
            $update = new MasterNumericQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            if(!empty($postval['drtype']) && !empty($postval['noteid']) && !empty($postval['replycmt'])){
                 UIElementsAdmin::callfromupdate($postval['noteid'],$postval['replycmt'],$postval['drtype'],$postval['questionid'],$user_session['id']);
             }
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Update</div>");
            if (empty($this->session->get('redirectdta'))) {
                return $response->redirect("numericquestion");
            } else {
                return $response->redirect($this->session->get('redirectdta'));
            }
        }
        if ($id != "") {
            $questype = MasterQuesType::find(array("status=1"));
            $this->view->setVar("qtype", $questype);
            $quessource = MasterQuestionSource::find(array("status=1"));
            $this->view->setVar("qsources", $quessource);
            $getSub = MasterSubject::find(array());
            $this->view->setVar("subdet", $getSub);
            $question = MasterNumericQuestion::findFirst(array("qid='" . $id . "'"));
            $this->view->setVar("question", $question);
            $mquestion = MasterQuestion::findFirst(array("questionid='" . $question->questionid . "'"));
            $this->view->setVar("mquestion", $mquestion);
            $this->view->setVar("noteid",$noteid);
            $this->view->setVar("drtype",$type);
        } else {
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something Wrong. Please Try again.</div>");
            return $response->redirect("numericquestion");
        }
    }

    /**
     * This funtion is for delete Reason question
     * @param  [int] $id [Question id]
     * @return [return Status]     [return Status]
     * @author Rajesh
     */
    function deletenuquestionAction($id) {
        $response = new \Phalcon\Http\Response();
        if ($id != "") {
            $phql = "DELETE FROM MasterNumericQuestion WHERE qid = '" . $id . "'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Reason Question Deleted Successfully</div>");
            return $response->redirect("numericquestion");
        }
    }

}

?>