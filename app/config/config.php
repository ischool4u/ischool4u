<?php

$config = new Phalcon\Config(array(
	'database' => array(
		'adapter' => 'mysql',
		'host' => 'localhost',
		'username' => 'root',
		'password' => '',
		'dbname' => 'ischool4u_database'
		),
	'phalcon' => array(
		'controllersDir' => '/../app/controllers/',
		'modelsDir' => '/../app/models/',
		'libraryDir' => '/../app/library/',
		'functionDir' => '/../app/functions/',
		'viewsDir' => '/../app/views/',
		'baseUri' => '/ischool4u/'
		),
	));
// Code added by sekhar / dhiraj..................