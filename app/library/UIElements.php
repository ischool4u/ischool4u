<?php

/**
 * UIElements
 *
 * Helps to build UI elements for the application
 */
use Phalcon\Tag as Tag;
abstract class UIElements
{
	
    // header code for front end view
    public function getHeader($pageaction='',$meta_tag='')
    {
		
		?>
            
            <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
             <?php
            echo Tag::getTitle();
            echo Tag::stylesheetLink('css/bootstrap.css');
            echo Tag::stylesheetLink('css/asish.css');
            echo Tag::stylesheetLink('css/elastislide.css');
			echo Tag::stylesheetLink('css/elastislide1.css');
			echo Tag::stylesheetLink('css/elastislide2.css');
			echo Tag::stylesheetLink('css/elastislide-bottom.css');
            ?>
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <![endif]-->
        <?php
		switch ($pageaction){
			
            case 'postad' :
            ?>
             <script type="text/javascript">
                function getsubcategory(id)
                {
                    if(id!='')
                    {
                        $('#p_cid').val(id);
                        $('#col_1').val("");
                        $('#col_2').val("");
                        $('#col_3').val("");
                        $('#col_4').val("");
                        $('.cat_id').removeClass("select_ctrg");
                        $('#cat_id'+id).addClass("select_ctrg");
                        $(".col_1,.col_2,.col_3,.col_4").html("");
                        $('.cat_submit').prop('disabled', true);
                        var cat_loc="col_0";
                        $.ajax(
                                {
                                    type: 'POST',
                                    url: '<?php echo BASEURL;?>category/child',
                                    data: 'category_id='+id+'&level='+cat_loc,
                                    success: function(data) {
                                        if(data)
                                        {
                                            if (data==1) {
                                                $('.cat_submit').prop('disabled', false);
                                            }
                                            else
                                            {
                                                $(".col_1").html(data);     
                                            }
                                            
                                        }
                                    }
                                });
                    }
                }
                function subChild(id)
                {
                    if(id!='') 
                    {
                        
                        var x = document.getElementById(""+id).parentNode.className;
                        var getClassName = x.split(' ');
                        var cat_loc=getClassName[getClassName.length-1];//alert(cat_loc);

                        $('.cat_submit').prop('disabled', true);    
                        $('#'+cat_loc).val(id);
                            $.ajax(
                                {
                                    type: 'POST',
                                    url: '<?php echo BASEURL;?>category/child',
                                    data: 'category_id='+id+'&level='+cat_loc,
                                    success: function(data) {
                                        if(data)
                                        {
                                            if (cat_loc=="col_1") {
                                                $(".col_2").html("");
                                                $(".col_3").html("");
                                                $(".col_4").html("");
                                                $(".col_2").html(data);
                                                $('#col_2').val("");
                                                $('#col_3').val("");
                                                $('#col_4').val("");
                                                $('.child').removeClass("select_ctr_1");
                                                $('#'+id).addClass("select_ctr_1");
                                            }
                                            if (cat_loc=="col_2") {
                                                $(".col_3").html("");
                                                $(".col_4").html("");
                                                $(".col_3").html(data);
                                                $('#col_3').val("");
                                                $('#col_4').val("");
                                                $('.child').removeClass("select_ctr_2");
                                                $('#'+id).addClass("select_ctr_2");
                                            }
                                            if (cat_loc=="col_3") {
                                                $(".col_4").html("");
                                                $(".col_4").html(data);
                                                $('#col_4').val("");
                                                $('.child').removeClass("select_ctr_3");
                                                $('#'+id).addClass("select_ctr_3");
                                            }
                                             
                                        }
                                    }
                                });
                      
                        
                    }
                    

                }
                function selectItem(item)
                {
                    if(item!='') 
                    {

                        var x = document.getElementById(""+item).parentNode.className;
                        var getClassName = x.split(' ');
                        var cat_loc=getClassName[getClassName.length-1];//alert(cat_loc);
                        $('#cid').val(item);
                        
                        $('#'+cat_loc).val(item);
                        if (cat_loc=="col_1") {
                            $(".col_2").html("");
                            $(".col_3").html("");
                            $(".col_4").html("");
                            $('#col_2').val("");
                            $('#col_3').val("");
                            $('#col_4').val("");
                            $('.child').removeClass("select_ctr_1");
                        };
                        if (cat_loc=="col_2") {
                            $(".col_3").html("");
                            $(".col_4").html("");
                            $('#col_3').val("");
                            $('#col_4').val("");
                            $('.child').removeClass("select_ctr_2");
                        };
                        if (cat_loc=="col_3") {
                            $(".col_4").html("");
                            $('#col_4').val("");
                            $('.child').removeClass("select_ctr_3");
                        };


                        $(".child").removeClass('is_select');
                        $("."+item).addClass('is_select');
                        $('.cat_submit').prop('disabled', false);
                    }
                }
                function saveCat()
                {
                    var p_cid_val = document.getElementsByName("p_cid")[0].value;
                    var cat_1_val=document.getElementsByName("col_1")[0].value;
                    var cat_2_val=document.getElementsByName("col_2")[0].value;
                    var cat_3_val=document.getElementsByName("col_3")[0].value;
                    var cat_4_val=document.getElementsByName("col_4")[0].value;
                    var cid=document.getElementsByName("cid")[0].value;
                    $.ajax(
                    {
                        type: 'POST',
                        url: '<?php echo BASEURL;?>postad/adcat',
                        data: 'p_cid='+p_cid_val+'&cat_1='+cat_1_val+'&cat_2='+cat_2_val+'&cat_3='+cat_3_val+'&cat_4='+cat_4_val+'&cid='+cid,
                        success: function(data) {
                            if(data!="error")
                            {
                                window.location.replace('advert');
                                //$(".col_4").html(data);
                            }
                            else
                            {
                                var msgs="<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Please Try Again :(</div>";
                                $(".error_msg").html(msgs);
                            }
                        }
                    });
                    
                }
               
            </script>
            <?php
            break;
            
			default :
			
			break;
			
		}
        ?>
        </head>	
    <body>
        <!--  Header start  -->
    <header>
        
    </header>
    <!--  Header end  -->
        <?php
    }
    
    
}
