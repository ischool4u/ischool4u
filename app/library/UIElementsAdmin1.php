<?php
/**
 * UIElementsAdmin
 *
 * Helps to build UI elements for the application
 */
use Phalcon\Tag as Tag;

abstract class UIElementsAdmin {

    // header code for Admin end view

    static function getHeadnav($users) {
        ?>
        <!-- start: header -->
        <header class="header" >
            <div class="logo-container">
                <a href="<?= BASEURL ?>admin" class="logo">
                    <img src="<?= BASEURL ?>frontend/img/logo.png" height="35" alt="Ischool4u" />
                </a>

                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label=""></i>
                </div>
            </div>

            <!-- start: search & user box -->
            <div class="header-right">
                <span class="separator"></span>
                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="<?= BASEURL ?>assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                        </figure>
                        <div class="profile-info" >
                            <span class="name"><?= $users['name'] ?></span>
                            <span class="role"><?= strtolower($users['role_name']) ?></span>
                        </div>

                        <i class="fa custom-caret"></i>
                    </a>

                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> My Profile</a>
                            </li>

                            <li>
                                <a role="menuitem" tabindex="-1" href="<?= BASEURL ?>admin/end"><i class="fa fa-power-off"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->
        <?php
    }

    static function getSidebar($access='') {
        $allaa=$access;
        $access = json_decode($access['access_role']);
        ?>
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">

            <div class="sidebar-header">
                <div class="sidebar-title">
        	Navigation
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content" style="right:-13px !important;">
                    <nav id="menu" class="nav-main" role="navigation">
                        <ul class="nav nav-main">
                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 0): ?>
                                    <li class="nav-active">
                                        <a href="<?= BASEURL ?>admin">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <span>Dashboard</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 1): ?>
                                    <li class="">
                                        <a href="<?= BASEURL ?>sitesetting">
                                            <i class="fa fa-cogs" aria-hidden="true"></i>
                                            <span>Site Setting</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 1): ?>
                                    <li class="nav-parent"> <!-- Admin user Menu  -->
                                        <a>
                                            <i class="fa fa-university" aria-hidden="true"></i>
                                            <span>Admin Users</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>adminusers">
                			 View Admin
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>adminrole">
                			 View Admin Role
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 2): ?>
                                    <li class="nav-parent"> <!-- Users Menu  -->
                                        <a>
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                            <span> Users</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>users">
                			 View Users
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>users/wallet">
                			User Wallet
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 3): ?>
                                    <li class="nav-parent"> <!-- Course detail Menu  -->
                                        <a>
                                            <i class="fa fa-book" aria-hidden="true"></i>
                                            <span> Course Detail</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>course/supcategory">
                			 Parent Course
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>course/coursecategory">
                			 Courses
                                                </a>
                                            </li>
                                             <li>
                                                <a href="<?= BASEURL ?>course">
                			 Assign Subject To Course
                                                </a>
                                            </li> 
                                            
                                        </ul>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                                    <li class="nav-parent"> <!-- Course detail Menu  -->
                                        <a>
                                            <i class="fa fa-sliders" aria-hidden="true"></i>
                                            <span> Packages</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>course/packages">
                             View Packages
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>fixedtest/fixpackage">
                    View Fixed Packages
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>course/practicepackage">
                    View Practice Packages
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 4): ?>
                                    <li class="nav-parent"> <!-- Admin Subject module  Menu  -->
                                        <a>
                                            <i class="fa fa-cubes" aria-hidden="true"></i>
                                            <span> Subject </span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>subject">
                			 View Subject
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>subject/subsubject">
                			 View Sub Subject
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>subject/topics">
                			 View Topics
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>subject/subtopics">
                			 View Sub Topics
                                                </a>
                                            </li>
                                        </ul>
                                    </li> <!-- Admin end subject module   Menu  -->
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <!-- Concept module  Menu  -->
        	
            <!-- ADMIN concept MODULE MENU-->
                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 5): ?>
                                    <li class="nav-parent"> 
                                        <a><!-- Question module  Menu  -->
                                            <i class="fa  fa-question " aria-hidden="true"></i>
                                            <span>Question </span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>questionbank">
                			 Single/Multiple Question
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>questionbank/viewmmquestion">
                			 Match Question
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>questionbank/raquestion">
                			 Reasoning Question 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>numericquestion">
                			 Numeric Question 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>paraquestion">
                			 Paragraph Question
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>subtheory">
                			 Subjective and Theory Question
                                                </a>
                                            </li>
                                            <?php if($allaa['is_verifier']!=1){?>
                                            <li>
                                                <a href="<?= BASEURL ?>exams/examassign">
                			Question List
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>questions/view_question_source">
                			 Question Source
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>questions/view_question_type">
                			Question Types
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>										
                                    </li><!--ADMIN QUESTION MODULE MENU-->
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 6): ?>
                                    <li class="nav-parent"> <!-- Course detail Menu  -->
                                        <a>
                                            <i class="fa fa-book" aria-hidden="true"></i>
                                            <span>Exams</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>exams">
                			Exams Type
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>exams/practice">
                            Practice Tests
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>exams/viewassess">
                			Assessment Tests
                                                </a>
                                            </li>
<!--                                            <li>
                                                <a href="<?= BASEURL ?>tests" title="Subject wise, Topic wise or Unit wise">
                			Add and Manage Tests
                                                </a>
                                            </li>-->
                                            <li>
                                                <a href="<?= BASEURL ?>fixedtest" title="Fixed Test">
                			Fixed test
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>freetest" title="Free Test">
                			Previous Paper Test
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 7): ?>
                                    <li class="nav-parent"> 
                                        <a><!-- Concept module  Menu  -->
                                            <i class="fa fa-language" aria-hidden="true"></i>
                                            <span>Tips </span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>tips">
                			 Add & Manage Tips
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>tips/tipstype">
                			 Add & Manage Tips Type
                                                </a>
                                            </li>
                                        </ul>										
                                    </li><!--ADMIN concept MODULE MENU-->
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 8): ?>
                                    <!--<li class="nav-parent">
                                        <a>
                                            <i class="fa fa-file-text" aria-hidden="true"></i>
                                            <span>Pages</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>pages">
                			<!-- View & Manages Pages
                                                </a>
                                            </li>
                                        </ul>
                                    </li>-->
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 9): ?>
                                   <!-- <li class="nav-parent"> 
                                        <a>
                                            <i class="fa fa-sitemap" aria-hidden="true"></i>
                                            <span>Navigation</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>navigation">
                			<!--Menu type
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= BASEURL ?>navigation/assignmenu">
                			<!--Assign Menu
                                                </a>
                                            </li>
                                        </ul>
                                    </li>-->
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 10): ?>
                                    <li class="">
                                        <a href="<?= BASEURL ?>article">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span>Article</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 11): ?>
                                    <!---<li class="">
                                        <a href="<?= BASEURL ?>socialmedia">
                                    <!--        <i class="fa fa-share-alt " aria-hidden="true"></i>
                                            <span>Social Media</span>
                                        </a>
                                    </li>-->
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 12): ?>
                                    <li class="nav-parent"> <!-- Admin News -->
                                        <a><!-- News module  -->
                                            <i class="fa fa-external-link" aria-hidden="true"></i>
                                            <span>News </span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li>
                                                <a href="<?= BASEURL ?>news">
                			 View & Manages News
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 13): ?>
                                    <li class="">
                                        <a href="<?= BASEURL ?>sitesetting/questionoftheday">
                                            <i class="fa fa-cogs" aria-hidden="true"></i>
                                            <span>Question Of The Day</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php foreach ($access as $val): ?>
                                <?php if ($val == 14): ?>
                                    <li class="">
                                        <a href="<?= BASEURL ?>sitesetting/quizlist">
                                            <i class="fa fa-cogs" aria-hidden="true"></i>
                                            <span>Quiz Setting</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php foreach($access as $val): ?>
    <?php if($val==15): ?>
    <li class="">
        <a href="<?= BASEURL ?>blog/bloglist">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span>Blog</span>
        </a>
    </li>
    <?php endif; ?>
    <?php endforeach; ?>
                        </ul>
                    </nav>
                </div>

            </div>

        </aside>
        <!-- end: sidebar -->
        <?php
    }

    //  Static kljaskfdj
    static function getfrontSidebar($fixpack) {
        ?>
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">
            <div class="sidebar-header">
                <div class="sidebar-title">
        	Navigation
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <nav id="menu" class="nav-main" role="navigation">
                        <ul class="nav nav-main">
                            <li class="nav-active">
                                <a href="<?= BASEURL ?>index/dashboard">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-parent"> <!-- Admin Panel-CMS Menu  -->
                                <a><!-- Page module  Menu  -->
                                    <i class="fa fa-sitemap" aria-hidden="true"></i>
                                    <span>Subject</span>
                                </a>
                                <ul class="nav nav-children">
                                    <li>
                                        <a href="<?= BASEURL ?>user/subsubjects/chemistry">
        								Chamistry
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= BASEURL ?>user/subsubjects/physics">
        								Physics
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= BASEURL ?>user/subsubjects/math">
        								Math
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= BASEURL ?>user/subsubjects/biology">
        								Biology
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </nav>
                </div>

            </div>

        </aside>
        <!-- end: sidebar -->
        <?php
    }

    //  lkajhsjakdsf
    static function getBreadcrumds($dash=null) {
        ?>

        <header class="page-header front-header">
            <div class="user-bread">
                <div class="pull-left">    
                  <ol class="breadcrumbs">
                    <li>
                        <a href="<?= BASEURL ?>admin">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li><span>Dashboard</span></li>
                </ol>
            </div>
            </div>
        </header>

        <?php
    }

    static function getContent($val=null) {
        ?>
        <!-- start: page -->
        <div class="row">
            <div class="col-md-6 col-lg-12 col-xl-6">
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="chart-data-selector" id="salesSelectorWrapper">
                                    <h2>
        			Sales:
                                        <strong>
                                            <select class="form-control" id="salesSelector">
        <?php if (isset($val) && $val != '') {
            echo 'kljafsd';
        } else {
            echo 'kljdsoi';
        } ?>
                                                <option value="Porto Admin" selected>Porto Admin</option>
                                                <option value="Porto Drupal" >Porto Drupal</option>
                                                <option value="Porto Wordpress" >Porto Wordpress</option>
                                            </select>
                                        </strong>
                                    </h2>

                                    <div id="salesSelectorItems" class="chart-data-selector-items mt-sm">
                                        <!-- Flot: Sales Porto Admin -->
                                        <div class="chart chart-sm" data-sales-rel="Porto Admin" id="flotDashSales1" class="chart-active"></div>
                                        <script>

                                            var flotDashSales1Data = [{
                                                    data: [
                                                        ["Jan", 140],
                                                        ["Feb", 240],
                                                        ["Mar", 190],
                                                        ["Apr", 140],
                                                        ["May", 180],
                                                        ["Jun", 320],
                                                        ["Jul", 270],
                                                        ["Aug", 180]
                                                    ],
                                                    color: "#0088cc"
                                                }];

                                            // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                        </script>

                                        <!-- Flot: Sales Porto Drupal -->
                                        <div class="chart chart-sm" data-sales-rel="Porto Drupal" id="flotDashSales2" class="chart-hidden"></div>
                                        <script>

                                            var flotDashSales2Data = [{
                                                    data: [
                                                        ["Jan", 240],
                                                        ["Feb", 240],
                                                        ["Mar", 290],
                                                        ["Apr", 540],
                                                        ["May", 480],
                                                        ["Jun", 220],
                                                        ["Jul", 170],
                                                        ["Aug", 190]
                                                    ],
                                                    color: "#2baab1"
                                                }];

                                            // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                        </script>

                                        <!-- Flot: Sales Porto Wordpress -->
                                        <div class="chart chart-sm" data-sales-rel="Porto Wordpress" id="flotDashSales3" class="chart-hidden"></div>
                                        <script>

                                            var flotDashSales3Data = [{
                                                    data: [
                                                        ["Jan", 840],
                                                        ["Feb", 740],
                                                        ["Mar", 690],
                                                        ["Apr", 940],
                                                        ["May", 1180],
                                                        ["Jun", 820],
                                                        ["Jul", 570],
                                                        ["Aug", 780]
                                                    ],
                                                    color: "#734ba9"
                                                }];

                                            // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                                        </script>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-4 text-center">
                                <h2 class="panel-title mt-md">Sales Goal</h2>
                                <div class="liquid-meter-wrapper liquid-meter-sm mt-lg">
                                    <div class="liquid-meter">
                                        <meter min="0" max="100" value="35" id="meterSales"></meter>
                                    </div>
                                    <div class="liquid-meter-selector" id="meterSalesSel">
                                        <a href="#" data-val="35" class="active">Monthly Goal</a>
                                        <a href="#" data-val="28">Annual Goal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="row">
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-primary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-primary">
                                            <i class="fa fa-life-ring"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">Support Questions</h4>
                                            <div class="info">
                                                <strong class="amount">1281</strong>
                                                <span class="text-primary">(14 unread)</span>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(view all)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-secondary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-secondary">
                                            <i class="fa fa-usd"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">Total Profit</h4>
                                            <div class="info">
                                                <strong class="amount">$ 14,890.30</strong>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(withdraw)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-tertiary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-tertiary">
                                            <i class="fa fa-shopping-cart"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">Today's Orders</h4>
                                            <div class="info">
                                                <strong class="amount">38</strong>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(statement)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <section class="panel panel-featured-left panel-featured-quartenary">
                            <div class="panel-body">
                                <div class="widget-summary">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-quartenary">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">
                                            <h4 class="title">Today's Visitors</h4>
                                            <div class="info">
                                                <strong class="amount">3765</strong>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(report)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>

                        <h2 class="panel-title">Best Seller</h2>
                        <p class="panel-subtitle">Customize the graphs as much as you want, there are so many options and features to display information using Porto Admin Template.</p>
                    </header>
                    <div class="panel-body">

                        <!-- Flot: Basic -->
                        <div class="chart chart-md" id="flotDashBasic"></div>
                        <script>

                            var flotDashBasicData = [{
                                    data: [
                                        [0, 170],
                                        [1, 169],
                                        [2, 173],
                                        [3, 188],
                                        [4, 147],
                                        [5, 113],
                                        [6, 128],
                                        [7, 169],
                                        [8, 173],
                                        [9, 128],
                                        [10, 128]
                                    ],
                                    label: "Series 1",
                                    color: "#0088cc"
                                }, {
                                    data: [
                                        [0, 115],
                                        [1, 124],
                                        [2, 114],
                                        [3, 121],
                                        [4, 115],
                                        [5, 83],
                                        [6, 102],
                                        [7, 148],
                                        [8, 147],
                                        [9, 103],
                                        [10, 113]
                                    ],
                                    label: "Series 2",
                                    color: "#2baab1"
                                }, {
                                    data: [
                                        [0, 70],
                                        [1, 69],
                                        [2, 73],
                                        [3, 88],
                                        [4, 47],
                                        [5, 13],
                                        [6, 28],
                                        [7, 69],
                                        [8, 73],
                                        [9, 28],
                                        [10, 28]
                                    ],
                                    label: "Series 3",
                                    color: "#734ba9"
                                }];

                            // See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

                        </script>

                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Server Usage</h2>
                        <p class="panel-subtitle">It's easy to create custom graphs on Porto Admin Template, there are several graph types that you can use, such as lines, bars, pie charts, etc...</p>
                    </header>
                    <div class="panel-body">

                        <!-- Flot: Curves -->
                        <div class="chart chart-md" id="flotDashRealTime"></div>

                    </div>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-3 col-lg-6">
                <section class="panel panel-transparent">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>

                        <h2 class="panel-title">My Profile</h2>
                    </header>
                    <div class="panel-body">
                        <section class="panel panel-group">
                            <header class="panel-heading bg-primary">

                                <div class="widget-profile-info">
                                    <div class="profile-picture">
                                        <img src="assets/images/!logged-user.jpg">
                                    </div>
                                    <div class="profile-info">
                                        <h4 class="name text-semibold">John Doe</h4>
                                        <h5 class="role">Administrator</h5>
                                        <div class="profile-footer">
                                            <a href="#">(edit profile)</a>
                                        </div>
                                    </div>
                                </div>

                            </header>
                            <div id="accordion">
                                <div class="panel panel-accordion panel-accordion-first">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
                                                <i class="fa fa-check"></i> Tasks
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1One" class="accordion-body collapse in">
                                        <div class="panel-body">
                                            <ul class="widget-todo-list">
                                                <li>
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" checked="" id="todoListItem1" class="todo-check">
                                                        <label class="todo-label" for="todoListItem1"><span>Lorem ipsum dolor sit amet</span></label>
                                                    </div>
                                                    <div class="todo-actions">
                                                        <a class="todo-remove" href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="todoListItem2" class="todo-check">
                                                        <label class="todo-label" for="todoListItem2"><span>Lorem ipsum dolor sit amet</span></label>
                                                    </div>
                                                    <div class="todo-actions">
                                                        <a class="todo-remove" href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="todoListItem3" class="todo-check">
                                                        <label class="todo-label" for="todoListItem3"><span>Lorem ipsum dolor sit amet</span></label>
                                                    </div>
                                                    <div class="todo-actions">
                                                        <a class="todo-remove" href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="todoListItem4" class="todo-check">
                                                        <label class="todo-label" for="todoListItem4"><span>Lorem ipsum dolor sit amet</span></label>
                                                    </div>
                                                    <div class="todo-actions">
                                                        <a class="todo-remove" href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="todoListItem5" class="todo-check">
                                                        <label class="todo-label" for="todoListItem5"><span>Lorem ipsum dolor sit amet</span></label>
                                                    </div>
                                                    <div class="todo-actions">
                                                        <a class="todo-remove" href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="todoListItem6" class="todo-check">
                                                        <label class="todo-label" for="todoListItem6"><span>Lorem ipsum dolor sit amet</span></label>
                                                    </div>
                                                    <div class="todo-actions">
                                                        <a class="todo-remove" href="#">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                            <hr class="solid mt-sm mb-lg">
                                            <form method="get" class="form-horizontal form-bordered">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="input-group mb-md">
                                                            <input type="text" class="form-control">
                                                            <div class="input-group-btn">
                                                                <button type="button" class="btn btn-primary" tabindex="-1">Add</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-accordion">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1Two">
                                                <i class="fa fa-comment"></i> Messages
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1Two" class="accordion-body collapse">
                                        <div class="panel-body">
                                            <ul class="simple-user-list mb-xlg">
                                                <li>
                                                    <figure class="image rounded">
                                                        <img src="assets/images/!sample-user.jpg" alt="Joseph Doe Junior" class="img-circle">
                                                    </figure>
                                                    <span class="title">Joseph Doe Junior</span>
                                                    <span class="message">Lorem ipsum dolor sit.</span>
                                                </li>
                                                <li>
                                                    <figure class="image rounded">
                                                        <img src="assets/images/!sample-user.jpg" alt="Joseph Junior" class="img-circle">
                                                    </figure>
                                                    <span class="title">Joseph Junior</span>
                                                    <span class="message">Lorem ipsum dolor sit.</span>
                                                </li>
                                                <li>
                                                    <figure class="image rounded">
                                                        <img src="assets/images/!sample-user.jpg" alt="Joe Junior" class="img-circle">
                                                    </figure>
                                                    <span class="title">Joe Junior</span>
                                                    <span class="message">Lorem ipsum dolor sit.</span>
                                                </li>
                                                <li>
                                                    <figure class="image rounded">
                                                        <img src="assets/images/!sample-user.jpg" alt="Joseph Doe Junior" class="img-circle">
                                                    </figure>
                                                    <span class="title">Joseph Doe Junior</span>
                                                    <span class="message">Lorem ipsum dolor sit.</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </section>
            </div>
            <div class="col-xl-3 col-lg-6">
                <section class="panel panel-transparent">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>

                        <h2 class="panel-title">My Stats</h2>
                    </header>
                    <div class="panel-body">
                        <section class="panel">
                            <div class="panel-body">
                                <div class="small-chart pull-right" id="sparklineBarDash"></div>
                                <script type="text/javascript">
                                    var sparklineBarDashData = [5, 6, 7, 2, 0, 4 , 2, 4, 2, 0, 4 , 2, 4, 2, 0, 4];
                                </script>
                                <div class="h4 text-bold mb-none">488</div>
                                <p class="text-xs text-muted mb-none">Average Profile Visits</p>
                            </div>
                        </section>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="circular-bar circular-bar-xs m-none mt-xs mr-md pull-right">
                                    <div class="circular-bar-chart" data-percent="45" data-plugin-options='{ "barColor": "#2baab1", "delay": 300, "size": 50, "lineWidth": 4 }'>
                                        <strong>Average</strong>
                                        <label><span class="percent">45</span>%</label>
                                    </div>
                                </div>
                                <div class="h4 text-bold mb-none">12</div>
                                <p class="text-xs text-muted mb-none">Working Projects</p>
                            </div>
                        </section>
                        <section class="panel">
                            <div class="panel-body">
                                <div class="small-chart pull-right" id="sparklineLineDash"></div>
                                <script type="text/javascript">
                                    var sparklineLineDashData = [15, 16, 17, 19, 10, 15, 13, 12, 12, 14, 16, 17];
                                </script>
                                <div class="h4 text-bold mb-none">89</div>
                                <p class="text-xs text-muted mb-none">Pending Tasks</p>
                            </div>
                        </section>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>

                        <h2 class="panel-title">
                            <span class="label label-primary label-sm text-normal va-middle mr-sm">198</span>
                            <span class="va-middle">Friends</span>
                        </h2>
                    </header>
                    <div class="panel-body">
                        <div class="content">
                            <ul class="simple-user-list">
                                <li>
                                    <figure class="image rounded">
                                        <img src="assets/images/!sample-user.jpg" alt="Joseph Doe Junior" class="img-circle">
                                    </figure>
                                    <span class="title">Joseph Doe Junior</span>
                                    <span class="message truncate">Lorem ipsum dolor sit.</span>
                                </li>
                                <li>
                                    <figure class="image rounded">
                                        <img src="assets/images/!sample-user.jpg" alt="Joseph Junior" class="img-circle">
                                    </figure>
                                    <span class="title">Joseph Junior</span>
                                    <span class="message truncate">Lorem ipsum dolor sit.</span>
                                </li>
                                <li>
                                    <figure class="image rounded">
                                        <img src="assets/images/!sample-user.jpg" alt="Joe Junior" class="img-circle">
                                    </figure>
                                    <span class="title">Joe Junior</span>
                                    <span class="message truncate">Lorem ipsum dolor sit.</span>
                                </li>
                            </ul>
                            <hr class="dotted short">
                            <div class="text-right">
                                <a class="text-uppercase text-muted" href="#">(View All)</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group input-search">
                            <input type="text" class="form-control" name="q" id="q" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xl-6 col-lg-12">
                <section class="panel">
                    <header class="panel-heading panel-heading-transparent">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>

                        <h2 class="panel-title">Company Activity</h2>
                    </header>
                    <div class="panel-body">
                        <div class="timeline timeline-simple mt-xlg mb-md">
                            <div class="tm-body">
                                <div class="tm-title">
                                    <h3 class="h5 text-uppercase">November 2013</h3>
                                </div>
                                <ol class="tm-items">
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-none">7 months ago.</p>
                                            <p>
        					It's awesome when we find a good solution for our projects, Porto Admin is <span class="text-primary">#awesome</span>
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-none">7 months ago.</p>
                                            <p>
        					Checkout! How cool is that!
                                            </p>
                                            <div class="thumbnail-gallery">
                                                <a class="img-thumbnail lightbox" href="assets/images/projects/project-4.jpg" data-plugin-options='{ "type":"image" }'>
                                                    <img class="img-responsive" width="215" src="assets/images/projects/project-4.jpg">
                                                    <span class="zoom">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <section class="panel panel-transparent">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>

                        <h2 class="panel-title">Global Stats</h2>
                    </header>
                    <div class="panel-body">
                        <div id="vectorMapWorld" style="height: 350px; width: 100%;"></div>
                    </div>
                </section>
            </div>
            <div class="col-lg-6 col-md-12">
                <section class="panel">
                    <header class="panel-heading panel-heading-transparent">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>

                        <h2 class="panel-title">Projects Stats</h2>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped mb-none">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project</th>
                                        <th>Status</th>
                                        <th>Progress</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Porto - Responsive HTML5 Template</td>
                                        <td><span class="label label-success">Success</span></td>
                                        <td>
                                            <div class="progress progress-sm progress-half-rounded m-none mt-xs light">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
        						100%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Porto - Responsive Drupal 7 Theme</td>
                                        <td><span class="label label-success">Success</span></td>
                                        <td>
                                            <div class="progress progress-sm progress-half-rounded m-none mt-xs light">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
        						100%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Tucson - Responsive HTML5 Template</td>
                                        <td><span class="label label-warning">Warning</span></td>
                                        <td>
                                            <div class="progress progress-sm progress-half-rounded m-none mt-xs light">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
        						60%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Tucson - Responsive Business WordPress Theme</td>
                                        <td><span class="label label-success">Success</span></td>
                                        <td>
                                            <div class="progress progress-sm progress-half-rounded m-none mt-xs light">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
        						90%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Porto - Responsive Admin HTML5 Template</td>
                                        <td><span class="label label-warning">Warning</span></td>
                                        <td>
                                            <div class="progress progress-sm progress-half-rounded m-none mt-xs light">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
        						45%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Porto - Responsive HTML5 Template</td>
                                        <td><span class="label label-danger">Danger</span></td>
                                        <td>
                                            <div class="progress progress-sm progress-half-rounded m-none mt-xs light">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
        						40%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Porto - Responsive Drupal 7 Theme</td>
                                        <td><span class="label label-success">Success</span></td>
                                        <td>
                                            <div class="progress progress-sm progress-half-rounded mt-xs light">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
        						95%
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- end: page -->
        <?php
    }

    static function getTest() {
        ?>
        <head>

            <!-- Basic -->
            <meta charset="UTF-8">

            <meta name="keywords" content="" />
            <meta name="description" content="Top label education portal Ischool4u">
            <meta name="author" content="">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <?php
            echo Tag::stylesheetLink('myadmin/css/bootstrap.css');
            echo Tag::stylesheetLink('myadmin/css/font-awesome.css');
            echo Tag::stylesheetLink('assets/stylesheets/jasny-bootstrap.css');
            echo Tag::stylesheetLink('assets/stylesheets/test-style.css');
            echo Tag::javascriptInclude('myadmin/js/jquery.js');
            echo Tag::javascriptInclude('myadmin/js/bootstrap.js');
            echo Tag::javascriptInclude('assets/javascripts/jasny-bootstrap.js');
            ?>
            <script type="text/javascript">
              
                function Decrement() {
                    if (document.getElementById) {
                        minutes = document.getElementById("minutes");
                        seconds = document.getElementById("seconds");
                        // if less than a minute remaining
                        if (seconds < 59) {
                            seconds.value = secs;
                        } else {
                            minutes.value = getminutes();
                            seconds.value = getseconds();
                        }
                        secs--;
                        t= setTimeout('Decrement()',1000);
                    }
                }
                function getminutes() {
                    // minutes is seconds divided by 60, rounded down
                    mins = Math.floor(secs / 60);
                    return mins;
                }
                function getseconds() {
                    // take mins remaining (as seconds) away from total seconds remaining
                    return secs-Math.round(mins *60);
                }
                function pause() { 
                    if( flagTimer=='resume')
                    {
                        clearTimeout(t);
                        t=0;
                        document.getElementById('Pause').value="Resume";
                        flagTimer='pause';
                    }
                    else
                    {
                        flagTimer='resume';
                        document.getElementById('Pause').value="Pause";
                        resume();
                    }
                
                }
                function resume() {
                    t= setTimeout('Decrement()',1000);
                }
                countdown();
            </script>

            <?php
        }
        public function getpagination($table='',$question='',$getval='')
        {
           $currentPage=$_GET['page'];
           $i = 0;
           $cond="b.subject={$getval['subject']} AND b.subsubject={$getval['subsubject']} AND b.topics={$getval['topic']}";
           if($getval['subtopic']!=''){
            $cond.=" AND b.subtopics={$getval['subtopic']}";
           }
           $phql   = "SELECT a.questionid,a.".$question.",a.status,a.qid,b.questionid,b.subject,b.subsubject,b.topics,b.subtopics FROM ".$table." a , MasterQuestion b WHERE a.questionid = b.questionid AND $cond";
           $datanew= $this->modelsManager->executeQuery($phql);
           //$datanew = $table::find(array());
           $data=array("");
           foreach ($datanew as $val) {
               $data[$i]['question'] = $val->$question;
               $data[$i]['questionid'] = $val->questionid;
               $data[$i]['status'] = $val->status;
               $data[$i]['qid'] = $val->qid;
               $i++;
           }
           $paginator = new \Phalcon\Paginator\Adapter\NativeArray(array(
               "data" => $data,
               "limit" => 10,
               "page" => $currentPage
               )
           );
           return $paginator->getPaginate();
        }
        public function callfromupdate($id='',$cmnt='',$type='',$questionid='',$userid=''){
        $response = new \Phalcon\Http\Response();
        if(!$this->session->has("admin"))
        {  
            return $response->redirect("admin/login");
        }
        $errornew = MasterDnote::findFirst("id='".$id."'");
        if($errornew){
            $postVal['user_id'] = $userid;
            $postVal['deid'] = $id;
            $postVal['type'] = $type;
            $postVal['solved'] = $cmnt;
            $postVal['questionid'] = $questionid;
            $error = new MasterSolved();
            if($error->save($postVal)){
                $phql = "UPDATE MasterDnote SET status = 0 where id=".$id."";
                $status = $this->modelsManager->executeQuery($phql);
                $data['uid'] = $userid;
                $data['doubt_id'] = $id;
                $data['friend_id'] = $errornew->user_id;
                $message=($type==1)?"ischool4u has replied to your error of question":"ischool4u has replied to your doubt of question";
                $data['message'] = htmlentities($message);
                $data['link'] = base64_encode($questionid);;
                $data['notification_type'] = ($type==1)?13:12;
                $data['add_date'] = date("Y-m-d H:i:s");
                $data['type'] = 2;
                $wall = new MasterNotification();
                $wall->save($data);
                if($status){
                    return 1;
                }
            }

        }
    }
        static function getHeader($pageaction = '', $meta_tag = '') {
            ?>
            <head>
            <!-- Basic -->
            <meta charset="UTF-8">

            <meta name="keywords" content="" />
            <meta name="description" content="Top label education portal Ischool4u">
            <meta name="author" content="">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <?php
            echo '<!-- Web font -->' . "\n";
            echo '<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">' . "\n";

            echo '<!-- Vendor CSS -->' . "\n";
            echo Tag::stylesheetLink('myadmin/css/bootstrap.css');
            echo Tag::stylesheetLink('myadmin/css/font-awesome.css');
            echo UIElementsAdmin::frontcss();
            // echo Tag::stylesheetLink('frontend/css/theme-elements.css');
            ?>
            <?php
            switch ($pageaction) {
                case 'adm_login':
                    echo Tag::getTitle();
                    ?>
                    <script type="text/javascript">
                        function mailTest()
                        {
                            var email=$("#username").val();
                            var blanktest=/\S/;
                            var filter=/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
                            if(!blanktest.test(email))
                            {

                                $("#username").css("border","1px solid #FF0014");
                                $("#username").focus();
                                $("#error").html("<b style='color:#EE0000'>Enter Email-ID</b>");
                                return false;
                            }
                            else if(!filter.test(email))
                            {
                                $("#username").css("border","1px solid #FF0014");
                                $("#username").focus();
                                $("#error").html("<b style='color:#EE0000'>Enter Correct Email-ID i.e : abc@xyz.com</b>");
                                return false;
                            }
                            else
                            {
                                $("#username").css("border","");
                                $("#error").html("");
                            }
                        }
                        function logValidation()
                        {
                            var pass=$("#pass").val();
                            var blanktest=/\S/;
                            // var filter=/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
                            mailTest();
                            if(!blanktest.test(pass))
                            {
                                $("#pass").css("border","1px solid #FF0014");
                                $("#pass").focus();
                                $("#error1").html("<b style='color:#EE0000'>Enter Password</b>");
                                return false;
                            }
                            else
                            {
                                $("#pass").css("border","");
                                $("#error1").html("");
                            }
                        }
                        function checkmail()
                        {

                            mailTest();
                        }
                    </script><?php
                    break;
                case 'tables':
                    echo Tag::stylesheetLink('assets/vendor/select2/select2.css');
                    echo Tag::stylesheetLink('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css');
                    echo Tag::stylesheetLink('assets/countdown/jquery.countdown.css');
                    break;
                case 'test':
                    echo Tag::stylesheetLink('assets/stylesheets/normalize.css');
                    echo Tag::stylesheetLink('assets/stylesheets/ot-style.css');
                    break;
                case 'updates':
                    echo Tag::stylesheetLink('assets/vendor/magnific-popup/magnific-popup.css');
                    echo Tag::stylesheetLink('assets/vendor/bootstrap-datepicker/css/datepicker3.css');
                    echo Tag::stylesheetLink('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css');
                    echo Tag::stylesheetLink('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css');
                    // echo Tag::stylesheetLink('assets/stylesheets/editor.css');
                    break;
                case 'adminusers':
                    echo Tag::stylesheetLink('assets/vendor/select2/select2.css');
                    echo Tag::stylesheetLink('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css');
                    echo Tag::stylesheetLink('assets/vendor/magnific-popup/magnific-popup.css');
                    break;
                case 'dashboard':
                    echo Tag::getTitle();
                    ?>
                    <script type="text/javascript">
                        function number(id,count){
                            var numb=$("#"+id).val();
                            var blanktest=/\S/;
                            var filter=/^\d{10}$/;
                            if(!blanktest.test(numb))
                            {

                                $("#"+id).css("border","1px solid #FF0014");
                                $("#"+id).focus();
                                $("#error").html("<b style='color:#EE0000'>Enter Contact No</b>");
                                return false;
                            }
                            else if(!filter.test(contact))
                            {
                                $("#contact_no").css("border","1px solid #FF0014");
                                $("#contact_no").focus();
                                $("#error").html("<b style='color:#EE0000'>Enter Correct Contact No i.e : 9439075212</b>");
                                return false;
                            }
                            else
                            {
                                $("#contact_no").css("border","");
                                $("#error").html("");
                            }
                        }	

                        function Alfa(id){
                            var checkid=$("#"+id).val();                		
                            if (!checkid.match(/^[a-zA-Z]+$/) && checkid !="")
                            {
                                checkid.value="";
                                $("#"+id).css("border","1px solid #FF0014");
                                $("#"+id).focus();
                                $("#"+id+1).html("<b style='color:#EE0000'>Enter Only Alphabet</b>");
                            }
                            else
                            {
                                $("#"+id).css("border","");
                                $("#"+id+1).html("");
                            }						
                						
                        }

                    </script>

                    <?php
                    echo Tag::stylesheetLink('assets/vendor/magnific-popup/magnific-popup.css');
                    echo Tag::stylesheetLink('assets/vendor/bootstrap-datepicker/css/datepicker3.css');

                    echo Tag::stylesheetLink('assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css');
                    echo Tag::stylesheetLink('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css');
                    echo Tag::stylesheetLink('assets/vendor/morris/morris.css');
                    break;
                /**
                 *  @AUTHOR PREETISH 
                 * VALIDATION FOR PAGE ADD AND UPDATE
                 * @description for pageform
                 */
                case 'pageform':

                    echo Tag::stylesheetLink('assets/vendor/magnific-popup/magnific-popup.css');
                    echo Tag::stylesheetLink('assets/vendor/bootstrap-datepicker/css/datepicker3.css');
                    echo Tag::stylesheetLink('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css');
                    echo '<!-- Specific Page Vendor CSS -->' . "\n";
                    echo Tag::stylesheetLink('assets/vendor/summernote/summernote.css');
                    echo Tag::stylesheetLink('assets/vendor/summernote/summernote-bs3.css');
                    ?>

                    <script type="text/javascript">
                        function page_page()
                        {
                            var es=$("#page").val();
                            var blanktest=/\S/;
                            //var filter=/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
                            if(!blanktest.test(es))
                            {

                                $("#page").css("border","1px solid #FF0014");
                                $("#page").focus();
                                $("#error").html("<b style='color:#EE0000 ;'>Enter Page Name </b>");
                                return false;
                            }
                            else
                            {
                                $("#page").css("border","");
                                $("#error").html("");
                            }
                        }
                        function page_metakeyword()
                        {
                            var ed=$("#metakeyword").val();
                            var blanktest=/\S/;
                            if(!blanktest.test(ed))
                            {

                                $("#metakeyword").css("border","1px solid #FF0014");
                                $("#metakeyword").focus();
                                $("#error1").html("<b style='color:#EE0000'>Enter Meta key word </b>");
                                return false;
                            }
                            else
                            {
                                $("#metakeyword").css("border","");
                                $("#error1").html("");
                            }
                        }
                        function page_desc()
                        {
                            var ef=$("#desc").val();
                            var blanktest=/\S/;
                            if(!blanktest.test(ef))
                            {

                                $("#desc").css("border","1px solid #FF0014");
                                $("#desc").focus();
                                //$("#er").html("<b style='color:#EE0000'>Enter Description </b>");
                                return false;
                            }
                            else
                            {
                                $("#desc").css("border","");
                                $("#error").html("");
                            }
                        }
                        function page_metadata()
                        {
                            var eg=$("#metadata").val();
                            var blanktest=/\S/;
                            if(!blanktest.test(eg))
                            {

                                $("#metadata").css("border","1px solid #FF0014");
                                $("#metadata").focus();
                                $("#er1").html("<b style='color:#EE0000'>Enter Meta Data </b>");
                                return false;
                            }
                            else
                            {
                                $("#metadata").css("border","");
                                $("#er1").html("");
                            }
                        }

                        function Page_submitValidation()
                        {                            
                            page_page();
                            page_metakeyword();
                            page_desc();
                            page_metadata();
                            page_metadesc();
                	                   
                            if(document.getElementById("status").value == "")
                            {
                                $("#status").css("border","1px solid #FF0014");
                                $("#status").focus();
                                $("#ere1").html("<b style='color:#EE0000'>Please Select status </b>");
                                return false;

                            }



                        }
                        function page_metadesc()
                        {   
                            var desc1=document.form2.metadesc.value;
                            var blanktest=/\S/;
                            if(!blanktest.test(desc1))
                            {
                                $("#metadesc").css("border","1px solid #FF0014");
                                $("#metadesc").focus();
                                return false;
                            }                               

                        }
                        function Page_pagecheck()
                        {                        
                            page_page();


                        }
                        function page_metakeywordcheck()
                        {                        
                            page_metakeyword();


                        }
                        function page_desccheck()
                        {                        
                            page_desc();


                        }
                        function page_metadatacheck()
                        {                        
                            page_metadata();


                        }

                    </script>


                    <?php
                    break;
                /**
                 * @author preetish priyabrata
                 * @created on 2/june/2015
                 * #case for quetion type
                 */
                case 'questiontype':
                    echo Tag::stylesheetLink('assets/vendor/select2/select2.css');
                    echo Tag::stylesheetLink('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css');
                    break;
                default:
                    break;
            }
            echo '<!-- Theme CSS -->' . "\n";
            echo Tag::stylesheetLink('assets/stylesheets/theme.css');
            echo '<!-- Skin CSS -->' . "\n";
            echo Tag::stylesheetLink('assets/stylesheets/skins/default.css');
            echo '<!-- Theme Custom CSS -->' . "\n";
            echo Tag::stylesheetLink('assets/stylesheets/theme-custom.css');
            // echo Tag::javascriptInclude('assets/vendor/modernizr/modernizr.js');
            ?>
            </head>	
            <body>
            <div class="load_box" style="display:none;">
            <div class="loading" id="loader">
            <span>I</span>
            <span>n</span>
            <span>s</span>
            <span>e</span>
            <span>r</span>
            <span>t</span>
            <span>i</span>
            <span>n</span>
            <span>g</span>
            <span>.</span>
            <span>.</span>
            <span>.</span>
        </div> 
        </div> 
            <?php
        }

        static function getFooter($pageaction = '') {
            echo UIElementsAdmin::frontjs();
            // echo Tag::javascriptInclude('myadmin/js/jquery.js');
            // echo Tag::javascriptInclude('frontend/vendor/jquery.validation/jquery.validation.js');
            // echo Tag::javascriptInclude('myadmin/js/bootstrap.js');
            echo Tag::javascriptInclude('assets/vendor/nanoscroller/nanoscroller.js');
            echo Tag::javascriptInclude('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js');
            switch ($pageaction) {
                case 'updates' :
                    echo Tag::javascriptInclude('assets/ckeditor/ckeditor.js');
                    echo Tag::javascriptInclude('assets/ckfinder/ckfinder.js');
                    echo Tag::javascriptInclude('assets/ckeditor/plugins/ckeditor_wiris/core/display.js');
                    break;
                default:
                    break;
            }
            switch ($pageaction) {
                case 'adm_login':
                    break;
                case 'tables':
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js');
                    echo '<!-- Examples -->' . "\n";
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.default.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.row.with.details.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.tabletools.js');
                    echo Tag::javascriptInclude('assets/countdown/jquery.countdown.js');
                    echo Tag::javascriptInclude('assets/countdown/script.js');
                    break;
                case 'updates':
                    echo '<!-- Vendor -->' . "\n";
                     echo Tag::javascriptInclude('assets/ckeditor/ckeditor.js');
                    echo Tag::javascriptInclude('assets/ckfinder/ckfinder.js');
                    echo Tag::javascriptInclude('assets/ckeditor/plugins/ckeditor_wiris/core/display.js');
                    echo Tag::javascriptInclude('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js');
                     echo Tag::javascriptInclude('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js');
                    echo Tag::javascriptInclude('assets/vendor/magnific-popup/magnific-popup.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-placeholder/jquery.placeholder.js');
                    echo '<!-- Specific Page Vendor -->' . "\n";
                    echo Tag::javascriptInclude('assets/vendor/jquery-autosize/jquery.autosize.js');
                    echo Tag::javascriptInclude('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js');
                    echo '<!-- Theme Initialization Files -->' . "\n";
                    echo Tag::javascriptInclude('assets/javascripts/theme.init.js');
                    echo Tag::javascriptInclude('assets/javascripts/question.js');
                    echo Tag::javascriptInclude('frontend/vendor/jquery.validation/jquery.validation.js');
                    ?>
                    <script>
                        // $( '#editor' ).ckeditor();
                        // CKEDITOR.replace( 'editors' );
                        $(function(){
                            CKEDITOR.replace( 'ques_name',
                            {  
                                toolbar:'Full',
                                removeButtons:'ckeditor_wiris_CAS'
                            });
                        })
                    </script>
                    <?php
                    break;
                case 'adminusers':
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js');
                    echo Tag::javascriptInclude('assets/javascripts/theme.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-appear/jquery.appear.js');
                    echo Tag::javascriptInclude('assets/vendor/magnific-popup/magnific-popup.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-easypiechart/jquery.easypiechart.js');
                    echo Tag::javascriptInclude('assets/vendor/snap-svg/snap.svg.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.default.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.row.with.details.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.tabletools.js');
                    break;
                case 'dashboard':
                    echo '<!-- Vendor -->' . "\n";
                    echo Tag::javascriptInclude('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js');
                    echo Tag::javascriptInclude('assets/vendor/magnific-popup/magnific-popup.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-placeholder/jquery.placeholder.js');
                    echo '<!-- Specific Page Vendor -->' . "\n";
                    echo Tag::javascriptInclude('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-appear/jquery.appear.js');
                    echo Tag::javascriptInclude('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-easypiechart/jquery.easypiechart.js');
                    echo Tag::javascriptInclude('assets/vendor/flot/jquery.flot.js');
                    echo Tag::javascriptInclude('assets/vendor/flot-tooltip/jquery.flot.tooltip.js');
                    echo Tag::javascriptInclude('assets/vendor/flot/jquery.flot.pie.js');
                    echo Tag::javascriptInclude('assets/vendor/flot/jquery.flot.categories.js');
                    echo Tag::javascriptInclude('assets/vendor/flot/jquery.flot.resize.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-sparkline/jquery.sparkline.js');
                    echo Tag::javascriptInclude('assets/vendor/raphael/raphael.js');
                    echo Tag::javascriptInclude('assets/vendor/morris/morris.js');
                    echo Tag::javascriptInclude('assets/vendor/gauge/gauge.js');
                    echo Tag::javascriptInclude('assets/vendor/snap-svg/snap.svg.js');
                    echo Tag::javascriptInclude('assets/vendor/liquid-meter/liquid.meter.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/jquery.vmap.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/data/jquery.vmap.sampledata.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/maps/jquery.vmap.world.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js');
                    echo Tag::javascriptInclude('assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js');
                    echo '<!-- Theme Initialization Files -->' . "\n";
                    echo Tag::javascriptInclude('assets/javascripts/theme.init.js');
                    echo '<!-- Examples -->' . "\n";
                    echo Tag::javascriptInclude('assets/javascripts/dashboard/examples.dashboard.js');
                    break;
                case 'pageform':
                    echo '<!-- Vendor -->' . "\n";
                    echo Tag::javascriptInclude('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js');
                    echo Tag::javascriptInclude('assets/vendor/magnific-popup/magnific-popup.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-placeholder/jquery.placeholder.js');
                    echo '<!-- Specific Page Vendor -->' . "\n";
                    echo Tag::javascriptInclude('assets/vendor/summernote/summernote.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-autosize/jquery.autosize.js');
                    echo Tag::javascriptInclude('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js');
                    echo '<!-- Theme Initialization Files -->' . "\n";
                    echo Tag::javascriptInclude('assets/javascripts/theme.init.js');
                    break;
                /**
                 * @author preetish priyabrata
                 * function switch case for question type
                 * @created on 2/june/2015
                 */
                case 'questiontype':
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js');
                    echo Tag::javascriptInclude('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js');
                    echo Tag::javascriptInclude('assets/javascripts/theme.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.default.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.row.with.details.js');
                    echo Tag::javascriptInclude('assets/javascripts/tables/examples.datatables.tabletools.js');
                    break;
                default:
                    break;
            }
            echo Tag::javascriptInclude('assets/javascripts/theme.js');
            echo Tag::javascriptInclude('assets/javascripts/theme.custom.js');
            ?>

            <?php
            echo "</body>";
        }

        static function question() {
            echo Tag::javascriptInclude('assets/javascripts/question.js');
        }

        static function news() {
            echo Tag::javascriptInclude('assets/javascripts/news.js');
        }

        static function frontcss() {?>
            <!-- Basic -->
            <meta charset="UTF-8">
            <meta name="keywords" content="" />
            <meta name="description" content="Top label education portal Ischool4u">
            <meta name="author" content="">
            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <!--Webfont Start-->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
            <!--Webfont End-->
            <?php
            // echo Tag::stylesheetLink('frontend/vendor/bootstrap/bootstrap.css');
            // echo Tag::stylesheetLink('frontend/vendor/fontawesome/css/font-awesome.css');
            echo Tag::stylesheetLink('frontend/vendor/owlcarousel/owl.carousel.min.css');
            echo Tag::stylesheetLink('frontend/vendor/owlcarousel/owl.theme.default.min.css');
            echo Tag::stylesheetLink('frontend/vendor/magnific-popup/magnific-popup.css');
            echo Tag::stylesheetLink('frontend/css/theme.css');
            echo Tag::stylesheetLink('frontend/css/theme-elements.css');
            echo Tag::stylesheetLink('frontend/css/theme-blog.css');
            echo Tag::stylesheetLink('frontend/css/theme-shop.css');
            echo Tag::stylesheetLink('frontend/css/theme-animate.css');
            echo Tag::stylesheetLink('frontend/vendor/rs-plugin/css/settings.css');
            echo Tag::stylesheetLink('frontend/vendor/circle-flip-slideshow/css/component.css');
            echo Tag::stylesheetLink('frontend/css/skins/default.css');
            echo Tag::stylesheetLink('frontend/css/custom.css');
            //echo '</head>' . "\n" . '<body>';
        }

        static function frontjs() {
            
            echo Tag::javascriptInclude('frontend/vendor/jquery/jquery.js');
            echo Tag::javascriptInclude('frontend/vendor/jquery.appear/jquery.appear.js');
            echo Tag::javascriptInclude('frontend/vendor/jquery.easing/jquery.easing.js');
            echo Tag::javascriptInclude('frontend/vendor/jquery-cookie/jquery-cookie.js');
            echo Tag::javascriptInclude('frontend/vendor/bootstrap/bootstrap.js');
            echo Tag::javascriptInclude('frontend/vendor/common/common.js');
            echo Tag::javascriptInclude('frontend/vendor/jquery.validation/jquery.validation.js');
            echo Tag::javascriptInclude('frontend/vendor/jquery.stellar/jquery.stellar.js');
            echo Tag::javascriptInclude('frontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js');
            echo Tag::javascriptInclude('frontend/vendor/jquery.gmap/jquery.gmap.js');
            echo Tag::javascriptInclude('frontend/vendor/isotope/jquery.isotope.js');
            echo Tag::javascriptInclude('frontend/vendor/owlcarousel/owl.carousel.js');
            echo Tag::javascriptInclude('frontend/vendor/jflickrfeed/jflickrfeed.js"');
            echo Tag::javascriptInclude('frontend/vendor/magnific-popup/jquery.magnific-popup.js');
            echo Tag::javascriptInclude('frontend/js/vide/vide.js');
            echo Tag::javascriptInclude('frontend/js/theme.js');
            echo Tag::javascriptInclude('frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js');
            echo Tag::javascriptInclude('frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js');
            echo Tag::javascriptInclude('frontend/vendor/circle-flip-slideshow/js/jquery.flipshow.js');
            echo Tag::javascriptInclude('frontend/js/views/view.home.js');
            echo Tag::javascriptInclude('frontend/views/view.contact.js');
            echo Tag::javascriptInclude('frontend/js/custom.js');
            echo Tag::javascriptInclude('frontend/js/theme.init.js');
            echo Tag::javascriptInclude('assets/javascripts/form_validation.js');
            echo Tag::javascriptInclude('assets/javascripts/form_validation1.js');
            echo Tag::javascriptInclude('assets/vendor/modernizr/modernizr.js');
            //echo "</body>\n";
        }
        public function practiseList($practice_id = NULL, $prac_condition = NULL,$min_ques=NULL){
            $topicarr=array();
            if (!empty($prac_condition)) {
                $getsSub = MasterCourse::findFirst("id='".$prac_condition['course']."'");
                $sSublist = json_decode($getsSub->subsubject,true);
                $qtype = implode(",",json_decode($prac_condition['q_type'],true));
                $easy=round($prac_condition['ql_easy']/10);
                $medium=round($prac_condition['ql_medium']/10);
                $hard=round($prac_condition['ql_hard']/10);
                $toataltyqu=(int)$easy+$medium+$hard;
                 foreach ($sSublist as $ssname) {
                     $sqlquery="SELECT count(*) as totalquestion,topics FROM MasterQuestion WHERE subsubject=".$ssname." AND q_type IN(".$qtype.") AND q_status=1 AND LOCATE(2,e_type) AND q_level IN(1,2,3) GROUP BY topics order by topics,q_level";
                     $datanew= $this->modelsManager->executeQuery($sqlquery);
                     foreach($datanew as $topicwise){
                         $easyquesionid='';
                         $mediquesionid='';
                         $hardquesionid='';
                         $e=1;
                         $m=1;
                         $h=1;
                        if($topicwise->totalquestion>=$min_ques){
                            for($p=0;$p<=$topicwise->totalquestion;$p++){
                                //insert into table for easy question
                                if($e==1){
                                    $cond=" q_status=1";
                                    if($easyquesionid!=''){
                                        $eaasyid=rtrim($easyquesionid,',');
                                        $cond.=" AND questionid NOT IN(".$eaasyid.")";
                                    }
                                   $sqleasy="SELECT * FROM MasterQuestion WHERE topics=".$topicwise->topics." AND q_type IN(".$qtype.") AND q_level=1 AND ".$cond." AND LOCATE(2,e_type) AND q_status=1 order by RAND() LIMIT ".$easy;
                            $dataeasy= $this->modelsManager->executeQuery($sqleasy);
                            
                            if($dataeasy->count()<$easy){
                                $e=0;
                            }
                            foreach($dataeasy as $lasteasy){
                                $savedata=array();
                                $savedata['practice_series']=$practice_id;
                                $savedata['topic_id']=$lasteasy->topics;
                                $savedata['q_type']=$lasteasy->q_type;
                                $savedata['q_level']=$lasteasy->q_level;
                                $savedata['q_id']=$lasteasy->questionid;
                                $savedata['table_id']=$lasteasy->tableid;
                                $addprac= new PracticeQuestionList();
                                $addprac->save($savedata);
                                $easyquesionid.="'".$lasteasy->questionid."',";
                                 } 
                                }
                                //insert into table for medium question
                                if($m==1){
                                     $condmed=" q_status=1";
                                    if($mediquesionid!=''){
                                        $medid=rtrim($mediquesionid,',');
                                        $condmed.=" AND questionid NOT IN(".$medid.")";
                                    }
                                   $sqlmed="SELECT * FROM MasterQuestion WHERE topics=".$topicwise->topics." AND q_type IN(".$qtype.") AND q_level=2 AND ".$condmed." AND LOCATE(2,e_type) AND q_status=1 order by RAND() LIMIT ".$medium;
                            $datamed= $this->modelsManager->executeQuery($sqlmed);
                            if($datamed->count()<$medium){
                                $m=0;
                            }
                            foreach($datamed as $lasteasy){
                                $savedata=array();
                                $savedata['practice_series']=$practice_id;
                                $savedata['topic_id']=$lasteasy->topics;
                                $savedata['q_type']=$lasteasy->q_type;
                                $savedata['q_level']=$lasteasy->q_level;
                                $savedata['q_id']=$lasteasy->questionid;
                                $savedata['table_id']=$lasteasy->tableid;
                                $addprac= new PracticeQuestionList();
                                $addprac->save($savedata);
                                 $mediquesionid.="'".$lasteasy->questionid."',";
                                 } 
                                }
                                 //insert into table for hard question
                                if($h==1){
                                    $condhard=" q_status=1";
                                    if($hardquesionid){
                                        $hardid=rtrim($hardquesionid,',');
                                        $condhard.=" AND questionid NOT IN(".$hardid.")";
                                    }
                                   $sqlhard="SELECT * FROM MasterQuestion WHERE topics=".$topicwise->topics." AND q_type IN(".$qtype.") AND q_level=3 AND ".$condhard." AND LOCATE(2,e_type) AND q_status=1 order by RAND() LIMIT ".$hard;
                            $datahard= $this->modelsManager->executeQuery($sqlhard);
                            if($datahard->count()<$hard){
                                $h=0;
                            }
                            foreach($datahard as $lasteasy){
                                $savedata=array();
                                $savedata['practice_series']=$practice_id;
                                $savedata['topic_id']=$lasteasy->topics;
                                $savedata['q_type']=$lasteasy->q_type;
                                $savedata['q_level']=$lasteasy->q_level;
                                $savedata['q_id']=$lasteasy->questionid;
                                $savedata['table_id']=$lasteasy->tableid;
                                $addprac= new PracticeQuestionList();
                                $addprac->save($savedata);
                                $hardquesionid.="'".$lasteasy->questionid."',";
                                 } 
                                }
                            }
                    }else{
                       array_push($topicarr,$topicwise->topics);
                    }
                }
                }
            }
            if(!empty($topicarr)){
            return implode(",",$topicarr);
           }else{
            return 1;
           }
        }
         public function assessmentList($assessment_id = NULL, $prac_condition = NULL){
            if (!empty($prac_condition)) {
                $easy=round($prac_condition['ql_easy']/10);
                $medium=round($prac_condition['ql_medium']/10);
                $hard=round($prac_condition['ql_hard']/10);
                $subtopi=implode(",",$prac_condition['subtio']);
                     $sqlquery="SELECT * FROM MasterQuestion WHERE subtopics IN(".$subtopi.") AND q_type NOT IN(6,5,8) AND q_status=1 AND LOCATE(2,e_type) AND q_level IN(1,2,3)";
                        $datanew= $this->modelsManager->executeQuery($sqlquery);
                        $totalloop=$datanew->count();
                         $easyquesionid='';
                         $mediquesionid='';
                         $hardquesionid='';
                         $e=1;
                         $m=1;
                         $h=1;
                         $total=1;
                            for($p=0;$p<=$totalloop;$p++){
                                //insert into table for easy question
                                if($total>=$prac_condition['no_qs']){
                                    break;
                                }
                                if($e==1){
                                    $cond=" q_status=1";
                                    if($easyquesionid!=''){
                                        $eaasyid=rtrim($easyquesionid,',');
                                        $cond.=" AND questionid NOT IN(".$eaasyid.")";
                                    }
                                   $sqleasy="SELECT * FROM MasterQuestion WHERE subtopics IN(".$subtopi.") AND q_type NOT IN(6,5,8) AND q_level=1 AND ".$cond." AND LOCATE(2,e_type) AND q_status=1 order by RAND() LIMIT ".$easy;
                            $dataeasy= $this->modelsManager->executeQuery($sqleasy);
                            if($dataeasy->count()<$easy){
                                $e=0;
                            }
                            foreach($dataeasy as $lasteasy){
                                if($total>=$prac_condition['no_qs']){
                                    break;
                                }
                                $savedata=array();
                                $savedata['assessment_id']=$assessment_id;
                                $savedata['q_id']=$lasteasy->questionid;
                                $addprac= new AssessmentQuestionList();
                                $addprac->save($savedata);
                                $easyquesionid.="'".$lasteasy->questionid."',";
                                $total++;
                                 } 
                                }
                                //insert into table for medium question
                                if($m==1){
                                     $condmed=" q_status=1";
                                    if($mediquesionid!=''){
                                        $medid=rtrim($mediquesionid,',');
                                        $condmed.=" AND questionid NOT IN(".$medid.")";
                                    }
                                   $sqlmed="SELECT * FROM MasterQuestion WHERE subtopics IN(".$subtopi.") AND q_type NOT IN(6,5,8) AND q_level=2 AND ".$condmed." AND LOCATE(2,e_type) AND q_status=1 order by RAND() LIMIT ".$medium;
                            $datamed= $this->modelsManager->executeQuery($sqlmed);
                            if($datamed->count()<$medium){
                                $m=0;
                            }
                            foreach($datamed as $lasteasy){
                                if($total>=$prac_condition['no_qs']){
                                    break;
                                }
                                $savedata=array();
                                 $savedata['assessment_id']=$assessment_id;
                                $savedata['q_id']=$lasteasy->questionid;
                                $addprac= new AssessmentQuestionList();
                                $addprac->save($savedata);
                                 $mediquesionid.="'".$lasteasy->questionid."',";
                                 $total++;
                                 } 
                                }
                                 //insert into table for hard question
                                if($h==1){
                                    $condhard=" q_status=1";
                                    if($hardquesionid){
                                        $hardid=rtrim($hardquesionid,',');
                                        $condhard.=" AND questionid NOT IN(".$hardid.")";
                                    }
                                   $sqlhard="SELECT * FROM MasterQuestion WHERE subtopics IN(".$subtopi.") AND q_type NOT IN(6,5,8) AND q_level=3 AND ".$condhard." AND LOCATE(2,e_type) AND q_status=1 order by RAND() LIMIT ".$hard;
                            $datahard= $this->modelsManager->executeQuery($sqlhard);
                            if($datahard->count()<$hard){
                                $h=0;
                            }
                            foreach($datahard as $lasteasy){
                                if($total>=$prac_condition['no_qs']){
                                    break;
                                }
                                $savedata=array();
                                $savedata['assessment_id']=$assessment_id;
                                $savedata['q_id']=$lasteasy->questionid;
                                $addprac= new AssessmentQuestionList();
                                $addprac->save($savedata);
                                $hardquesionid.="'".$lasteasy->questionid."',";
                                $total++;
                                 } 
                                }
                            }
                        }
        }
    }
    ?>