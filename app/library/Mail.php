<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;

require_once __DIR__ . '/../../vendor/Swift/swift_required.php';

/**
 *
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component {

    protected $_transport;

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     */
    public function getTemplate($name, $params) {
        $parameters = array_merge(array(
            'publicUrl' => BASEURL
                ), $params);
        return $this->view->getRender('emailTemplates', $name, $parameters, function($view) {
                    $view->setRenderLevel(View::LEVEL_LAYOUT);
                });

        return $view->getContent();
    }

    /**
     * Sends e-mails via gmail based on predefined templates
     *
     * @param array $to
     * @param string $subject
     * @param string $name
     * @param array $params
     */
    public function send($to, $subject, $name, $params) {
        $template = $this->getTemplate($name, $params);
        // Create the message
        $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setTo($to)
                ->setFrom(array(
                    'Emailid' => 'root@ischool4u.com' //replace Emailid with your emailid
                ))
                ->setBody($template, 'text/html');
        if (!$this->_transport) {
            $this->_transport = Swift_SmtpTransport::newInstance(
                            'smtp.gmail.com', '465', 'ssl'
                    )
                    ->setUsername('root@ischool4u.com')//replace Emailid with your emailid
                    ->setPassword('2489DF45@6hg'); //replace PASSWORD with your password
        }

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($this->_transport);

        return $mailer->send($message);
    }

}