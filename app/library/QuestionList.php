<?php
use Phalcon\Tag as Tag;

abstract class QuestionList {
    /**
     * [getQuestion : To get the details of a question Given by question_id]
     * @param  [Varchar] $question_id [id of a question]
     */
    static function getQuestion($question_id) {
        $questionsres = MasterQuestion::findFirst(array("questionid = '" . $question_id . "'"));
        ?>
        <div class="panel">
            <div class="panel-body">
                <?php
                if ($questionsres->tableid == 1):
                    $question = MasterQuestionBank::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' "));
                    ?>
                    <div class="qcontent">
                        <?= $question->question ?>                                         
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge badge-inverse pull-left">A</span> 
                            <?= strip_tags($question->option1,"<sub><sup>") ?>
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-inverse pull-left">B</span> 
                            <?= strip_tags($question->option2,"<sub><sup>") ?>
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-inverse pull-left">C</span> 
                            <?= strip_tags($question->option3,"<sub><sup>") ?>
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-inverse pull-left">D</span> 
                            <?= strip_tags($question->option4,"<sub><sup>") ?>
                        </li>
                    </ul>
                <?php elseif ($questionsres->tableid == 2):
                    $question = MasterMatchQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                    <div class="qcontent">
                    <?= $question->question ?>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">A</span> <?= $question->a ?>
                            </div>
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">P</span> <?= $question->p ?>
                            </div>
                        </div>
                        <!-- 1st row -->
                        <div class="col-md-12">
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">B</span> <?= $question->b ?>
                            </div>
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">Q</span> <?= $question->q ?>
                            </div>
                        </div>
                        <!-- 2nd row -->
                        <div class="col-md-12">
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">C</span> <?= $question->c ?>
                            </div>
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">R</span> <?= $question->r ?>
                            </div>
                        </div>
                        <!-- 3rd row -->
                        <div class="col-md-12">
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">D</span> <?= $question->d ?>
                            </div>
                            <div class="col-md-6 mat_table">
                                <span class="badge bg-default">S</span> <?= $question->s ?>
                            </div>
                        </div>  
                        <!-- 4th row -->
                    </div>
                <?php elseif ($questionsres->tableid == 3):
                    $question = MasterNumericQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                    <div class="qcontent">
                        <?= $question->question ?>
                    </div>
                <?php elseif ($questionsres->tableid == 4):
                    $question = MasterParaQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                    
                    <div class="qcontent">
                        <?= $question->para ?>
                    </div>
                    <?php $opt = 1;
                    for ($i = 1; $i <= 4; $i++): ?>
                        <div class="para-opt">
                            <?php
                            if ($i == 1 && $question->question1 != '') {
                                echo $question->question1;
                            } elseif ($i == 2 && $question->question2 != '') {
                                echo $question->question2;
                            } elseif ($i == 3 && $question->question3 != '') {
                                echo $question->question3;
                            } elseif ($i == 4 && $question->question4 != '') {
                                echo $question->question4;
                            }
                            ?>
                        </div>
                        <div class="qoption">
                            <div class="">
                                <?php if ($i == 1 && $question->question1 != ''): ?>
                                    <ul class="list-group">
                                        <li class="list-group-item" id="<?= $question->questionid ?>1A" ><?= $question->option1 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>1B" ><?= $question->option2 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>1C" ><?= $question->option3 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>1D" ><?= $question->option4 ?></li>
                                    </ul>
                                <?php elseif ($i == 2 && $question->question2 != ''): ?>
                                    <ul class="list-group">
                                        <li class="list-group-item" id="<?= $question->questionid ?>2A" ><?= $question->option5 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>2B" ><?= $question->option6 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>2C" ><?= $question->option7 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>2D" ><?= $question->option8 ?></li>
                                    </ul>
                                <?php elseif ($i == 3 && $question->question3 != ''): ?>
                                    <ul class="list-group">
                                        <li class="list-group-item" id="<?= $question->questionid ?>3A" ><?= $question->option9 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>3A" ><?= $question->option10 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>3A" ><?= $question->option11 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>3A" ><?= $question->option12 ?></li>
                                    </ul>
                                <?php elseif ($i == 4 && $question->question4 != ''): ?>
                                    <ul class="list-group">
                                        <li class="list-group-item" id="<?= $question->questionid ?>4A" ><?= $question->option13 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>4A" ><?= $question->option14 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>4A" ><?= $question->option15 ?></li>
                                        <li class="list-group-item" id="<?= $question->questionid ?>4A" ><?= $question->option16 ?></li>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                <?php elseif ($questionsres->tableid == 5):
                    $question = MasterReasonQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                    <div class="qcontent" data-ans="<?= $question->ans ?>">
                        <?= $question->question ?>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item" id="<?= $question->questionid ?>A"><span class="badge badge-inverse pull-left">A</span> <?= $question->option1 ?></li>
                        <li class="list-group-item" id="<?= $question->questionid ?>B"><span class="badge badge-inverse pull-left">B</span> <?= $question->option2 ?></li>
                        <li class="list-group-item" id="<?= $question->questionid ?>C"><span class="badge badge-inverse pull-left">C</span> <?= $question->option3 ?></li>
                        <li class="list-group-item" id="<?= $question->questionid ?>D"><span class="badge badge-inverse pull-left">D</span> <?= $question->option4 ?></li>
                        <?php if (!empty($question->option5)): ?>
                            <li class="list-group-item" id="<?= $question->questionid ?>E"><span class="badge badge-inverse pull-left">E</span> <?= $question->option5 ?></li>
                        <?php endif; ?>
                    </ul>
                <?php elseif ($questionsres->tableid == 6):
                    $question = MasterSubtheoQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' "));
                    $mquestion = MasterQuestion::findFirst("questionid='" . $question->questionid . "'");
                            ?>
                    <div class="qcontent" data-ans="">
                        <?= $question->question ?>
                    </div>
                <?php endif; ?>
            </div>
        </div><?php
    }
}?>