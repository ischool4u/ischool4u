<?php

$router = new Phalcon\Mvc\Router(); 
// $router->add('index/', array(
//     'controller' => 'index',
//     'action'     => 'index'
// ));
$router->add('/module/{pagename:[a-zA-Z0-9\-]+}', array(
	'controller' => 'subjectlist',
	'action'     => 'index',
	'pagename'   => 1
));
$router->add('/module/{pagename:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'index',
    'pagename'   => 1
));
$router->add('/module/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'topics',
    'pagename'   => 1,
    'sub'   => 1
));
$router->add('/module/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'topics',
    'pagename'   => 1,
    'sub'   => 1
));
$router->add('/module/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'subtopics',
    'pagename'   => 1,
    'sub'   => 1
));
$router->add('/module/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'subtopics',
    'pagename'   => 1,
    'sub'   => 1,
    'topic'   => 1
));
$router->add('/question/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'question',
    'pagename'   => 1,
    'sub'   => 1,
    'topic' => 1
));
$router->add('/question/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'question',
    'pagename'   => 1,
    'sub'   => 1,
    'topic'   => 1
));
$router->add('/question/challenged_question/{pagename:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'challenged_question',
    'pagename'   => 1
));
$router->add('/question/challenged_question/{pagename:[a-zA-Z0-9\-]+}/{id:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'challenged_question',
    'pagename'   => 1,
    'id'   => 1
));
$router->add('/tip/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'subtopicstip',
    'pagename'   => 1,
    'sub'   => 1,
    'topic' => 1
));
$router->add('/tip/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'subtopicstip',
    'pagename'   => 1,
    'sub'   => 1,
    'topic'   => 1
));
$router->add('/tip/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/{subtopic:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'tips',
    'pagename'   => 1,
    'sub'   => 1,
    'topic' => 1,
    'subtopic' => 1
));
$router->add('/tip/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/{subtopic:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'tips',
    'pagename'   => 1,
    'sub'   => 1,
    'topic'   => 1,
    'subtopic' => 1
));
$router->add('/articles/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'subtopics',
    'pagename'   => 1,
    'sub'   => 1,
    'topic' => 1
));
$router->add('/articles/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'subtopics',
    'pagename'   => 1,
    'sub'   => 1,
    'topic'   => 1
));
$router->add('/articles/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/{subtopic:[a-zA-Z0-9\-]+}', array(
    'controller' => 'subjectlist',
    'action'     => 'viewarticle',
    'pagename'   => 1,
    'sub'   => 1,
    'topic' => 1,
    'subtopic' => 1
));
$router->add('/articles/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/{subtopic:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'subjectlist',
    'action'     => 'viewarticle',
    'pagename'   => 1,
    'sub'   => 1,
    'topic'   => 1,
    'subtopic' => 1
));
$router->add('/readarticles/{slug:[a-zA-Z0-9\-]+}', array(
    'controller' => 'article',
    'action'     => 'articles',
    'slug'   => 1
));
$router->add('/readarticles/{slug:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'article',
    'action'     => 'articles',
    'slug'   => 1
));
$router->add('/assessment/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}', array(
    'controller' => 'assessment',
    'action'     => 'index',
    'pagename'   => 1,
    'sub'   => 1,
    'topic' => 1
));
$router->add('/assessment/{pagename:[a-zA-Z0-9\-]+}/{sub:[a-zA-Z0-9\-]+}/{topic:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'assessment',
    'action'     => 'index',
    'pagename'   => 1,
    'sub'   => 1,
    'topic'   => 1
));
$router->add('/assessmenttest/{id:[a-zA-Z0-9\-]+}', array(
    'controller' => 'assessment',
    'action'     => 'assesstest',
    'id'   => 1
));
$router->add('/assessmenttest/{id:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'assessment',
    'action'     => 'assesstest',
    'id'   => 1
));
$router->add('/assessmentresult/{id:[a-zA-Z0-9\-]+}/', array(
    'controller' => 'assessment',
    'action'     => 'result',
    'id'   => 1
));