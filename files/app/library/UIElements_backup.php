<?php

/**
 * UIElements
 *
 * Helps to build UI elements for the application
 */
use Phalcon\Tag as Tag;
abstract class UIElements
{
	
    // header code for front end view
    public function getHeader($pageaction='')
    {
		
		?>
            <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="robots" content="noindex, nofollow">
            
            <?php
            echo Tag::getTitle();
            echo Tag::stylesheetLink('css/bootstrap.css');
            echo Tag::stylesheetLink('css/asish.css');
            echo Tag::stylesheetLink('css/elastislide.css');
            ?>
           
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        <?php
		switch ($pageaction){
			case 'register' :
			?>
            <script type="text/javascript">
			function registerValidate()
			{
				var first_name=$("#first_name").val();
				var last_name=$("#last_name").val();
				var mail_id =$("#mail_id").val();
				var pass=$("#pass").val();
				var contact_no=$("#contact_no").val();
				var confirm_email=$("#confirm_email").val();
				var confirm_pwd=$("#confirm_pwd").val();
				var contact_no=$("#contact_no").val();
				var blanktest=/\S/;
				var filter=/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
				if(!blanktest.test(first_name))
				{
					$("#first_name").css("background-color","#FCDFD1");
					$("#first_name").focus();
					return false;
				}
				else
				{
					$("#first_name").css("background-color","");
				}
				if(!blanktest.test(last_name))
				{
					$("#last_name").css("background-color","#FCDFD1");
					$("#last_name").focus();
					return false;
				}
				else
				{
					$("#last_name").css("background-color","");
				}
				if(!blanktest.test(mail_id))
				{
					$("#mail_id").css("background-color","#FCDFD1");
					$("#mail_id").focus();
					return false;
				}
				else if(!filter.test(mail_id))
				{
					$("#mail_id").css("background-color","#FCDFD1");
					$("#mail_id").focus();
					return false;
				}
				else
				{
					$("#mail_id").css("background-color","");
				}
				if(!blanktest.test(confirm_email))
				{
					$("#confirm_email").css("background-color","#FCDFD1");
					$("#confirm_email").focus();
					return false;
				}
				else if(mail_id!=confirm_email)
				{
					$("#confirm_email").css("background-color","#FCDFD1");
					$("#errorMsg").html("Email ID Mismatch");
					$("#confirm_email").focus();
					return false;
				}
				else
				{
					$("#errorMsg").html("");
					$("#confirm_email").css("background-color","");
				}
				if(!blanktest.test(pass))
				{
					$("#pass").css("background-color","#FCDFD1");
					$("#pass").focus();
					return false;
				}
				else
				{
					$("#pass").css("background-color","");
				}
				if(!blanktest.test(confirm_pwd))
				{
					$("#confirm_pwd").css("background-color","#FCDFD1");
					$("#confirm_pwd").focus();
					return false;
				}
				else if(pass!=confirm_pwd)
				{
					$("#confirm_pwd").css("background-color","#FCDFD1");
					$("#errorMsg").html("Password Mismatch");
					$("#confirm_pwd").focus();
					return false;
				}
				else
				{
					$("#errorMsg").html("");
					$("#confirm_pwd").css("background-color","");
				}
				if(!blanktest.test(contact_no))
				{
					$("#contact_no").css("background-color","#FCDFD1");
					$("#contact_no").focus();
					return false;
				}
				else
				{
					$("#contact_no").css("background-color","");
				}
				if($(".privacy").is(":checked")==false)
				{
					$("#errorMsg").html("Tick the Privacy Policy to forward");
					return false;
				}
				else
				{
					$("#errorMsg").html("");
				}
			}
			</script>
        	
            <?php
			break;
			case 'validate' :
			?>
             <script type="text/javascript">
				function verifiedValidation()
				{
					var verfied_code=$("#verify_code").val();
					var blanktest=/\S/;
					if(!blanktest.test(verfied_code))
					{
						$("#verify_code").css("background-color","#FCDFD1");
						$("#verify_code").focus();
						return false;
					}
					else
					{
						$("#verify_code").css("background-color","");
					}
				}
			</script>
            <?php
			break;
			case 'forgotpassword' :
			?>
             <script type="text/javascript">
				function forgotValidation()
				{
					var email=$("#email").val();
					var blanktest=/\S/;
					var filter=/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
					if(!blanktest.test(email))
					{
						$("#email").css("background-color","#FCDFD1");
						$("#email").focus();
						return false;
					}
					else if(!filter.test(email))
					{
						$("#email").css("background-color","#FCDFD1");
						$("#email").focus();
						return false;
					}
					else
					{
						$("#email").css("background-color","");
					}
				}
			</script>
            <?php
			break;
            case 'myaccount' :
            ?>
            <script type="text/javascript">
                function promote(id)
                {
                    $("#promote"+id).slideToggle();
                    $(".stats").hide(200);
                    $(".rightlist2").removeClass('rightlist2_active');
                    $("#promot_btn"+id).addClass('rightlist2_active');
                    
                }
                function deleteAlert()
                {
                    confirm("Are you want to delete this Ad? :(");
                }
                function stats(id)
                {
                    $("#stats"+id).slideToggle();
                    $(".promote").hide(200);
                    $(".rightlist2").removeClass('rightlist2_active');
                    $("#stats_btn"+id).addClass('rightlist2_active');
                }
                var total=0;
                var price=0;
                function addprice(pid,ad_t)
                {
                    total = Number($("#tot_prc"+pid).val());
                    price = Number($("#pric"+pid+ad_t).val());//alert($("#pric"+pid+ad_t).val());
                    total += price;
                    var total = total.toFixed(2);
                    $("#ad_total"+pid).html(total);
                    $("#tot_prc"+pid).val(total) ;
                }
                function removeprice(pid,ad_t)
                {
                    total = Number($("#tot_prc"+pid).val());
                    price = Number($("#pric"+pid+ad_t).val());//alert($("#pric"+ad_t).val());
                    total -= price;
                    var total = total.toFixed(2);
                    $("#ad_total"+pid).html(total);
                    $("#tot_prc"+pid).val(total) ;
                }
                function adtypetag(post_id,ad_types)
                {

                    if (ad_types==1) 
                    {
                        if (document.getElementById('urgent'+post_id).checked) {
                            addprice(post_id,1);
                            $("#ur"+post_id).show();
                        }
                        else
                        {
                            removeprice(post_id,1);
                            $("#ur"+post_id).hide();
                        }
                    }
                    if (ad_types==2) 
                    {
                        if (document.getElementById('featured'+post_id).checked) {
                            addprice(post_id,2);
                            $("#fe"+post_id).show();
                        }
                        else
                        {
                            removeprice(post_id,2);
                            $("#fe"+post_id).hide();
                        }
                    }
                    if (ad_types==3) 
                    {
                        if (document.getElementById('spotlight'+post_id).checked) {
                            addprice(post_id,3);
                            $("#sp"+post_id).show();
                        }
                        else
                        {
                            removeprice(post_id,3);
                            $("#sp"+post_id).hide();
                        }
                    }
                    $("#tot"+post_id).show();
                }
                
            </script>
            <?php
            break;
            case 'advert' :

            echo Tag::stylesheetLink('myadmin/jasny-bootstrap/css/jasny-bootstrap.min.css');
            ?>

             <script type="text/javascript">
                function postcodeData()
                {
                    var code=$("#postcode").val();
                    var blanktest=/\S/;
                    if(!blanktest.test(code))
                    {
                        $("#postcode").css("background-color","#FCDFD1");
                        $("#postcode").focus();
                        return false;
                    }
                    else
                    {
                        $("#postcode").css("background-color","");
                        $.ajax(
                        {
                            type: 'POST',
                            url: '<?php echo BASEURL;?>location/postcode',
                            data: 'postcode='+code,
                            success: function(data) {
                                if(data)
                                {
                                    $("#loc_list").html(data); 
                                }
                            }
                        });
                    }
                }
                var total=0;
                var price=0;
                function addprice(ad_t)
                {
                    total = Number($("#total_price").val());
                    price = Number($("#price"+ad_t).val());//alert(price);
                    total += price;
                    var total = total.toFixed(2);
                    $("#tot_count").html(total); 
                    $("#total_price").val(total);
                }
                function removeprice(ad_t)
                {
                    total = Number($("#total_price").val());
                    price = Number($("#price"+ad_t).val());//alert(price);
                    total -= price;
                    var total = total.toFixed(2);
                    $("#tot_count").html(total); 
                    $("#total_price").val(total);
                }
                function pricetag(price_types)
                {

                    if (price_types==1) 
                    {
                        if (document.getElementById('urgentChk1').checked) {
                            document.getElementById('urgentChk').checked=true;
                            addprice(1);
                            chkall();
                        }
                        else
                        {
                            document.getElementById('urgentChk').checked=false;
                            removeprice(1);
                            chkall();
                        }
                    }

                    if (price_types==5) 
                    {
                        if (document.getElementById('selectall').checked) {
                            document.getElementById('urgentChk').checked=true;
                            document.getElementById('urgentChk1').checked=true;
                            document.getElementById('featured').checked=true;
                            document.getElementById('spotlight').checked=true;
                             if (document.getElementById('webLink').checked) {
                               
                                    total = Number($("#price4").val());
                                    price1 = Number($("#price1").val());
                                    price2 = Number($("#price2").val());
                                    price3 = Number($("#price3").val());
                                }
                                else
                                {   
                                    total = 0;
                                    price1 = Number($("#price1").val());
                                    price2 = Number($("#price2").val());
                                    price3 = Number($("#price3").val());
                                }
                            
                            total = total + price1 + price2 + price3;
                            var total = total.toFixed(2);
                            $("#tot_count").html(total); 
                            $("#total_price").val(total);
                        }
                        else
                        {
                            document.getElementById('urgentChk').checked=false;
                            document.getElementById('urgentChk1').checked=false;
                            document.getElementById('featured').checked=false;
                            document.getElementById('spotlight').checked=false;
                            total = Number($("#total_price").val());
                            price1 = Number($("#price1").val());
                            price2 = Number($("#price2").val());
                            price3 = Number($("#price3").val());
                            total = total - price1 - price2 - price3;
                            var total = total.toFixed(2);
                            $("#tot_count").html(total); 
                            $("#total_price").val(total);
                        }
                    }
                    if (price_types==4) 
                    {
                        if(document.getElementById('webLink').checked) 
                        {
                            $('#web_link').prop('disabled', false);
                            addprice(4);
                        }
                        else
                        {
                            $('#web_link').prop('disabled', true);
                            removeprice(4);
                        }
                
                    }
                    if (price_types==2) 
                    {
                        if (document.getElementById('featured').checked) {
                            addprice(2);
                            chkall();
                        }
                        else
                        {
                            document.getElementById('featured').checked=false;
                            removeprice(2);
                            chkall();
                        }
                    }
                    if (price_types==3) 
                    {
                        if (document.getElementById('spotlight').checked) {
                            addprice(3);
                            chkall();
                        }
                        else
                        {
                            document.getElementById('spotlight').checked=false;
                            removeprice(3);
                            chkall();
                        }
                    }
                }
                function chkall(){
                    var chkal=0;
                    if (document.getElementById('spotlight').checked) {
                         chkal++;
                    }
                    else
                    {
                         chkal--;
                    }
                    if (document.getElementById('featured').checked) {
                         chkal++;
                    }
                    else
                    {
                         chkal--;
                    }
                    if (document.getElementById('urgentChk1').checked) {
                         chkal++;
                    }
                    else
                    {
                         chkal--;
                    }
                    if (chkal==3) {
                        document.getElementById('selectall').checked=true;
                    }
                    else
                    {
                        document.getElementById('selectall').checked=false;
                    }
                }
                function urgent()
                {
                    if (document.getElementById('urgentChk').checked) {
                        document.getElementById('urgentChk1').checked=true;
                        pricetag(1);
                        //alert("checked");
                    }
                    else
                    {
                        document.getElementById('urgentChk1').checked=false;
                        pricetag(1);
                    }
                }
                
                
                function postValidation()
                {
                    var title=$("#ad_title").val();
                    var blanktest=/\S/;
                    if(!blanktest.test(title))
                    {
                        $("#ad_title").css("background-color","#FCDFD1");
                        $("#ad_title").focus();
                        return false;
                    }
                    else
                    {
                        $("#ad_title").css("background-color","");
                    }
                    var price=$("#item_price").val();
                    if(!blanktest.test(price))
                    {
                        $("#item_price").css("background-color","#FCDFD1");
                        $("#item_price").focus();
                        return false;
                    }
                    else
                    {
                        $("#item_price").css("background-color","");
                    }
                    var desc=$("#ad_description").val();
                    if(!blanktest.test(desc))
                    {
                        $("#ad_description").css("background-color","#FCDFD1");
                        $("#ad_description").focus();
                        return false;
                    }
                    else
                    {
                        $("#ad_description").css("background-color","");
                    }
                    if (document.getElementById('webLink').checked) 
                    {
                        $('#web_link').prop('disabled', false);
                        var web_link=$("#web_link").val();
                        if(!blanktest.test(web_link))
                        {
                            $("#web_link").css("background-color","#FCDFD1");
                            $("#web_link").focus();
                            return false;
                        }
                        else
                        {
                            $("#web_link").css("background-color","");
                        }
                    }
                    else
                    {
                        $("#web_link").css("background-color","");
                    }
                    if(document.getElementById('contact1').checked) 
                    {
                        var u_email=$("#user_email").val();
                        if(!blanktest.test(u_email))
                        {
                            $("#user_email").css("background-color","#FCDFD1");
                            $("#user_email").focus();
                            return false;
                        }
                        else
                        {
                            $("#user_email").css("background-color","");
                        }
                    }
                    else
                    {
                        $("#user_email").css("background-color","");
                    }
                    if(document.getElementById('contact2').checked) 
                    {
                        var u_phone=$("#phone").val();
                        var u_name=$("#userName").val();
                        if(!blanktest.test(u_phone))
                        {
                            $("#phone").css("background-color","#FCDFD1");
                            $("#phone").focus();
                            return false;
                        }
                        else
                        {
                            $("#phone").css("background-color","");
                        }
                        if(!blanktest.test(u_name))
                        {
                            $("#userName").css("background-color","#FCDFD1");
                            $("#userName").focus();
                            return false;
                        }
                        else
                        {
                            $("#userName").css("background-color","");
                        }
                    }
                    else
                    {
                        $("#phone").css("background-color","");
                        $("#userName").css("background-color","");
                    }
                }
                function contactMail()
                {
                    if(document.getElementById('contact1').checked) 
                    {
                        $('#user_email').prop('disabled', false);
                    }
                    else
                    {
                        $('#user_email').prop('disabled', true);
                    }
                }
                function contactPhone()
                {
                    if(document.getElementById('contact2').checked) 
                    {
                        $('#phone').prop('disabled', false);
                        $('#userName').prop('disabled', false);
                        $('#is_whatsapp').prop('disabled', false);
                    }
                    else
                    {
                        $('#phone').prop('disabled', true);
                        $('#userName').prop('disabled', true);
                        $('#is_whatsapp').prop('disabled', true);
                    }
                    
                }
                // select condition as a attribute
                function selectAttribute(sel_id,cat_id)
                {
                    var sel_value = $("#select"+sel_id).val();//alert(sel_value);
                    $.ajax(
                    {
                        type: 'POST',
                        url: '<?php echo BASEURL;?>postad/condition',
                        data: 'selectval='+sel_id+'&optionval='+sel_value+'&cat='+cat_id,
                        success: function(data) {
                            if(data)
                            {
                                $("#selected_cond"+sel_id).css("display","block"); 
                                $("#selected_cond"+sel_id).html(data); 
                            }
                        }
                    });
                }
                function radioAttr(sel_id,cat_id)
                {
                    var sel_value = $("#select"+sel_id).val();//alert(sel_value);
                    $.ajax(
                    {
                        type: 'POST',
                        url: '<?php echo BASEURL;?>postad/condition',
                        data: 'selectval='+sel_id+'&optionval='+sel_value+'&cat='+cat_id,
                        success: function(data) {
                            if(data)
                            {
                                $("#radio_cond"+sel_id).css("display","block"); 
                                $("#radio_cond"+sel_id).html(data); 
                            }
                        }
                    });
                }
                function checkAttr(sel_id,cat_id,chk_no)
                {
                    if($("#chk"+chk_no).is(":checked")==false)
                    {
                        $("#chk_cond"+chk_no).hide(); 
                    }
                    else
                    {
                        var sel_value = $("#chk"+chk_no).val();//alert(sel_value);
                        $.ajax(
                        {
                            type: 'POST',
                            url: '<?php echo BASEURL;?>postad/condition',
                            data: 'selectval='+sel_id+'&optionval='+sel_value+'&cat='+cat_id,
                            success: function(data) {
                                if(data)
                                {
                                    $("#chk_cond"+chk_no).css("display","block"); 
                                    $("#chk_cond"+chk_no).html(data); 
                                }
                            }
                        });
                    }
                    
                }
            </script>
            <?php
            break;
            case 'postad' :
            ?>
             <script type="text/javascript">
                function getsubcategory(id)
                {
                    if(id!='')
                    {
                        $('#p_cid').val(id);
                        $('#col_1').val("");
                        $('#col_2').val("");
                        $('#col_3').val("");
                        $('#col_4').val("");

                        $(".col_1,.col_2,.col_3,.col_4").html("");
                        $('.cat_submit').prop('disabled', true);
                        var cat_loc="col_0";
                        $.ajax(
                                {
                                    type: 'POST',
                                    url: '<?php echo BASEURL;?>category/child',
                                    data: 'category_id='+id+'&level='+cat_loc,
                                    success: function(data) {
                                        if(data)
                                        {
                                            $(".col_1").html(data); 
                                        }
                                    }
                                });
                    }
                }
                function subChild(id)
                {
                    if(id!='') 
                    {
                        
                        var x = document.getElementById(""+id).parentNode.className;
                        var getClassName = x.split(' ');
                        var cat_loc=getClassName[getClassName.length-1];//alert(cat_loc);

                        $('.cat_submit').prop('disabled', true);    
                        $('#'+cat_loc).val(id);
                            $.ajax(
                                {
                                    type: 'POST',
                                    url: '<?php echo BASEURL;?>category/child',
                                    data: 'category_id='+id+'&level='+cat_loc,
                                    success: function(data) {
                                        if(data)
                                        {
                                            if (cat_loc=="col_1") {
                                                $(".col_2").html("");
                                                $(".col_3").html("");
                                                $(".col_4").html("");
                                                $(".col_2").html(data);
                                            }
                                            if (cat_loc=="col_2") {
                                                $(".col_3").html("");
                                                $(".col_4").html("");
                                                $(".col_3").html(data);
                                            }
                                            if (cat_loc=="col_3") {
                                                $(".col_4").html("");
                                                $(".col_4").html(data);
                                            }
                                             
                                        }
                                    }
                                });
                      
                        
                    }
                    

                }
                function selectItem(item)
                {
                    if(item!='') 
                    {

                        var x = document.getElementById(""+item).parentNode.className;
                        var getClassName = x.split(' ');
                        var cat_loc=getClassName[getClassName.length-1];//alert(cat_loc);
                        $('#cid').val(item);
                        
                        $('#'+cat_loc).val(item);

                        if (cat_loc=="col_1") {
                            $(".col_2").html("");
                            $(".col_3").html("");
                            $(".col_4").html("");
                        };
                        if (cat_loc=="col_2") {
                            $(".col_3").html("");
                            $(".col_4").html("");
                        };
                        if (cat_loc=="col_3") {
                            $(".col_4").html("");
                        };


                        $(".child").removeClass('is_select');
                        $("."+item).addClass('is_select');
                        $('.cat_submit').prop('disabled', false);
                    }
                }
                function saveCat()
                {
                    var p_cid_val = document.getElementsByName("p_cid")[0].value;
                    var cat_1_val=document.getElementsByName("col_1")[0].value;
                    var cat_2_val=document.getElementsByName("col_2")[0].value;
                    var cat_3_val=document.getElementsByName("col_3")[0].value;
                    var cat_4_val=document.getElementsByName("col_4")[0].value;
                    var cid=document.getElementsByName("cid")[0].value;
                    $.ajax(
                    {
                        type: 'POST',
                        url: '<?php echo BASEURL;?>postad/adcat',
                        data: 'p_cid='+p_cid_val+'&cat_1='+cat_1_val+'&cat_2='+cat_2_val+'&cat_3='+cat_3_val+'&cat_4='+cat_4_val+'&cid='+cid,
                        success: function(data) {
                            if(data!="error")
                            {
                                window.location.replace('advert');
                                //$(".col_4").html(data);
                            }
                            else
                            {
                                var msgs="<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Please Try Again :(</div>";
                                $(".error_msg").html(msgs);
                            }
                        }
                    });
                    
                }
               
            </script>
            <?php
            break;
            case 'location' :
            ?>
             <script type="text/javascript">
                function getsubcategory(id)
                {
                    if(id!='')
                    {
                    
                        $('#p_lid').val(id);

                        $(".col_1,.col_2,.col_3,.col_4").html("");
                        $('.loc_submit').prop('disabled', true);
                        var cat_loc="col_0";
                        $.ajax(
                                {
                                    type: 'POST',
                                    url: '<?php echo BASEURL;?>location/child',
                                    data: 'loc_id='+id+'&level='+cat_loc,
                                    success: function(data) {
                                        if(data)
                                        {
                                            $(".col_1").html(data); 
                                        }
                                    }
                                });
                    }
                }
                function subChild(id)
                {
                    if(id!='') 
                    {
                        
                        var x = document.getElementById(""+id).parentNode.className;
                        var getClassName = x.split(' ');
                        var cat_loc=getClassName[getClassName.length-1];//alert(cat_loc);

                        $('.loc_submit').prop('disabled', true);    
                        $('#'+cat_loc).val(id);
                            $.ajax(
                                {
                                    type: 'POST',
                                    url: '<?php echo BASEURL;?>location/child',
                                    data: 'loc_id='+id+'&level='+cat_loc,
                                    success: function(data) {
                                        if(data)
                                        {
                                            if (cat_loc=="col_1") {
                                                $(".col_2").html("");
                                                $(".col_3").html("");
                                                $(".col_4").html("");
                                                $(".col_2").html(data);
                                            }
                                            if (cat_loc=="col_2") {
                                                $(".col_3").html("");
                                                $(".col_4").html("");
                                                $(".col_3").html(data);
                                            }
                                            if (cat_loc=="col_3") {
                                                $(".col_4").html("");
                                                $(".col_4").html(data);
                                            }
                                             
                                        }
                                    }
                                });
                      
                        
                    }
                    

                }
                function selectItem(item)
                {
                    if(item!='') 
                    {

                        var x = document.getElementById(""+item).parentNode.className;
                        var getClassName = x.split(' ');
                        var cat_loc=getClassName[getClassName.length-1];//alert(cat_loc);
                        
                        
                        $('#'+cat_loc).val(item);
                        $('#lid').val(item);

                        if (cat_loc=="col_1") {
                            $(".col_2").html("");
                            $(".col_3").html("");
                            $(".col_4").html("");
                        };
                        if (cat_loc=="col_2") {
                            $(".col_3").html("");
                            $(".col_4").html("");
                        };
                        if (cat_loc=="col_3") {
                            $(".col_4").html("");
                        };


                        $(".child").removeClass('is_select');
                        $("."+item).addClass('is_select');
                        $('.loc_submit').prop('disabled', false);
                    }
                }
                function saveloc()
                {
                    var p_lid_val = document.getElementsByName("p_lid")[0].value;
                    var cat_1_val=document.getElementsByName("col_1")[0].value;
                    var cat_2_val=document.getElementsByName("col_2")[0].value;
                    var cat_3_val=document.getElementsByName("col_3")[0].value;
                    var cat_4_val=document.getElementsByName("col_4")[0].value;
                    var lid=document.getElementsByName("lid")[0].value;
                    $.ajax(
                    {
                        type: 'POST',
                        url: '<?php echo BASEURL;?>postad/loc',
                        data: 'p_loc='+p_lid_val+'&loc_1='+cat_1_val+'&loc_2='+cat_2_val+'&loc_3='+cat_3_val+'&loc_4='+cat_4_val+'&lid='+lid,
                        success: function(data) {
                            if(data!="error")
                            {
                                window.location.replace('advert');
                                //$(".col_4").html(data);
                            }
                            else
                            {
                                var msgs="<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Please Try Again :(</div>";
                                $(".error_msg").html(msgs);
                            }
                        }
                    });
                    
                }
               
            </script>
            <?php
            break;
			default :
			
			break;
			
		}
        ?>
        </head>	
    <body>
<header>
            <div class="_gray02">
            <div class="container">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 hidden-xs">&nbsp;</div>
                        
                        <div class="col-lg-6 col-sm-6 col-xs-12 _logpan">
                            <div class="row">
                                <div class="col-lg-2 col-sm-2 col-xs-4 _top5">
                                <?php
                                $ses_id=$this->session->get("user");
                                if (isset($ses_id)) {
                                    echo Phalcon\Tag::linkTo("session/logout", "<span class='glyphicon glyphicon-lock right5'></span>Logout");
                                }
                                else
                                {
                                    echo Phalcon\Tag::linkTo("index/signin", "<span class='glyphicon glyphicon-lock right5'></span>LOGIN");
                                }
                                ?>
                                    
                                </div>
                                
                                <div class="col-lg-3 col-sm-3 col-xs-4 _top5">
                                    <a href="#" title="My Account">
                                        <span class="glyphicon glyphicon-user"></span>
                                        my account
                                    </a>
                                </div>
                                
                                <div class="col-lg-4 col-sm-3 col-xs-4 _top5 _left0">
                                    <a href="#" title="Help &amp; Support">
                                        <span class="glyphicon glypicon-hand"></span>
                                        HELP &amp; SUPPORT  
                                   </a>
                                </div>
                                
                                <div class="col-lg-3 col-sm-3 col-xs-12 pull-right">
                                    <div class="row">
                                        
                                        <?php echo Phalcon\Tag::linkTo(array('postad/', 'free advert', 'class' => 'btn _adbtn'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="clearfix"></div>
        
        <div class="_logopan">
            <div class="container">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-2 col-sm-2 col-xs-12 logo">
                            <?=Phalcon\Tag::linkTo("index", "<img src='".BASEDIR."images/Huxur_logo.png'>")?>
                        </div>
                        
                        <div class="col-lg-3 col-sm-3 col-xs-12 _slog1 slog_2">
                            Discover A Smart <br> Marketplace...
                        </div>  
                        
                        <div class="col-lg-3 col-sm-3 col-xs-12">
                            <div class="row _tag12">
                                <input class="form-control icon2" placeholder="Nationwide" type="text">
                            </div>
                        </div> 
                        
                        <div class="col-lg-3 col-sm-3 col-xs-12">
                            <div class="row _tag12">
                                <input class="form-control icon3" placeholder="92,20,546 Ads near you" type="text">
                            </div>
                        </div> 
                        
                        <div class="col-lg-1 col-sm-1 col-xs-12 _slog1 _sloglast">
                            <div class="row">
                                <button type="submit" class="btn btn-warning glyphicon glyphicon glyphicon-search"></button>
                            </div>
                        </div>                           
                    </div>
                </div>
            </div>
        </div>
        
        <div class="clearfix"></div>
        </header>
        <?php
    }
    
    public static function topBanner(){

        ?>
            <div class="container">
                <div class="col-lg-6 pull-right">
                    <div class="row">
                        <h2>Vivamus et risus a neque rutrum ultricies</h2>
                        <h4>Praesent a ultricies nisl. In auctor leo vitae faucibus porttitor. </h4>
                    </div>
                </div>
            </div>
        <?php
    }

    public static function catMenu(){
        ?>
            <div class="container">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="listicon">
                            <ul>
                              <li><a href="#"><img src="<?=BASEDIR?>images/car-icon.png">Motors</a></li>
                              <li><a href="#"><img src="<?=BASEDIR?>images/tag-icon.png">For Sale</a></li>
                              <li><a href="#"><img src="<?=BASEDIR?>images/home-icon.png">Property</a></li>
                              <li><a href="#"><img src="<?=BASEDIR?>images/tie-icon.png">Jobs</a></li>
                              <li><a href="#"><img src="<?=BASEDIR?>images/setting-icon.png">Services</a></li>
                              <li><a href="#"><img src="<?=BASEDIR?>images/meeting-icon.png">Community</a></li>
                              <li><a href="#"><img src="<?=BASEDIR?>images/pet.png">Pets</a></li>
                          </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }

    public static function bottomMenu(){
        ?>
            <div class="container">
                <div class="col-lg-12">
                    <div class="row listicon_btm">
                        <ul>
                            <li>
                                <a href="#">
                                    Second Hand
                                    <img src="<?=BASEDIR?>images/btmicon1.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Vehicle
                                    <img src="<?=BASEDIR?>images/btmicon2.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Housing
                                    <img src="<?=BASEDIR?>images/btmicon3.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Recruitment
                                    <img src="<?=BASEDIR?>images/btmicon4.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Second Hand
                                    <img src="<?=BASEDIR?>images/btmicon1.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Vehicle
                                    <img src="<?=BASEDIR?>images/btmicon2.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Housing
                                    <img src="<?=BASEDIR?>images/btmicon3.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Recruitment
                                    <img src="<?=BASEDIR?>images/btmicon4.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Second Hand
                                    <img src="<?=BASEDIR?>images/btmicon1.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Vehicle
                                    <img src="<?=BASEDIR?>images/btmicon2.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Housing
                                    <img src="<?=BASEDIR?>images/btmicon3.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Recruitment
                                    <img src="<?=BASEDIR?>images/btmicon4.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Second Hand
                                    <img src="<?=BASEDIR?>images/btmicon1.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Vehicle
                                    <img src="<?=BASEDIR?>images/btmicon2.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Housing
                                    <img src="<?=BASEDIR?>images/btmicon3.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Recruitment
                                    <img src="<?=BASEDIR?>images/btmicon4.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Second Hand
                                    <img src="<?=BASEDIR?>images/btmicon1.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Vehicle
                                    <img src="<?=BASEDIR?>images/btmicon2.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Housing
                                    <img src="<?=BASEDIR?>images/btmicon3.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Recruitment
                                    <img src="<?=BASEDIR?>images/btmicon4.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php
    }
    public static function socialMenu(){
        ?>
            <div class="row">
                <div class="col-lg-4 hidden-xs">&nbsp;</div>
                
                <div class="col-lg-4 center">
                    <label>Follow us on :</label>
                    <span>
                        <a href="#"><img src="<?=BASEDIR?>images/facebook.png" alt=""></a>
                        <a href="#"><img src="<?=BASEDIR?>images/twitter.png" alt=""></a>
                        <a href="#"><img src="<?=BASEDIR?>images/pint.png" alt=""></a>
                        <a href="#"><img src="<?=BASEDIR?>images/gplus.png" alt=""></a>
                    </span>
                </div>
                
                <div class="col-lg-4 hidden-xs">&nbsp;</div>
            </div>
        <?php
    }
    public static function footer($pageaction=''){
        ?>
            <div class="container">
                <div class="col-lg-12">
                    <div class="row">
                        <ul>
                            <li><a href="#">Help &amp; Contact</a></li>
                            <li><a href="#">My Huxur</a></li>
                            <li><a href="#">About Huxur</a></li>
                            <li><a href="#">Partners  For Business</a></li>
                            <li><a href="#">Popular Search</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Cookies Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                        <div class="clearfix height8"></div>
                        
                        <p>© 2014  Huxur  |  All Right Reserved.</p>
                        
                    </div>
                </div>
            </div>  
            <?php 
            echo Tag::javascriptInclude('myadmin/js/jquery.min.js');
            echo Tag::javascriptInclude('js/bootstrap.min.js');
            echo Tag::javascriptInclude('myadmin/jasny-bootstrap/js/jasny-bootstrap.min.js');
        	echo Tag::javascriptInclude('js/modernizr.custom.17475.js');
        	echo Tag::javascriptInclude('js/jquery.elastislide.js');
        	echo Tag::javascriptInclude('js/jquery.elastislide1.js');
        ?>
        <!-- Include all compiled plugins (below), or include individual files as needed -->

        <script type="text/javascript">
            
            $( '#carousel' ).elastislide();
            $( '#carouselL' ).elastislide();
            $( '#carouselR' ).elastislide();
            $( '#carouselM' ).elastislide();
            
        </script>
    </body>  
        <?php
    }
    public static function disclaimer($translate){
        /*if(!Phalcon\Session::get('disclaimer')){
            echo '<div class="alert alert-info">
            <a class="close" data-dismiss="alert" href="#">×</a>
            ', $translate->_('disclaimer', array(
                'framework' => '<a href="http://phalconphp.com">Phalcon PHP Framework</a>',
                'official' => '<a href="https://www.php.net">'.$translate['accessOf'].'</a>'
            )), '
            </div>';
            Phalcon\Session::set('disclaimer', true);
        }*/
    }
}
