<?php
class AdminController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    public function indexAction(){
        $response = new \Phalcon\Http\Response();
        if(!$this->session->has("admin"))
        {  
            return $response->redirect("admin/login");
        }
        $dnote = MasterDnote::find("status=1 AND type='2'");
        $this->view->setVar("dnote", $dnote);
        $enote = MasterDnote::find("status=1 AND type='1'");
        $this->view->setVar("enote", $enote);
    }
    public function loginAction(){
        $response = new \Phalcon\Http\Response();
        if($this->session->has("admin"))
        {
            return $response->redirect("admin");
        }
        if ($this->request->isPost()) {
            //echo"<pre>";print_r($this->request->getPost());exit();
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');
            //
            $admin = MasterAdminUsers::findFirst(array("mail_id = :email: AND pass=:password: AND is_active=:is_active:",'bind' => array('email' => $email,"password"=>base64_encode($password), "is_active"=>1)));
            
            if ($admin) {
                
                $id=$admin->aid;
                $name=$admin->first_name." ".$admin->last_name;
                $type = ($admin->type=='3')?'superadmin':'admin';
                $role = MasterRole::findFirst("id='".$admin->type."' ");
                $this->session->set('admin', array(
                    'id' => $id,
                    'type' => $type,
                    'name' => $name,
                    'role_name' => $role->role_name,
                    'access_role' => $role->role_access,
                    'role' => $role->role
                ));
                return $response->redirect("admin");
            }
            else{
                $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Invalid Login Credential. Please try again with valid login id and password !</div>");
                return $response->redirect("admin/login");
            }
        }
    }
    public function endAction()
    {
        $response = new \Phalcon\Http\Response();
        $this->session->remove('admin');
        $this->flash->success('Goodbye!');
        return $response->redirect("admin/login");
    }
    public function signupAction(){

    }

    function serrorAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")){
            $user_session = $this->session->get("user");
        }else{
            return $response->redirect("admin");
        }
        
        $error = MasterDnote::findFirst("id='".$id."'");
        if($error){
            $postVal = $this->request->getPost();
            $postVal['user_id'] = $user_session['id'];
            $postVal['deid'] = $id;
            $postVal['type'] = 1;
            $error = new MasterSolved();
            if($error->save($postVal)){
                $phql = "UPDATE MasterDnote SET status = 0 where id=".$id."";
                $status = $this->modelsManager->executeQuery($phql);
                if($status){
                    return $response->redirect("admin");
                }
            };

        }
    }

    function sdoubtAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")){
            $user_session = $this->session->get("user");
        }else{
            return $response->redirect("admin");
        }
        
        $error = MasterDnote::findFirst("id='".$id."'");
        if($error){
            $postVal = $this->request->getPost();
            $postVal['user_id'] = $user_session['id'];
            $postVal['deid'] = $id;
            $postVal['type'] = 2;
            $error = new MasterSolved();
            if($error->save($postVal)){
                $phql = "UPDATE MasterDnote SET status = 0 where id=".$id."";
                $status = $this->modelsManager->executeQuery($phql);
                if($status){
                    return $response->redirect("admin");
                }
            };

        }
    }

}
