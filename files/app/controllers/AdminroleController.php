<?php
class AdminroleController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    function indexAction()
    {
        $role = MasterRole::find();
        $this->view->setVar("role", $role);
    }

    function addAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $postval['role_access'] = json_encode($postval['role_access']);
            $postval['role'] = json_encode($postval['role']);
            $adminrole = new MasterRole();
            $adminrole->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Admin Role Successfully Add</div>");
            return $response->redirect("adminrole");
        }
    }
    function updateAction($id){
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {//echo $id;
            $data = $this->request->getPost();
            $postval['id'] = $data['id'];
            $postval['role_name'] = $data['role_name'];
            $postval['role_access'] = json_encode($data['role_access']);
            $postval['role'] = json_encode($data['role']);
            $Addsite = new MasterRole();
            $Addsite->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Role Updated Successfully</div>");
            return $response->redirect("adminrole");
        }
        if ($id != '') {
            $urole = MasterRole::findFirst("id='" . $id . "'");
            $this->view->setVar("urole", $urole);
        }
    }
    
}
?>