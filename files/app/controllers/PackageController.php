<?php
class PackageController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Discover A Smart Marketplace... ');
        parent::initialize();
    }

    function selectpackageAction()
    {
    	$response = new \Phalcon\Http\Response();
    	if($this->request->isPost()){
            $postval = $this->request->getPost();
	        if ($this->session->has("user")) {
	            $user_session = $this->session->get("user");
	            $this->view->setVar("user_session", $user_session);
	        }else{
	            return $response->redirect("index");
	        }
            $url = $postval['url'];
            $package = MasterPackages::findFirst(array("id='".$postval['packageid']."'"));
            $point = MasterUserpoint::findFirst("uid='{$user_session['id']}'");
            if($package->price<$point->points || $package->price==$point->points){
                $points = $point->points-$package->price;
                $pdata = ["uid"=>$point->uid,"points"=>$points];
                $mup = new MasterUserpoint();
                $mup->save($pdata);
            }else{
                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>You Don't Have sufficient Points Please purchese some points.</div>");
                return $response->redirect($url);
            }
            $postval['userid'] = $user_session['id'];
            $postval['nots'] = $package->not;
            $postval['test_type'] = $package->test_type;
            
           	$userpackage = new MasterUserPackage();
            $userpackage->save($postval);
            return $response->redirect($url);
        }
        exit();
    }
}
?>