<?php

class QuestionbankController extends ControllerBase
{
	public function initialize() 
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
     * Question View Part Only retrive data from database and show the data.
     * @return [array] [Fetched Data]
     * @return Rajesh
     */
    function indexAction()
    {
        //$data = MasterQuestionBank::find(array());
        $data=UIElementsAdmin::getpagination("MasterQuestionBank",'question');
        // $data=$this->getpagination("MasterQuestionBank");
        $this->view->setVar("questions", $data);
        $qtype = MasterQuesType::find(array());
        $this->view->setVar("qtype", $qtype);
    }
    /**
     * Add questions
     * @return [array] [Return array]
     * @author Rajesh
     */
    function addquestionAction()
    {
        $response = new \Phalcon\Http\Response();
        $table = 1;
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['e_type'] = json_encode($postval['e_type']);
            $postval['cat'] = json_encode($val);
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['created'] = date("Y-m-d h:i:s");
            $questionid = MasterQuestion::find()->getLast();
            $questionid = str_split($questionid->questionid,3);
            $questionid = $questionid['2']+1; $questionid = 'QID000'.$questionid;
            $qid = MasterQuestionBank::find()->getLast();
            $qid = $qid->qid+1;
            $postval['qusid'] = $qid;
            $postval['questionid'] = $questionid;
            $postval['tableid'] = 1;
            $update= new MasterQuestionBank();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Added</div>");
            return $response->redirect("questionbank");
        }
        $questype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $questype);
        $quessource = MasterQuestionSource::find(array("status=1"));
        $this->view->setVar("qsources", $quessource);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }
    /**
     * Update questions
     * @param  [int] $id [Question id]
     * @return [massage]     [Success Massage]
     * @author Rajesh
     */
    function updatequestionAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['e_type'] = json_encode($postval['e_type']);
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['modified'] = date("Y-m-d h:i:s");
            $user_session = $this->session->get('admin');
            $postval['modified_by'] = $user_session['id'];
            $postval['qusid'] = $postval['qid'];
            $postval['tableid'] = 1;
            $qid = MasterQuestion::findFirst(array("questionid='".$postval['questionid']."'"));
            $postval['id'] = $qid->id;
            $update= new MasterQuestionBank();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Update</div>");
            return $response->redirect("questionbank");
        }
        if($id!="")
        {
            $questype = MasterQuesType::find(array("status=1"));
            $this->view->setVar("qtype", $questype);
            $quessource = MasterQuestionSource::find(array("status=1"));
            $this->view->setVar("qsources", $quessource);
            $getSub = MasterSubject::find(array());
            $this->view->setVar("subdet", $getSub);
            $question = MasterQuestionBank::findFirst(array("qid='".$id."'"));
            $this->view->setVar("question", $question);
            $mquestion = MasterQuestion::findFirst(array("questionid='".$question->questionid."'"));
            $this->view->setVar("mquestion", $mquestion);
        }else{
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something Wrong. Please Try again.</div>");
            return $response->redirect("questionbank");
        }
    }
    /**
     * This funtion is for delete question
     * @param  [int] $id [Question id]
     * @return [return Status]     [return Status]
     * @author Rajesh
     */
    function deletequestionAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterQuestionBank WHERE qid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Deleted Successfully</div>");
            return $response->redirect("questionbank");
        }
    }
    /**
     * Update Question Status
     * @param  [int] $u_status [status code]
     * @param  [int] $id       [Question id]
     * @return [status]           [Return status]
     * @author Rajesh
     */
    function updqbstatusAction($u_status,$id)
    {
    	$response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterQuestionBank SET status = 0 where qid=".$id."";
            }else{
                $phql = "UPDATE MasterQuestionBank SET status = 1 where qid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Status Updated Successfully</div>");
            return $response->redirect("questionbank");
        }
    }
    function addmmquestionAction()
    {
        $response = new \Phalcon\Http\Response();        
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['created'] = date("Y-m-d h:i:s");
            $questionid = MasterQuestion::find()->getLast();
            $questionid = str_split($questionid->questionid,3);
            $questionid = $questionid['2']+1; $questionid = 'QID000'.$questionid;
            $qid = MasterMatchQuestion::find()->getLast();
            $qid = $qid->qid+1;
            $postval['qusid'] = $qid;
            $postval['questionid'] = $questionid;
            $postval['subject_path'] = $postval['cat'];
            $postval['tableid'] = 2;
            $postval['e_type'] = json_encode(array($postval['e_type']));
            $update= new MasterMatchQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Match type Question Successfully Added</div>");
            return $response->redirect("questionbank");
        }
        $questype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $questype);
        $quessource = MasterQuestionSource::find(array("status=1"));
        $this->view->setVar("qsources", $quessource);
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
        $examtype = MasterExamType::find(array("status=1"));
        $this->view->setVar("examtype", $examtype);
    }
    function viewmmquestionAction()
    {
        // $data = MasterMatchQuestion::find(array());
        $data=UIElementsAdmin::getpagination("MasterMatchQuestion",'question');
        $this->view->setVar("mquestions", $data);
        $qtype = MasterQuesType::find(array());
        $this->view->setVar("qtype", $qtype);
    }
    function fullmatchquestionAction()
    {

    }
    function updmmquestionAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['qusid'] = $postval['qid'];
            $postval['modified'] = date('Y-m-d h:i:s');
            $user_session = $this->session->get('admin');
            $postval['modified_by'] = $user_session['id'];
            $postval['tableid'] = 2;
            $qid = MasterQuestion::findFirst(array("questionid='".$postval['questionid']."'"));
            $postval['id'] = $qid->id;
            $update= new MasterMatchQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Update</div>");
            return $response->redirect("questionbank/viewmmquestion");
        }
        if($id!="")
        {
            $questype = MasterQuesType::findFirst(array("qtid=5 AND status=1"));
            $this->view->setVar("qtype", $questype);
            $quessource = MasterQuestionSource::find(array("status=1"));
            $this->view->setVar("qsources", $quessource);
            $getSub = MasterSubject::find(array("status=1"));
            $this->view->setVar("subdet", $getSub);
            $question = MasterMatchQuestion::findFirst(array("qid='".$id."'"));
            $this->view->setVar("question", $question);
            $mquestion = MasterQuestion::findFirst(array("questionid='".$question->questionid."'"));
            $this->view->setVar("mquestion", $mquestion);
        }else{
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something Wrong. Please Try again.</div>");
            return $response->redirect("questionbank");
        }
    }

    function viewquestionAction($id){
        $question = MasterQuestionBank::findFirst(array("qid"=>$id));
        $this->view->setVar("question", $question);
    }

    function raquestionAction(){
        $data=UIElementsAdmin::getpagination("MasterReasonQuestion",'question');
        // $data = MasterReasonQuestion::find(array());
        $this->view->setVar("questions", $data);
        $qtype = MasterQuesType::find(array());
        $this->view->setVar("qtype", $qtype);
    }

    function addraquestionAction()
    {
        $response = new \Phalcon\Http\Response();
        // $table = 1;
        if($this->request->isPost()){ 
            $postval = $this->request->getPost();
            $postval['e_type'] = json_encode($postval['e_type']);
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['created'] = date("Y-m-d h:i:s");
            $questionid = MasterQuestion::find()->getLast();
            $questionid = str_split($questionid->questionid,3);
            $questionid = $questionid['2']+1; $questionid = 'QID000'.$questionid;
            $qid = MasterReasonQuestion::find()->getLast();
            $qid = $qid->qid+1;
            $postval['qusid'] = $qid;
            $postval['questionid'] = $questionid;
            $postval['tableid'] = 5;
            $update= new MasterReasonQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Added</div>");
            return $response->redirect("questionbank/raquestion");
        }
        $questype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $questype);
        $quessource = MasterQuestionSource::find(array("status=1"));
        $this->view->setVar("qsources", $quessource);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }
    /**
     * Update Reason questions
     * @param  [int] $id [Question id]
     * @return [massage]     [Success Massage]
     * @author Rajesh
     */
    function updateraquestionAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['modified'] = date("Y-m-d h:i:s");
            $user_session = $this->session->get('admin');
            $postval['modified_by'] = $user_session['id'];
            $postval['qusid'] = $postval['qid'];
            $postval['tableid'] = 5;
            $qid = MasterQuestion::findFirst(array("questionid='".$postval['questionid']."'"));
            $postval['id'] = $qid->id;
            $update= new MasterReasonQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Update</div>");
            return $response->redirect("questionbank/raquestion");
        }
        if($id!="")
        {
            $questype = MasterQuesType::find(array("status=1"));
            $this->view->setVar("qtype", $questype);
            $quessource = MasterQuestionSource::find(array("status=1"));
            $this->view->setVar("qsources", $quessource);
            $getSub = MasterSubject::find(array());
            $this->view->setVar("subdet", $getSub);
            $question = MasterReasonQuestion::findFirst(array("qid='".$id."'"));
            $this->view->setVar("question", $question);
            $mquestion = MasterQuestion::findFirst(array("questionid='".$question->questionid."'"));
            $this->view->setVar("mquestion",$mquestion);
        }else{
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something Wrong. Please Try again.</div>");
            return $response->redirect("questionbank");
        }
    }
    /**
     * This funtion is for delete Reason question
     * @param  [int] $id [Question id]
     * @return [return Status]     [return Status]
     * @author Rajesh
     */
    function deleteraquestionAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterReasonQuestion WHERE qid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Reason Question Deleted Successfully</div>");
            return $response->redirect("questionbank/raquestion");
        }
    }
    // 
    // 
    // Ajax call Start here....
    // 
    // 
    function getsubsubjectAction()
    {
        $getVal = $this->request->getPost();
        $getSubSub = MasterSubSubject::find(array("subid='".$getVal['subjectid']."' "));
        echo '<option value="">[ Select Sub Subject ]</option>';
        foreach($getSubSub as $value){ ?>
        <option value="<?=$value->ssid?>"><?=$value->ssname?></option>
        <?php }
        exit();
    }
    function gettopicsAction()
    {
        $getVal = $this->request->getPost();
        $gettopics = MasterTopics::find(array("subid='".$getVal['subjectid']."' AND ssubid='".$getVal['ssubid']."' "));
        echo '<option value="">[ Select Topics ]</option>';
        foreach($gettopics as $value){ ?>
        <option value="<?=$value->tid?>"><?=$value->tname?></option>
        <?php }
        exit();
    }
    function getsubtopicsAction()
    {
        $getVal = $this->request->getPost();
        $getsubtopics = MasterSubTopics::find(array("subid='".$getVal['subjectid']."' AND ssubid='".$getVal['ssubid']."' AND tid='".$getVal['topics']."' "));
        echo '<option value="">[ Select Sub Topics ]</option>';
        foreach($getsubtopics as $value){ ?>
        <option value="<?=$value->stid?>"><?=$value->stname?></option>
        <?php }
        exit();
    }
    function getanswerAction()
    {
        $getVal = $this->request->getPost();
        echo '<option value="">[ Select Answer ]</option>';
        if($getVal['q_type']==2){ ?>
        <option value="A">A</option>
        <option value="B">B</option>
        <option value="C">C</option>
        <option value="D">D</option>
        <?php }elseif($getVal['q_type']==3){ ?>
        <option value="AB">AB</option>
        <option value="AC">AC</option>
        <option value="AD">AD</option>
        <option value="BC">BC</option>
        <option value="BD">BD</option>
        <?php }
        exit();
    }

    function testAction(){

    }
    
}
?>