<?php
class BookmarksController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize(); 
    }
    //====//
    function bookAction()
    {
        if($this->request->isPost()){
            $postVal = $this->request->getPost();
            $qid = $postVal['qid'];
            $user = $this->session->get("user");
            $userid = $user['id'];
            $type = $postVal['type'];
            if($qid!=''){
                $users = MasterBookmarks::findFirst(array("user_id='".$userid."' AND questionid='".$qid."' AND type='1'"));
                if(isset($users->id)!=''){
                    $phql = "DELETE FROM MasterBookmarks WHERE id='$users->id'";
                    $this->modelsManager->executeQuery($phql);
                    echo 'Successfully Deleted';
                }else{
                    $phql = "INSERT INTO MasterBookmarks (user_id,questionid,type) VALUES ('$userid','$qid','$type') ";
                    $this->modelsManager->executeQuery($phql);
                    echo 'Successfully Entry';
                }
                //replace with new notification with prevoius notification
                $link = base64_encode($qid); // addslashes(htmlentities(BASEURL . "question/challenged_question/" . $courses->quesid));
                $chk_like = MasterNotification::findFirst(array("uid='{$userid}' AND notification_type=3 AND link='{$link}'"));
                $cne = $this->modelsManager->executeQuery($rep_sql);
                if (empty($chk_like->id)) {
                    $rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$userid}' AND notification_type=3 GROUP BY add_date ORDER BY add_date ASC";
                    $cne = $this->modelsManager->executeQuery($rep_sql);
                    $totcnt = $cne->count();
                    $add_date = date("Y-m-d H:i:s");
                    $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$userid}' AND notification_type=3 ORDER BY add_date ASC LIMIT 0,1";
                    $dele = $this->modelsManager->executeQuery($del_sql);
                    if ($totcnt > 1) {
                        foreach ($dele as $deval) {
                            $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                            $this->modelsManager->executeQuery($deldate);
                        }
                    }
                    //add notificatio table
                    $message = "like a question";
                    $sqlnot = "INSERT INTO  MasterNotification (uid,message,add_date,status,type,link,notification_type) VALUES ('{$userid}','{$message}','{$add_date}','1','1','{$link}',3)";
                    $status = $this->modelsManager->executeQuery($sqlnot);
                }
            }else{
                echo 'Error';
            }
        }
        exit();
    }

    function attemptAction()
    {
        if($this->request->isPost()){
            $postVal = $this->request->getPost();
            $qid = $postVal['qid'];
            $notication = $postVal['notication'];
            $chkopt = $postVal['giveans'];
            $checkid = $postVal['checkid'];
            $user = $this->session->get("user");
            $userid = $user['id'];
            
            if(!empty($notication) && !empty($chkopt)){
                //replace with new notification with prevoius notification
                    $add_date = date("Y-m-d H:i:s");
                if ($userid != $notication) {
                    $rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$userid}' AND notification_type=11 GROUP BY add_date ORDER BY add_date ASC";
                    $cne = $this->modelsManager->executeQuery($rep_sql);
                    $totcnt = $cne->count();
                    $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$userid}' AND notification_type=11 ORDER BY add_date ASC LIMIT 0,1";
                    $dele = $this->modelsManager->executeQuery($del_sql);
                    if ($totcnt > 1) {
                        foreach ($dele as $deval) {
                            $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                            $this->modelsManager->executeQuery($deldate);
                        }
                    }
                    //add notification tabel
                    $message = "comment on a challenged question";
                    $link = base64_encode($qid); // addslashes(htmlentities(BASEURL . "question/challenged_question/" . base64_encode($qid)));
                    $sqlnot = "INSERT INTO  MasterNotification (uid,friend_id,message,add_date,status,type,link,notification_type) VALUES ('{$userid}','{$notication}','{$message}','{$add_date}','1','1','$link','11')";
                    $getnot = $this->modelsManager->executeQuery($sqlnot);
                }
                                $anscm = "INSERT INTO  MasterChallengeans (uid,question_id,optionans,add_date) VALUES ('{$userid}','{$qid}','{$chkopt}','{$add_date}')";
                                $ansnot = $this->modelsManager->executeQuery($anscm);
            }
            // echo $userid.' '.$qid.' '.$checkid;exit();
            if($qid!=''){
                $users = MasterQattempt::findFirst(array("user_id='".$userid."' AND questionid='".$qid."'"));
                if(isset($users->id)!=''){
                    $phql = "UPDATE MasterQattempt SET checks='$checkid' WHERE id='$users->id'";
                    $this->modelsManager->executeQuery($phql);
                    echo 'Successfully Deleted';
                }else{
                    $phql = "INSERT INTO MasterQattempt (user_id,questionid,checks) VALUES ('$userid','$qid','$checkid') ";
                    $this->modelsManager->executeQuery($phql);
                    echo 'Successfully Entry';
                }
            }else{
                echo 'Error';
            }
        }
        exit();
    }

    function snoteAction($id='')
    {
        if($this->request->isPost()){
            $postVal = $this->request->getPost();
            $qid = $postVal['qid'];
            $note = $postVal['note'];
            $user = $this->session->get("user");
            $type = $postVal['type'];
            $userid = $user['id'];
            if($qid!=''){
                $phql = "INSERT INTO MasterSnote (user_id,questionid,note,type) VALUES ('$userid','$qid','$note','$type') ";
                $this->modelsManager->executeQuery($phql);
                echo 'Successfully Entry';
            }
        }
        if(isset($id) && $id==''){
            $phql = "DELETE FROM MasterSnote WHERE id='$id'";
            $this->modelsManager->executeQuery($phql);
            echo 'Successfully Deleted';
        }
        exit();
    }
    function dnoteAction($id='')
    {
        if($this->request->isPost()){
            $postVal = $this->request->getPost();
            $qid = $postVal['qid'];
            $note = $postVal['note'];
            $type = 2;
            $user = $this->session->get("user");
            $userid = $user['id'];
            if($qid!=''){
                $phql = "INSERT INTO MasterDnote (user_id,questionid,note,type) VALUES ('$userid','$qid','$note','$type') ";
                $this->modelsManager->executeQuery($phql);
                echo 'Successfully Entry';
            }
        }
        if(isset($id) && $id==''){
            $phql = "DELETE FROM MasterSnote WHERE id='$id'";
            $this->modelsManager->executeQuery($phql);
            echo 'Successfully Deleted';
        }
        exit();
    }
    function enoteAction($id='')
    {
        if($this->request->isPost()){
            $postVal = $this->request->getPost();
            $qid = $postVal['qid'];
            $note = $postVal['note'];
            $type = 1;
            $user = $this->session->get("user");
            $userid = $user['id'];
            if($qid!=''){
                $phql = "INSERT INTO MasterDnote (user_id,questionid,note,type) VALUES ('$userid','$qid','$note','$type') ";
                $this->modelsManager->executeQuery($phql);
                echo 'Successfully Entry';
            }
        }
        if(isset($id) && $id==''){
            $phql = "DELETE FROM MasterSnote WHERE id='$id'";
            $this->modelsManager->executeQuery($phql);
            echo 'Successfully Deleted';
        }
        exit();
    }
    function notificationAction() {
        if ($this->request->isPost()) {
            $postVal = $this->request->getPost();
            $qid = $postVal['qid'];
            $notication = $postVal['notication'];
            $chkopt = $postVal['giveans'];
            $user = $this->session->get("user");
            $userid = $user['id'];
            if (!empty($notication) && !empty($chkopt)) {
                //replace with new notification with prevoius notification
                $add_date = date("Y-m-d H:i:s");
                 if ($userid != $notication) {
                $rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$userid}' AND notification_type=11 GROUP BY add_date ORDER BY add_date ASC";
                $cne = $this->modelsManager->executeQuery($rep_sql);
                $totcnt = $cne->count();
                $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$userid}' AND notification_type=11 ORDER BY add_date ASC LIMIT 0,1";
                $dele = $this->modelsManager->executeQuery($del_sql);
                if ($totcnt > 1) {
                    foreach ($dele as $deval) {
                        $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                        $this->modelsManager->executeQuery($deldate);
                    }
                }
                //add notification tabel
                $message = "comment on a challenged question";
                $link =base64_encode($qid); // addslashes(htmlentities(BASEURL . "question/challenged_question/" . base64_encode($qid)));
                $sqlnot = "INSERT INTO  MasterNotification (uid,friend_id,message,add_date,status,type,link,notification_type) VALUES ('{$userid}','{$notication}','{$message}','{$add_date}','1','1','$link','11')";
                $getnot =  $this->modelsManager->executeQuery($sqlnot);
                 }
                $anscm = "INSERT INTO  MasterChallengeans (uid,question_id,optionans,add_date) VALUES ('{$userid}','{$qid}','{$chkopt}','{$add_date}')";
                $ansnot = $this->modelsManager->executeQuery($anscm);
            }
        }
    }

    function booksAction()
    {
        if($this->request->isPost()){
            $postVal = $this->request->getPost();
            $qid = $postVal['qid'];
            $user = $this->session->get("user");
            $userid = $user['id'];
            $type = $postVal['type'];
            if($qid!=''){
                $users = MasterBookmarks::findFirst(array("user_id='".$userid."' AND questionid='".$qid."'"));
                if(isset($users->id)!=''){
                    $phql = "DELETE FROM MasterBookmarks WHERE id='$users->id'";
                    $this->modelsManager->executeQuery($phql);
                    echo 'Successfully Deleted';
                }else{
                    $phql = "INSERT INTO MasterBookmarks (user_id,questionid,type) VALUES ('$userid','$qid','$type') ";
                    $this->modelsManager->executeQuery($phql);
                    echo 'Successfully Entry';
                }
            }else{
                echo 'Error';
            }
        }
        exit();
    }
    
}
?>