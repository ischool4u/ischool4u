<?php

class SitesettingController extends ControllerBase {

    public function initialize() {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    public function indexAction() {

        $data = SiteSetting::find();
        $this->view->setVar("siteinfo", $data);
    }

    public function updateAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $path = rand(1, 1000) . $file->getName();
                    $newpath = 'files/' . $path;
                    $file->moveTo($newpath);
                }
            } else {
                if ($postval['id_setting']) {
                    $data = SiteSetting::findFirst(array("id_setting='" . $postval['id_setting'] . "'"));
                    $path = $data['logo'];
                } else {
                    $path = '';
                }
            }
            $postval['logo'] = $path;
            $Addsite = new SiteSetting();
            $Addsite->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Site Configured Successfully</div>");
            return $response->redirect("sitesetting");
        }
    }

    function questionofthedayAction() {
        if ($this->request->isGet()) {
            $getVal = $this->request->get();
            if (isset($getVal['submit'])) {
                $where = "LOCATE ('1',e_type) ";
                if ($getVal['subject'] == '') {
                    unset($getVal['subject']);
                } else {
                    $where .= " AND subject='" . $getVal['subject'] . "' ";
                    $subsubdet = MasterSubSubject::find("subid = '" . $getVal['subject'] . "' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if ($getVal['subsubject'] == '') {
                    unset($getVal['subsubject']);
                } else {
                    $where .= " AND subsubject='" . $getVal['subsubject'] . "' ";
                    $mTopicdet = MasterTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if ($getVal['topics'] == '') {
                    unset($getVal['topics']);
                } else {
                    $where .= " AND topics='" . $getVal['topics'] . "' ";
                    $msTopicdet = MasterSubTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND tid='" . $getVal['topics'] . "' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if ($getVal['subtopics'] == '') {
                    unset($getVal['subtopics']);
                } else {
                    $where .= " AND subtopics='" . $getVal['subtopics'] . "' ";
                };
                if ($getVal['qtype'] == '') {
                    unset($getVal['qtype']);
                } else {
                    if ($where == '') {
                        $where .= "q_type='" . $getVal['qtype'] . "' ";
                    } else {
                        $where .= " AND q_type='" . $getVal['qtype'] . "' ";
                    }
                };
                if ($getVal['qlevel'] == '') {
                    unset($getVal['qlevel']);
                } else {
                    if ($where == '') {
                        $where .="q_level='" . $getVal['qlevel'] . "'";
                    } else {
                        $where .=" AND q_level='" . $getVal['qlevel'] . "' ";
                    }
                };
                unset($getVal['_url'], $getVal['submit']);
                if ($where == '') {
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion';
                } else {
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion WHERE ' . $where;
                }
                $question = $this->modelsManager->executeQuery($phql);
                $this->view->setVar("question", $question);
            }
            $qoftype = QuestionOfday::find();
            $ap = '';
            foreach ($qoftype as $value) {
                $ap[$value->questionid] = date("d-m-Y", strtotime($value->show_date));
            }
            $this->view->setVar("ap", $ap);
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
        $qtype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $qtype);
    }

    function savedateAction() {
        $postVal = $this->request->getPost();
        $date = date("Y-m-d", strtotime($postVal['date']));
        $sat = QuestionOfday::findFirst(array("show_date='" . $date . "' AND questionid!='" . $postVal['qid'] . "'"));
        if (isset($sat->id_qday) != '') {
            echo 1;
            exit;
        }
        $users = QuestionOfday::findFirst(array("questionid='" . $postVal['qid'] . "'"));
        if (isset($users->id_qday) != '') {
            $phql = "UPDATE  QuestionOfday SET show_date='$date' WHERE id_qday='$users->id_qday'";
            $this->modelsManager->executeQuery($phql);
            echo $postVal['date'];
            exit;
        } else {
            $phql = "INSERT INTO QuestionOfday (questionid,show_date) VALUES ('{$postVal['qid']}','$date') ";
            $this->modelsManager->executeQuery($phql);
            echo $postVal['date'];
            exit;
        }
    }

    function quizlistAction() {
        if ($this->request->isGet()) {
            $getVal = $this->request->get();
            if (isset($getVal['submit'])) {
                $where = "LOCATE ('1',e_type) ";
                if ($getVal['subject'] == '') {
                    unset($getVal['subject']);
                } else {
                    $where .= " AND subject='" . $getVal['subject'] . "' ";
                    $subsubdet = MasterSubSubject::find("subid = '" . $getVal['subject'] . "' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if ($getVal['subsubject'] == '') {
                    unset($getVal['subsubject']);
                } else {
                    $where .= " AND subsubject='" . $getVal['subsubject'] . "' ";
                    $mTopicdet = MasterTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if ($getVal['topics'] == '') {
                    unset($getVal['topics']);
                } else {
                    $where .= " AND topics='" . $getVal['topics'] . "' ";
                    $msTopicdet = MasterSubTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND tid='" . $getVal['topics'] . "' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if ($getVal['subtopics'] == '') {
                    unset($getVal['subtopics']);
                } else {
                    $where .= " AND subtopics='" . $getVal['subtopics'] . "' ";
                };
                if ($getVal['qtype'] == '') {
                    unset($getVal['qtype']);
                } else {
                    if ($where == '') {
                        $where .= "q_type='" . $getVal['qtype'] . "' ";
                    } else {
                        $where .= " AND q_type='" . $getVal['qtype'] . "' ";
                    }
                };
                if ($getVal['qlevel'] == '') {
                    unset($getVal['qlevel']);
                } else {
                    if ($where == '') {
                        $where .="q_level='" . $getVal['qlevel'] . "'";
                    } else {
                        $where .=" AND q_level='" . $getVal['qlevel'] . "' ";
                    }
                };
                unset($getVal['_url'], $getVal['submit']);
                if ($where == '') {
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion';
                } else {
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion WHERE ' . $where;
                }
                $question = $this->modelsManager->executeQuery($phql);
                $this->view->setVar("question", $question);
            }
            $qoftype = MasterQuiz::find();
            $ap = '';
            foreach ($qoftype as $value) {
                $ap[$value->questionid] = 1;
            }
            $this->view->setVar("ap", $ap);
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
        $qtype = MasterQuesType::find(array("status=1 AND qtid='1'"));
        $this->view->setVar("qtype", $qtype);
        $course = MasterCourse::find(array("status='1'"));
        $this->view->setVar("course", $course);
    }

    function removequizAction() {
        $postVal = $this->request->getPost();
        $phql = "DELETE FROM  MasterQuiz WHERE questionid='{$postVal['qid']}'";
        $this->modelsManager->executeQuery($phql);
        echo 1;
        exit;
    }

    function addquizAction($id='') {
        $response = new \Phalcon\Http\Response();
        $postVal = $this->request->getPost();
        $postVal['questionid'] = json_encode($postVal['qid']);
        $postVal['start_date'] = date("Y-m-d", strtotime($postVal['start_date']));
        $postVal['end_date'] = date("Y-m-d", strtotime($postVal['end_date']));
        $postVal['status'] = 1;
        if ($id != '') {
            $postVal['quiz_id'] = $id;
        }
        $con = new MasterQuiz();
        $con->save($postVal);
        return $response->redirect("sitesetting/viewqizqus");
    }

    function viewqodAction() {
        $qod = QuestionOfday::find("status='1' ORDER BY id_qday DESC");
        foreach ($qod as $qval) {
            $qids[] = $qval->questionid;
        }
        $qids = implode("','", $qids);
        $mq = MasterQuestion::find("questionid IN ('{$qids}')");
        foreach ($mq as $key => $value) {
            $mqid[$value->questionid] = ["table" => $value->tableid];
        }

        $this->view->setVar("qod", $qod);
        $this->view->setVar("qids", $mqid);
    }

    function viewqizqusAction() {
        $mqus = MasterQuiz::find("status='1' ORDER BY quiz_id DESC");
        $course = MasterCourse::find("status='1'");
        foreach ($course as $key => $value) {
            $array[$value->id] = $value->name;
        }
        $this->view->setVar("mqus", $mqus);
        $this->view->setVar("course", $array);
        $this->view->setVar("qids", $mqid);
    }

    function viewqqusAction($id='') {
        $response = new \Phalcon\Http\Response();
        if ($id != '') {
            $mqus = MasterQuiz::findFirst("quiz_id='{$id}' AND status='1'");
            $qod = json_decode($mqus);
            foreach ($qod as $qval) {
                $qids[] = $qval->questionid;
            }
            $qids = implode("','", $qids);
            $mq = MasterQuestion::find("questionid IN ('{$qids}')");
            $this->view->setVar("mqus", $mqus);
            $this->view->setVar("mq", $mq);
        } else {
            return $response->redirect("sitesetting/viewqizqus");
        }
    }

}

?>