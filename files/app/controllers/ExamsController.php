<?php
class ExamsController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
     * Show all Exam types
     * @return [result] [Result from database and show]
     * @author Rajesh Sardar
     */
    function indexAction()
    {
        $data = MasterExamType::find();
        $this->view->setVar("exam_type", $data);
    }
    /**
     * Add Exam type
     * @return [status] [Return status whether exam type is added or not]
     * @author Rajesh Sardar
     */
    function addexamtypeAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $postval['q_type'] = json_encode($postval['q_type']);
            $postval['course'] = json_encode($postval['course']);
            $postval['ql_easy'] = json_encode($postval['ql_easy']);
            $postval['ql_medium'] = json_encode($postval['ql_medium']);
            $postval['ql_hard'] = json_encode($postval['ql_hard']);
            $postval['date'] = preg_replace("(/)", "-", $copy_date);
            $Addques= new MasterExamType();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Successfully Add</div>");
            return $response->redirect("exams");
        }
        $supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("qtype", $qtype);
    }
    /**
     * Update Detail of perticular exam type
     * @param  [int] $id [Exam type id]
     * @return [return]     [Return update status]
     * @author Rajesh Sardar
     */
    function updexamtypeAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $exam_type = MasterExamType::findFirst(array("etid='".$id."'"));
            $this->view->setVar("exam_type", $exam_type);
            $supcat = MasterSupCat::find("status=1");
            $this->view->setVar("supcat", $supcat);
            $course = MasterCourse::find("scid='".$exam_type->sup_cat."' AND status=1");
            $this->view->setVar("course", $course);
            $qtype = MasterQuesType::find("status=1");
            $this->view->setVar("qtype", $qtype);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['q_type'] = json_encode($postval['q_type']);
            $postval['course'] = json_encode($postval['course']);
            $postval['ql_easy'] = json_encode($postval['ql_easy']);
            $postval['ql_medium'] = json_encode($postval['ql_medium']);
            $postval['ql_hard'] = json_encode($postval['ql_hard']);
            if($postval['no_qs']==''){$postval['no_qs']=null;};
            if($postval['time']==''){$postval['time']=null;};
            $date = strtotime($postval['date']);
            $postval['date'] = date("Y-m-d",$date);
            $update= new MasterExamType();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Source Successfully Update</div>");
            return $response->redirect("exams");
        }
    }
    /**
     * Delete Perticular exam type
     * @param  [int] $id [Exam type id]
     * @return [return]     [Return delete status]
     * @author Rajesh Sardar
     */
    function delexamtypeAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterExamType WHERE etid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Deleted Successfully</div>");
            return $response->redirect("exams");
        }
    }
    /**
     * Update status of Exam type
     * @param  [int] $u_status [Update status code]
     * @param  [int] $id       [Exam type perticular id]
     * @return [return]           [Return Success Status]
     * @author Rajesh Sardar
     */
    function updextystatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterExamType SET status = 0 where etid=".$id."";
            }else{
                $phql = "UPDATE MasterExamType SET status = 1 where etid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Status Updated Successfully</div>");
            return $response->redirect("exams");
        }
    }


    ##############################################################
    ################## Exam Assign ###############################
    ##############################################################
    
    
    function examassignAction()
    {
        if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if(isset($getVal['submit'])){
                $where = '';
                if($getVal['subject']==''){
                    unset($getVal['subject']);
                }else{
                    $where .= "subject='".$getVal['subject']."' ";
                    $subsubdet = MasterSubSubject::find("subid = '".$getVal['subject']."' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if($getVal['subsubject']==''){
                    unset($getVal['subsubject']);
                }else{
                    $where .= " AND subsubject='".$getVal['subsubject']."' ";
                    $mTopicdet = MasterTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if($getVal['topics']==''){
                    unset($getVal['topics']);
                }else{
                    $where .= " AND topics='".$getVal['topics']."' ";
                    $msTopicdet = MasterSubTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND tid='".$getVal['topics']."' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if($getVal['subtopics']==''){
                    unset($getVal['subtopics']);
                }else{
                    $where .= " AND subtopics='".$getVal['subtopics']."' ";
                };
                if($getVal['qtype']==''){unset($getVal['qtype']);}else{
                    if($where==''){
                        $where .= "q_type='".$getVal['qtype']."' ";
                    }else{
                        $where .= " AND q_type='".$getVal['qtype']."' ";
                    }
                };
                if($getVal['qlevel']==''){
                    unset($getVal['qlevel']);
                }else{
                    if($where==''){
                        $where .="q_level='".$getVal['qlevel']."'";
                    }else{
                        $where .=" AND q_level='".$getVal['qlevel']."' ";
                    }
                };
                if($getVal['etype']==''){
                    unset($getVal['etype']);
                }else{
                    $where .= " AND LOCATE ('".$getVal['etype']."',e_type) ";
                };
                unset($getVal['_url'],$getVal['submit']);
                if($where==''){
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion';
                }else{
                    $phql = 'SELECT qusid,tableid,questionid,q_type,e_type FROM MasterQuestion WHERE '.$where;
                }
                $question = $this->modelsManager->executeQuery($phql);
                $this->view->setVar("question", $question);
                
            }
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
        $qtype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $qtype);
        
    }

    /**
     * Update question detail (throuing question id to question update link)
     * @param  [int] $id [questionid]
     * @return [redirect]     [redirect to question update table]
     */
    function updquestionAction($id)
    {
        $response = new \Phalcon\Http\Response();
        $tableid = MasterQuestion::findFirst(array("questionid='".$id."'"));
        if($tableid->tableid==1){
            $qid = MasterQuestionBank::findFirst(array("questionid='".$id."'"));
            return $response->redirect("questionbank/updatequestion/".$qid->qid);
        }else{
            $qid = MasterMatchQuestion::findFirst(array("questionid='".$id."'"));
            return $response->redirect("questionbank/updmmquestion/".$qid->qid);
        }
    }
    /**
     * Update exam type with their respective value
     * @return [status] [Retrun Status when it success]
     */
    function updqtAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            if($postval['qid']!="")
            {
                $phql = "UPDATE MasterQuestion SET e_type = '".$postval['etype']."' where questionid='".$postval['qid']."' ";
                $status = $this->modelsManager->executeQuery($phql);
                if($status){
                    echo 'Status Ok';
                }
                exit();
            }
        }
        
    }

    function viewassessAction()
    {
         if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if($getVal['submit']){
                if($getVal['subject']==''){
                    unset($getVal['subject']);
                }else{
                    $where= ' subject="'.$getVal['subject'].'"';
                    $subsubdet = MasterSubSubject::find("subid = '".$getVal['subject']."' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if($getVal['subsubject']==''){
                    unset($getVal['subsubject']);
                }else{
                    $where .= ' AND subsubject ="'.$getVal['subsubject'].'"';
                    $mTopicdet = MasterTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if($getVal['topics']==''){
                    unset($getVal['topics']);
                }else{
                    $where .= ' AND topics="'.$getVal['topics'].'"';
                    $msTopicdet = MasterSubTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND tid='".$getVal['topics']."' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if($getVal['subtopics']==''){
                    unset($getVal['subtopics']);
                }else{
                    $where .= ' AND subtopics="'.$getVal['subtopics'].'"';
                };
               $arr=array($where);
            }else{
              $arr=array();  
            }
             $assessment = MasterAssessment::find($arr);
              $this->view->setVar("assessment", $assessment);
               
        }
         $getSub = MasterSubject::find(array("status=1"));
         $this->view->setVar("subdet", $getSub);
    }

    function assessmentAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $postval['subtopics'] = json_encode($postval['subtopics']);
            $Addques= new MasterAssessment();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Successfully Added</div>");
            return $response->redirect("exams/viewassess");
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
    }

    function updassessAction($id)
    {
       $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $postval['subtopics'] = json_encode($postval['subtopics']);
            $Addques= new MasterAssessment();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Updated Successfully</div>");
            return $response->redirect("exams/viewassess");
        }
        if($id!=''){
            $assessment = MasterAssessment::findFirst(array("id='$id'"));
            $this->view->setVar("assessment", $assessment);
        }
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
        $subsubdet = MasterSubSubject::find("subid='$assessment->subject' AND status=1");
        $this->view->setVar("subsubdet", $subsubdet);
        $mTopicdet = MasterTopics::find("subid='$assessment->subject' AND ssubid='$assessment->subsubject' AND status=1");
        $this->view->setVar("mTopicdet", $mTopicdet);
        $msTopicdet = MasterSubTopics::find("subid='$assessment->subject' AND ssubid='$assessment->subsubject' AND tid='$assessment->topics' AND status=1");
        $this->view->setVar("msTopicdet", $msTopicdet);
    }

    function upstassessAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterAssessment SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterAssessment SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Status Updated Successfully</div>");
            return $response->redirect("exams/viewassess");
        }
    }

    function getsubsubjectAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $msSubdet = MasterSubSubject::find("subid = '".$postval['subid']."' AND status=1");
            echo "<option value=''>[ Select Sub Subject ]</option>";
            foreach($msSubdet as $msSubres){
                echo "<option value='".$msSubres->ssid."'>".$msSubres->ssname."</option>";
            }
        }
    }
    function gettopicsAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $mTopicdet = MasterTopics::find("subid = '".$postval['subid']."' AND ssubid='".$postval['ssubid']."' AND status=1");
            echo "<option value=''>[ Select Topics ]</option>";
            foreach($mTopicdet as $mTopicres){
                echo "<option value='".$mTopicres->tid."'>".$mTopicres->tname."</option>";
            }
        }
    }
    function getsubtopicsAction()
    {
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $msubTopicdet = MasterSubTopics::find("subid = '".$postval['subid']."' AND ssubid='".$postval['ssubid']."' AND tid='".$postval['tid']."' AND status=1");
            echo "<option value=''>[ Select Sub Topics ]</option>";
            foreach($msubTopicdet as $msubTopicres){
                echo "<option value='".$msubTopicres->stid."'>".$msubTopicres->stname."</option>";
            }
        }
    }


// Ajax............ 
// 
    function getstcheckboxAction()
    {
        if($this->request->isPost()){
            $getVal = $this->request->getPost();
            $getsubtopics = MasterSubTopics::find(array("subid='".$getVal['subjectid']."' AND ssubid='".$getVal['ssubid']."' AND tid='".$getVal['topics']."' "));
            foreach($getsubtopics as $value){ ?>
                <div class="checkbox-custom checkbox-default">
                      <input type="checkbox" id="st<?=$value->stid?>" value="<?=$value->stid?>" name="subtopics[]">
                      <label for="st<?=$value->stid?>"><?=$value->stname?></label>
                </div>
            <?php
            }
        }
        exit();
    }

    function getcourseAction()
    {
        if($this->request->isPost()){
            $getVal = $this->request->getPost();
        }
        $course = MasterCourse::find("scid='".$getVal['sup_cat']."' AND status=1");
        echo '<option value="">[-- Choose course --]</option>';
        foreach($course as $courseRes){
    ?>
        <option value="<?=$courseRes->id?>"><?=$courseRes->name?></option>
        
    <?php
        }
        exit();
    }

    function deleteassessAction($id){
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterAssessment WHERE id = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Assessment Deleted Successfully</div>");
            return $response->redirect("exams/viewassess");
        }
    }

    function practiceAction()
    {
        $data = MasterPractice::find();
        $this->view->setVar("exam_type", $data);
    }

    function addpracticeAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $postval['q_type'] = json_encode($postval['q_type']);
            $postval['ql_easy'] = json_encode($postval['ql_easy']);
            $postval['ql_medium'] = json_encode($postval['ql_medium']);
            $postval['ql_hard'] = json_encode($postval['ql_hard']);
            $Addques= new MasterPractice();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Practice Test Successfully Add</div>");
            return $response->redirect("exams/practice");
        }
        $supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("qtype", $qtype);
    }

    function updatepracticeAction($id='')
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $exam_type = MasterPractice::findFirst(array("id='".$id."'"));
            $this->view->setVar("exam_type", $exam_type);
            $supcat = MasterSupCat::find("status=1");
            $this->view->setVar("supcat", $supcat);
            $course = MasterCourse::find("scid='".$exam_type->sup_cat."' AND status=1");
            $this->view->setVar("course", $course);
            $qtype = MasterQuesType::find("status=1");
            $this->view->setVar("qtype", $qtype);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['q_type'] = json_encode($postval['q_type']);
            $postval['ql_easy'] = json_encode($postval['ql_easy']);
            $postval['ql_medium'] = json_encode($postval['ql_medium']);
            $postval['ql_hard'] = json_encode($postval['ql_hard']);
            $update= new MasterPractice();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Practice Successfully Update</div>");
            return $response->redirect("exams/practice");
        }
    }
}
?>