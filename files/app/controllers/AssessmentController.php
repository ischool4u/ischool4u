<?php
class AssessmentController extends ControllerBase
{
	public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
    }

    function indexAction()
    {
    	$subject=$this->dispatcher->getParam("pagename");
        $subsubject=$this->dispatcher->getParam("sub");
        $topic=$this->dispatcher->getParam("topic");
    	$response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
            $fixpack = MasterPackages::find("course='".$user_session['course']."' AND sup_cat='".$user_session['sup_cat']."'");
            $this->view->setVar("fixpack", $fixpack);
        }else{
            return $response->redirect("index");
        }
        if(isset($subsubject) && isset($subject)){
            $user = $this->session->get("user");
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subject = MasterSubject::findFirst(array("slug='".$subject."'"));
            $subsubject = MasterSubSubject::findFirst(array("slug='".$subsubject."'"));
            $topic = MasterTopics::findFirst(array("slug='".$topic."'"));
            $assess = MasterAssessment::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."'"));
            $subtopics = MasterSubTopics::find(array("subid='".$subject->id."' AND ssubid='".$subsubject->ssid."' AND tid='".$topic->tid."'"));
            $aid = '';$i=0;$asscount=$assess->count()-1;
            foreach ($assess as $value) {
                if($i==$asscount){
                    $aid .= "'".$value->id."'";
                }else{
                    $aid .= "'".$value->id."',";
                }
                $i++;
            }
            if($aid !=''){
                $assess_result = MasterResult::find(array("userid='".$user_session['id']."' and assess_id IN (".$aid.") "));
                foreach($assess_result as $val){
                    $assess_r = array(
                        $val->assess_id => $val->topicper,
                        'od'.$val->assess_id => array(
                                'noq' => $val->rights+$val->wrongs+$val->skiped,
                                'right' =>$val->rights,
                                'noqus' => $val->noq,
                                'nora' => $val->nora
                            )
                        );
                }
                $this->view->setVar("assess_result", $assess_r);
            }
            // follow code here
            $follow = $this->followAction();
            $cond = "uid='{$user_session['id']}'";
            $chalenge1 = MasterFriendlist::findFirst(array($cond));
           
            $arr = array();
            if(!empty($chalenge1->accept_friend)){
            $arr = json_decode($chalenge1->accept_friend);
            }

            $cond2 = "LOCATE({$user_session['id']},accept_friend)";
            $chalenge2 = MasterFriendlist::find(array($cond2));
            $arr1 = array();
            foreach ($chalenge2 as $val) {
                $arr1[] = $val->uid;
            }

            $this->view->setVar("follow",$follow);
            $this->view->setVar("follower",$arr1);
            $this->view->setVar("following",$arr);
            //===========================================
            $this->view->setVar("assess", $assess);
            $this->view->setVar("subject", $subject);
            $this->view->setVar("subsubject", $subsubject);
            $this->view->setVar("topics", $topic);
            $this->view->setVar("subtopics", $subtopics);
        }
    }

    function assesstestAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $id=$this->dispatcher->getParam("id");
        $this->view->setVar("assessid", $id);
        //=========================================================//
        $assess = MasterAssessment::findFirst(array("id='".$id."'"));
        $this->view->setVar("assess", $assess);
        $subtopics = json_decode($assess->subtopics);
        $topics = MasterTopics::findFirst(array("tid='".$assess->topics."'"));
        $this->view->setVar("topics", $topics);
        //---------------------------------------------------------//
        $etype = MasterExamType::findFirst(array("etid='2'"));
        if (isset($etype->etid)) {
            $this->view->setVar("etype", $etype);
            $ques_type = json_decode($etype->q_type,true);
            $q_typeid = implode(',', $ques_type);//======= Type of question =============
            $subtopics_list = implode(',', $subtopics);//======= Type of question =============
            $level = json_decode($etype->ql_easy,true);//======= Easy % Level ===
            $total_question = $etype->no_qs;//======= Total No of question =============
            $total_time = $etype->time;//======= Total Time of exam =============
            $getTotques = $total_question;//=======instance of $total_question ==========
            $count_current_question_no = 0;
            $this->view->setVar("subtopics_list", $subtopics_list);
            
            // print_r($q_typeid);exit();
            $questionIdlist = array();
            $e_type = $etype->etid;//print_r($e_type);exit();
            //foreach($q_typeid as $value){  
                //foreach($subtopics as $val){
                    $getHard = $getTotques * ($level[2]/100);
                    $hardCount = 0;
                    if ($getHard > 0 && $getHard < 1) {
                        $hardCount = 1;
                    }
                    elseif ($getHard <= 0) {
                        $hardCount = 0;
                    }
                    else
                    {
                        $hardCount = round($getHard);
                    }
                    if ($hardCount>=0) {
                        $hard = MasterQuestion::find(array("subject='".$assess->subject."' AND subsubject='".$assess->subsubject."' AND topics='".$assess->topics."' AND subtopics IN (".$subtopics_list.") AND q_type IN (".$q_typeid.") AND q_level='3' AND e_type LIKE '%".$e_type."%' limit ".$hardCount.""));
                        //$hard = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_level = '3' limit ".$hardCount.""));
                        
                        if(count($hard)>0)
                        {
                            $current_hard_que_count = 0;
                            foreach ($hard as $key => $hardId) {
                                //====== Get Table Nam from mastre question TABLE =====//
                                $table_id = $hardId->tableid;
                                if ($table_id == 1) {
                                    $table_name = "MasterQuestionBank";
                                    $question = $table_name::findFirst(array("questionid='".$hardId->questionid."' and status='1' "));
                                    if (count($question)>0) {
                                        //echo $question->questionid;
                                        // print_r(expression)
                                        $getQuestiondet[$count_current_question_no] = array(
                                            'questionid' => $question->questionid,
                                            'question_type' => $hardId->q_type,
                                            'question' => $question->question,
                                            'a' => $question->option1,
                                            'b' => $question->option2,
                                            'c' => $question->option3,
                                            'd' => $question->option4,
                                            'ans' => $question->ans,
                                            'topicid' => $hardId->subtopics
                                            );
                                        //print_r($questionIdlist);
                                        //print_r($getQuestiondet[$count_current_question_no]);
                                        //print_r($questionIdlist);
                                        $questionIdlist[] = $getQuestiondet[$count_current_question_no];
                                        //print_r($questionIdlist);
                                        $hardCount = ++$current_hard_que_count;
                                        $count_current_question_no++;
                                    }//exit();
                                }
                                // elseif ($table_id == 2) {
                                //     $table_name = "Master_matchQuestion";
                                // }
                                // elseif ($table_id == 3) {
                                //     $table_name = "MasterNumericQuestion";
                                // }
                                // elseif ($table_id == 4) {
                                //     $table_name = "MasterParaQuestion";
                                // }
                                // elseif ($table_id == 5) {
                                //     $table_name = "MasterReasonQuestion";
                                // }
                                //-----------------------------------------------------//
                                
                            }//exit();
                            $remainCount = $getTotques - $hardCount;
                        }
                        else
                        {
                            $remainCount = $getTotques - 0;  
                            $hardCount = 0; 
                        }
                    }
                    //echo $hardCount;
                    // ========= Get Medium question count =============


                    $getMedium = $getTotques * ($level[1]/100);
                    $mediumCount = 0;
                    if ($getMedium > 0 && $getMedium < 1) {
                        $mediumCount = 1;
                    }
                    elseif ($getMedium <= 0) {
                        $mediumCount = 0;
                    }
                    else
                    {
                        $mediumCount = round($getMedium);   
                    }
                    if ($mediumCount>=0) 
                    {
                        if ($hardCount>0) {
                            if($mediumCount < $getTotques - $hardCount)
                            {
                                $medium = MasterQuestion::find(array("subject='".$assess->subject."' AND subsubject='".$assess->subsubject."' AND topics='".$assess->topics."' AND subtopics IN (".$subtopics_list.") AND q_type IN (".$q_typeid.") AND q_level='2' AND e_type LIKE '%".$e_type."%' limit ".$mediumCount.""));
                                //$hard = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_level = '3' limit ".$hardCount.""));
                                if(count($medium)>0)
                                {   
                                    $current_med_que_count = 0;
                                    foreach ($medium as $key => $mediumID) {
                                        //====== Get Table Nam from mastre question TABLE =====//
                                        $table_id = $mediumID->tableid;
                                        if ($table_id == 1) {
                                            $table_name = "MasterQuestionBank";
                                            $question = $table_name::findFirst(array("questionid='".$mediumID->questionid."' and status='1' "));
                                            if (isset($question->questionid)) {
                                                $getQuestiondet[$count_current_question_no]= array(
                                                    'questionid' => $question->questionid,
                                                    'question_type' => $mediumID->q_type,
                                                    'question' => $question->question,
                                                    'a' => $question->option1,
                                                    'b' => $question->option2,
                                                    'c' => $question->option3,
                                                    'd' => $question->option4,
                                                    'ans' => $question->ans,
                                                    'topicid' => $hardId->subtopics
                                                    );
                                                $questionIdlist[] = $getQuestiondet[$count_current_question_no];
                                                $mediumCount = ++$current_med_que_count;
                                                $count_current_question_no++;
                                            }
                                        }
                                        // elseif ($table_id == 2) {
                                        //     $table_name = "Master_matchQuestion";
                                        // }
                                        // elseif ($table_id == 3) {
                                        //     $table_name = "MasterNumericQuestion";
                                        // }
                                        // elseif ($table_id == 4) {
                                        //     $table_name = "MasterParaQuestion";
                                        // }
                                        // elseif ($table_id == 5) {
                                        //     $table_name = "MasterReasonQuestion";
                                        // }
                                        //-----------------------------------------------------//
                                        
                                    }
                                    $remainCount = $remainCount - $mediumCount;
                                }
                                else
                                {
                                    $remainCount = $getTotques - 0;  
                                    $mediumCount = 0; 
                                }
                            }
                        }
                    }//print_r($questionIdlist);
                    //---------------------------------------//
                    // GET EASY QESTION LIST
                    // --------------------------------------// 
                    if ($remainCount>0) {
                        {
                            $easy = MasterQuestion::find(array("subject='".$assess->subject."' AND subsubject='".$assess->subsubject."' AND topics='".$assess->topics."' AND subtopics IN (".$subtopics_list.") AND q_type IN (".$q_typeid.") AND q_level='1' AND e_type LIKE '%".$e_type."%' limit ".$remainCount.""));
                            //$hard = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_level = '3' limit ".$hardCount.""));
                            if(count($easy)>0)
                            {
                                $current_easy_count = 0;
                                foreach ($easy as $key => $easyID) {
                                    //====== Get Table Nam from mastre question TABLE =====//
                                    $table_id = $easyID->tableid;
                                    if ($table_id == 1) {
                                        $table_name = "MasterQuestionBank";
                                        $question = $table_name::findFirst(array("questionid='".$easyID->questionid."' and status='1' "));
                                        if(isset($question->questionid)) {
                                            $getQuestiondet[$count_current_question_no]= array(
                                                'questionid' => $question->questionid,
                                                'question_type' => $easyID->q_type,
                                                'question' => $question->question,
                                                'a' => $question->option1,
                                                'b' => $question->option2,
                                                'c' => $question->option3,
                                                'd' => $question->option4,
                                                'ans' => $question->ans,
                                                'topicid' => $hardId->subtopics
                                                );
                                            
                                           $questionIdlist[] = $getQuestiondet[$count_current_question_no];
                                            // $mediumCount = ++$current_easy_count;
                                            //$count_current_question_no++;
                                        }
                                    }
                                    // elseif ($table_id == 2) {
                                    //     $table_name = "Master_matchQuestion";
                                    // }
                                    // elseif ($table_id == 3) {
                                    //     $table_name = "MasterNumericQuestion";
                                    // }
                                    // elseif ($table_id == 4) {
                                    //     $table_name = "MasterParaQuestion";
                                    // }
                                    // elseif ($table_id == 5) {
                                    //     $table_name = "MasterReasonQuestion";
                                    // }
                                    //-----------------------------------------------------//
                                    
                                }
                                //$remainCount = $remainCount - $mediumCount;
                            }
                            // else
                            // {
                            //     $remainCount = $getTotques - 0;  
                            //     $mediumCount = 0; 
                            // }
                        }
                    }
                    // echo "<pre>";print_r($questionIdlist);
           $this->view->setVar("questionid",$getQuestiondet);
        }
        // exit();
    }

    function resultAction(){
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $postVal = $this->request->getPost('userans');
        $postq = $this->request->getPost('qID');
        $topid = $this->request->getPost('topicid');
        $time = MasterExamType::findFirst("etid='2'");
        $time = $time->time*60;
        $exm_timetaken=explode(":",$this->request->getPost('time_taken'));
        $hr=$exm_timetaken[0]*60*60;
        $min=$exm_timetaken[1]*60;
        $sec=$exm_timetaken[2];
        $total=(int)$hr+(int)$min+(int)$sec;
        $subtopics_list = explode(',',$this->request->getPost('subtopics_list'));
        for($i=0;$i<=count($postq)-1;$i++){
            $array[] = array(
                'qid' => $postq[$i],
                'ans' => $postVal[$i]
                );
        }

        // Geting topics 
        $tid = array();
        for($i=0;$i<=count($topid)-1;$i++){
            $d = array(
                $postq[$i]=>$topid[$i]
                );
            $tid = array_merge($tid,$d);
        }
        // echo '<pre>';print_r($tid);
        $topic_count = array();
        foreach($subtopics_list as $stl){
            $i=0;
            foreach($topid as $tpid){
                if($stl==$tpid){
                    $i++;
                }
            }
            $topic_count[$stl] = $i;
        }
        // echo '<pre>';print_r($topic_count);
        // **********************************
        
        
        $postval['userid'] = $user_session['id'];
        $postval['assess_id'] = $this->request->getPost('testid');
        $postval['test_type'] = $this->request->getPost('test_type');

        // Check the answers
        $qid = '';
        foreach($array as $arrays){
            $check_q[$arrays['qid']]=$arrays['ans'];
        }
        // echo '<pre>';print_r($check_q);
        for($i=0;$i<=count($array)-1;$i++){
            if($i==count($array)-1){
                $qid .= "'".$array[$i]['qid']."'";
            }else{
                $qid .= "'".$array[$i]['qid']."',";
            }
        }
        $questions = MasterQuestionBank::find("questionid IN (".$qid.")");
        $right=0;$wrong=0;$skiped=0;
        $rqpt = array(); // Right question per topics....
        foreach($questions as $qus){
            $id = $tid[$qus->questionid];
            if($check_q[$qus->questionid]!=''){
                if($check_q[$qus->questionid]==$qus->ans){
                    $right++;
                    if(!empty($rqpt[$id])){
                        $rqpt[$id] = $rqpt[$id]+1;
                    }else{
                        $rqpt[$id] = 1;
                    }
                }else{
                    $wrong++;
                }
            }else{
                $skiped++;
            }
        }
        // echo '<pre>';print_r($rqpt);
        // Topic percentages...
        // *********************************************
        $topicper = array();
        foreach($subtopics_list as $stl){
            if($topic_count[$stl]==0){
                $topicper[$stl] = 100;
            }else{
                if(!empty($rqpt[$stl])){
                    $topicper[$stl] = round($rqpt[$stl]/$topic_count[$stl]*100);
                }else{
                    $topicper[$stl] = 0;
                }
            }
        }
        // *******************************************
        
        $postval['topicper'] = json_encode($topicper);
        $resultid = MasterResult::findFirst(array("userid='".$user_session['id']."' and assess_id='".$this->request->getPost('testid')."' and test_type='".$this->request->getPost('test_type')."' "));
        
        if($resultid){
            $pre_per = round($resultid->rights/($resultid->rights+$resultid->wrongs+$resultid->skiped)*100);
            $postval['id']=$resultid->id;
            $postval['noq'] = $resultid->noq+count($array);
            $recent_per = round($right/($right+$wrong+$skiped)*100);
            if($pre_per<$recent_per){
                $postval['remark'] = 9;
            }elseif($pre_per==$recent_per){
                $postval['remark'] = 0;
            }else{
                $postval['remark'] = 1;
            }
            $postval['nora'] = $resultid->nora+$right;
            $postval['total_time'] = number_format($resultid->total_time+(($time-$total)/60),2,'.','');
        }else{
            $postval['noq'] = count($array);
            $postval['remark'] = ((round($right/($right+$wrong+$skiped)*100))>10)?9:0;
            $postval['nora'] = $right;
            $postval['total_time'] = number_format(($time-$total)/60,2,'.','');
        }
        $postval['rights'] = $right;
        $postval['wrongs'] = $wrong;
        $postval['skiped'] = $skiped;
        $postval['exam_detail'] = json_encode($array);
        
        $postval['time_taken'] = ($time-$total)/60;
        $postval['date'] = date("Y-m-d, H:i:s");
        // echo '<pre>';print_r($postval);

        $result= new MasterResult();
        if($result->save($postval)){
            $rid = $result->id;
        }
        return $response->redirect("assessment/assessresult/".$rid);
        exit();
    }

    function assessresultAction($id=''){
        if(isset($id) && $id!=''){
            $response = new \Phalcon\Http\Response();
            if ($this->session->has("user")) {
                $user_session = $this->session->get("user");
                $this->view->setVar("user_session", $user_session);
            }else{
                return $response->redirect("index");
            }
            $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
            $result = MasterResult::findFirst(array("id='".$id."' and userid='".$user_session['id']."' "));
            if(empty($result)){
                return $response->redirect("index");
            }
            $ques = json_decode($result->exam_detail);$qs = '';$j=0;
            foreach ($ques as $value) {
                if(count($ques)-1==$j){
                    $qs .= "'".$value->qid."'";
                }else{
                    $qs .= "'".$value->qid."',";
                }
                $j++;
                $anscheck[$value->qid]=$value->ans;
            }
            $questions = MasterQuestionBank::find(array("questionid IN (".$qs.")"));
            $assess = MasterAssessment::findFirst(array("id='".$result->assess_id."' "));
            $stopics = json_decode($assess->subtopics);$st='';$i=0;
            foreach($stopics as $val){
                if(count($stopics)-1==$i){
                    $st .= "'".$val."'";
                }else{
                    $st .= "'".$val."',";
                }
                $i++;
            }
            $topic = MasterTopics::findFirst(array("tid='".$assess->topics."' "));
            $subtopic = MasterSubTopics::find(array("stid IN (".$st.")"));


            $this->view->setVar("course", $course);
            $this->view->setVar("assresult", $result);
            $this->view->setVar("assess", $assess);
            $this->view->setVar("topic", $topic);
            $this->view->setVar("subtopic", $subtopic);
            $this->view->setVar("questions", $questions);
            $this->view->setVar("anscheck", $anscheck);
        }
    }

    function shareAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $result = MasterResult::findFirst("id='".$postval['id']."'");
            if($result){
                $assess = MasterAssessment::findFirst("id='".$result->assess_id."'");
                $topic = MasterTopics::findFirst("tid='".$assess->topics."'");
                $q = $result->rights.'/'.($result->rights+$result->wrongs+$result->skiped);
                $acu = $result->rights/($result->rights+$result->wrongs+$result->skiped)*100;
                $time = $result->time_taken;
                $message = ' Practice '.$assess->title.' of '.$topic->tname;
                $message .= '<div class="row">'.
                '<div class="col-md-3">'.
                            '<img src="http://localhost/ischool4u/assets/images/is_qs.png" class="topic-img">'.
                            '<label class="status-label">'.$q.'</label>'.
                        '</div>'.
                        '<div class="col-md-3">'.
                            '<img src="http://localhost/ischool4u/assets/images/is_ac.png" class="topic-img">'.
                            '<label class="status-label">'.round($acu).'%</label>'.
                        '</div>'.
                        '<div class="col-md-3">'.
                            '<img src="http://localhost/ischool4u/assets/images/is_sp.png" class="topic-img">'.
                            '<label class="status-label">'.$time.'m</label>'.
                        '</div>'.
                    '</div>';
                $data['uid'] = $user_session['id'];
                $data['message'] = htmlentities($message);
                $data['notification_type'] = '5';
                $data['add_date'] = date("Y-m-d H:i:s");
                $wall = new MasterNotification();
                if($wall->save($data)){
                    echo 'Successfully Share you score.';
                }
                exit();
            }else{
                echo 'There is no result';
            }
        }
        exit();
    }

    function followAction()
    {
        $array=[];
        $date = date("Y-m-d");
        $user_session = $this->session->get("user");
        $follow = MasterFollow::find("observe_date='{$date}' AND status='1' AND userid!='$user_session[id]'");
        foreach($follow as $fval){
            $array[] = $fval->userid;
        }
        
        $uid = implode("','", $array);
        $user = MasterUsers::find("uid IN ('{$uid}')");
        foreach($follow as $flval){
            foreach($user as $uval){
                if($uval->uid==$flval->userid){
                    $data[] = [
                        "userid"=>$flval->userid,
                        "img"=>$uval->img,
                        "name"=>$uval->first_name.' '.$uval->last_name,
                        "per"=>$flval->per
                    ];
                }
            }
        }
        return $data;
        exit();
    }
}
?>