<?php
class TestController extends ControllerBase
{
    public function initialize()
    {
        //echo "init";exit;
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        
    }

    function subjectwiseAction()
    {
    	$response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
          $postval=$this->request->getPost();
      }
      $met = MasterExamType::findFirst("etid='".$postval['exam_type']."'");
      $mtest = MasterTests::findFirst("test_type='1' AND exam_type='{$met->etid}' AND couse='{$user_session['course']}' ");
      $q_type =  json_decode($mtest->qint);
      for($i=0;$i<count($q_type);$i=$i+4){
        $qtype[$q_type[$i]] = ["pos"=>$q_type[$i+2],"neg"=>$q_type[$i+3]];
    }

        // echo '<pre>';print_r($qtype);
    $time = $met->time*60;
    $exm_timetaken=explode(":",$postval['time_taken']);
    $hr=$exm_timetaken[0]*60*60;
    $min=$exm_timetaken[1]*60;
    $sec=$exm_timetaken[2];
    $total=(int)$hr+(int)$min+(int)$sec;
    $postval['time_taken'] = round(($time-$total)/60);
    $subject_list = explode(',',$postval['subtopics_list']);

    for($i=0;$i<=count($postval['qID'])-1;$i++){
        $array[] = array(
            'qid' => $postval['qID'][$i],
            'ans' => $postval['userans'][$i]
            );
        $userans[$postval['qID'][$i]] = $postval['userans'][$i];
    }

    $qusids = implode("','",$postval['qID']);
    $qusids = "'".$qusids."'";
    for ($i=1; $i <=5 ; $i++) {
     $qusdetail = MasterQuestion::find("tableid='".$i."' AND questionid IN (".$qusids.") ");
     if(!empty($qusdetail)){
      foreach($qusdetail as $val){
       $twq[$i][] = $val->questionid;
       $type[] = [$val->questionid,$val->q_type];
       if($val->q_type=='3'){
        $match_qid[] = $val->questionid;
    }
}
}	
}

$mcount = array("a","b","c","d");
foreach($match_qid as $mval){
    $mua[$mval]["rans"] = $postval[$mval.'rans'];
    foreach($mcount as $mcval){
        $mua[$mval]["uans"][] = $postval[$mval.'uans'.$mcval];
    }
}

        // add q_type in array
foreach($type as $val){
    foreach($array as $av){
        if($val[0]==$av['qid']){
            if(key($array)==0){
                $ar = count($array)-1;
                $array[$ar]['qtype'] = $val[1];
                $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
            }else{
                $ar = key($array)-1;
                $array[$ar]['qtype'] = $val[1];
                $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
            }
        }
    }
}

        // **************


$tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion'];
for ($i=1; $i <=5 ; $i++) {
 if(!empty($twq[$i])){
  $qusd = implode("','",$twq[$i]);
  $qusd = "'".$qusd."'";
  $qusdetail = $tables[$i]::find("questionid IN (".$qusd.") ");
  foreach($qusdetail as $val){
   $ans[$val->questionid] = $val->ans;
}
}
}

$getarray = $this->checkans($postval['qID'],$userans,$ans,$tarray,$qtype,$array,$mua);

$array = $getarray['exam_type'];
        // echo '<pre>';print_r($array);
$postval['exam_detail'] = json_encode($array);

        // echo '<pre>';print_r($array);
        // echo '<pre>';print_r($getarray);
        // echo '<pre>';print_r($ans);
        // echo '<pre>';print_r($userans);
$postval['userid'] = $user_session['id'];
$postval['rights'] = $getarray['right'];
$postval['wrongs'] = $getarray['wrong'];
$postval['skiped'] = $getarray['skiped'];
$postval['test_on'] = $postval['subject'];
$postval['date'] = date("Y-m-d, H:i:s");

        // echo $right.'</br>'.$wrong.'</br>'.$skiped;
        // echo '<pre>';print_r($postval);

        // 
        // Package no of test deduction
        // 
$package = MasterUserPackage::findfirst("userid='".$user_session['id']."' AND test_type='1'");
if($package->nots==1){
    $phql = "DELETE FROM MasterUserPackage WHERE id = '".$package->id."'";
}else{
    $not = $package->nots-1;
    $phql = "UPDATE MasterUserPackage SET nots = '".$not."' where id=".$package->id."";
}
$status = $this->modelsManager->executeQuery($phql);

$accurecy = ($postval['rights']/$postval['rights']+$postval['rights']+$postval['rights'])*100;
$data = ["userid"=>$user_session['id'],"accurecy"=>$accurecy];
$follow= new MasterFollow();
$follow->save($data);

$result= new MasterResult();
if($result->save($postval)){
    $rid = $result->id;
}

return $response->redirect("test/subjectwiseresult/".$rid);
exit();
        // 
}
public function checkans($qid='',$uans='',$rans='',$tarray='',$qtype='',$array='',$mua=''){
        // Check Ans
    $right = 0;$wrong = 0;$skiped = 0;$unattempt=0;
    foreach ($qid as $value) {
        if(!empty($uans[$value])){
                /**
                 * if result ==  skipped
                 */
                if($uans[$value]=='skipped'){
                    $skiped = $skiped+1;
                    $array[$tarray[$value][0][1]]["markpos"] = 0;
                    $array[$tarray[$value][0][1]]["status"] = 'skipped';
                }
                //====== if result ==  skipped =======
                else{ // If results have some results...
                    switch ($tarray[$value][0][0]) {
                        case '1':
                        if($uans[$value]==$rans[$value]){
                            $right = $right+1;
                            $array[$tarray[$value][0][1]]["markpos"] = $qtype[1]['pos'];
                        }else{
                            $wrong = $wrong+1;
                            $array[$tarray[$value][0][1]]["markneg"] = $qtype[1]['neg'];
                        }
                        $array[$tarray[$value][0][1]]["status"] = 'attempt';
                        break;
                        case '2':
                        $uns = str_split($uans[$value]);
                        $rns = str_split($rans[$value]);
                        if(count($rns)!= count($uns)){
                            $wrong = $wrong+1;
                            $array[$tarray[$value][0][1]]["markneg"] = $qtype[2]['neg'];
                        }else{
                            if(count(array_intersect($uns,$rns))!=count($rns)){
                                $wrong = $wrong+1;
                                $array[$tarray[$value][0][1]]["markneg"] = $qtype[2]['neg'];
                            }else{
                                $right = $right+1;
                                $array[$tarray[$value][0][1]]["markpos"] = $qtype[2]['pos'];
                            }
                        }
                        $array[$tarray[$value][0][1]]["status"] = 'attempt';
                        break;
                        case '3':
                        $getmua = $mua[$value];
                        $multiright=0;$multiwrong=0;$multiskiped=0;$msg=0;
                        for($i=0;$i<count($getmua['rans']);$i++){
                            if(!empty($getmua['uans'][$i])){
                                $rns = str_split($getmua['rans'][$i]);
                                if(count($rns)!=count($getmua['uans'][$i])){
                                    $multiwrong = $multiwrong+1;
                                    
                                }else{
                                    if(count(array_intersect($rns,$getmua['uans'][$i]))!=count($rns)){
                                        $multiwrong = $multiwrong+1;
                                        
                                    }else{
                                        $multiright = $multiright+1;
                                    }
                                }
                            }else{
                                $multiskiped = $multiskiped+1;
                            }
                            
                        }
                        if($multiwrong!=0){
                            $array[$tarray[$value][0][1]]["markneg"] = $qtype[3]['neg'];
                            $array[$tarray[$value][0][1]]["ans"] = $getmua['uans'];
                            $right = $right+1;
                        }elseif($multiskiped==count($getmua['rans'])){
                            $array[$tarray[$value][0][1]]["markpos"] = 0;
                            $array[$tarray[$value][0][1]]["ans"] = $getmua['uans'];
                            $skiped = $skiped+1;
                        }else{
                            $array[$tarray[$value][0][1]]["markpos"] = $qtype[3]['pos'];
                            $array[$tarray[$value][0][1]]["ans"] = $getmua['uans'];
                            $right = $right+1;
                        }
                        $array[$tarray[$value][0][1]]["status"] = 'attempt';
                        break;
                        case '4':
                        if($uans[$value]==$rans[$value]){
                            $right = $right+1;
                            $array[$tarray[$value][0][1]]["markpos"] = $qtype[4]['pos'];
                        }else{
                            $wrong = $wrong+1;
                            $array[$tarray[$value][0][1]]["markneg"] = $qtype[4]['neg'];
                        }
                        $array[$tarray[$value][0][1]]["status"] = 'attempt';
                        break;
                        default:
                            # code...
                        break;
                    }
                }
                //======= If results have some results... =======
            }else{ // If user is not attempted the question
                $unattempt = $unattempt+1;
                $skiped = $skiped+1;
                $array[$tarray[$value][0][1]]["markpos"] = 0;
                $array[$tarray[$value][0][1]]["status"] = 'unattempt';
            }
            //========= End of unattempted ============
        }

        return ["right"=>$right,"wrong"=>$wrong,"skiped"=>$skiped,"unattempt"=>$unattempt,"exam_type"=>$array];
    }

    function multicheck(){
        // echo '<pre>';print_r($match_qid);
    }

    function subjectwiseresultAction($resultid='')
    {
    	$response = new \Phalcon\Http\Response();
     if ($this->session->has("user")) {
         $user_session = $this->session->get("user");
         $this->view->setVar("user_session", $user_session);
     }else{
         return $response->redirect("index");
     }
     $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
     $result = MasterResult::findFirst(array("id='".$resultid."' and userid='".$user_session['id']."' "));
     $test = MasterTests::findFirst(array("id='".$result->testid."'"));
    	// $result->tid;
     $examresult = json_decode($result->exam_detail);

     $qint = json_decode($test->qint);
     for($i=0;$i<count($qint);$i=$i+4){
        $qtype[] = $qint[$i];
    }
        // echo '<pre>';print_r($qint);
    foreach($qtype as $qval){
        $pos = 0;$neg = 0;$count=0;
        foreach($examresult as $erval){
            if($erval->qtype==$qval){
                if(!isset($erval->markpos)){
                    $neg = $neg+$erval->markneg;
                    $count++;
                }else{
                    $pos = $pos+$erval->markpos;
                    $count++;
                }
            }
        }
        $resultmark[] = ['pos'=>$pos,'neg'=>$neg,'count'=>$count,'type'=>$qval];
    }
        // echo '<pre>';print_r($resultmark);

    $qids = json_decode($result->exam_detail);
    foreach($qids as $qid){
      $qds[] = $qid->qid;
      $type[$qid->qid] = $qid->qtype;
  }


  foreach($qids as $val){
      $anscheck[$val->qid] = $val->ans;
  }

  $exam_time = MasterExamType::findFirst("etid='".$test->exam_type."' ");
  $qid = "'".implode("','", $qds)."'";
  $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion'];
  foreach($tables as $key=>$tval){
    $questions[$key] = $tval::find(array("questionid IN (".$qid.")"));
}

$this->view->setVar("course", $course);
$this->view->setVar("result", $result);
$this->view->setVar("questions", $questions);
$this->view->setVar("anscheck", $anscheck);
$this->view->setVar("exam_time", $exam_time);
$this->view->setVar("resultmark", $resultmark);
$this->view->setVar("type", $type);

    	// $result = MasterResult::findFirst("id='".$resultid."' ");
    	// echo '<pre>';print_r($resultmark);
        // exit();
}

function unitwiseAction($id='')
{
    $response = new \Phalcon\Http\Response();
    if ($this->session->has("user")) {
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
    }else{
        return $response->redirect("index");
    }
    if($this->request->isPost()){
        $postval=$this->request->getPost();
    }
    $subject_list = explode(',',$postval['subtopics_list']);
    $mtest = MasterTests::findFirst("id='{$id}'");
    $met = MasterExamType::findFirst("etid='{$mtest->exam_type}'");
    
    $time = $met->time*60;
    $exm_timetaken=explode(":",$postval['time_taken']);
    $hr=$exm_timetaken[0]*60*60;
    $min=$exm_timetaken[1]*60;
    $sec=$exm_timetaken[2];
    $total=(int)$hr+(int)$min+(int)$sec;
    $postval['time_taken'] = round(($time-$total)/60);

    $q_type =  json_decode($mtest->qint);
    for($i=0;$i<count($q_type);$i=$i+4){
        $qtype[$q_type[$i]] = ["pos"=>$q_type[$i+2],"neg"=>$q_type[$i+3]];
    }

    for($i=0;$i<=count($postval['qID'])-1;$i++){
        $array[] = array(
            'qid' => $postval['qID'][$i],
            'ans' => $postval['userans'][$i]
            );
        $userans[$postval['qID'][$i]] = $postval['userans'][$i];
    }

    $qusids = implode("','",$postval['qID']);
    $qusids = "'".$qusids."'";
    for ($i=1; $i <=5 ; $i++) { 
        $qusdetail = MasterQuestion::find("tableid='".$i."' AND questionid IN (".$qusids.") ");
        if(!empty($qusdetail)){
            foreach($qusdetail as $val){
                $twq[$i][] = $val->questionid;
                    $type[] = [$val->questionid,$val->q_type]; // $type contain type of question 
                    if($val->q_type=='3'){
                        $match_qid[] = $val->questionid; // Match type questions...
                    }
                }
            } 
        }

        $mcount = array("a","b","c","d");
        foreach($match_qid as $mval){
            $mua[$mval]["rans"] = $postval[$mval.'rans'];
            foreach($mcount as $mcval){
                $mua[$mval]["uans"][] = $postval[$mval.'uans'.$mcval];
            }
        }

        // add question type in array
        foreach($type as $val){
            foreach($array as $av){
                if($val[0]==$av['qid']){
                    if(key($array)==0){
                        $ar = count($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }else{
                        $ar = key($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }
                }
            }
        }

        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion'];
        for ($i=1; $i <=5 ; $i++) {
            if(!empty($twq[$i])){
                $qusd = implode("','",$twq[$i]);
                $qusd = "'".$qusd."'";
                $qusdetail = $tables[$i]::find("questionid IN (".$qusd.") ");
                foreach($qusdetail as $val){
                    $ans[$val->questionid] = $val->ans;
                }
            }
        }

        
        $getarray = $this->checkans($postval['qID'],$userans,$ans,$tarray,$qtype,$array,$mua);
        $array = $getarray['exam_type'];
        $postval['exam_detail'] = json_encode($array);

        // echo '<pre>';print_r($ans);
        // echo '<pre>';print_r($userans);
        $postval['userid'] = $user_session['id'];
        $postval['rights'] = $getarray['right'];
        $postval['wrongs'] = $getarray['wrong'];
        $postval['skiped'] = $getarray['skiped'];
        $postval['test_on'] = $mtest->unit;
        $postval['date'] = date("Y-m-d, H:i:s");
        // echo $right.'</br>'.$wrong.'</br>'.$skiped;
        // echo '<pre>';print_r($postval);
        // 
        // Package no of test deduction
        // 
        $package = MasterUserPackage::findfirst("userid='".$user_session['id']."' AND test_type='3'");
        if($package->nots==1){
            $phql = "DELETE FROM MasterUserPackage WHERE id = '".$package->id."'";
        }else{
            $not = $package->nots-1;
            $phql = "UPDATE MasterUserPackage SET nots = '".$not."' where id=".$package->id."";
        }
        $status = $this->modelsManager->executeQuery($phql);

        $result= new MasterResult();
        if($result->save($postval)){
            $rid = $result->id;
        }
        $accurecy = ($postval['rights']/$postval['rights']+$postval['rights']+$postval['rights'])*100;
        $data = ["userid"=>$user_session['id'],"accurecy"=>$accurecy];
        $follow= new MasterFollow();
        $follow->save($data);
        return $response->redirect("test/unitwiseresult/".$rid);
        exit();
    }

    function unitwiseresultAction($resultid='')
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $result = MasterResult::findFirst(array("id='".$resultid."' and userid='".$user_session['id']."' "));
        $test = MasterTests::findFirst(array("id='".$result->testid."'"));
        // $result->tid;
        $qids = json_decode($result->exam_detail);

        $qint = json_decode($test->qint);
        for($i=0;$i<count($qint);$i=$i+4){
            $qtype[] = $qint[$i];
        }
        // echo '<pre>';print_r($qint);
        foreach($qtype as $qval){
            $pos = 0;$neg = 0;$count=0;
            foreach($examresult as $erval){
                if($erval->qtype==$qval){
                    if(!isset($erval->markpos)){
                        $neg = $neg+$erval->markneg;
                        $count++;
                    }else{
                        $pos = $pos+$erval->markpos;
                        $count++;
                    }
                }
            }
            $resultmark[] = ['pos'=>$pos,'neg'=>$neg,'count'=>$count,'type'=>$qval];
        }

        foreach($qids as $qid){
            $qds[] = $qid->qid;
        }

        foreach($qids as $val){
            $anscheck[$val->qid] = $val->ans;
        }
        $exam_time = MasterExamType::findFirst("etid='".$test->exam_type."' ");
        $qid = "'".implode("','", $qds)."'";
        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion'];
        foreach($tables as $key=>$tval){
            $questions[$key] = $tval::find(array("questionid IN (".$qid.")"));
        }


        $this->view->setVar("course", $course);
        $this->view->setVar("result", $result);
        $this->view->setVar("resultmark", $resultmark);
        $this->view->setVar("questions", $questions);
        $this->view->setVar("anscheck", $anscheck);
        $this->view->setVar("exam_time", $exam_time);
        // $result = MasterResult::findFirst("id='".$resultid."' ");
        // echo '<pre>';print_r($result);
    }

    function fullsyllabusAction(){
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval=$this->request->getPost();
        }
        $met = MasterExamType::findFirst("etid='".$postval['exam_type']."'");
        $mtest = MasterTests::findFirst("test_type='5' AND exam_type='{$met->etid}' AND couse='{$user_session['course']}' ");
        $q_type =  json_decode($mtest->qint);
        for($i=0;$i<count($q_type);$i=$i+4){
            $qtype[$q_type[$i]] = ["pos"=>$q_type[$i+2],"neg"=>$q_type[$i+3]];
        }

        // echo '<pre>';print_r($qtype);
        $time = $met->time*60;
        $exm_timetaken=explode(":",$postval['time_taken']);
        $hr=$exm_timetaken[0]*60*60;
        $min=$exm_timetaken[1]*60;
        $sec=$exm_timetaken[2];
        $total=(int)$hr+(int)$min+(int)$sec;
        $postval['time_taken'] = round(($time-$total)/60);
        $subject_list = explode(',',$postval['subtopics_list']);

        for($i=0;$i<=count($postval['qID'])-1;$i++){
            $array[] = array(
                'qid' => $postval['qID'][$i],
                'ans' => $postval['userans'][$i]
                );
            $userans[$postval['qID'][$i]] = $postval['userans'][$i];
        }

        $qusids = implode("','",$postval['qID']);
        $qusids = "'".$qusids."'";
        for ($i=1; $i <=5 ; $i++) {
            $qusdetail = MasterQuestion::find("tableid='".$i."' AND questionid IN (".$qusids.") ");
            if(!empty($qusdetail)){
                foreach($qusdetail as $val){
                    $twq[$i][] = $val->questionid;
                    $type[] = [$val->questionid,$val->q_type];
                    if($val->q_type=='3'){
                        $match_qid[] = $val->questionid;
                    }
                }
            }   
        }

        $mcount = array("a","b","c","d");
        foreach($match_qid as $mval){
            $mua[$mval]["rans"] = $postval[$mval.'rans'];
            foreach($mcount as $mcval){
                $mua[$mval]["uans"][] = $postval[$mval.'uans'.$mcval];
            }
        }

        // add q_type in array
        foreach($type as $val){
            foreach($array as $av){
                if($val[0]==$av['qid']){
                    if(key($array)==0){
                        $ar = count($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }else{
                        $ar = key($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }
                }
            }
        }
        
        // **************
        

        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion'];
        for ($i=1; $i <=5 ; $i++) {
            if(!empty($twq[$i])){
                $qusd = implode("','",$twq[$i]);
                $qusd = "'".$qusd."'";
                $qusdetail = $tables[$i]::find("questionid IN (".$qusd.") ");
                foreach($qusdetail as $val){
                    $ans[$val->questionid] = $val->ans;
                }
            }
        }

        $getarray = $this->checkans($postval['qID'],$userans,$ans,$tarray,$qtype,$array,$mua);

        $array = $getarray['exam_type'];
        // echo '<pre>';print_r($array);
        $postval['exam_detail'] = json_encode($array);

        $postval['userid'] = $user_session['id'];
        $postval['rights'] = $getarray['right'];
        $postval['wrongs'] = $getarray['wrong'];
        $postval['skiped'] = $getarray['skiped'];
        $postval['date'] = date("Y-m-d, H:i:s");

        // echo $right.'</br>'.$wrong.'</br>'.$skiped;
        // echo '<pre>';print_r($postval);
        
        $result= new MasterResult();
        if($result->save($postval)){
            $rid = $result->id;
        }

        $accurecy = ($postval['rights']/$postval['rights']+$postval['rights']+$postval['rights'])*100;
        $data = ["userid"=>$user_session['id'],"accurecy"=>$accurecy];
        $follow= new MasterFollow();
        $follow->save($data);

        return $response->redirect("test/fullsyllabusresult/".$rid);
        exit();
    }

    function fullsyllabusresultAction($resultid=''){
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $result = MasterResult::findFirst(array("id='".$resultid."' and userid='".$user_session['id']."' "));
        $test = MasterTests::findFirst(array("id='".$result->testid."'"));
        // $result->tid;
        $examresult = json_decode($result->exam_detail);

        $qint = json_decode($test->qint);
        for($i=0;$i<count($qint);$i=$i+4){
            $qtype[] = $qint[$i];
        }
        // echo '<pre>';print_r($qint);
        foreach($qtype as $qval){
            $pos = 0;$neg = 0;$count=0;
            foreach($examresult as $erval){
                if($erval->qtype==$qval){
                    if(!isset($erval->markpos)){
                        $neg = $neg+$erval->markneg;
                        $count++;
                    }else{
                        $pos = $pos+$erval->markpos;
                        $count++;
                    }
                }
            }
            $resultmark[] = ['pos'=>$pos,'neg'=>$neg,'count'=>$count,'type'=>$qval];
        }
        // echo '<pre>';print_r($resultmark);

        $qids = json_decode($result->exam_detail);
        foreach($qids as $qid){
            $qds[] = $qid->qid;
            $type[$qid->qid] = $qid->qtype;
        }


        foreach($qids as $val){
            $anscheck[$val->qid] = $val->ans;
        }

        $exam_time = MasterExamType::findFirst("etid='".$test->exam_type."' ");
        $qid = "'".implode("','", $qds)."'";
        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion'];
        foreach($tables as $key=>$tval){
            $questions[$key] = $tval::find(array("questionid IN (".$qid.")"));
        }

        $this->view->setVar("course", $course);
        $this->view->setVar("result", $result);
        $this->view->setVar("questions", $questions);
        $this->view->setVar("anscheck", $anscheck);
        $this->view->setVar("exam_time", $exam_time);
        $this->view->setVar("resultmark", $resultmark);
        $this->view->setVar("type", $type);
    }

    function swshareAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $result = MasterResult::findFirst("id='".$postval['id']."'");
            if($result){

                
                
                $q = $result->rights.'/'.($result->rights+$result->wrongs+$result->skiped);
                $acu = $result->rights/($result->rights+$result->wrongs+$result->skiped)*100;
                $time = $result->time_taken;
                $message = ' Attempt subject wise test on'.$result->test_on;
                $message .= '<div class="row">'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_qs.png" class="topic-img">'.
                '<label class="status-label">'.$q.'</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_ac.png" class="topic-img">'.
                '<label class="status-label">'.round($acu).'%</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_sp.png" class="topic-img">'.
                '<label class="status-label">'.number_format($time,2,'.','').'m</label>'.
                '</div>'.
                '</div>';
                $data['uid'] = $user_session['id'];
                $data['message'] = htmlentities($message);
                $data['notification_type'] = '5';
                $data['add_date'] = date("Y-m-d H:i:s");
                $wall = new MasterNotification();
                if($wall->save($data)){
                    $phql = "UPDATE MasterResult SET share = '1' where id='{$postval['id']}'";
                    $status = $this->modelsManager->executeQuery($phql);
                    if($status){
                        echo 'Successfully Shared your score.';
                    }
                }
                exit();
            }else{
                echo 'There is no result';
            }
        }
        exit();
    }

    function fsshareAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $result = MasterResult::findFirst("id='".$postval['id']."'");
            if($result){

                
                
                $q = $result->rights.'/'.($result->rights+$result->wrongs+$result->skiped);
                $acu = $result->rights/($result->rights+$result->wrongs+$result->skiped)*100;
                $time = $result->time_taken;
                $message = ' Attempt Full Syllabus Test';
                $message .= '<div class="row">'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_qs.png" class="topic-img">'.
                '<label class="status-label">'.$q.'</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_ac.png" class="topic-img">'.
                '<label class="status-label">'.round($acu).'%</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_sp.png" class="topic-img">'.
                '<label class="status-label">'.number_format($time,2,'.','').'m</label>'.
                '</div>'.
                '</div>';
                $data['uid'] = $user_session['id'];
                $data['message'] = htmlentities($message);
                $data['notification_type'] = '5';
                $data['add_date'] = date("Y-m-d H:i:s");
                $wall = new MasterNotification();
                if($wall->save($data)){
                    $phql = "UPDATE MasterResult SET share = '1' where id='{$postval['id']}'";
                    $status = $this->modelsManager->executeQuery($phql);
                    if($status){
                        echo 'Successfully Share you score.';
                    }
                }
                exit();
            }else{
                echo 'There is no result';
            }
        }
        exit();
    }

    function ftshareAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $result = MasterResult::findFirst("id='".$postval['id']."'");
            if($result){

                
                
                $q = $result->rights.'/'.($result->rights+$result->wrongs+$result->skiped);
                $acu = $result->rights/($result->rights+$result->wrongs+$result->skiped)*100;
                $time = $result->time_taken;
                $message = ' Attempt Fixed Test on '.$result->test_on;
                $message .= '<div class="row">'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_qs.png" class="topic-img">'.
                '<label class="status-label">'.$q.'</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_ac.png" class="topic-img">'.
                '<label class="status-label">'.round($acu).'%</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_sp.png" class="topic-img">'.
                '<label class="status-label">'.number_format($time,2,'.','').'m</label>'.
                '</div>'.
                '</div>';
                $data['uid'] = $user_session['id'];
                $data['message'] = htmlentities($message);
                $data['notification_type'] = '5';
                $data['add_date'] = date("Y-m-d H:i:s");
                $wall = new MasterNotification();
                if($wall->save($data)){
                    $phql = "UPDATE MasterResult SET share = '1' where id='{$postval['id']}'";
                    $status = $this->modelsManager->executeQuery($phql);
                    if($status){
                        echo 'Successfully Share you score.';
                    }
                }
                exit();
            }else{
                echo 'There is no result';
            }
        }
        exit();
    }

    function twshareAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $result = MasterResult::findFirst("id='".$postval['id']."'");
            if($result){
                $q = $result->rights.'/'.($result->rights+$result->wrongs+$result->skiped);
                $acu = $result->rights/($result->rights+$result->wrongs+$result->skiped)*100;
                $time = $result->time_taken;
                $message = ' Attempt Topic wise test on '.$result->test_on;
                $message .= '<div class="row">'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_qs.png" class="topic-img">'.
                '<label class="status-label">'.$q.'</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_ac.png" class="topic-img">'.
                '<label class="status-label">'.round($acu).'%</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_sp.png" class="topic-img">'.
                '<label class="status-label">'.$time.'m</label>'.
                '</div>'.
                '</div>';
                $data['uid'] = $user_session['id'];
                $data['message'] = htmlentities($message);
                $data['notification_type'] = '5';
                $data['add_date'] = date("Y-m-d H:i:s");
                $wall = new MasterNotification();
                if($wall->save($data)){
                    $phql = "UPDATE MasterResult SET share = '1' where id='{$postval['id']}'";
                    $status = $this->modelsManager->executeQuery($phql);
                    if($status){
                        echo 'Successfully Share you score.';
                    }
                }
                exit();
            }else{
                echo 'There is no result';
            }
        }
        exit();
    }

    function uwshareAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $result = MasterResult::findFirst("id='".$postval['id']."'");
            if($result){

                
                
                $q = $result->rights.'/'.($result->rights+$result->wrongs+$result->skiped);
                $acu = $result->rights/($result->rights+$result->wrongs+$result->skiped)*100;
                $time = $result->time_taken;
                $message = ' Attempt Unit wise test '.$result->test_on;
                $message .= '<div class="row">'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_qs.png" class="topic-img">'.
                '<label class="status-label">'.$q.'</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_ac.png" class="topic-img">'.
                '<label class="status-label">'.round($acu).'%</label>'.
                '</div>'.
                '<div class="col-md-3">'.
                '<img src="http://localhost/ischool4u/assets/images/is_sp.png" class="topic-img">'.
                '<label class="status-label">'.$time.'m</label>'.
                '</div>'.
                '</div>';
                $data['uid'] = $user_session['id'];
                $data['message'] = htmlentities($message);
                $data['notification_type'] = '5';
                $data['add_date'] = date("Y-m-d H:i:s");
                $wall = new MasterNotification();
                if($wall->save($data)){
                    $phql = "UPDATE MasterResult SET share = '1' where id='{$postval['id']}'";
                    $status = $this->modelsManager->executeQuery($phql);
                    if($status){
                        echo 'Successfully Share you score.';
                    }
                }
                exit();
            }else{
                echo 'There is no result';
            }
        }
        exit();
    }

    function fixtestAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval=$this->request->getPost();
        }
        $fixtest = MasterFixtest::findFirst("id='".$postval['testid']."'");
        $etype = MasterExamType::findFirst("sup_cat='{$user_session['sup_cat']}' AND exam_type='5' AND course LIKE '%{$user_session['course']}%'");
        $mtest = MasterTests::findFirst("test_type='5' AND exam_type='{$etype->etid}' AND couse='{$user_session['course']}' ");
        $q_type =  json_decode($mtest->qint);
        for($i=0;$i<count($q_type);$i=$i+4){
            $qtype[$q_type[$i]] = ["pos"=>$q_type[$i+2],"neg"=>$q_type[$i+3]];
        }
        $time = $fixtest->time*60;
        $exm_timetaken=explode(":",$postval['time_taken']);
        $hr=$exm_timetaken[0]*60*60;
        $min=$exm_timetaken[1]*60;
        $sec=$exm_timetaken[2];
        $total=(int)$hr+(int)$min+(int)$sec;
        $postval['time_taken'] = round(($time-$total)/60);

        for($i=0;$i<=count($postval['qID'])-1;$i++){
            $array[] = array(
                'qid' => $postval['qID'][$i],
                'ans' => $postval['userans'][$i]
                );
            $userans[$postval['qID'][$i]] = $postval['userans'][$i];
        }

        $qusids = implode("','",$postval['qID']);
        $qusids = "'".$qusids."'";
        for ($i=1; $i <=5 ; $i++) {
            $qusdetail = MasterQuestion::find("tableid='".$i."' AND questionid IN (".$qusids.") ");
            if(!empty($qusdetail)){
                foreach($qusdetail as $val){
                    $twq[$i][] = $val->questionid;
                    $type[] = [$val->questionid,$val->q_type];
                    if($val->q_type=='3'){
                        $match_qid[] = $val->questionid;
                    }
                }
            }   
        }

        $mcount = array("a","b","c","d");
        foreach($match_qid as $mval){
            $mua[$mval]["rans"] = $postval[$mval.'rans'];
            foreach($mcount as $mcval){
                $mua[$mval]["uans"][] = $postval[$mval.'uans'.$mcval];
            }
        }

        // add q_type in array
        foreach($type as $val){
            foreach($array as $av){
                if($val[0]==$av['qid']){
                    if(key($array)==0){
                        $ar = count($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }else{
                        $ar = key($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }
                }
            }
        }
        
        // **************
        

        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion'];
        for ($i=1; $i <=5 ; $i++) {
            if(!empty($twq[$i])){
                $qusd = implode("','",$twq[$i]);
                $qusd = "'".$qusd."'";
                $qusdetail = $tables[$i]::find("questionid IN (".$qusd.") ");
                foreach($qusdetail as $val){
                    $ans[$val->questionid] = $val->ans;
                }
            }
        }

        $getarray = $this->checkans($postval['qID'],$userans,$ans,$tarray,$qtype,$array,$mua);

        $array = $getarray['exam_type'];
        // echo '<pre>';print_r($array);
        $postval['exam_detail'] = json_encode($array);

        $postval['userid'] = $user_session['id'];
        $postval['rights'] = $getarray['right'];
        $postval['wrongs'] = $getarray['wrong'];
        $postval['skiped'] = $getarray['skiped'];
        $postval['test_on'] = $fixtest->name;
        $postval['date'] = date("Y-m-d, H:i:s");
        // echo '<pre>';print_r($postval);

        $result= new MasterResult();
        if($result->save($postval)){
            $rid = $result->id;
        }

        $accurecy = ($postval['rights']/$postval['rights']+$postval['rights']+$postval['rights'])*100;
        $data = ["userid"=>$user_session['id'],"accurecy"=>$accurecy];
        $follow= new MasterFollow();
        $follow->save($data);

        return $response->redirect("test/fixedresult/".$rid);
        exit();
    }

    function topicwiseAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval=$this->request->getPost();
        }
        $mt = MasterTests::findFirst("id='".$postval['testid']."'");
        $examtype = MasterExamType::findFirst("etid='".$mt->exam_type."'");

        //  Time taken....
        $time = $examtype->time*60;
        $exm_timetaken=explode(":",$postval['time_taken']);
        $hr=$exm_timetaken[0]*60*60;
        $min=$exm_timetaken[1]*60;
        $sec=$exm_timetaken[2];
        $total=(int)$hr+(int)$min+(int)$sec;
        $postval['time_taken'] = round(($time-$total)/60);
        //===========================================
        
        $q_type =  json_decode($mt->qint);
        for($i=0;$i<count($q_type);$i=$i+4){
            $qtype[$q_type[$i]] = ["pos"=>$q_type[$i+2],"neg"=>$q_type[$i+3]];
        }

        for($i=0;$i<=count($postval['qID'])-1;$i++){
            $array[] = array(
                'qid' => $postval['qID'][$i],
                'ans' => $postval['userans'][$i]
                );
            $userans[$postval['qID'][$i]] = $postval['userans'][$i];
        }

        $qusids = implode("','",$postval['qID']);
        $qusids = "'".$qusids."'";
        for ($i=1; $i <=5 ; $i++) { 
            $qusdetail = MasterQuestion::find("tableid='".$i."' AND questionid IN (".$qusids.") ");
            if(!empty($qusdetail)){
                foreach($qusdetail as $val){
                    $twq[$i][] = $val->questionid;
                    $type[] = [$val->questionid,$val->q_type]; // $type contain type of question 
                    if($val->q_type=='3'){
                        $match_qid[] = $val->questionid; // Match type questions...
                    }
                }
            } 
        }

        // Multi match answers.....
        $mcount = array("a","b","c","d");
        foreach($match_qid as $mval){
            $mua[$mval]["rans"] = $postval[$mval.'rans'];
            foreach($mcount as $mcval){
                $mua[$mval]["uans"][] = $postval[$mval.'uans'.$mcval];
            }
        }
        //=====================================

        // add question type in array
        foreach($type as $val){
            foreach($array as $av){
                if($val[0]==$av['qid']){
                    if(key($array)==0){
                        $ar = count($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }else{
                        $ar = key($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }
                }
            }
        }
        //========================================


        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion'];
        for ($i=1; $i <=5 ; $i++) {
            if(!empty($twq[$i])){
                $qusd = implode("','",$twq[$i]);
                $qusd = "'".$qusd."'";
                $qusdetail = $tables[$i]::find("questionid IN (".$qusd.") ");
                foreach($qusdetail as $val){
                    $ans[$val->questionid] = $val->ans;
                }
            }
        }

        
        $getarray = $this->checkans($postval['qID'],$userans,$ans,$tarray,$qtype,$array,$mua);
        $array = $getarray['exam_type'];
        $postval['exam_detail'] = json_encode($array);
        $postval['userid'] = $user_session['id'];
        $postval['rights'] = $getarray['right'];
        $postval['wrongs'] = $getarray['wrong'];
        $postval['skiped'] = $getarray['skiped'];
        $postval['test_on'] = $postval['topic'];
        $postval['date'] = date("Y-m-d, H:i:s");

        // echo '<pre>';print_r($postval);

        $package = MasterUserPackage::findfirst("userid='".$user_session['id']."' AND test_type='2'");
        if($package->nots==1){
            $phql = "DELETE FROM MasterUserPackage WHERE id = '".$package->id."'";
        }else{
            $not = $package->nots-1;
            $phql = "UPDATE MasterUserPackage SET nots = '".$not."' where id=".$package->id."";
        }
        $status = $this->modelsManager->executeQuery($phql);

        $result= new MasterResult();
        if($result->save($postval)){
            $rid = $result->id;
        }
        $accurecy = ($postval['rights']/$postval['rights']+$postval['rights']+$postval['rights'])*100;
        $data = ["userid"=>$user_session['id'],"accurecy"=>$accurecy];
        $follow= new MasterFollow();
        $follow->save($data);
        return $response->redirect("test/topicresult/".$rid);
        // exit();
    }

    function fixedresultAction($resultid='')
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $result = MasterResult::findFirst(array("id='".$resultid."' and userid='".$user_session['id']."' "));
        $etype = MasterExamType::findFirst("sup_cat='{$user_session['sup_cat']}' AND exam_type='5' AND course LIKE '%{$user_session['course']}%'");
        $test = MasterTests::findFirst("test_type='5' AND exam_type='{$etype->etid}' AND couse='{$user_session['course']}' ");
        // $result->tid;
        $examresult = json_decode($result->exam_detail);

        $qint = json_decode($test->qint);
        for($i=0;$i<count($qint);$i=$i+4){
            $qtype[] = $qint[$i];
        }
        
        foreach($qtype as $qval){
            $pos = 0;$neg = 0;$count=0;
            foreach($examresult as $erval){
                if($erval->qtype==$qval){
                    if(!isset($erval->markpos)){
                        $neg = $neg+$erval->markneg;
                        $count++;
                    }else{
                        $pos = $pos+$erval->markpos;
                        $count++;
                    }
                }
            }
            $resultmark[] = ['pos'=>$pos,'neg'=>$neg,'count'=>$count,'type'=>$qval];
        }
        // echo '<pre>';print_r($resultmark);
        // exit();

        $qids = json_decode($result->exam_detail);
        foreach($qids as $qid){
            $qds[] = $qid->qid;
            $type[$qid->qid] = $qid->qtype;
        }


        foreach($qids as $val){
            $anscheck[$val->qid] = $val->ans;
        }

        $exam_time = MasterExamType::findFirst("etid='".$test->exam_type."' ");

        $qid = "'".implode("','", $qds)."'";
        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion'];
        foreach($tables as $key=>$tval){
            $questions[$key] = $tval::find(array("questionid IN (".$qid.")"));
        }
        $this->view->setVar("course", $course);
        $this->view->setVar("result", $result);
        $this->view->setVar("questions", $questions);
        $this->view->setVar("anscheck", $anscheck);
        $this->view->setVar("exam_time", $exam_time);
        $this->view->setVar("resultmark", $resultmark);
    }

    function topicresultAction($resultid='')
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $result = MasterResult::findFirst(array("id='".$resultid."' and userid='".$user_session['id']."' "));
        $test = MasterTests::findFirst(array("id='".$result->testid."'"));
        // $result->tid;
        $qids = json_decode($result->exam_detail);

        $qint = json_decode($test->qint);
        for($i=0;$i<count($qint);$i=$i+4){
            $qtype[] = $qint[$i];
        }
        // echo '<pre>';print_r($qint);
        foreach($qtype as $qval){
            $pos = 0;$neg = 0;$count=0;
            foreach($qids as $erval){
                if($erval->qtype==$qval){
                    if(!isset($erval->markpos)){
                        $neg = $neg+$erval->markneg;
                        $count++;
                    }else{
                        $pos = $pos+$erval->markpos;
                        $count++;
                    }
                }
            }
            $resultmark[] = ['pos'=>$pos,'neg'=>$neg,'count'=>$count,'type'=>$qval];
        }
        // echo '<pre>';print_r($resultmark);

        foreach($qids as $qid){
            $qds[] = $qid->qid;
            $type[$qid->qid] = $qid->qtype;
        }
        $qid = "'".implode("','", $qds)."'";
        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion'];
        foreach($tables as $key=>$tval){
            $questions[$key] = $tval::find(array("questionid IN (".$qid.")"));
        }

        // Status.. of question
        foreach($qids as $val){
            $status[$val->qid] = $val->status;
        }

        $exam_time = MasterExamType::findFirst("etid='".$test->exam_type."' ");
        $this->view->setVar("course", $course);
        $this->view->setVar("result", $result);
        $this->view->setVar("questions", $questions);
        $this->view->setVar("status", $status);
        $this->view->setVar("exam_time", $exam_time);
        $this->view->setVar("resultmark", $resultmark);
        $this->view->setVar("type", $type);
        // $result = MasterResult::findFirst("id='".$resultid."' ");
        // echo '<pre>';print_r($result);
        // exit();
    }

    function freetestAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval=$this->request->getPost();
        }
        $freetest = MasterFreetest::findFirst("id='".$postval['testid']."'");        
        $qids =  json_decode($freetest->question_id);
        
        $time = $freetest->time*60;
        $exm_timetaken=explode(":",$postval['time_taken']);
        $hr=$exm_timetaken[0]*60*60;
        $min=$exm_timetaken[1]*60;
        $sec=$exm_timetaken[2];
        $total=(int)$hr+(int)$min+(int)$sec;
        $postval['time_taken'] = round(($time-$total)/60);

        for($i=0;$i<=count($postval['qID'])-1;$i++){
            $array[] = array(
                'qid' => $postval['qID'][$i],
                'ans' => $postval['userans'][$i]
                );
            $userans[$postval['qID'][$i]] = $postval['userans'][$i];
        }

        $qusids = implode("','",$postval['qID']);
        $qusids = "'".$qusids."'";
        for ($i=1; $i <=5 ; $i++) {
            $qusdetail = MasterQuestion::find("tableid='".$i."' AND questionid IN (".$qusids.") ");
            if(!empty($qusdetail)){
                foreach($qusdetail as $val){
                    $twq[$i][] = $val->questionid;
                    $type[] = [$val->questionid,$val->q_type];
                    if($val->q_type=='3'){
                        $match_qid[] = $val->questionid;
                    }
                }
            }   
        }

        $mcount = array("a","b","c","d");
        foreach($match_qid as $mval){
            $mua[$mval]["rans"] = $postval[$mval.'rans'];
            foreach($mcount as $mcval){
                $mua[$mval]["uans"][] = $postval[$mval.'uans'.$mcval];
            }
        }

        // add q_type in array
        foreach($type as $val){
            foreach($array as $av){
                if($val[0]==$av['qid']){
                    if(key($array)==0){
                        $ar = count($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }else{
                        $ar = key($array)-1;
                        $array[$ar]['qtype'] = $val[1];
                        $tarray[$array[$ar]['qid']][] = [$val[1],$ar];
                    }
                }
            }
        }
        
        // **************
        

        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion'];
        for ($i=1; $i <=5 ; $i++) {
            if(!empty($twq[$i])){
                $qusd = implode("','",$twq[$i]);
                $qusd = "'".$qusd."'";
                $qusdetail = $tables[$i]::find("questionid IN (".$qusd.") ");
                foreach($qusdetail as $val){
                    $ans[$val->questionid] = $val->ans;
                }
            }
        }

        $getarray = $this->checkans($postval['qID'],$userans,$ans,$tarray,$qtype,$array,$mua);

        $array = $getarray['exam_type'];
        // echo '<pre>';print_r($array);
        $postval['exam_detail'] = json_encode($array);

        $postval['userid'] = $user_session['id'];
        $postval['rights'] = $getarray['right'];
        $postval['wrongs'] = $getarray['wrong'];
        $postval['skiped'] = $getarray['skiped'];
        $postval['test_on'] = $freetest->name;
        $postval['date'] = date("Y-m-d, H:i:s");
        // echo '<pre>';print_r($postval);

        $result= new MasterResult();
        if($result->save($postval)){
            $rid = $result->id;
        }
        return $response->redirect("test/freetestresult/".$rid);
        exit();
    }

    function freetestresultAction($resultid='')
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $freetest = MasterFreetest::findFirst();
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $result = MasterResult::findFirst(array("id='".$resultid."' and userid='".$user_session['id']."' "));
        // $etype = MasterExamType::findFirst("sup_cat='{$user_session['sup_cat']}' AND exam_type='5' AND course LIKE '%{$user_session['course']}%'");
        // $test = MasterTests::findFirst("test_type='5' AND exam_type='{$etype->etid}' AND couse='{$user_session['course']}' ");
        // $result->tid;
        $examresult = json_decode($result->exam_detail);

        // $qint = json_decode($test->qint);
        // for($i=0;$i<count($qint);$i=$i+4){
        //     $qtype[] = $qint[$i];
        // }
        
        // foreach($qtype as $qval){
        //     $pos = 0;$neg = 0;$count=0;
        //     foreach($examresult as $erval){
        //         if($erval->qtype==$qval){
        //             if(!isset($erval->markpos)){
        //                 $neg = $neg+$erval->markneg;
        //                 $count++;
        //             }else{
        //                 $pos = $pos+$erval->markpos;
        //                 $count++;
        //             }
        //         }
        //     }
        //     $resultmark[] = ['pos'=>$pos,'neg'=>$neg,'count'=>$count,'type'=>$qval];
        // }
        // echo '<pre>';print_r($resultmark);
        // exit();

        $qids = json_decode($result->exam_detail);
        foreach($qids as $qid){
            $qds[] = $qid->qid;
            $type[$qid->qid] = $qid->qtype;
            $anscheck[$qid->qid] = $qid->ans;
        }

        $exam_time = 

        $qid = "'".implode("','", $qds)."'";
        $tables = ['1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion'];
        foreach($tables as $key=>$tval){
            $questions[$key] = $tval::find(array("questionid IN (".$qid.")"));
        }
        $this->view->setVar("course", $course);
        $this->view->setVar("result", $result);
        $this->view->setVar("questions", $questions);
        $this->view->setVar("anscheck", $anscheck);
        $this->view->setVar("exam_time", $exam_time);
        $this->view->setVar("resultmark", $resultmark);
    }

    function quizAction($id='')
    {
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $but = MasterQuiz::findFirst("quiz_id='{$id}'");
        $time = 20*60;
        $exm_timetaken=explode(":",$postval['time_taken']);
        $hr=$exm_timetaken[0]*60*60;
        $min=$exm_timetaken[1]*60;
        $sec=$exm_timetaken[2];
        $total=(int)$hr+(int)$min+(int)$sec;
        $postval['time_taken'] = round(($time-$total)/60);
        for($i=0;$i<=count($postval['qID'])-1;$i++){
            $array[] = array(
                'qid' => $postval['qID'][$i],
                'ans' => $postval['userans'][$i]
                );
            $userans[$postval['qID'][$i]] = $postval['userans'][$i];
        }
        $qids = json_decode($but->questionid);
        $qids = implode("','", $qids);
        $skipped=$right=$wrong=0;
        $mqb = MasterQuestionBank::find("questionid IN ('{$qids}')");
        foreach($array as $key=>$arval){
            foreach($mqb as $mqbval){
                if($mqbval->questionid==$arval['qid']){
                    if($arval['ans']=='skipped'){
                        $array[$key]['markpos'] = 0;
                        $array[$key]['status'] = 'skipped';
                        $skipped++;
                    }else{
                        if($mqbval->ans==$arval['ans']){
                            $array[$key]['markpos'] = 3;
                            $array[$key]['status'] = 'attempt';
                            $right++;
                        }else{
                            $array[$key]['markneg'] = 1;
                            $array[$key]['status'] = 'attempt';
                            $wrong++;
                        }
                    }
                }
            }
        }
        $postval['right'] = $right;
        $postval['wrong'] = $wrong;
        $postval['skipped'] = $skipped;
        $postval['date'] = date("Y-m-d, H:i:s");
        if(!empty($user_session)){
            $postval['exam_detail'] = json_encode($array);
            $postval['userid'] = $user_session['id'];
            $postval['test_type'] = 7;
            $result= new MasterResult();
            $result->save($postval);
        }else{
            $postval['exam_detail'] = $array;
        }
        $this->view->setVar("quiz", $postval);
    }
}

?>