<?php
class SubjectlistController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Discover A Smart Marketplace... ');
        parent::initialize();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $fixpack = MasterPackages::find("course='".$user_session['course']."' AND sup_cat='".$user_session['sup_cat']."'");
            $this->view->setVar("fixpack", $fixpack);
        }
    }
    function indexAction()
    {
        $slug=$this->dispatcher->getParam("pagename");
    	$response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($slug!=''){
            $user = $this->session->get("user");
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subject = MasterSubject::findFirst(array("slug='".$slug."' AND status=1"));
            if($subject->id!=''){
                $package = MasterUserPackage::findFirst("userid='".$user['id']."' AND test_type='1'");
                if($package){
                    $this->view->setVar("package", 'open');
                }else{
                    $this->view->setVar("package", 'close');
                }
                $subsubjects = MasterSubSubject::find(array("subid='".$subject->id."' AND status=1"));
                $this->view->setVar("subsubjects", $subsubjects);
                $this->view->setVar("subjects", $slug);
                $follow = $this->followAction();
                $cond = "uid='{$user_session['id']}'";
                $chalenge1 = MasterFriendlist::findFirst(array($cond));
               
                $arr = array();
                if(!empty($chalenge1->accept_friend)){
                $arr = json_decode($chalenge1->accept_friend);
                }

                $cond2 = "LOCATE({$user_session['id']},accept_friend)";
                $chalenge2 = MasterFriendlist::find(array($cond2));
                $arr1 = array();
                foreach ($chalenge2 as $val) {
                    $arr1[] = $val->uid;
                }

                $this->view->setVar("follow",$follow);
                $this->view->setVar("follower",$arr1);
                $this->view->setVar("following",$arr);
            }else{
                echo 'lkajsfdka';
                exit();
            }
        }else{
            $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something wrong please try again.</div>");
            return $response->redirect("index/dashboard");
        }
    }
    function topicsAction()
    {
        $subject=$this->dispatcher->getParam("pagename");
        $subsubject=$this->dispatcher->getParam("sub");
    	$response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($subsubject!=''){
            $user = $this->session->get("user");
            $package = MasterUserPackage::findFirst("userid='".$user['id']."' AND test_type='3'");
            if($package){
                $this->view->setVar("package", 'open');
            }else{
                $this->view->setVar("package", 'close');
            }
            // print_r($user_session);
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subjects = MasterSubject::findFirst(array("slug='".$subject."'"));
            if($subjects->id!=''){
                $subsubjects = MasterSubSubject::findFirst(array("slug='".$subsubject."'"));
                if($subsubjects->ssid!=''){
                    $topics = MasterTopics::find(array("subid='".$subjects->id."' AND ssubid='".$subsubjects->ssid."' AND status='1'"));
                    $tests = MasterTests::find("test_type='3' AND couse='".$user['course']."'");                    
                    $this->view->setVar("topics", $topics);
                    $this->view->setVar("subject", $subjects);
                    $this->view->setVar("subsubject", $subsubjects);
                    $this->view->setVar("tests", $tests);
                    $follow = $this->followAction();
                    $cond = "uid='{$user_session['id']}'";
                    $chalenge1 = MasterFriendlist::findFirst(array($cond));
                   
                    $arr = array();
                    if(!empty($chalenge1->accept_friend)){
                    $arr = json_decode($chalenge1->accept_friend);
                    }

                    $cond2 = "LOCATE({$user_session['id']},accept_friend)";
                    $chalenge2 = MasterFriendlist::find(array($cond2));
                    $arr1 = array();
                    foreach ($chalenge2 as $val) {
                        $arr1[] = $val->uid;
                    }

                    $this->view->setVar("follow",$follow);
                    $this->view->setVar("follower",$arr1);
                    $this->view->setVar("following",$arr);
                }
            }
        }
    }
    function subtopicsAction()
    {
        $subject=$this->dispatcher->getParam("pagename");
        $subsubject=$this->dispatcher->getParam("sub");
        $topic=$this->dispatcher->getParam("topic");
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($topic!=''){
            $user = $this->session->get("user");
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subjects = MasterSubject::findFirst(array("slug='".$subject."'"));
            if($subjects->id!=''){
                $subsubjects = MasterSubSubject::findFirst(array("slug='".$subsubject."'"));
                if($subsubjects->ssid!=''){
                    $topics = MasterTopics::findFirst(array("slug='".$topic."'"));
                    if($topics->tid!=''){
                        $subtopic = MasterSubTopics::find(array("subid='".$subjects->id."' AND ssubid='".$subsubjects->ssid."' AND tid='".$topics->tid."'"));
                    }
                    $this->view->setVar("subtopic", $subtopic);
                    $this->view->setVar("subject", $subjects);
                    $this->view->setVar("subsubject", $subsubjects);
                    $this->view->setVar("topic",$topics);
                    // follow code here
            $follow = $this->followAction();
            $cond = "uid='{$user_session['id']}'";
            $chalenge1 = MasterFriendlist::findFirst(array($cond));
           
            $arr = array();
            if(!empty($chalenge1->accept_friend)){
            $arr = json_decode($chalenge1->accept_friend);
            }

            $cond2 = "LOCATE({$user_session['id']},accept_friend)";
            $chalenge2 = MasterFriendlist::find(array($cond2));
            $arr1 = array();
            foreach ($chalenge2 as $val) {
                $arr1[] = $val->uid;
            }

            $this->view->setVar("follow",$follow);
            $this->view->setVar("follower",$arr1);
            $this->view->setVar("following",$arr);
            //===========================================
                }
            }
        }
    }
    function subtopicstipAction()
    {
        $subject=$this->dispatcher->getParam("pagename");
        $subsubject=$this->dispatcher->getParam("sub");
        $topic=$this->dispatcher->getParam("topic");
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($topic!=''){
            $user = $this->session->get("user");
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subjects = MasterSubject::findFirst(array("slug='".$subject."'"));
            if($subjects->id!=''){
                $subsubjects = MasterSubSubject::findFirst(array("slug='".$subsubject."'"));
                if($subsubjects->ssid!=''){
                    $topics = MasterTopics::findFirst(array("slug='".$topic."'"));
                    if($topics->tid!=''){
                        $subtopic = MasterSubTopics::find(array("subid='".$subjects->id."' AND ssubid='".$subsubjects->ssid."' AND tid='".$topics->tid."'"));
                    }
                    $this->view->setVar("subtopic", $subtopic);
                    $this->view->setVar("subject", $subjects);
                    $this->view->setVar("subsubject", $subsubjects);
                    $this->view->setVar("topic",$topics);
                }
            // follow code here
            $follow = $this->followAction();
            $cond = "uid='{$user_session['id']}'";
            $chalenge1 = MasterFriendlist::findFirst(array($cond));
           
            $arr = array();
            if(!empty($chalenge1->accept_friend)){
            $arr = json_decode($chalenge1->accept_friend);
            }

            $cond2 = "LOCATE({$user_session['id']},accept_friend)";
            $chalenge2 = MasterFriendlist::find(array($cond2));
            $arr1 = array();
            foreach ($chalenge2 as $val) {
                $arr1[] = $val->uid;
            }

            $this->view->setVar("follow",$follow);
            $this->view->setVar("follower",$arr1);
            $this->view->setVar("following",$arr);
            //===========================================
            }
        }
    }
    function questionAction()//$examtype = "assessment"
    {
        $subjct=$this->dispatcher->getParam("pagename");
        $currentPage=$_GET['page'];
        $subsubjct=$this->dispatcher->getParam("sub");
        $topics=$this->dispatcher->getParam("topic");
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }

        if(isset($subjct) && isset($subsubjct)){
            $getVal=$this->request->get();
            $user = $this->session->get("user");
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subject = MasterSubject::findFirst(array("slug='".$subjct."'"));
            $subsubject = MasterSubSubject::findFirst(array("slug='".$subsubjct."'"));
            $topic = MasterTopics::findFirst(array("slug='".$topics."'"));
            $qtype = MasterQuesType::find(array("status=1"));
            if (count($qtype)) {
                $this->view->setVar("qtype", $qtype);
            }
            $package = MasterUserPackage::findFirst("userid='".$user['id']."' AND test_type='2'");
            if($package){
                $this->view->setVar("package", 'open');
            }else{
                $this->view->setVar("package", 'close');
            }
            $etype = MasterPractice::findFirst(array("sup_cat='{$user_session['sup_cat']}' AND course='{$user_session['course']}' AND status = '1'"));
            if (isset($etype->id)){
                $level = json_decode($etype->ql_easy,true);
                $e_type = '1';//easy
                $getVal=$this->request->get();
                if(!empty($getVal['qlavel']) && $getVal['qlavel']==1){
                    $level = json_decode($etype->ql_easy,true);
                }elseif(!empty($getVal['qlavel']) && $getVal['qlavel']==2){
                    $level = json_decode($etype->ql_medium,true);
                }elseif(!empty($getVal['qlavel']) && $getVal['qlavel']==3){
                    $level = json_decode($etype->ql_hard,true);
                }
                
                
                if($getVal['qtype']!=''){
                    $qustype = str_split($getVal['qtype']);
                    $qustype = implode(',', $qustype);
                    $questions = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND q_type IN (".$qustype.") AND e_type LIKE '%".$e_type."%' "));

                }else{
                    
                    $questions = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' "));
                    
                }
                
                // echo $questions->count();exit();
                if($questions->count()!=0){
                    $getTotques = $questions->count();
                    $questionIdlist = array();
                    //for ($i=0; $i < count($level); $i++) { 
                    // ========= Get hard question count =============
                    // echo 'total = '.$getTotques."<br>";
                    $getHard = $getTotques * ($level[2]/100);
                    $hardCount = 0;
                    if ($getHard > 0 && $getHard < 1) {
                        $hardCount = 1;
                    }
                    elseif ($getHard <= 0) {
                        $hardCount = 0;
                    }
                    else
                    {
                        $hardCount = round($getHard);
                    }
                    if ($hardCount>=0){
                        if(!empty($qustype)){
                            $hard = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_level = '3' AND q_type IN (".$qustype.") limit ".$hardCount.""));
                        }else{
                            $hard = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_level = '3' limit ".$hardCount.""));
                        }
                        
                        // echo '<pre>';print_r($hard);
                        if(count($hard)>0)
                        {
                            foreach ($hard as $key => $hardId) {
                                array_push($questionIdlist, $hardId->questionid);
                            }
                            $remainCount = $getTotques - $hardCount;
                        }
                        else
                        {
                            $remainCount = $getTotques - 0;
                            $hardCount = 0; 
                        }
                    }
                    // echo 'hard = '.$hardCount."-".$remainCount."<br>"
                    // 
                    // ========= Get Medium question count =============

                    $getMedium = $getTotques * ($level[1]/100);
                    $mediumCount = 0;
                    if ($getMedium > 0 && $getMedium < 1) {
                        $mediumCount = 1;
                    }
                    elseif ($getMedium <= 0) {
                        $mediumCount = 0;
                    }
                    else
                    {
                        $mediumCount = round($getMedium);   
                    }
                    if ($mediumCount>=0) {
                        if(!empty($qustype)){
                            $mid = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_type IN (".$qustype.") AND q_level = '2' limit ".$mediumCount.""));
                        }else{
                            $mid = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_level = '2' limit ".$mediumCount.""));
                        }
                        if(count($mid)>0)
                        {
                            foreach ($mid as $key => $midId) {
                                array_push($questionIdlist, $midId->questionid);
                            }
                            $remainCount = $getTotques - $mediumCount;
                        }
                        else
                        {
                            $remainCount = $getTotques - 0;
                            $mediumCount = 0;   
                        }
                    }
                    // echo 'mid = '.$mediumCount."-".$remainCount."<br>";
                    //============== Get Easy Question ====================
                    if ($remainCount>0) {
                        if(!empty($qustype)){
                            $easy = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_type IN (".$qustype.") AND q_level = '1' limit ".$remainCount.""));
                        }else{
                            $easy = MasterQuestion::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND e_type LIKE '%".$e_type."%' AND q_level = '1' limit ".$remainCount.""));
                        }
                        if (count($easy)>0) {
                            foreach ($easy as $key => $easyId) {
                                array_push($questionIdlist, $easyId->questionid);
                            }
                        }
                        else
                        {
                            $remainCount = $getTotques - 0;   
                            $remainCount = 0;
                        }
                    }
                    if($getVal['atp']==3){
                        $questionid = MasterBookmarks::find("user_id='".$user_session['id']."' AND type='1' ");
                    }elseif($getVal['atp']==2){
                        $questionid = MasterQattempt::find("user_id='".$user_session['id']."' ");
                    }elseif($getVal['atp']==1){
                        $questionid = MasterQattempt::find("user_id='".$user_session['id']."' ");
                    }
                    foreach($questionid as $val){
                        $books[]=$val->questionid;
                    }
                    if($getVal['atp']==2 && isset($getVal['atp'])){
                        $questionIdlist = array_diff($questionIdlist,$books);
                    }elseif(isset($getVal['atp']) && $getVal['atp']==1 || $getVal['atp']==3){
                        $questionIdlist = array_intersect($questionIdlist,$books);
                    }
                    // $this->view->setVar("questionsIDlist", $questionIdlist);
                    $paginator = new \Phalcon\Paginator\Adapter\NativeArray(array(
                               "data" => $questionIdlist,
                               "limit" => 4,
                               "page" => $currentPage
                                )
                   );

                   $page = $paginator->getPaginate();
                   $this->view->setVar("questionsIDlist", $page);
                }else{
                    $this->view->setVar("nodata", "No Data Found");
                }
            }
            
            if ($this->request->isGet()) {
                // print_r($this->request->get());exit();
            }
            $this->view->setVar("etypes", $etype);
            $this->view->setVar("subject", $subject);
            $this->view->setVar("subsubject", $subsubject);
            $this->view->setVar("topics", $topic);
            
        }else{
            $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something wrong please try again.</div>");
            return $response->redirect("index/dashboard");
        }
    }
    function tipsAction()
    {
        $subjects=$this->dispatcher->getParam("pagename");
        $subsubjects=$this->dispatcher->getParam("sub");
        $topics=$this->dispatcher->getParam("topic");
        $subtopics=$this->dispatcher->getParam("subtopic");
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if(isset($subsubjects) && isset($subjects)){
            $user = $this->session->get("user");
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subject = MasterSubject::findFirst(array("slug='".$subjects."'"));
            $subsubject = MasterSubSubject::findFirst(array("slug='".$subsubjects."'"));
            $cat = '"'.$subject->id.'","'.$subsubject->ssid.'"';
            $topic = MasterTopics::findFirst(array("slug='".$topics."'"));
            $subtopic = MasterSubTopics::findFirst("slug='".$subtopics."'");
            $tipstype = MasterConceptType::find(array("status=1"));
            $tips = MasterConcept::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND subtopics='".$subtopic->stid."' AND status='1'"));
            $this->view->setVar("tipstype", $tipstype);
            $this->view->setVar("tips", $tips);
            $this->view->setVar("subject", $subject);
            $this->view->setVar("subsubject", $subsubject);
            $this->view->setVar("topics", $topic);
            
        }
    }

    function viewarticleAction()
    {
        $subject=$this->dispatcher->getParam("pagename");
        $subsubject=$this->dispatcher->getParam("sub");
        $topic=$this->dispatcher->getParam("topic");
        $subtopic=$this->dispatcher->getParam("subtopic");
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $user = $this->session->get("user");
        $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
        $this->view->setVar("course", $course);
        if(isset($subject)){
            $subject=MasterSubject::findFirst("slug='".$subject."'");
            if(isset($subject->id)){
                $subsubject=MasterSubSubject::findFirst("slug='".$subsubject."'");
                if(isset($subsubject->ssid)){
                    $topic=MasterTopics::findFirst("slug='".$topic."'");
                    if(isset($topic->tid)){
                        $subtopic=MasterSubTopics::findFirst("slug='".$subtopic."'");
                        if(isset($subtopic->stid)){
                            $article=MasterArticle::find("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND subtopics='".$subtopic->stid."'");
                            if($article->count()!=0){
                                $this->view->setVar("article", $article);
                                $this->view->setVar("subject", $subject);
                                $this->view->setVar("subsubject", $subsubject);
                                $this->view->setVar("topics", $topic);

                            }
                        }
                    }
                }
            }
        }

        $this->view->setVar("subject", $subject);
        $this->view->setVar("subsubject", $subsubject);
        $this->view->setVar("topics", $topic);
        
    }
    
    function challenged_questionAction()
    {
        $response = new \Phalcon\Http\Response();
        $qid = $this->dispatcher->getParam("pagename");
        $noticationid= $this->dispatcher->getParam("id");
        if($noticationid!=''){
            $upd="UPDATE MasterNotification SET status='0' WHERE id='$noticationid'";
            $this->modelsManager->executeQuery($upd);
             $notication = MasterNotification::findFirst(array("id='".$noticationid."'"));
             $this->view->setVar("notication", $notication->uid);
        }
        $qid=base64_decode($qid);
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $reply=  MasterChallengeans::find(array("question_id='".$qid."' ORDER BY id"));
        $rr='';
        $i=0;
        foreach($reply as $val){
             $user = MasterUsers::findFirst(array("uid='".$val->uid."'"));
            $rr[$i]['name']=$user->first_name." ".$user->last_name;
            $rr[$i]['ans']=$val->optionans;
            $rr[$i]['add_date']=date("jS M , h:i A",strtotime($val->add_date));
            $i++;
        }
        $this->view->setVar("reply", $rr);
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $this->view->setVar("course", $course);
        $qusdetail = MasterQuestion::findFirst(array("questionid='".$qid."'"));
        $subject = MasterSubject::findFirst(array("id='".$qusdetail->subject."'"));
        $subsubject = MasterSubSubject::findFirst(array("ssid='".$qusdetail->subsubject."'"));
        $topic = MasterTopics::findFirst(array("tid='".$qusdetail->topics."'"));
        $this->view->setVar("subject", $subject);
        $this->view->setVar("subsubject", $subsubject);
        $this->view->setVar("topics", $topic);
        $this->view->setVar("questionsIDlist", array($qusdetail->questionid));
        $this->view->setVar("ans", $qusdetail->ans);

    }

    function filterAction(){
        $response = new \Phalcon\Http\Response();
        $getVal=$this->request->get();
        if(!empty($getVal)){
            $qtype = (!empty($getVal['qtype']))?$getVal['qtype']:'';
            $qlavel = (!empty($getVal['qlavel']))?$getVal['qlavel']:'';
            $atp = (!empty($getVal['atp']))?$getVal['atp']:'';
            
            return $response->redirect($getVal['url']."?qtype=".$qtype.'&qlavel='.$qlavel.'&atp='.$atp);
        }
    }

    function followAction()
    {
        $array=[];
        $date = date("Y-m-d");
        $user_session = $this->session->get("user");
        $follow = MasterFollow::find("observe_date='{$date}' AND status='1' AND userid!='$user_session[id]'");
        foreach($follow as $fval){
            $array[] = $fval->userid;
        }
        
        $uid = implode("','", $array);
        $user = MasterUsers::find("uid IN ('{$uid}')");
        foreach($follow as $flval){
            foreach($user as $uval){
                if($uval->uid==$flval->userid){
                    $data[] = [
                        "userid"=>$flval->userid,
                        "img"=>$uval->img,
                        "name"=>$uval->first_name.' '.$uval->last_name,
                        "per"=>$flval->per
                    ];
                }
            }
        }
        return $data;
        exit();
    }
}