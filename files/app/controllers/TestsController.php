<?php
/**
* Pages Controller
* Controller Name: Test Controller
* Author By: Rajesh
* Created On:8/08/2015
* Modified: N/A
**/
class TestsController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        
    }

    function indexAction()
    {
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
     error_reporting(0);
     if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if($getVal['submit']){
                if($getVal['subject']==''){
                    unset($getVal['subject']);
                }else{
                    $where= ' subject="'.$getVal['subject'].'"';
                    $subsubdet = MasterSubSubject::find("subid = '".$getVal['subject']."' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if($getVal['subsubject']==''){
                    unset($getVal['subsubject']);
                }else{
                    $where .= ' AND subsubject ="'.$getVal['subsubject'].'"';
                    $mTopicdet = MasterTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if($getVal['topics']==''){
                    unset($getVal['topics']);
                }else{
                    $where .= ' AND topics="'.$getVal['topics'].'"';
                    $msTopicdet = MasterSubTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND tid='".$getVal['topics']."' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if($getVal['subtopics']==''){
                    unset($getVal['subtopics']);
                }else{
                    $where .= ' AND subtopics="'.$getVal['subtopics'].'"';
                };
               $arr=array($where);
            }else{
              $arr=array();  
            }
                $tests= MasterTests::find($arr);
                $this->view->setVar("tests", $tests);
        }
    }

    function addtestsAction()
    {
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    	$response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $postval['sort'] = json_encode($postval['sort']);
            $extyid = $postval['exam_type'];
            $qint = array();
            $exam = MasterExamType::findFirst("etid='".$extyid."' AND status=1");
            if(isset($exam->etid) && $exam->etid!=''){
            	$qtype = json_decode($exam->q_type);
            	foreach($qtype as $value){
            		$qus = "qint".$value;
            		$qint = array_merge($qint,$postval[$qus]);
            	}
            }
            $postval['qint'] = json_encode($qint);
            $Addques= new MasterTests();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Test Successfully Add</div>");
            return $response->redirect("tests");
        }
        $supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);
        $course = MasterCourse::find("status=1");
        $this->view->setVar("course", $course);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("qtype", $qtype);
    }

    function deletetestAction($id=''){
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
          $phql = "DELETE FROM MasterTests WHERE id = '{$id}'";
          $this->modelsManager->executeQuery($phql);
          $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Test Successfully Deleted</div>");
          return $response->redirect("tests");
        }
    }

    function unitAction()
    {
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    	$response = new \Phalcon\Http\Response();
    	if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }

        // print_r($user_session);exit();
    }

    function addunitAction()
    {
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    	$course = MasterCourse::find("status=1");
        $this->view->setVar("course", $course);
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
    }

    function examtAction()
    {
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        if($this->request->isPost()){
            $getVal = $this->request->getPost();
            $test_type = $getVal['test_type'];
            $exam_type = MasterExamType::find(array("exam_type='".$test_type."' AND status='1'"));
                echo "<option value=''>-- Select Test --</option>";
            foreach($exam_type as $value){
                echo "<option value='".$value->etid."'>".$value->etname."</option>";
            }
        }else{
        	echo 'No Post data';
        }
        exit();
    }
    
    function testcurAction()
    {
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        if($this->request->isPost()){
            $getVal = $this->request->getPost();
            $etid = $getVal['etid'];
            $exam_type = MasterExamType::findFirst(array("etid='".$etid."'"));
            $examcourse = json_decode($exam_type->course);
            $course = MasterCourse::find(array("status=1"));
                echo "<option value=''>-- Select Course --</option>";
            foreach($course as $value){
            	foreach($examcourse as $val){
            		if($value->id==$val){
            			echo "<option value='".$value->id."'>".$value->name."</option>";
            		}
            	}
            }
        }else{
        	echo 'No Post data';
        }
        exit();
    }

    function stuwiseAction()
    {
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        
    	if($this->request->isPost()){
            $getVal = $this->request->getPost();
            if(empty($getVal['course'])){
                echo 'Choose a Course';
                exit();
            }
            $course = $getVal['course'];
            $test_type = $getVal['test_type'];
            $etid = $getVal['etid'];
            $subjects = MasterSubject::find(array("status='1'"));
            $q_type = MasterExamType::findFirst(array("etid='".$etid."'"));
            $ques_type = MasterQuesType::find("status=1");
            $q_type = json_decode($q_type->q_type);
            if($test_type==1){
            	$subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
            	$subject = json_decode($subject->subject); ?>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Choose Subject</label>
                        <div class="col-md-6">
                <?php
                if(!empty($subject)){
            	foreach($subject as $value){
            		foreach($subjects as $val){
            			if($val->id==$value){ ?>
            				<div class="checkbox-custom checkbox-default">
                              	<input type="checkbox" id="curs<?=$value?>" value="<?=$value?>" name="sort[]">
                              	<label for="curs<?=$value?>" ><?=$val->subjectname?></label>
                            </div>
            			<?php }
            		}
            	}}else{
            		echo 'No subject assign to this course. Please check the course Module.';
            	}

            	?>
            			</div>
            		</div>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Question in a Test</label>
            				<div class="clearfix"></div>
                            <div class="col-md-12">
                            	<?php
                            		foreach($ques_type as $value){
                            			foreach($q_type as $val){
                            				if($value->qtid==$val){ ?>
											<div class="col-md-3 control-label"><?=$value->qtname?></div>
											<input type="hidden" name="qint<?=$val?>[]" value="<?=$val?>" class="form-control">
											<div class="col-md-3">
												<label for="">No. Q</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control">
											</div>
											<div class="col-md-3">
												<label for="">Max Mark</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control">
											</div>
											<div class="col-md-3">
												<label for="">Min Mark</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control">
											</div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3">
                                                <label for="">Easy</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Medium</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Hard</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="clearfix"></div>
                            			<?php }
                            			}
                            		}
                            	?>
                            </div>
            		</div>
            <?php }elseif($test_type==2){
            	$subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
            	$subject = json_decode($subject->subject); ?>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Choose Topics</label>
            			<div class="clearfix"></div>
                        <div class="col-md-12">
                        <table class="table table-bordered">
                        	<tr>
			                <?php
			                if(!empty($subject)){
			            	foreach($subject as $value){
			            		foreach($subjects as $val){
			            			if($val->id==$value){ ?>
			            				<td>
			                            <label for=""><?=$val->subjectname?></label>
			                            <hr>
			                            <div class="clearfix"></div>
			                            <?php $subsubject = MasterSubSubject::find(array("subid='$val->id' AND status=1"));?>
			                            <?php
			                            	foreach($subsubject as $ssres){
			                            		$topics = MasterTopics::find(array("subid='".$val->id."' AND ssubid='".$ssres->ssid."' AND status=1"));
			                            		if($topics->count()){
                                                    echo $ssres->ssname;
                                                    echo '<div class="clearfix"></div>';
                                                    foreach($topics as $value){
			                            		?>
			                            		
			                            		<div class="checkbox-custom checkbox-default">
                                                    <input type="checkbox" id="curs<?=$value->tid?>" value="<?=$value->tid?>" name="sort[]">
                                                    <label for="curs<?=$value->tid?>" ><?=$value->tname?></label>
                                                </div>
                                                <div class="clearfix"></div>
                            					
                            		<?php
                                                    }
                            					}
			                            	}
			                            	echo '<div class="clearfix"></div>';
			                            ?>
			            			<?php }
			            		}
			            	}}else{
			            		echo 'No subject assign to this course. Please check the course Module.';
			            		} ?>
			            		</td>
			            		</tr>
			            </table>
            			</div>
            		</div>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Question in a Test</label>
            				<div class="clearfix"></div>
                            <div class="col-md-12">
                            	<?php
                            		foreach($ques_type as $value){
                            			foreach($q_type as $val){
                            				if($value->qtid==$val){ ?>
											<div class="col-md-3 control-label"><?=$value->qtname?></div>
											<input type="hidden" name="qint<?=$val?>[]" value="<?=$val?>" class="form-control">
											<div class="col-md-3">
												<label for="">No. Q</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control" >
											</div>
											<div class="col-md-3">
												<label for="">Max Mark</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control" >
											</div>
											<div class="col-md-3">
												<label for="">Negetive Mark</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control" >
											</div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3">
                                                <label for="">Easy</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Medium</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Hard</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="clearfix"></div>
                            			<?php }
                            			}
                            		}
                            	?>
                            </div>
            		</div>
            <?php }elseif($test_type==3){
            	$subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
            	$subject = json_decode($subject->subject); ?>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Choose Topics</label>
            			<div class="clearfix"></div>
                        <div class="col-md-12">
                        <table class="table table-bordered">
                        	<tr>
			                <?php
			                if(!empty($subject)){
			            	foreach($subject as $value){
			            		foreach($subjects as $val){
			            			if($val->id==$value){ ?>
			            				<td>
			                            <label for=""><?=$val->subjectname?></label>
			                            <hr>
			                            <div class="clearfix"></div>
			                            <?php $subsubject = MasterSubSubject::find(array("subid='$val->id' AND status=1"));?>
			                            <?php
			                            	foreach($subsubject as $ssres){
			                            		$topics = MasterTopics::find(array("subid='".$val->id."' AND ssubid='".$ssres->ssid."' AND status=1"));
			                            		if($topics->count()){
                                                    echo $ssres->ssname;
                                                    echo '<div class="clearfix"></div>';
                                                    foreach($topics as $value){
			                            		?>
			                            		
			                            		<div class="checkbox-custom checkbox-default">
                              						<input type="checkbox" id="curs<?=$value->tid?>" value="<?=$value->tid?>" name="sort[]">
                              						<label for="curs<?=$value->tid?>" ><?=$value->tname?></label>
                            					</div>
                            					<div class="clearfix"></div>
                            					
                            		<?php
                                                    }
                            					}
			                            	}
			                            	echo '<div class="clearfix"></div>';
			                            ?>
			            			<?php }
			            		}
			            	}}else{
			            		echo 'No subject assign to this course. Please check the course Module.';
			            		} ?>
			            		</td>
			            		</tr>
			            </table>
            			</div>
            		</div>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Unit name</label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" name="unit">
                        </div>
            		</div>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Question in a Test</label>
            				<div class="clearfix"></div>
                            <div class="col-md-12">
                            	<?php
                            		foreach($ques_type as $value){
                            			foreach($q_type as $val){
                            				if($value->qtid==$val){ ?>
											<div class="col-md-3 control-label"><?=$value->qtname?></div>
											<input type="hidden" name="qint<?=$val?>[]" value="<?=$val?>" class="form-control">
											<div class="col-md-3">
												<label for="">No. Q</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control" >
											</div>
											<div class="col-md-3">
												<label for="">Max Mark</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control" >
											</div>
											<div class="col-md-3">
												<label for="">Negetive Mark</label>
												<input type="text" name="qint<?=$val?>[]" class="form-control" >
											</div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3">
                                                <label for="">Easy</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Medium</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Hard</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="clearfix"></div>
                            			<?php }
                            			}
                            		}
                            	?>
                            </div>
            		</div>
            <?php }elseif($test_type==5){
                $subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
                $subject = json_decode($subject->subject); ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Choose Subject</label>
                        <div class="col-md-6">
                <?php
                if(!empty($subject)){
                foreach($subject as $value){
                    foreach($subjects as $val){
                        if($val->id==$value){ ?>
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" id="curs<?=$value?>" value="<?=$value?>" name="sort[]">
                                <label for="curs<?=$value?>" ><?=$val->subjectname?></label>
                            </div>
                        <?php }
                    }
                }}else{
                    echo 'No subject assign to this course. Please check the course Module.';
                }

                ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Question in a Test</label>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <?php
                                    foreach($ques_type as $value){
                                        foreach($q_type as $val){
                                            if($value->qtid==$val){ ?>
                                            <div class="col-md-3 control-label"><?=$value->qtname?></div>
                                            <input type="hidden" name="qint<?=$val?>[]" value="<?=$val?>" class="form-control">
                                            <div class="col-md-3">
                                                <label for="">No. Q</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control" >
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Max Mark</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control" >
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Negetive Mark</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control" >
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3">
                                                <label for="">Easy</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Medium</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Hard</label>
                                                <input type="text" name="qint<?=$val?>[]" class="form-control">
                                            </div>
                                            <div class="clearfix"></div>
                                        <?php }
                                        }
                                    }
                                ?>
                            </div>
                    </div>
            <?php }
            
        }else{
        	echo 'No Post data';
        }
        exit();
    }

    function subjectwiseAction($exam_type='',$couse='',$subject=''){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if($exam_type=='' || $couse==''){
            return $response->redirect("index/dashboard");
        }

        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='1'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }

        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $exam_id = MasterExamType::findFirst(array("exam_type='".$exam_type."' AND sup_cat='".$sup_cat."'"));
        $couse = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("test_type='".$exam_type."' AND exam_type='".$exam_id->etid."' AND couse='".$couse."' AND status='1'"));
        $this->view->setVar("master_exam", $master_exam);
        $subjects = json_decode($master_exam->sort);
        $q_type = json_decode($master_exam->qint);
        $subject = MasterSubject::findFirst("slug='".strtolower($subject)."'");
        $qcount=0;
        // echo $subject->id;
        for($i=0;$i<=count($q_type);$i=$i+7){
            if(isset($q_type[$i])){
                $count=1;
                for($j=4;$j<=6;$j++){
                    $qus = MasterQuestion::find(array("subject='".$subject->id."' AND q_type='".$q_type[$i]."' AND q_level='{$count}' AND e_type LIKE '%2%' ORDER BY rand() LIMIT ".$q_type[$j]."  "));
                    if(!empty($qus)){
                        foreach($qus as $val){
                            $questions[$q_type[$i]][] = $val->questionid;
                            $qcount++;
                            $qids[] = $val->questionid;
                        }
                    }
                    $count++;
                }
            }
        }
        // exit();
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("qwttype", $qwttype);
        $this->view->setVar("etype", $exam_id);
        $this->view->setVar("subject", $subject);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
    }

    function topicwiseAction($exam_type='',$topic=''){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if($exam_type=='' || $topic==''){
            return $response->redirect("index/dashboard");
        }
        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='2'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $course = $user_session['course'];
        $exam_id = MasterExamType::findFirst(array("exam_type='".$exam_type."' AND sup_cat='".$sup_cat."' AND course LIKE '%{$course}%'"));
        $master_exam = MasterTests::findFirst(array(" test_type='".$exam_type."' AND exam_type='".$exam_id->etid."' AND couse='".$user_session['course']."' AND status='1'"));
        // echo '<pre>';print_r($master_exam->id);
        $this->view->setVar("master_exam", $master_exam);
        // $subjects = json_decode($master_exam->sort);
        $q_type = json_decode($master_exam->qint);
        $qcount=0;
        for($i=0;$i<=count($q_type);$i=$i+7){
            if(isset($q_type[$i])){
                $count=1;
                for($j=4;$j<=6;$j++){
                    $qus = MasterQuestion::find(array("topics='".$topic."' AND q_type='".$q_type[$i]."' AND q_level='{$count}' AND e_type LIKE '%2%' ORDER BY RAND() LIMIT ".$q_type[$j]." "));
                    foreach($qus as $val){
                        $questions[$q_type[$i]][] = $val->questionid;
                        $qcount++;
                        $qids[] = $val->questionid;
                    }
                    $count++;
                }
            }
        }
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("etype", $exam_id);
        $topic = MasterTopics::findFirst("tid='".$topic."'");
        $this->view->setVar("topic", $topic);
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
        // echo '<pre>';print_r($questions);
        // exit();
    }

    function unitwiseAction($id)
    {
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='3'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $test = MasterTests::findFirst("id='".$id."'");

        $topics = str_replace("[","",$test->sort);
        $topics = str_replace("]","",$topics);
        
        $sup_cat = $user_session['sup_cat'];
        $exam_id = MasterExamType::findFirst(array("exam_type='".$test->test_type."' AND sup_cat='".$sup_cat."'"));
        $qdetail = json_decode($test->qint);
        $qcount=0;
        for($i=0;$i<=count($qdetail);$i=$i+7){
            if(isset($qdetail[$i])){
                $count=1;
                for($j=4;$j<=6;$j++){
                    $qus = MasterQuestion::find(array("topics IN ({$topics}) AND q_type='".$qdetail[$i]."' AND q_level='{$count}' AND e_type LIKE '%2%' ORDER BY RAND() LIMIT ".$qdetail[$j]." "));
                    if(!empty($qus)){
                        foreach($qus as $val){
                            $questions[$qdetail[$i]][] = $val->questionid;
                            $qcount++;
                            $qids[] = $val->questionid;
                        }
                    }
                    $count++;
                }
            }
        }
        
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("etype",$exam_id);
        $this->view->setVar("unit",$test->unit);
        $this->view->setVar("unitid",$test->id);
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
    }

    function fixedtestAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='4'");
        if(!isset($package->id)){
            return $response->redirect("index/dashboard");
        }
        $pack = MasterPackages::findFirst("id='{$package->packageid}'");
        $end_exam_time = json_decode($pack->fixtest_id);
        foreach($end_exam_time as $val){
            if($val[0]==$id){
                date_default_timezone_set('Asia/Calcutta');
                if(strtotime($val[3])<strtotime(now) || strtotime($val[1])!=strtotime(date("Y-m-d"))){
                    return $response->redirect("index/dashboard");
                }
            }
        }
        $fixtest = MasterFixtest::findFirst("id='".$id."'");
        $questions = json_decode($fixtest->questionid);
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("fixtest", $fixtest);
    }

    function freetestsAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $freetest = MasterFreetest::find("sup_cat='{$user_session['sup_cat']}' AND course='{$user_session['course']}'");
        $this->view->setVar("freetest", $freetest);
    }

    function freetestAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        
        $freetest = MasterFreetest::findFirst("id='{$id}'");
        $questions = json_decode($freetest->questionid);
        $qid = implode("','", $questions);
        $qtype = MasterQuesType::find("status='1'");
        $qcount=0;
        
        foreach($qtype as $qvalue){
            $qus = MasterQuestion::find(array("q_type='".$qvalue->qtid."' AND questionid IN ('{$qid}') "));
            if($qus->count()!=0){
                foreach($qus as $val){
                    $question[$qvalue->qtid][] = $val->questionid;
                    $qcount++;
                    $qids[] = $val->questionid;
                }
            }
        }
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("questionsIDlist", $question);
        $this->view->setVar("freetest", $freetest);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
    }

    function fullsyllabusAction(){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        // if($exam_type=='' || $couse==''){
        //     return $response->redirect("index/dashboard");
        // }

        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='5'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }

        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $exam_id = MasterExamType::findFirst(array("exam_type='5' AND sup_cat='".$sup_cat."' AND course LIKE '%{$user_session['course']}%'"));
        $couse = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("test_type='5' AND exam_type='".$exam_id->etid."' AND couse='".$couse."' AND status='1'"));
        $this->view->setVar("master_exam", $master_exam);
        $subjects = json_decode($master_exam->sort);
        // $subjects = str_replace("[", "", $subjects);$subjects = str_replace("]", "", $subjects);
        $q_type = json_decode($master_exam->qint);
        $subject = MasterSubject::find("status='1'");
        $qcount = 0;
        foreach($subjects as $subval){
            for($i=0;$i<=count($q_type);$i=$i+4){
                if(isset($q_type[$i])){
                    $qus = MasterQuestion::find(array("subject='{$subval}' AND q_type='".$q_type[$i]."' AND e_type LIKE '%2%' ORDER BY rand() LIMIT ".$q_type[$i+1]."  "));
                    
                    if(!empty($qus)){
                        foreach($qus as $val){
                            $questions[$subval][$q_type[$i]][] = $val->questionid;
                            $qcount++;
                            $qids[] = $val->questionid;
                        }
                    }
                }
            }
        }
        // echo '<pre>';print_r($subjects);
        // echo '<pre>';print_r($questions);
        // exit();
        
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("etype", $exam_id);
        $this->view->setVar("subject", $subject);
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
        // exit();
    }

    function swtdescAction($exam_type='',$couse='',$subject=''){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if($exam_type=='' || $couse==''){
            return $response->redirect("index/dashboard");
        }

        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='1'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }

        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $exam_id = MasterExamType::findFirst(array("exam_type='".$exam_type."' AND sup_cat='".$sup_cat."'"));
        $couse = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("test_type='".$exam_type."' AND exam_type='".$exam_id->etid."' AND couse='".$couse."' AND status='1'"));
        $this->view->setVar("master_exam", $master_exam);
        $this->view->setVar("submit_redirect", $exam_type.'/'.$couse.'/'.$subject);
        $this->view->setVar("subject",$subject);
    }

    function uwtdescAction($id){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='3'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $master_exam = MasterTests::findFirst("id='".$id."'");
        $this->view->setVar("master_exam", $master_exam);
        $this->view->setVar("submit_redirect", $id);
    }

    function fsdescAction(){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='5'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $master_exam = MasterTests::findFirst("test_type='5'");
        $this->view->setVar("master_exam", $master_exam);
    }

    function twtdescAction($exam_type='',$topic=''){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if($exam_type=='' || $topic==''){
            return $response->redirect("index/dashboard");
        }
        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='2'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $exam_id = MasterExamType::findFirst(array("exam_type='".$exam_type."' AND sup_cat='".$sup_cat."'"));
        $couse = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("test_type='".$exam_type."' AND exam_type='".$exam_id->etid."' AND couse='".$user_session['course']."' AND status='1'"));
        $this->view->setVar("master_exam", $master_exam);
        $this->view->setVar("topic", $topic);
        $this->view->setVar("submit_redirect", $exam_type.'/'.$topic);
    }

    function fwtdescAction($id){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='".$user_session['id']."' AND test_type='3'");
        if(empty($package)){
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $master_exam = MasterTests::findFirst("id='".$id."'");
        $this->view->setVar("master_exam", $master_exam);
        $this->view->setVar("submit_redirect", $id);
    }

    function fixpackAction($packid=''){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if($packid==''){
            return $response->redirect("index/dashboard");
        }
        $upack = MasterUserPackage::findFirst("userid='{$user_session['id']}' AND test_type='4'");
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $package = MasterPackages::findFirst("id='{$packid}'");
        $fixtest = MasterFixtest::find("sup_cat='{$user_session['sup_cat']}' AND course='{$user_session['course']}'");
        foreach ($fixtest as $key => $value) {
            $ft[$value->id] = $value->name;
        }

        $this->view->setVar("fixtest", $ft);
        $this->view->setVar("package", $package);
        $this->view->setVar("course", $course);
        $this->view->setVar("upack", $upack);

    }

    function checkpointsAction($id){
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterPackages::findFirst("id='{$id}'");
        $point = MasterUserpoint::findFirst("uid='{$user_session['id']}'");
        if($package->price<$point->point || $package->price==$point->point){
            
        }
    }

    function quizAction($id)
    {
        $response = new \Phalcon\Http\Response();
        $postval=$this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $but = MasterQuiz::findFirst("quiz_id='{$id}'");
    }

}
?>