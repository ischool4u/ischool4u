<?php

class TipsController extends ControllerBase
{
    public function initialize() 
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
    $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    /**
     * concept View Part Only retrive data from database and show the data.
     * @return [array] [Fetched Data]
     * @return Preetish priyabrata
     */
     function indexAction()
    {
        if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if(isset($getVal['submit'])){
                $subject = $getVal['subject'];
                $subsubject = $getVal['subsubject'];
                $topics = $getVal['topics'];
                $subtopics = $getVal['subtopics'];
                if($subtopics!=''){
                    $tips = MasterTips::find(array("subject='$subject' AND subsubject='$subsubject' AND topics='$topics' AND subtopics='$subtopics' AND status='1'"));
                }
                if($topics!=''){
                    $tips = MasterTips::find(array("subject='$subject' AND subsubject='$subsubject' AND topics='$topics' AND status='1'"));
                }
                if($subsubject!=''){
                    $tips = MasterTips::find(array("subject='$subject' AND subsubject='$subsubject' AND status='1'"));
                }
                if($subject!=''){
                    $tips = MasterTips::find(array("subject='$subject' AND status='1'"));
                }

                // if($getVal['subject']==''){
                //     unset($getVal['subject']);
                // }else{
                //     $where .= '"'.$getVal['subject'].'"';
                //     $subsubdet = MasterSubSubject::find("subid = '".$getVal['subject']."' AND status=1");
                //     $this->view->setVar("subsubdet", $subsubdet);
                // };
                // if($getVal['subsubject']==''){
                //     unset($getVal['subsubject']);
                // }else{
                //     $where .= ',"'.$getVal['subsubject'].'"';
                //     $mTopicdet = MasterTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND status=1");
                //     $this->view->setVar("mTopicdet", $mTopicdet);
                // };
                // if($getVal['topics']==''){
                //     unset($getVal['topics']);
                // }else{
                //     $where .= ',"'.$getVal['topics'].'"';
                //     $msTopicdet = MasterSubTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND tid='".$getVal['topics']."' AND status=1");
                //     $this->view->setVar("msTopicdet", $msTopicdet);
                // };
                // if($getVal['subtopics']==''){
                //     unset($getVal['subtopics']);
                // }else{
                //     $where .= ',"'.$getVal['subtopics'].'"]';
                // };
                // if($getVal['contype']==''){
                //     unset($getVal['contype']);
                //     $conce = MasterTips::find(array("cat LIKE '".$where."%'"));
                // }else{
                //     $con = $getVal['contype'];
                //     $conce = MasterTips::find(array("cat LIKE '".$where."%' AND contype='".$con."'"));
                // };
                $this->view->setVar("tips", $tips);
            }else{
                $tips = MasterTips::find(array("status='1'"));
                $this->view->setVar("tips", $tips);
            }
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $tipstype = MasterTipsType::find(array());
        $this->view->setVar("tipstype", $tipstype);
    }
    /**
     * addconcept Add new  Part Only Store data from  and show the data.
     * @return [array] [Fetched Data]
     * @return Preetish priyabrata
     */
    function addtipsAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $con= new MasterTips();
            $con->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Tip Successfully Added</div>");
            return $response->redirect("tips");
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $tipstype = MasterTipsType::find(array());
        $this->view->setVar("tipstype", $tipstype);
    }
    function updtipsAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $tips = MasterTips::findFirst("tid='".$id."'");
            $this->view->setVar("tips", $tips);
            $tipstype = MasterTipsType::find(array(""));
            $this->view->setVar("tipstype", $tipstype); 
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();                      
            $update= new MasterTips();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Tip Successfully Update</div>");
            return $response->redirect("tips");
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $getSubSub = MasterSubSubject::find(array("subid='".$tips->subject."' "));
        $this->view->setVar("subsub", $getSubSub);
        $topic = MasterTopics::find(array("subid='".$tips->subject."' AND ssubid='".$tips->subsubject."' "));
        $this->view->setVar("topic", $topic);
        $subtopic = MasterSubTopics::find(array("subid='".$tips->subject."' AND ssubid='".$tips->subsubject."' AND tid='".$tips->topics."' "));
        $this->view->setVar("subtopic", $subtopic);
    }

    function tipstypeAction()
    {
        $tipstype = MasterTipsType::find(array());
        $this->view->setVar("tipstype", $tipstype);
    }
    function addtiptypeAction()
    {
      $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){ 
            $postval = $this->request->getPost();
            $con= new MasterTipsType();
            $con->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Successfully Added</div>");
            return $response->redirect("tips/tipstype");
        }
    }
    function updtiptypeAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $update= new MasterTipsType();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Successfully Update</div>");
            return $response->redirect("tips/tipstype");
        }
    }
    function deletetiptypAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterTipsType WHERE id = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Deleted Successfully</div>");
            return $response->redirect("tips/tipstype");
        }
    }
    function updtiptystaAction($u_status,$id)
    {
      $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterTipsType SET status = 0 where id=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterTipsType SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Concept Type Status Updated Successfully</div>");
            return $response->redirect("tips/tipstype");
        }
    }
    /**
     * @author preetish priyabrata
     * Function created for slug value to check in database and send it status to add news
     * Created Date: 18-06-2015
     */

    static public function slugifyAction($text)
    { 
        if ($text=="") {
            echo $text="";exit();
        }
        else
        {
          $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
          // trim
          $text = trim($text, '-');
          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
          // lowercase
          $text = strtolower($text);
          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);
          if (empty($text))
          {
            return 'n-a';
          }
          $next = MasterConcept::find(array("slug = :slug:",'bind' => array('slug' => $text)));
            if (count($next)==1) {
                $msg="&nbsp;&nbsp;<span class='has-error'><label class='control-label' for='inputError'><i class='fa fa-times-circle-o'></i>&nbsp;Invalid Slug Name</label></span>";
                $status=0;
            }
            else
            {
                $msg="&nbsp;&nbsp;<span class='has-success'><label class='control-label' for='inputSuccess'><i class='fa fa-check'></i> Slug Name is Valid</label></span>";
                $status=1;
            }
          $slug_status=array("value"=>$text,"msg"=>$msg, "status"=>$status);
          echo json_encode($slug_status); exit();
        }
    }
}
?>