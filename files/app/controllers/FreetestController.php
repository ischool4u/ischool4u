<?php
class FreetestController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    function indexAction()
    {
        $freetest = MasterFreetest::find();
        $this->view->setVar("freetest", $freetest);
    }

    function addfreetestAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $mft = MasterFreetest::findFirst("sup_cat='{$postval['sup_cat']}' AND course='{$postval['course']}' ");
            
            if(isset($mft->id)){
                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Free test on this course and category is present.</div>");
                return $response->redirect("freetest");
            }
            
            $postval['questionid'] = json_encode($postval['questionid']);
            $fixtest = new MasterFreetest();
            $fixtest->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Free Test Successfully Added</div>");
            return $response->redirect("freetest");
        }
    	$supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }

    function updateAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $postval['questionid'] = json_encode($postval['questionid']);
            $fixtest = new MasterFixtest();
            $fixtest->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixtest Successfully Added</div>");
            return $response->redirect("fixedtest");
        }
        if($id!=''){
            $fixtest = MasterFixtest::findFirst(array("id='$id'"));
            $this->view->setVar("fixtest", $fixtest);
            $course = MasterCourse::find("scid='".$fixtest->sup_cat."'");
            $this->view->setVar("course", $course);
        }
        $supcat = MasterSupCat::find("status='1'");
        $this->view->setVar("supcat", $supcat);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }

    function updteststatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterFixtest SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterFixtest SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Status Updated Successfully</div>");
            return $response->redirect("fixedtest");
        }
    }

    function deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterFixtest WHERE id = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixedtest Deleted Successfully</div>");
            return $response->redirect("fixedtest");
        }
    }

    function fixpackageAction()
    {
        $data = MasterPackages::find(array("test_type='4'"));
        $this->view->setVar("package", $data);
    }

    function addpackagesAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            if(count($postval['fixid'])==count($postval['month'])){
                for ($i=0; $i <=count($postval['fixid'])-1 ; $i++) { 
                    $postval['fixtest_id'][$i] = [
                        $postval['fixid'][$i],
                        $postval['month'][$i]
                    ];
                }
            }
            $postval['fixtest_id'] = json_encode($postval['fixtest_id']);
            $postval['period'] = null;
            $postval['format'] = null;
            $postval['not'] = null;
            // exit();
            $package = new MasterPackages();
            $package->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fix Package Successfully Added</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
        $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);
    }

    function packageupdstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        { 
            if($u_status==2){
                $phql = "UPDATE MasterPackages SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterPackages SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixed Package Status Updated Successfully</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
    }

    function updatepackageAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!=''){
            $conditions = "id = :id:";
            $parameters = array("id" => $id);
            $package = MasterPackages::findFirst(array($conditions,"bind" => $parameters));
            $this->view->setVar("val", $package);
            $supcat = MasterSupCat::find(array("status=1"));
            $this->view->setVar("supcat", $supcat);
            $course = MasterCourse::find("scid='".$package->sup_cat."'");
            $this->view->setVar("course", $course);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['period'] = null;
            $postval['format'] = null;
            $postval['not'] = null;
            $updatepack= new MasterPackages();
            $updatepack->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Successfully Updated</div>");
            return $response->redirect("course/packages");
        }
    }


    // Get question ids
    
    function getqidAction()
    {
    	if($this->request->isPost()){
            $postval = $this->request->getPost();
        }
        $qid = MasterQuestion::find("subject='".$postval['subjectid']."' AND subsubject='".$postval['ssubid']."' AND topics='".$postval['topics']."' AND subtopics='".$postval['subtopic']."'");
        ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Questionid</th>
                        <th>Question</th>
                        <th>Q. Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($qid as $val): ?>
                    <?php
                        if($val->tableid==1){
                            $question = MasterQuestionBank::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==2){
                            $question = MasterMatchQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==3){
                            $question = MasterNumericQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==4){
                            $question = MasterParaQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==5){
                            $question = MasterReasonQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==6){
                            $question = MasterSubtheoQuestion::findFirst("questionid='".$val->questionid."'");
                        }
                        
                    ?>
                    <tr>
                        <td><?=$val->questionid?></td>
                        <td><?=$question->question?></td>
                        <td>
                        <?php
                            if($val->tableid==1){
                                echo 'Single or Multiple';
                            }elseif($val->tableid==2){
                                echo 'Match Making';
                            }elseif($val->tableid==3){
                                echo 'Numeric';
                            }elseif($val->tableid==4){
                                echo 'Paragraph';
                            }elseif($val->tableid==5){
                                echo 'Reasoning';
                            }elseif($val->tableid==6){
                                echo 'Subject or Theory';
                            }
                        ?>
                        </td>
                        <td><div class="btn btn-primary" onclick="selectqid('<?=$val->questionid?>')">Add</div></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php
        exit();
    }

    function getfixtestAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $fixtest = MasterFixtest::find("sup_cat='".$postval['sup_cat']."' AND course='".$postval['course']."'");
            if($fixtest->count()!=0){
                for($i=0;$i<=count($fixtest)-1;$i++){
                    echo $i+1;
                ?>

                    <div class="form-group">
                    <div class="col-md-3">
                        <select name="fixid[]" id="" class="form-control">
                            <option value="">-- Choose fixtest --</option>
                            <?php foreach($fixtest as $ft): ?>
                                <option value="<?=$ft->id?>"><?=$ft->name?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="date[]" data-plugin-datepicker class="form-control" placeholder="Starting Date">
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="stime[]" data-plugin-timepicker class="form-control" placeholder="Starting Time">
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="etime[]" data-plugin-timepicker class="form-control" placeholder="Ending Time">
                    </div>
                    </div>
                    <div class="clearfix"></div>
                <?php 
                }
            }else{
                echo 'No Fixtest are found.';
            }
        }
        exit();
    }
}
?>