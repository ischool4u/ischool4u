<?php
class SubtheoryController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
     * Question View Part Only retrive data from database and show the data.
     * @return [array] [Fetched Data]
     * @return Rajesh
     */
    function indexAction()
    {
        // $data = MasterSubtheoQuestion::find("");
        $data=UIElementsAdmin::getpagination("MasterSubtheoQuestion",'question');
        $this->view->setVar("questions", $data);
        $qtype = MasterQuesType::find(array());
        $this->view->setVar("qtype", $qtype);
    }
    /**
     * Add questions
     * @return [array] [Return array]
     * @author Rajesh
     */
    function addAction()
    {
        $response = new \Phalcon\Http\Response();
        $table = 1;
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['e_type'] = json_encode($postval['e_type']);
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['created'] = date("Y-m-d h:i:s");
            $questionid = MasterQuestion::find()->getLast();
            $questionid = str_split($questionid->questionid,3);
            $questionid = $questionid['2']+1; $questionid = 'QID000'.$questionid;
            $qid = MasterSubtheoQuestion::find()->getLast();
            $qid = $qid->qid+1;
            $postval['qusid'] = $qid;
            $postval['questionid'] = $questionid;
            $postval['tableid'] = 6;
            $update= new MasterSubtheoQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Added</div>");
            return $response->redirect("subtheory");
        }
        $questype = MasterQuesType::find(array("status=1"));
        $this->view->setVar("qtype", $questype);
        $quessource = MasterQuestionSource::find(array("status=1"));
        $this->view->setVar("qsources", $quessource);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }

    /**
     * Update questions
     * @param  [int] $id [Question id]
     * @return [massage]     [Success Massage]
     * @author Rajesh
     */
    function updateAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval = $this->request->getPost();

            $postval['e_type'] = json_encode($postval['e_type']);
            $postval['slug'] = str_replace(' ', '_', $postval['question']);
            $postval['modified'] = date("Y-m-d h:i:s");
            $user_session = $this->session->get('admin');
            $postval['modified_by'] = $user_session['id'];
            $postval['qusid'] = $postval['qid'];
            $postval['tableid'] = 6;
            $qid = MasterQuestion::findFirst(array("questionid='".$postval['questionid']."'"));
            $postval['id'] = $qid->id;
            
            $update= new MasterSubtheoQuestion();
            $update->save($postval);
            $questions = new MasterQuestion();
            $questions->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Question Successfully Update</div>");
            return $response->redirect("subtheory");
        }
        if($id!="")
        {
            $questype = MasterQuesType::find(array("status=1"));
            $this->view->setVar("qtype", $questype);
            $quessource = MasterQuestionSource::find(array("status=1"));
            $this->view->setVar("qsources", $quessource);
            $getSub = MasterSubject::find(array());
            $this->view->setVar("subdet", $getSub);
            $question = MasterSubtheoQuestion::findFirst(array("qid='".$id."'"));
            $this->view->setVar("question", $question);
            $mquestion = MasterQuestion::findFirst(array("questionid='".$question->questionid."'"));
            $this->view->setVar("mquestion", $mquestion);
        }else{
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something Wrong. Please Try again.</div>");
            return $response->redirect("subtheory");
        }
    }

    function deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterSubtheoQuestion WHERE qid = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Exam Type Deleted Successfully</div>");
            return $response->redirect("subtheory");
        }
    }
}
?>