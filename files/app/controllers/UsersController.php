<?php
class UsersController extends ControllerBase
{
    public function initialize()
    {
        //echo "init";exit;
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    public function indexAction(){ // Index Page
        $response = new \Phalcon\Http\Response();
        $package = MasterPackages::find();
        $this->view->setVar("package", $package);
        $course = MasterCourse::find("status=1");
        $this->view->setVar("course", $course);
    }
    public function addAction(){  // Add new Users
        
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $conditions = "mail_id = :mail_id:";
            $parameters = array("mail_id" => $this->request->getPost("mail_id"));
            $user = MasterUsers::find(array($conditions,"bind" => $parameters));
            if (count($user) != 0) {
                $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("mail_id")." already exists.</div>");
                return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
            }
            else
            {
                $passsword=$this->request->getPost("pass");
                $course=$this->request->getPost('course');
                $package = $this->request->getPost("package");
                $pack_data = MasterPackages::findFirst(array("id = :id:",'bind' => array("id"=>$package)));
                $pack_period=$pack_data->period;
                $pack_for=$pack_data->format;
                $pack_date=date('y-m-d');
                $mem_end = date('Y-m-d', strtotime($pack_date .'+'.$pack_period.' '.$pack_for.''));
                $totarr=array_merge($postval,array('pass'=>base64_encode($passsword),'course','mail_verified'=>0,'mem_start'=>date('y-m-d'),'mem_end'=>$mem_end,'mem_duration'=>$pack_period." ".$pack_for,'created'=>date('y-m-d h:m:s'),'sup_cat'=>$postval['sup_cat']));
                $AdminUsers = new MasterUsers();
                if($AdminUsers->save($totarr))
                {
                    $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Registered Successfully</div>");    
                    return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
                }
            }
            
        }
        $supcat = MasterSupCat::find(array());
        $this->view->setVar("supcat", $supcat);
    }

    public function exportAction()
    {
         
        $query = MasterUsers::find(array("limit"=>5));
        foreach ($query as $key) {
            print_r($key->first_name."-");
        }
    }

    /*public function filterAction()
    {
        if ($this->request->isPost()) {
            $strng_where=array();
            $postval=$this->request->getPost();
            if ($postval["mail_id"]!="") {
                $mail="mail_id='".$postval["mail_id"]."'";
                array_push($strng_where, $mail);
            }
            if ($postval["first_name"]!="") {
                $name="first_name='".$postval["first_name"]."'";
                array_push($strng_where, $name);
            }
            if ($postval["country"]!="") {
                 $country="country='".$postval["country"]."'";
                array_push($strng_where, $country);
            }
            if ($postval["state"]!="") {
                 $state="state='".$postval["state"]."'";
                array_push($strng_where, $state);
            }
            if ($postval["city"]!="") { 
                 $city="city='".$postval["city"]."'";
                array_push($strng_where, $city);
            }
            if ($postval["is_active"]!="") {
                 $is_active="is_active='".$postval["is_active"]."'";
                array_push($strng_where, $is_active);
            }
            if ($postval["uid"]!="") {
                 $uid="uid=".$postval["uid"]."";
                array_push($strng_where, $uid);
            }
            //$postval["order"]="hjg";
            $where_string=implode(" and ",$strng_where);

            $verifyUser=MasterUsers::find(array(
                        "conditions" => $where_string,
                        "limit" => 10
                    ));
            $this->view->setVar("user_fil", $verifyUser);

        }

    }*/
    
    public function updateAction($id)
    {
        if($id!="")
        {
            if ($this->request->isPost()) {//echo $id;
                
                $postval=$this->request->getPost();
                $conditions = "mail_id = :mail_id:";
                $parameters = array("mail_id" => $this->request->getPost("mail_id"));
                $user = MasterUsers::find(array($conditions,"bind" => $parameters));
                foreach ($user as $users) {
                    $uid=$users->uid;
                }
                //echo"<pre>";print_r($uid);exit();
                    $pack_id=$this->request->getPost("package");
                    $pack_ship = MasterPackages::findFirst(array("id=:id:",'bind' => array("id"=>$pack_id)));
                    $pack_period=$pack_ship->period;
                    $pack_for=$pack_ship->format;
                    $pack_date=date('y-m-d');
                    $mem_end= date('Y-m-d', strtotime($pack_date .'+'.$pack_period.' '.$pack_for.''));
                    $passsword=$this->request->getPost("pass");
                    $course=$this->request->getPost("course");
                    unset($postval["mem_start"]);
                    $totarr=array_merge($postval,array('pass'=>base64_encode($passsword),'mem_start'=>date('y-m-d'),'mem_end'=>$mem_end,'mem_duration'=>$pack_period." ".$pack_for,'modified'=>date('y-m-d h:m:s'),'sup_cat'=>$postval['sup_cat']));
                    //echo"<pre>";print_r($totarr);
                    //exit;
                    $AdminUsers = new MasterUsers();
                    $phql = "UPDATE MasterUsers SET sup_cat = :sup_cat:,first_name = :first_name:,last_name = :last_name:,mail_id = :mail_id:,pass = :pass:,contact_no = :contact_no:,course = :course:,package=:package:,address=:address:,city=:city:,state=:state:,zip=:zip:,country=:country:,marketing_pref = :marketing_pref:,mail_verified = :mail_verified:,is_active = :is_active:,mem_start=:mem_start:,mem_end=:mem_end:,mem_duration=:mem_duration:,modified = :modified: WHERE uid = ".$id."";
                
                
                if (count($user) != 0) {
                    if($uid==$id)
                    {
                        //echo $phql;exit();
                        $status = $this->modelsManager->executeQuery($phql,$totarr);
                        //echo "<pre>";print_r($status);exit();
                        $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated Successfully</div>");
                        return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
                    }
                    else
                    {
                       $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("mail_id")." already exists.</div>");
                        return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));    
                    }
                    
                }
                else
                {
                    //echo $phql;exit();
                    $status = $this->modelsManager->executeQuery($phql,$totarr);
                    $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated Successfully</div>");
                    return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
                }
            }      
            //
            else
            {
                $user_upd = MasterUsers::findFirst(array("uid = :uid:",'bind' => array( 'uid' => $id)));
                $this->view->setVar("uid", $user_upd);
                $supcat = MasterSupCat::find(array());
                $this->view->setVar("supcat", $supcat);
            }
            
        }
        
    }
    public function UpdstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterUsers SET is_active = 0 where uid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterUsers SET is_active = 1 where uid=".$id."";    
            }
            
                    //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

            $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>User Status Updated Successfully</div>");
             return $response->redirect("users");
             
        }
    }
    function searchAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if(isset($getVal['course']) && isset($getVal['package'])){
                $this->view->setVar("courseid", $getVal['course']);
                $this->view->setVar("package", $getVal['package']);
                // $course = MasterCourse::find();
                // $this->view->setVar("course", $course);
                // $package = MasterPackages::find();
                // $this->view->setVar("package", $package);
            }elseif(isset($getVal['course'])){
                $this->view->setVar("courseid", $getVal['course']);
                // $package = MasterPackages::find();
                // $this->view->setVar("package", $package);
                // $course = MasterCourse::find();
                // $this->view->setVar("course", $course);
            }elseif(isset($getVal['package'])){
                $this->view->setVar("package", $getVal['package']);
                // $course = MasterCourse::find();
                // $this->view->setVar("course", $course);
                // $package = MasterPackages::find();
                // $this->view->setVar("package", $package);
            }
        }else{
            return $response->redirect("users");
        }
    }
    public function deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterUsers WHERE uid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Deleted Successfully</div>");
                return $response->redirect("users");
        }
        
    }
    public function mailAction()
    {
        
        $postval=$this->request->getPost();
        $mail_to=$this->request->getPost("chkNo");
        //print_r($mail_to);
        $this->view->setVar("mail_id", $mail_to);
    }
    public function mailtoAction()
    {

        if(is_array($_POST['email_to']) && count($_POST['email_to']) > 0){
            $postval=$this->request->getPost();
            //print_r($postval);
            foreach($_POST['email_to'] as $id){
                //print_r($id);
            }

             $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Send Successfully</div>");
             return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
            
        }
        
    }
    public function chkboxAction(){
        if ($this->request->isPost()) 
        {
           if ($this->request->getPost("todo")=="deleteall") {
               if(is_array($_POST['chkNo']) && count($_POST['chkNo']) > 0){
                    // delete
                    //$count = 0;
                    foreach($_POST['chkNo'] as $id){
                        $phql = "DELETE FROM MasterUsers WHERE uid = '".$id."'";
                        $this->modelsManager->executeQuery($phql);
                    }
                     $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Deleted Successfully</div>");
                     return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
                    
                }
           }
           if ($this->request->getPost("todo")=="activeall") {
               if(is_array($_POST['chkNo']) && count($_POST['chkNo']) > 0){
                    // delete
                    //$count = 0;
                    foreach($_POST['chkNo'] as $id){
                        $phql = "UPDATE MasterUsers SET is_active = 1 where uid=".$id."";    
                        $status = $this->modelsManager->executeQuery($phql);
                    }
                     $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Active Successfully</div>");
                     return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
                    
                }
           }
           if ($this->request->getPost("todo")=="inactiveall") {
               if(is_array($_POST['chkNo']) && count($_POST['chkNo']) > 0){
                    // delete
                    //$count = 0;
                    foreach($_POST['chkNo'] as $id){
                        $phql = "UPDATE MasterUsers SET is_active = 0 where uid=".$id."";    
                        $status = $this->modelsManager->executeQuery($phql);
                    }
                     $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Inactive Successfully</div>");
                     return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
                    
                }
           }
           if ($this->request->getPost("todo")=="mailto") {
               return $this->dispatcher->forward(array("controller"=>"users","action" => "mail"));
           }
        }
        else
        {
             return $this->dispatcher->forward(array("controller"=>"users","action" => "index"));
        }
    }

    public function filterAction(){
        $response = new \Phalcon\Http\Response();
         if ($this->request->isPost()) 
         {
            
            $first_name = $this->request->getPost('first_name');
            $last_name = $this->request->getPost('last_name');
            $mail_id = $this->request->getPost('mail_id');
            $contact_no = $this->request->getPost('contact_no');
            $package = MasterPackages::find();
            $this->view->setVar("package", $package);
            $user = MasterUsers::find(array("first_name = :first_name: or  last_name = :last_name: or mail_id = :mail_id: or  contact_no = :contact_no: " ,'bind' => array('first_name'=>$first_name,'last_name'=>$last_name,'mail_id'=>$mail_id,'contact_no'=>$contact_no)));
            $this->view->setVar("user", $user);
        }
    }

    public function walletAction(){
        $response = new \Phalcon\Http\Response();
        $course = MasterCourse::find("status=1");
        $this->view->setVar("course", $course);
        if($this->request->get('search'))
        {
            $getVal=$this->request->get();
            $query = $this->modelsManager->createQuery("SELECT * FROM MasterUsers WHERE first_name LIKE '%{$getVal['search']}%' OR last_name LIKE '%{$getVal['search']}%' or mail_id LIKE '%{$getVal['search']}%' ");
            $user =$query->execute();
        }else{
            $user = MasterUsers::find(array("order" => "uid DESC"));
        }
        foreach($user as $uval){
            $uid[] = $uval->uid;
        }
        $uid = implode(",", $uid);
        $points = MasterUserpoint::find("uid IN ({$uid})");
        foreach ($points as $key => $pvalue) {
            $pv[$pvalue->uid]=$pvalue->points;
        }
        $this->view->setVar("user", $user);
        $this->view->setVar("userpoint", $pv);
    }

    public function pointsAction(){
        $postval = $this->request->getPost();
        $id = $postval['id'];
        $point = $postval['point'];
        $user = MasterUserpoint::findFirst("uid='{$id}'");
        if($user->uid==''){
            $data = ["uid"=>$id,"points"=>$point];
            $pointdb = new MasterUserpoint();
            $pointdb->save($data);
            echo 'Successfully Added User points. Total point is:'.$point;
        }else{
            $point = $user->points+$point;
            $phql = "UPDATE MasterUserpoint SET points='{$point}' WHERE uid = '{$id}'";
            $this->modelsManager->executeQuery($phql);
            echo 'Successfully Added User points. Total point is:'.$point;
        }
        exit();

    }

    public function packageAction($id)
    {
        $points = MasterUserpoint::findFirst("uid='".$id."'");
        $mup = MasterUserPackage::find("userid='{$id}'");
        foreach($mup as $mval){
            $mvalue[] = $mval->packageid;
            $mupval[$mval->packageid] = ['test_type'=>$mval->test_type,'not'=>$mval->nots];
        }
        $mvalue = implode("','", $mvalue);
        $package = MasterPackages::find("id IN ('{$mvalue}')");
        $mr = MasterResult::find("userid='{$id}' ");
        $this->view->setVar("mr", $mr);
        $this->view->setVar("package", $package);
        $this->view->setVar("points", $points);
    }

}
?>
