<?php
class CourseController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    public function indexAction()
    {
    	$data = MasterCourse::find();
        $this->view->setVar("course", $data);
    }
   	public function addcourseAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $postval['subject'] = json_encode($postval['subject']);
            $postval['subsubject'] = json_encode($postval['subsubject']);
//            $Addcourse= new MasterCourse();
//            $Addcourse->save($postval);
            $sql="UPDATE MasterCourse SET subject='{$postval['subject']}',subsubject='{$postval['subsubject']}' WHERE id={$postval['id']}";
            $this->modelsManager->executeQuery($sql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Course Successfully Added</div>");
            return $response->redirect("course");
        }
        $course = MasterCourse::find(array("status=1"));
        $this->view->setVar("course", $course);
        $data = MasterSubject::find(array("status=1"));
        $this->view->setVar("subject", $data);
    }
    /**
     * [updatecourseAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function updatecourseAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $conditions = "id = :id:";
            $parameters = array("id" => $id);
            $course = MasterCourse::findFirst(array($conditions,"bind" => $parameters));
            $this->view->setVar("course", $course);
            $data = MasterSubject::find(array("status=1"));
            $this->view->setVar("subject", $data);
            $allcourse = MasterCourse::find();
            $this->view->setVar("allcourse", $allcourse);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['subject'] = json_encode($postval['subject']);
            $postval['subsubject'] = json_encode($postval['subsubject']);
            $sql="UPDATE MasterCourse SET subject='{$postval['subject']}',subsubject='{$postval['subsubject']}' WHERE id={$postval['id']}";
            $this->modelsManager->executeQuery($sql);
//            $update= new MasterCourse();
//            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Course Successfully Updated</div>");
            return $response->redirect("course");
        }
    }
    public function deletecourseAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterCourse WHERE id = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Course Deleted Successfully</div>");
                return $response->redirect("course");
        }

    }
    public function courseupdstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterCourse SET status = 0 where id=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterCourse SET status = 1 where id=".$id."";
            }
            //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Course Status Updated Successfully</div>");
             //return $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
             return $response->redirect("course");
        }
    }

    //packages

    public function packagesAction()
    {
        $data = MasterPackages::find(array("order" => "id"));
        $this->view->setVar("package", $data);
        // $course = MasterCourse::find(array("order" => "id"));
        // $this->view->setVar("course", $course);
    }
   	public function addpackagesAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
                if(empty($postval['sup_cat']) || empty($postval['test_type']) || empty($postval[''])){

                }
                $postval['sup_cat'] = json_encode($postval['sup_cat']);
                $postval['fixtest_id'] = null;
                $Addcourse = new MasterPackages();
                $Addcourse->save($postval);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Successfully Added</div>");
                return $response->redirect("course/packages");
        }
        $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);

    }
    /**
     * [updatepackagesAction Update Existing package]
     * @param  [int] $id [package ID]
     * @return [TRUE/FALSE]     [Update package]
     */
    public function updatepackagesAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $conditions = "id = :id:";
            $parameters = array("id" => $id);
            $package = MasterPackages::findFirst(array($conditions,"bind" => $parameters));
            $this->view->setVar("val", $package);
            $supcat = MasterSupCat::find(array("status=1"));
            $this->view->setVar("supcat", $supcat);
            //exit();
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['sup_cat'] = json_encode($postval['sup_cat']);
            //echo "<pre>";print_r($postval);exit();
            $updatepack= new MasterPackages();
            $updatepack->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Successfully Updated</div>");
            return $response->redirect("course/packages");
        }
    }

    public function deletepackagesAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterPackages WHERE id = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Deleted Successfully</div>");
                return $response->redirect("course/packages");
        }
    }

    public function packageupdstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        { 
            if($u_status==2){
                $phql = "UPDATE MasterPackages SET status = 0 where id=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterPackages SET status = 1 where id=".$id."";
            }
            //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Status Updated Successfully</div>");
             //return $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
             return $response->redirect("course/packages");
        }
    }

   public function searchpackageAction($id)
   {
        $response = new \Phalcon\Http\Response();
        $data = MasterCourse::find();
        $this->view->setVar("course", $data);
        if ($id!='') {
            $postval=$this->request->getPost();
            $conditions = "courseid = :courseid:";
            $parameters = array("courseid" => $id);
            $course = MasterPackages::find(array($conditions,"bind" => $parameters));
            $this->view->setVar("package", $course);
            //return $response->redirect("course/packages");
            //exit();
        }else{
            return $response->redirect("course/packages");
        }

   }
    
    #########################################################
    ###########  Super Category #############################
    #########################################################
    /**
     * Super Categroy View Part
     * @return [array] [super category array]
     */
    function supcategoryAction()
    {
      $data = MasterSupCat::find();
      $this->view->setVar("supcat", $data);
    }
    
    function addsupcategoryAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $postval['created'] = date('Y-m-d h:i:s');
            $Addcourse= new MasterSupCat();
            $check_sname = MasterSupCat::findFirst(array("scname" => $postval->scname));
            if(count($check_sname) !=1){
                $Addcourse->save($postval);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Super Category is Successfully Added</div>");
            }else{
                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Super Category is Not Successfully Added</div>");
            }
            return $response->redirect("course/supcategory");

        }
    }
    function updsupcategoryAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $scat = MasterSupCat::findFirst(array("scid='".$id."'"));
            $this->view->setVar("supcat", $scat);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $updatepack= new MasterSupCat();
            $updatepack->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Successfully Updated</div>");
            return $response->redirect("course/supcategory");
        }
    }
    function deletesupcategoryAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterSupCat WHERE scid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Super Category Deleted Successfully</div>");
                return $response->redirect("course/supcategory");
        }
    }
    function supcategorystatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        { 
            if($u_status==2){
                $phql = "UPDATE MasterSupCat SET status = 0 where scid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterSupCat SET status = 1 where scid=".$id."";
            }
            //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Super Category Status Updated Successfully</div>");
             //return $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
             return $response->redirect("course/supcategory");
        }
    }
    
    
  //add course or category here
     function coursecategoryAction()
    {
      $data = MasterCourse::find();
      $this->view->setVar("cat", $data);
       $data1 = MasterSupCat::find();
       $arr='';
       foreach($data1 as $vv){
           $arr[$vv->scid]=$vv->scname;
       }
       $this->view->setVar("supcat", $arr);
    }
    function addcategoryAction(){
      $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $Addcourse= new MasterCourse();
            $check_course = MasterCourse::findFirst(array("name"=>$postval->name));
            if(count($check_course)!=1){
                $Addcourse->save($postval);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Category is Successfully Added</div>");
            }else{
                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Category is Not Successfully Added</div>");
            }
            return $response->redirect("course/coursecategory");
        }  
        $data = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $data);
    }
    function categorystatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        { 
            if($u_status==2){
                $phql = "UPDATE MasterCourse SET status = 0 where id=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterCourse SET status = 1 where id=".$id."";
            }
            //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Category Status Updated Successfully</div>");
             return $response->redirect("course/coursecategory");
        }
    }
     function deletecategoryAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterCourse WHERE id = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Category Deleted Successfully</div>");
                return $response->redirect("course/coursecategory");
        }
    }  
     function updatecategoryAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $cat = MasterCourse::findFirst(array("id='".$id."'"));
            $this->view->setVar("cat", $cat);
            $data = MasterSupCat::find(array("status=1"));
            $this->view->setVar("supcat", $data);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $Addcourse = new MasterCourse();
            $Addcourse->save($postval);
            // $sql="UPDATE MasterCourse SET scid='{$postval['scid']}',name='{$postval['name']}',description='{$postval['desc']}',status='{$postval['status']}' WHERE id={$postval['id']}";
            // $this->modelsManager->executeQuery($sql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Category Successfully Updated</div>");
            return $response->redirect("course/coursecategory");
        }
    }
    
}

?>