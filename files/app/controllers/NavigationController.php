<?php
class NavigationController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
    * 
    * Function To index view Navigation type
    * Action Name: indexAction 
    * Created Date: 4-06-2015
    * Author By: Rajesh
    **/
    function indexAction()
    {        
        $data = MasterMenuType::find();
        $this->view->setVar("menu", $data);
    }
    /**
     * Add menu types
     * @return [massage] [return the success massage]
     * @author Rajesh <[email address]>
     */
    function addmenuAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $data = new MasterMenuType();
            $data->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Menu type Successfully Added</div>");
            return $response->redirect("navigation");
        }else{
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is something Wrong. Try again later.</div>");
            return $response->redirect("navigation");
        }
    }
    /**
     * Update menu 
     * @param  [int] $id [menu id]
     * @return [massage]     [Return sucess massage]
     * @author Rajesh <[email address]>
     */
    function updatemenuAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $data = MasterMenuType::findFirst(array("mid='".$id."'"));
            $this->view->setVar("menu", $data);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $update= new MasterMenuType();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Menu Type Successfully Updated</div>");
            return $response->redirect("navigation");
        }
    }
    /**
     * Delete menu Type
     * @param  [int] $id [Menu id]
     * @return [massage]     [Return sucess massage]
     * @author Rajesh <[email address]>
     */
    function deletemenuAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterMenuType WHERE mid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Menu Deleted Successfully</div>");
                return $response->redirect("navigation");
        }else{
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is some thing worng. Try again later.</div>");
            return $response->redirect("navigation");
        }
    }
    /**
     * One click Status update
     * @param  [ind] $u_status [description]
     * @param  [type] $id       [description]
     * @return [type]           [description]
     */
    function updmtstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterMenuType SET status = 0 where mid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterMenuType SET status = 1 where mid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Menu Type Status Updated Successfully</div>");
             return $response->redirect("navigation");
        }
    }
    function assignmenuAction()
    {
        $data = MasterAssignMenu::find();
        $this->view->setVar("assignmenu", $data);
        $mtyp = MasterMenuType::find();
        $this->view->setVar("menu_type", $mtyp);
        $page = MasterCms::find();
        $this->view->setVar("pages", $page);
    }

    function assigndeletemenuAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterAssignMenu WHERE amid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Menu Deleted Successfully</div>");
                return $response->redirect("navigation/assignmenu");
        }else{
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There is some thing worng. Try again later.</div>");
            return $response->redirect("navigation/assignmenu");
        }
    }
    function updateassignmenuAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!='') {
            
            $mtyp = MasterMenuType::find();
            $this->view->setVar("menu_type", $mtyp);
            $page = MasterCms::find();
            $this->view->setVar("pages", $page);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $update= new MasterMenuType();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Menu Successfully Updated</div>");
            return $response->redirect("navigation/assignmenu");
        }
        
    }
    function updamstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterAssignMenu SET status = 0 where amid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterAssignMenu SET status = 1 where amid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Menu Status Updated Successfully</div>");
             return $response->redirect("navigation/assignmenu");
        }else{
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Something is worng. Please check later.</div>");
            return $response->redirect("navigation/assignmenu");
        }
    }
    
}
?>