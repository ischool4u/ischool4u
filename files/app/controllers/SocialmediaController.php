<?php
class SocialmediaController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
     /**
    * Function To index view social media
    * Action Name: indexAction
    * Created Date: 4-06-2015
    * Author By: preetish priyabrata
    **/
    function indexAction()
    {
         $data = MasterSocialMedia::find();
        $this->view->setVar("social", $data);
    }
    function addAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) 
        {
            $postval=$this->request->getPost();
            
            $Addques= new MasterSocialMedia();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Social  Successfully Add</div>");
            return $response->redirect("socialmedia");
        }
    }
     /**
    * @author preetish priyabrata
    * Function created for update status in question type detail
    * Created on 4-june-2015
    */
      public function update_statusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterSocialMedia SET status = 0 where smid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterSocialMedia SET status = 1 where smid=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Social Media Status Updated Successfully</div>");
            return $response->redirect("socialmedia");
        }
    }
    /**
 * @author preetish priyabrata
 *function created for Social media Deleted Successfully
 */
     public function sm_deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterSocialMedia WHERE smid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Social Media Deleted Successfully</div>");
                return $response->redirect("socialmedia");
        }
    } 
    /**
* Function To update changees in question type
* Action Name: addpageAction
* Created Date: 26-05-2015
* Author By: preetish priyabrata
**/
     public function updateAction($id){
      $response = new \Phalcon\Http\Response();
        if ($id!='') {
            $postval=$this->request->getPost();
            $conditions = "smid = :smid:";
            $parameters = array("smid" => $id);
            $smupdet = MasterSocialMedia::find(array($conditions,"bind" => $parameters));
            $this->view->setVar("smupdet", $smupdet);
         //exit();
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $update= new MasterSocialMedia();
            $update->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Social Media Successfully Update</div>");
            return $response->redirect("socialmedia");
        }
     } 
}
?>