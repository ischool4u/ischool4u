<?php
class ArticleController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    /**
     * [indexAction description]
     * @return [type] [description]
     */
    function indexAction()
    {
          error_reporting(0);
        if($this->request->isGet())
        {
            $getVal=$this->request->get();
            if($getVal['submit']){
                if($getVal['subject']==''){
                    unset($getVal['subject']);
                }else{
                    $where= ' subject="'.$getVal['subject'].'"';
                    $subsubdet = MasterSubSubject::find("subid = '".$getVal['subject']."' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if($getVal['subsubject']==''){
                    unset($getVal['subsubject']);
                }else{
                    $where .= ' AND subsubject ="'.$getVal['subsubject'].'"';
                    $mTopicdet = MasterTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if($getVal['topics']==''){
                    unset($getVal['topics']);
                }else{
                    $where .= ' AND topics="'.$getVal['topics'].'"';
                    $msTopicdet = MasterSubTopics::find("subid = '".$getVal['subject']."' AND ssubid='".$getVal['subsubject']."' AND tid='".$getVal['topics']."' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if($getVal['subtopics']==''){
                    unset($getVal['subtopics']);
                }else{
                    $where .= ' AND subtopics="'.$getVal['subtopics'].'"';
                };
               $arr=array($where);
            }else{
              $arr=array();  
            }
                $article = MasterArticle::find($arr);
                $this->view->setVar("article", $article);
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }
    function addarticleAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $article= new MasterArticle();
            $article->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Article Successfully Add</div>");
            return $response->redirect("article");
        }
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }
    function updatearticleAction($id=''){
         $response = new \Phalcon\Http\Response();
        if ($this->request->isPost())
        {
            $postval=$this->request->getPost();
            $article= new MasterArticle();
            $article->save($postval);
            $this->flashSession->success("<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Article Updated Successfully</div>");
            return $response->redirect("article");
        }
        $getarticle= MasterArticle::findFirst(array("id='".$id."'"));
        $this->view->setVar("article", $getarticle);
        
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $subsubdet = MasterSubSubject::find("subid='$getarticle->subject' AND status=1");
        $this->view->setVar("subsubdet", $subsubdet);
        $mTopicdet = MasterTopics::find("subid='$getarticle->subject' AND ssubid='$getarticle->subsubject' AND status=1");
        $this->view->setVar("mTopicdet", $mTopicdet);
        $msTopicdet = MasterSubTopics::find("subid='$getarticle->subject' AND ssubid='$getarticle->subsubject' AND tid='$getarticle->topics' AND status=1");
        $this->view->setVar("msTopicdet", $msTopicdet);
    }
     function updarstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterArticle SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterArticle SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Article Status Updated Successfully</div>");
             return $response->redirect("article");
        }
    }
    function tipsAction()
    {
        $subjects=$this->dispatcher->getParam("pagename");
        $subsubjects=$this->dispatcher->getParam("sub");
        $topics=$this->dispatcher->getParam("topic");
        $subtopics=$this->dispatcher->getParam("subtopic");
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if(isset($subsubjects) && isset($subjects)){
            $user = $this->session->get("user");
            $course = MasterCourse::findFirst(array("id='".$user['course']."'"));
            $this->view->setVar("course", $course);
            $subject = MasterSubject::findFirst(array("slug='".$subjects."'"));
            $subsubject = MasterSubSubject::findFirst(array("slug='".$subsubjects."'"));
            $cat = '"'.$subject->id.'","'.$subsubject->ssid.'"';
            $topic = MasterTopics::findFirst(array("slug='".$topics."'"));
            $subtopic = MasterSubTopics::findFirst("slug='".$subtopics."'");
            $tipstype = MasterConceptType::find(array("status=1"));
            $tips = MasterConcept::find(array("subject='".$subject->id."' AND subsubject='".$subsubject->ssid."' AND topics='".$topic->tid."' AND subtopics='".$subtopic->stid."' AND status='1'"));
            $this->view->setVar("tipstype", $tipstype);
            $this->view->setVar("tips", $tips);
            $this->view->setVar("subject", $subject);
            $this->view->setVar("subsubject", $subsubject);
            $this->view->setVar("topics", $topic);
        }
    }
    function deletearticleAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterArticle WHERE id = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Article Deleted Successfully</div>");
            return $response->redirect("article");
        }
    }

    function articlesAction($slug)
    {
        // echo $slug;
        $article = MasterArticle::findFirst("slug='".$slug."'");
        $this->view->setVar("article", $article);
    }
}