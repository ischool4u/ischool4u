<?php
class AdminusersController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize(); 
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    public function indexAction(){
        //$this->view->setvar("magdate",'TRUE'); 
        
    	$user = MasterAdminUsers::find(array("order" => "first_name"));
        $this->view->setVar("user", $user);
    }
    public function addAction(){
    	if ($this->request->isPost()) {
            
            $postval=$this->request->getPost();
            $conditions = "mail_id = :mail_id:";
            $parameters = array("mail_id" => $this->request->getPost("mail_id"));
            $user = MasterAdminUsers::find(array($conditions,"bind" => $parameters));
            //echo"<pre>";print_r($user);exit();
            if (count($user) != 0) {
                $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("mail_id")."has been already exist you are not allow to use this Email ID</div>");
                $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
            }
            else
            {
                $passsword=$this->request->getPost("pass");
                
                $totarr=array_merge($postval,array('pass'=>base64_encode($passsword),'created'=>date('y-m-d h:m:s')));
                if ($this->request->hasFiles() == true) {
                    foreach ($this->request->getUploadedFiles() as $file) {
                        $path = rand(1, 1000) . $file->getName();
                        $newpath = 'author/' . $path;
                        $file->moveTo($newpath);
                        $totarr['profile_image']=$path;
                    }
                }
                $AdminUsers = new MasterAdminUsers();
                $AdminUsers->save($totarr);
               $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Registered Successfully</div>");
                //$this->dispatcher->forward(array("action" => "index"));
                $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
            }
        }
        $role = MasterRole::find();
        $this->view->setVar("roles", $role);
    }

    public function UpdateAction($id=''){
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $postval['modfied'] = date('y-m-d h:m:s');
            $postval['pass'] = base64_encode($postval['pass']);
            if ($this->request->hasFiles() == true) {
                    foreach ($this->request->getUploadedFiles() as $file) {
                        $path = rand(1, 1000) . $file->getName();
                        $newpath = 'author/' . $path;
                        $file->moveTo($newpath);
                        $postval['profile_image']=$path;
                    }
                }
            $AdminUsers = new MasterAdminUsers();
            $AdminUsers->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Admin User Updated Successfully</div>");
            //$this->dispatcher->forward(array("action" => "index"));
            return $response->redirect("adminusers");
        }
        $user = MasterAdminUsers::findFirst(array("aid='".$id."' "));
        $this->view->setVar("adminuser", $user);
        $role = MasterRole::find("");
        $this->view->setVar("roles", $role);
    }

    public function UpdstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $date = date('y-m-d h:m:s');
                $phql = "UPDATE MasterAdminUsers SET is_active = 0 , modfied='{$date}' where aid=".$id."";
            }
            else
            {
                $date = date('y-m-d h:m:s');
                $phql = "UPDATE MasterAdminUsers SET is_active = 1 , modfied='{$date}' where aid=".$id."";
            }
            
                    //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

             $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Admin Status Updated Successfully</div>");
             //return $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
             return $response->redirect("adminusers");
        }
    }

    public function deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterAdminUsers WHERE aid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Deleted Successfully</div>");
                return $response->redirect("adminusers");
        }
        
    }
}