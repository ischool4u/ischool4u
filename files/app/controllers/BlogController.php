<?php

class BlogController extends ControllerBase {

    public function initialize() {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    public function bloglistAction() {
        error_reporting(0);
        if ($this->request->isGet()) {
            $getVal = $this->request->get();
            if ($getVal['submit']) {
                if ($getVal['category']) {
                    $where = ' category="' . $getVal['category'] . '"';
                }
                if ($getVal['tag']) {
                    $where = " FIND_IN_SET('{$getVal['tag']}',tags)";
                }
                $arr = array($where);
            } else {
                $arr = array($where);
            }
            $arrr = array("order" => "id_blog DESC");
            $arr = array_merge($arr, $arrr);
            $blog = MasterBlog::find($arr);
            $this->view->setVar("blog", $blog);
        }
    }

    public function addblogAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $user_session = $this->session->get('admin');
            $postval = $this->request->getPost();
            $postval['tags'] = json_encode(explode(",", $postval['tags']));
            $postval['user_id'] = $user_session['id'];
            $postval['add_date'] = date("Y-m-d");
            $postval['totallike'] = 0;
            $article = new MasterBlog();
            $article->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Blog Successfully Add</div>");
            return $response->redirect("blog/bloglist/");
        }
    }

    public function updarstatusAction($u_status, $id) {
        $response = new \Phalcon\Http\Response();
        if ($id != "") {
            if ($u_status == 2) {
                $phql = "UPDATE MasterBlog SET status = 0 where id_blog=" . $id . "";
            } else {
                $phql = "UPDATE MasterBlog SET status = 1 where id_blog=" . $id . "";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Blog Status Updated Successfully</div>");
            return $response->redirect("blog/bloglist/");
        }
    }

    public function deleteblogAction($id='') {
        $response = new \Phalcon\Http\Response();
        if ($id != "") {
            $phql = "DELETE FROM MasterBlog WHERE id_blog = '" . $id . "'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Blog Deleted Successfully</div>");
            return $response->redirect("blog/bloglist/");
        }
    }

    public function updateblogAction($id='') {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $user_session = $this->session->get('admin');
            $postval = $this->request->getPost();
            $postval['tags'] = json_encode(explode(",", $postval['tags']));
            $upd = "UPDATE MasterBlog SET category='{$postval['category']}',tags='{$postval['tags']}',title='{$postval['title']}',subtitle='{$postval['subtitle']}',blog='{$postval['blog']}',slug='{$postval['slug']}',status='{$postval['status']}' WHERE id_blog='{$postval['id_blog']}'";
            $this->modelsManager->executeQuery($upd);
//            $article = new MasterBlog();
//            $article->save($postval);
            $this->flashSession->success("<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Blog Updated Successfully</div>");
            return $response->redirect("blog/bloglist/");
        }
        $blog = MasterBlog::findFirst(array("id_blog='" . $id . "'"));
        $this->view->setVar("blog", $blog);
    }

}

?>
