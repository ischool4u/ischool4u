<?php

class MasterSocials extends \Phalcon\Mvc\Model
{
    /**
     * @var integer
     */
    public $uid;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $mail_id;

    /**
     * @var string
     */
    public $is_active;

    public function initialize()
    {
        
    }
}